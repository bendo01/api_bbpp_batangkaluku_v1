<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAuthentificationAbilityRoleTable extends Migration
{
    /**
    * Run the migrations.
    *
    * @return void
    */
    public function up()
    {
        if (!Schema::connection('authentification')->hasTable('ability_role')) {
            Schema::connection('authentification')->create('ability_role', function (Blueprint $table) {
                $table->uuid('id')->primary();
                $table->uuid('ability_id');
                $table->uuid('role_id');
                $table->timestamps();
            });
            DB::statement('ALTER TABLE ONLY authentification.ability_role ALTER COLUMN id SET DEFAULT uuid_generate_v4();');
        }
    }

    /**
    * Reverse the migrations.
    *
    * @return void
    */
    public function down()
    {
        if (Schema::connection('authentification')->hasTable('ability_role')) {
            Schema::connection('authentification')->drop('ability_role');
        }
    }
}
