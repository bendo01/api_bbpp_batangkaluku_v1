<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAuthentificationSocialAccountsTable extends Migration
{
    /**
    * Run the migrations.
    *
    * @return void
    */
    public function up()
    {
        if (!Schema::connection('authentification')->hasTable('social_accounts')) {
            Schema::connection('authentification')->create('social_accounts', function (Blueprint $table) {
                $table->uuid('id')->primary();
                $table->uuid('user_id');
                $table->string('provider');
                $table->string('provider_user_id');
                $table->timestamps();
                $table->softDeletes();
            });
            DB::statement('ALTER TABLE ONLY authentification.social_accounts ALTER COLUMN id SET DEFAULT uuid_generate_v4();');
        }
    }

    /**
    * Reverse the migrations.
    *
    * @return void
    */
    public function down()
    {
        if (Schema::connection('authentification')->hasTable('social_accounts')) {
            Schema::connection('authentification')->drop('social_accounts');
        }
    }
}
