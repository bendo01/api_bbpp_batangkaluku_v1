<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePersonReferenceCareerIncomesTable extends Migration
{
    /**
    * Run the migrations.
    *
    * @return void
    */
    public function up()
    {
        if (!Schema::connection('person_reference_career')->hasTable('incomes')) {
            Schema::connection('person_reference_career')->create('incomes', function (Blueprint $table) {
                $table->uuid('id')->primary();
                $table->string('code');
                $table->string('name');
                $table->string('feeder_code');
                $table->double('minimum');
                $table->double('maximum')->nullable();
                $table->uuid('created_by')->nullable();
                $table->uuid('modified_by')->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
            DB::statement('ALTER TABLE ONLY person_reference_career.incomes ALTER COLUMN id SET DEFAULT uuid_generate_v4();');
        }
    }

    /**
    * Reverse the migrations.
    *
    * @return void
    */
    public function down()
    {
        if (Schema::connection('person_reference_career')->hasTable('incomes')) {
            Schema::connection('person_reference_career')->drop('incomes');
        }
    }
}
