<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePersonReferenceCareerCivilServantBracketsTable extends Migration
{
    /**
    * Run the migrations.
    *
    * @return void
    */
    public function up()
    {
        if (!Schema::connection('person_reference_career')->hasTable('civil_servant_brackets')) {
            Schema::connection('person_reference_career')->create('civil_servant_brackets', function (Blueprint $table) {
                $table->uuid('id')->primary();
                $table->string('code');
                $table->string('name');
                $table->string('group')->nullable();
                $table->string('group_name')->nullable();
                $table->integer('rank')->nullable();
                $table->uuid('created_by')->nullable();
                $table->uuid('modified_by')->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
            DB::statement('ALTER TABLE ONLY person_reference_career.civil_servant_brackets ALTER COLUMN id SET DEFAULT uuid_generate_v4();');
        }
    }

    /**
    * Reverse the migrations.
    *
    * @return void
    */
    public function down()
    {
        if (Schema::connection('person_reference_career')->hasTable('civil_servant_brackets')) {
            Schema::connection('person_reference_career')->drop('civil_servant_brackets');
        }
    }
}
