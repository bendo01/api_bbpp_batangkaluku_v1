<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePersonMasterIndividualsTable extends Migration
{
    /**
    * Run the migrations.
    *
    * @return void
    */
    public function up()
    {
        if (!Schema::connection('person_master')->hasTable('person_master.individuals')) {
            Schema::connection('person_master')->create('person_master.individuals', function (Blueprint $table) {
                $table->uuid('id')->primary();
                $table->string('code');
                $table->string('name');
                $table->date('birth_date');
                $table->string('birth_place');
                $table->uuid('birth_place_id')->nullable();
                $table->uuid('gender_id');
                $table->uuid('religion_id');
                $table->uuid('occupation_id');
                $table->uuid('education_id');
                $table->uuid('identification_type_id');
                $table->uuid('marital_status_id');
                $table->uuid('profession_id');
                $table->uuid('income_id')->nullable();
                $table->uuid('salary_source_id')->nullable();
                $table->uuid('country_id')->nullable();
                $table->boolean('deceased')->default(false);
                // $table->uuid('id_lecturer')->nullable();
                $table->uuid('created_by')->nullable();
                $table->uuid('modified_by')->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
            DB::statement('ALTER TABLE ONLY person_master.individuals ALTER COLUMN id SET DEFAULT uuid_generate_v4();');
        }
    }

    /**
    * Reverse the migrations.
    *
    * @return void
    */
    public function down()
    {
        if (Schema::connection('person_master')->hasTable('individuals')) {
            Schema::connection('person_master')->drop('individuals');
        }
    }
}
