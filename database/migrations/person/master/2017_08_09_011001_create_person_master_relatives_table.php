<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePersonMasterRelativesTable extends Migration
{
    /**
    * Run the migrations.
    *
    * @return void
    */
    public function up()
    {
        if (!Schema::connection('person_master')->hasTable('relatives')) {
            Schema::connection('person_master')->create('relatives', function (Blueprint $table) {
                $table->uuid('id')->primary();
                $table->uuid('individual_id');
                $table->uuid('relative_id');
                $table->uuid('relative_type_id');
                $table->uuid('created_by')->nullable();
                $table->uuid('modified_by')->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
            DB::statement('ALTER TABLE ONLY person_master.relatives ALTER COLUMN id SET DEFAULT uuid_generate_v4();');
        }
    }

    /**
    * Reverse the migrations.
    *
    * @return void
    */
    public function down()
    {
        if (Schema::connection('person_master')->hasTable('relatives')) {
            Schema::connection('person_master')->drop('relatives');
        }
    }
}
