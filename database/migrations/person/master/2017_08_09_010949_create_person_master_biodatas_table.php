<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePersonMasterBiodatasTable extends Migration
{
    /**
    * Run the migrations.
    *
    * @return void
    */
    public function up()
    {
        if (!Schema::connection('person_master')->hasTable('biodatas')) {
            Schema::connection('person_master')->create('biodatas', function (Blueprint $table) {
                $table->uuid('id')->primary();
                $table->double('height')->default(0);
                $table->double('weight')->default(0);
                $table->boolean('positive_blood_rhesus')->default(false);
                $table->uuid('blood_type_id');
                $table->uuid('hair_type_id');
                $table->uuid('hair_color_id');
                $table->uuid('eye_color_type_id');
                $table->uuid('individual_id');
                $table->uuid('created_by')->nullable();
                $table->uuid('modified_by')->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
            DB::statement('ALTER TABLE ONLY person_master.biodatas ALTER COLUMN id SET DEFAULT uuid_generate_v4();');
        }
    }

    /**
    * Reverse the migrations.
    *
    * @return void
    */
    public function down()
    {
        if (Schema::connection('person_master')->hasTable('biodatas')) {
            Schema::connection('person_master')->drop('biodatas');
        }
    }
}
