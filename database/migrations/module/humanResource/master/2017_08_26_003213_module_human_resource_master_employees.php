<?php
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModuleHumanResourceMasterEmployees extends Migration
{
    /**
    * Run the migrations.
    *
    * @return void
    */
    public function up()
    {
        if (!Schema::connection('module_human_resource_master')->hasTable('employees')) {
            Schema::connection('module_human_resource_master')->create('employees', function (Blueprint $table) {
                $table->uuid('id')->primary();
                $table->string('code');
                $table->uuid('individual_id');
                $table->uuid('status_id')->nullable();
                $table->uuid('contract_type_id')->nullable();
                $table->uuid('unit_id')->nullable();
                $table->uuid('institution_id')->nullable();
                $table->uuid('resign_type_id')->nullable();
                $table->integer('visa')->default(0);
                $table->uuid('created_by')->nullable();
                $table->uuid('modified_by')->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
            DB::statement('ALTER TABLE ONLY module_human_resource_master.employees ALTER COLUMN id SET DEFAULT uuid_generate_v4();');
        }
    }

    /**
    * Reverse the migrations.
    *
    * @return void
    */
    public function down()
    {
        if (Schema::connection('module_human_resource_master')->hasTable('employees')) {
            Schema::connection('module_human_resource_master')->drop('employees');
        }
    }
}
