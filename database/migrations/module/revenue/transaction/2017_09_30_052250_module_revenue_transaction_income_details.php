<?php
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModuleRevenueTransactionIncomeDetails extends Migration
{
    /**
    * Run the migrations.
    *
    * @return void
    */
    public function up()
    {
        if (!Schema::connection('module_revenue_transaction')->hasTable('income_details')) {
            Schema::connection('module_revenue_transaction')->create('income_details', function (Blueprint $table) {
                $table->uuid('id')->primary();
                $table->string('title');
                $table->date('publish_date');
                $table->decimal('amount', 5, 2)->default(0);
                $table->uuid('income_id');
                $table->uuid('created_by')->nullable();
                $table->uuid('modified_by')->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
            DB::statement('ALTER TABLE ONLY module_revenue_transaction.income_details ALTER COLUMN id SET DEFAULT uuid_generate_v4();');
        }
    }

    /**
    * Reverse the migrations.
    *
    * @return void
    */
    public function down()
    {
        if (Schema::connection('module_revenue_transaction')->hasTable('income_details')) {
            Schema::connection('module_revenue_transaction')->drop('income_details');
        }
    }
}
