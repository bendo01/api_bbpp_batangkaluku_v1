<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModuleTrainingTransactionTrainingClassFacilitator extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
         if (!Schema::connection('module_training_transaction')->hasTable('training_class_facilitator')) {
             Schema::connection('module_training_transaction')->create('training_class_facilitator', function (Blueprint $table) {
                 $table->uuid('id')->primary();
                 $table->uuid('training_class_id');
                 $table->uuid('individual_id');
                 $table->uuid('created_by')->nullable();
                 $table->uuid('modified_by')->nullable();
                 $table->timestamps();
                 $table->softDeletes();
             });
             DB::statement('ALTER TABLE ONLY module_training_transaction.training_class_facilitator ALTER COLUMN id SET DEFAULT uuid_generate_v4();');
         }
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
         if (Schema::connection('module_training_transaction')->hasTable('training_class_facilitator')) {
             Schema::connection('module_training_transaction')->drop('training_class_facilitator');
         }
     }
}
