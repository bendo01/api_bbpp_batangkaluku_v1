<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModuleTrainingTransactionTrainingClasses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
         if (!Schema::connection('module_training_transaction')->hasTable('training_classes')) {
             Schema::connection('module_training_transaction')->create('training_classes', function (Blueprint $table) {
                 $table->uuid('id')->primary();
                 $table->string('code');
                 $table->string('name');
                 $table->integer('duration_hour')->nullable();
                 $table->integer('duration_day')->nullable();
                 $table->date('start_date')->nullable();
                 $table->date('end_date')->nullable();
                 $table->decimal('expanse', 17, 2)->nullable();
                 $table->text('preparation')->nullable();
                 $table->text('implementation')->nullable();
                 $table->text('evaluation')->nullable();
                 $table->uuid('institution_id')->nullable();
                 $table->uuid('category_id');
                 $table->uuid('created_by')->nullable();
                 $table->uuid('modified_by')->nullable();
                 $table->timestamps();
                 $table->softDeletes();
             });
             DB::statement('ALTER TABLE ONLY module_training_transaction.training_classes ALTER COLUMN id SET DEFAULT uuid_generate_v4();');
         }
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
         if (Schema::connection('module_training_transaction')->hasTable('training_classes')) {
             Schema::connection('module_training_transaction')->drop('training_classes');
         }
     }
}
