<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModuleTrainingReferenceLearningMaterialCategories extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::connection('module_training_reference_learning_material')->hasTable('categories')) {
            Schema::connection('module_training_reference_learning_material')->create('categories', function (Blueprint $table) {
                $table->uuid('id')->primary();
                $table->string('code');
                $table->string('name');
                $table->text('description')->nullable();
                $table->uuid('created_by')->nullable();
                $table->uuid('modified_by')->nullable();
                $table->uuid('parent_id')->nullable();
                $table->uuid('category_id');
                $table->integer('_lft')->default(0);
                $table->integer('_rgt')->default(0);
                $table->timestamps();
                $table->softDeletes();
            });
            DB::statement('ALTER TABLE ONLY module_training_reference_learning_material.categories ALTER COLUMN id SET DEFAULT uuid_generate_v4();');
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::connection('module_training_reference_learning_material')->hasTable('categories')) {
            Schema::connection('module_training_reference_learning_material')->drop('categories');
        }
    }
}
