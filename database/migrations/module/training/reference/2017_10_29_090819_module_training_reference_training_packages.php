<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModuleTrainingReferenceTrainingPackages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
         if (!Schema::connection('module_training_reference')->hasTable('training_packages')) {
             Schema::connection('module_training_reference')->create('training_packages', function (Blueprint $table) {
                 $table->uuid('id')->primary();
                 $table->string('code');
                 $table->string('name');
                 $table->text('description')->nullable();
                 $table->uuid('created_by')->nullable();
                 $table->uuid('modified_by')->nullable();
                 $table->timestamps();
                 $table->softDeletes();
             });
             DB::statement('ALTER TABLE ONLY module_training_reference.training_packages ALTER COLUMN id SET DEFAULT uuid_generate_v4();');
         }
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
         if (Schema::connection('module_training_reference')->hasTable('training_packages')) {
             Schema::connection('module_training_reference')->drop('training_packages');
         }
     }
}
