<?php
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModuleInventoryTransactionPlacements extends Migration
{
    /**
    * Run the migrations.
    *
    * @return void
    */
    public function up()
    {
        if (!Schema::connection('module_inventory_transaction')->hasTable('placements')) {
            Schema::connection('module_inventory_transaction')->create('placements', function (Blueprint $table) {
                $table->uuid('id')->primary();
                $table->string('code');
                $table->string('name');
                $table->uuid('unit_id');
                $table->uuid('recident_room_id');
                $table->uuid('catalog_id');
                $table->uuid('condition_id');
                $table->uuid('created_by')->nullable();
                $table->uuid('modified_by')->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
            DB::statement('ALTER TABLE ONLY module_inventory_transaction.placements ALTER COLUMN id SET DEFAULT uuid_generate_v4();');
        }
    }

    /**
    * Reverse the migrations.
    *
    * @return void
    */
    public function down()
    {
        if (Schema::connection('module_inventory_transaction')->hasTable('placements')) {
            Schema::connection('module_inventory_transaction')->drop('placements');
        }
    }
}
