<?php
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommonReferenceEducationEducations extends Migration
{
    /**
    * Run the migrations.
    *
    * @return void
    */
    public function up()
    {
        if (!Schema::connection('common_reference_education')->hasTable('educations')) {
            Schema::connection('common_reference_education')->create('educations', function (Blueprint $table) {
                $table->uuid('id')->primary();
                $table->string('code');
                $table->string('name');
                $table->string('alphabetic')->nullable();
                $table->string('abbreviation')->nullable();
                $table->uuid('stage_id');
                $table->uuid('group_id');
                $table->uuid('category_id');
                $table->uuid('type_id');
                $table->boolean('institution')->default(false);
                $table->boolean('individual')->default(false);
                $table->uuid('created_by')->nullable();
                $table->uuid('modified_by')->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
            DB::statement('ALTER TABLE ONLY common_reference_education.educations ALTER COLUMN id SET DEFAULT uuid_generate_v4();');
        }
    }

    /**
    * Reverse the migrations.
    *
    * @return void
    */
    public function down()
    {
        if (Schema::connection('common_reference_education')->hasTable('educations')) {
            Schema::connection('common_reference_education')->drop('educations');
        }
    }
}
