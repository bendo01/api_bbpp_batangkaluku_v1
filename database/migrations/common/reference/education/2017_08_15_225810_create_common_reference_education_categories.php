<?php
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommonReferenceEducationCategories extends Migration
{
    /**
    * Run the migrations.
    *
    * @return void
    */
    public function up()
    {
        if (!Schema::connection('common_reference_education')->hasTable('categories')) {
            Schema::connection('common_reference_education')->create('categories', function (Blueprint $table) {
                $table->uuid('id')->primary();
                $table->string('code');
                $table->string('name');
                $table->boolean('institution')->default(false);
                $table->boolean('individual')->default(false);
                $table->uuid('created_by')->nullable();
                $table->uuid('modified_by')->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
            DB::statement('ALTER TABLE ONLY common_reference_education.categories ALTER COLUMN id SET DEFAULT uuid_generate_v4();');
        }
    }

    /**
    * Reverse the migrations.
    *
    * @return void
    */
    public function down()
    {
        if (Schema::connection('common_reference_education')->hasTable('categories')) {
            Schema::connection('common_reference_education')->drop('categories');
        }
    }
}
