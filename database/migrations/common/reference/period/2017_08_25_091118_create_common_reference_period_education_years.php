<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommonReferencePeriodEducationYears extends Migration
{
    /**
    * Run the migrations.
    *
    * @return void
    */
    public function up()
    {
        if (!Schema::connection('common_reference_period')->hasTable('education_years')) {
            Schema::connection('common_reference_period')->create('education_years', function (Blueprint $table) {
                $table->uuid('id')->primary();
                $table->string('name');
                $table->date('start_date')->nullable();
                $table->date('end_date')->nullable();
                $table->date('expired_date')->nullable();
                $table->integer('year');
                $table->string('feeder_name');
                $table->integer('thread')->default(0);
                $table->uuid('education_year_category_id')->nullable();
                $table->uuid('created_by')->nullable();
                $table->uuid('modified_by')->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
            DB::statement('ALTER TABLE ONLY common_reference_period.education_years ALTER COLUMN id SET DEFAULT uuid_generate_v4();');
        }
    }

    /**
    * Reverse the migrations.
    *
    * @return void
    */
    public function down()
    {
        if (Schema::connection('common_reference_period')->hasTable('education_years')) {
            Schema::connection('common_reference_period')->drop('education_years');
        }
    }
}
