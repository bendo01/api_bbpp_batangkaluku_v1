<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommonReferencePeriodWeekdays extends Migration
{
    /**
    * Run the migrations.
    *
    * @return void
    */
    /**
    * Run the migrations.
    *
    * @return void
    */
    public function up()
    {
        if (!Schema::connection('common_reference_period')->hasTable('weekdays')) {
            Schema::connection('common_reference_period')->create('weekdays', function (Blueprint $table) {
                $table->uuid('id')->primary();
                $table->integer('code');
                $table->string('name');
                $table->uuid('created_by')->nullable();
                $table->uuid('modified_by')->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
            DB::statement('ALTER TABLE ONLY common_reference_period.weekdays ALTER COLUMN id SET DEFAULT uuid_generate_v4();');
        }
    }

    /**
    * Reverse the migrations.
    *
    * @return void
    */
    public function down()
    {
        if (Schema::connection('common_reference_period')->hasTable('weekdays')) {
            Schema::connection('common_reference_period')->drop('weekdays');
        }
    }
}
