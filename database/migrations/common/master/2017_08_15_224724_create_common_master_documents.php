<?php
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommonMasterDocuments extends Migration
{
    /**
    * Run the migrations.
    *
    * @return void
    */
    public function up()
    {
        if (!Schema::connection('common_master')->hasTable('documents')) {
            Schema::connection('common_master')->create('documents', function (Blueprint $table) {
                $table->uuid('id')->primary();
                $table->string('title')->nullable();
                $table->string('filename')->nullable();
                $table->string('dir')->nullable();
                $table->string('type')->nullable();
                $table->bigInteger('size')->nullable();
                $table->string('url')->nullable();
                $table->uuid('documentable_id');
                $table->string('documentable_type');
                $table->string('type_id');
                $table->uuid('created_by')->nullable();
                $table->uuid('modified_by')->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
            DB::statement('ALTER TABLE ONLY common_master.documents ALTER COLUMN id SET DEFAULT uuid_generate_v4();');
        }
    }

    /**
    * Reverse the migrations.
    *
    * @return void
    */
    public function down()
    {
        if (Schema::connection('common_master')->hasTable('documents')) {
            Schema::connection('common_master')->drop('documents');
        }
    }
}
