<?php
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommonMasterResidencies extends Migration
{
    /**
    * Run the migrations.
    *
    * @return void
    */
    public function up()
    {
        if (!Schema::connection('common_master')->hasTable('residencies')) {
            Schema::connection('common_master')->create('residencies', function (Blueprint $table) {
                $table->uuid('id')->primary();
                $table->uuid('residenceable_id');
                $table->string('residenceable_type');
                $table->string('address');
                $table->string('code')->nullable();
                $table->string('name')->nullable();
                $table->string('citizens_association')->nullable();
                $table->string('neighborhood_association')->nullable();
                $table->uuid('province_id')->nullable();
                $table->uuid('regency_id')->nullable();
                $table->uuid('sub_district_id')->nullable();
                $table->uuid('village_id')->nullable();
                $table->string('regency_name')->nullable();
                $table->string('sub_district_name')->nullable();
                $table->string('village_name')->nullable();
                $table->uuid('resident_type_id')->nullable();
                $table->uuid('category_id')->nullable();
                $table->uuid('type_id')->nullable();
                $table->double('latitude')->default(0);
                $table->double('longitude')->default(0);
                $table->integer('zoom')->default(0);
                $table->uuid('created_by')->nullable();
                $table->uuid('modified_by')->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
            DB::statement('ALTER TABLE ONLY common_master.residencies ALTER COLUMN id SET DEFAULT uuid_generate_v4();');
        }
    }
    /**
    * Reverse the migrations.
    *
    * @return void
    */
    public function down()
    {
        if (Schema::connection('common_master')->hasTable('residencies')) {
            Schema::connection('common_master')->drop('residencies');
        }
    }
}
