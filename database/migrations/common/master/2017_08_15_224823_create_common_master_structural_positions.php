<?php
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommonMasterStructuralPositions extends Migration
{
    /**
    * Run the migrations.
    *
    * @return void
    */
    public function up()
    {
        if (!Schema::connection('common_master')->hasTable('structural_positions')) {
            Schema::connection('common_master')->create('structural_positions', function (Blueprint $table) {
                $table->uuid('id')->primary();
                $table->uuid('individual_id');
                $table->uuid('role_id');
                $table->date('start_date')->nullable();
                $table->date('end_date')->nullable();
                $table->string('decree_number')->nullable();
                $table->date('decree_date')->nullable();
                $table->uuid('structural_positionable_id');
                $table->string('structural_positionable_type');
                $table->uuid('created_by')->nullable();
                $table->uuid('modified_by')->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
            DB::statement('ALTER TABLE ONLY common_master.structural_positions ALTER COLUMN id SET DEFAULT uuid_generate_v4();');
        }
    }

    /**
    * Reverse the migrations.
    *
    * @return void
    */
    public function down()
    {
        if (Schema::connection('common_master')->hasTable('structural_positions')) {
            Schema::connection('common_master')->drop('structural_positions');
        }
    }
}
