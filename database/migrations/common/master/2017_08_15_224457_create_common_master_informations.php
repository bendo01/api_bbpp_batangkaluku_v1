<?php
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommonMasterInformations extends Migration
{
    /**
    * Run the migrations.
    *
    * @return void
    */
    public function up()
    {
        if (!Schema::connection('common_master')->hasTable('informations')) {
            Schema::connection('common_master')->create('informations', function (Blueprint $table) {
                $table->uuid('id')->primary();
                $table->uuid('informationable_id');
                $table->string('informationable_type');
                $table->text('vision')->nullable();
                $table->text('mission')->nullable();
                $table->text('intention')->nullable();
                $table->text('objective')->nullable();
                $table->uuid('created_by')->nullable();
                $table->uuid('modified_by')->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
            DB::statement('ALTER TABLE ONLY common_master.informations ALTER COLUMN id SET DEFAULT uuid_generate_v4();');
        }
    }

    /**
    * Reverse the migrations.
    *
    * @return void
    */
    public function down()
    {
        if (Schema::connection('common_master')->hasTable('informations')) {
            Schema::connection('common_master')->drop('informations');
        }
    }
}
