<?php
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommonMasterPhones extends Migration
{
    /**
    * Run the migrations.
    *
    * @return void
    */
    public function up()
    {
        if (!Schema::connection('common_master')->hasTable('phones')) {
            Schema::connection('common_master')->create('phones', function (Blueprint $table) {
                $table->uuid('id')->primary();
                $table->string('dial_number');
                $table->boolean('is_primary')->default(FALSE);
                $table->uuid('phoneable_id');
                $table->string('phoneable_type');
                $table->uuid('created_by')->nullable();
                $table->uuid('modified_by')->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
            DB::statement('ALTER TABLE ONLY common_master.phones ALTER COLUMN id SET DEFAULT uuid_generate_v4();');
        }
    }

    /**
    * Reverse the migrations.
    *
    * @return void
    */
    public function down()
    {
        if (Schema::connection('common_master')->hasTable('phones')) {
            Schema::connection('common_master')->drop('phones');
        }
    }
}
