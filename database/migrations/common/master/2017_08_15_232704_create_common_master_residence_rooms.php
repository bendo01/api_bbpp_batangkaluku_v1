<?php
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommonMasterResidenceRooms extends Migration
{
    /**
    * Run the migrations.
    *
    * @return void
    */
    public function up()
    {
        if (!Schema::connection('common_master')->hasTable('residence_rooms')) {
            Schema::connection('common_master')->create('residence_rooms', function (Blueprint $table) {
                $table->uuid('id')->primary();
                $table->uuid('residence_id');
                $table->string('code')->nullable();
                $table->string('name')->nullable();
                $table->string('floor')->nullable();
                $table->decimal('long', 5, 2)->default(0);
                $table->decimal('wide', 5, 2)->default(0);
                $table->decimal('high', 5, 2)->default(0);
                $table->double('latitude')->default(0);
                $table->double('longitude')->default(0);
                $table->integer('zoom')->default(0);
                $table->uuid('room_type_id');
                $table->uuid('created_by')->nullable();
                $table->uuid('modified_by')->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
            DB::statement('ALTER TABLE ONLY common_master.residence_rooms ALTER COLUMN id SET DEFAULT uuid_generate_v4();');
        }
    }
    /**
    * Reverse the migrations.
    *
    * @return void
    */
    public function down()
    {
        if (Schema::connection('common_master')->hasTable('residence_rooms')) {
            Schema::connection('common_master')->drop('residence_rooms');
        }
    }
}
