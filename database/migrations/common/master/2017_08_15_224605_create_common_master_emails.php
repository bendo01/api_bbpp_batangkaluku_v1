<?php
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommonMasterEmails extends Migration
{
    /**
    * Run the migrations.
    *
    * @return void
    */
    public function up()
    {
        if (!Schema::connection('common_master')->hasTable('emails')) {
            Schema::connection('common_master')->create('emails', function (Blueprint $table) {
                $table->uuid('id')->primary();
                $table->string('address');
                $table->boolean('is_primary')->default(FALSE);
                $table->uuid('emailable_id');
                $table->string('emailable_type');
                $table->uuid('created_by')->nullable();
                $table->uuid('modified_by')->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
            DB::statement('ALTER TABLE ONLY common_master.emails ALTER COLUMN id SET DEFAULT uuid_generate_v4();');
        }
    }

    /**
    * Reverse the migrations.
    *
    * @return void
    */
    public function down()
    {
        if (Schema::connection('common_master')->hasTable('emails')) {
            Schema::connection('common_master')->drop('emails');
        }
    }
}
