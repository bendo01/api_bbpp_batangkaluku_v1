<?php
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommonMasterDecreesTable extends Migration
{
    /**
    * Run the migrations.
    *
    * @return void
    */
    public function up()
    {
        if (!Schema::connection('common_master')->hasTable('decrees')) {
            Schema::connection('common_master')->create('decrees', function (Blueprint $table) {
                $table->uuid('id')->primary();
                $table->string('title');
                $table->string('code')->nullable();
                $table->text('description')->nullable();
                $table->date('start_date')->nullable();
                $table->date('end_date')->nullable();
                $table->string('type_id');
                $table->uuid('decreeable_id');
                $table->string('decreeable_type');
                $table->uuid('created_by')->nullable();
                $table->uuid('modified_by')->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
            DB::statement('ALTER TABLE ONLY common_master.decrees ALTER COLUMN id SET DEFAULT uuid_generate_v4();');
        }
    }
    /**
    * Reverse the migrations.
    *
    * @return void
    */
    public function down()
    {
        if (Schema::connection('common_master')->hasTable('decrees')) {
            Schema::connection('common_master')->drop('decrees');
        }
    }
}
