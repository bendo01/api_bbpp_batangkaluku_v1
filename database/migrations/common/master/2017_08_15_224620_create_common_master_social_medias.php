<?php
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommonMasterSocialMedias extends Migration
{
    /**
    * Run the migrations.
    *
    * @return void
    */
    public function up()
    {
        if (!Schema::connection('common_master')->hasTable('social_medias')) {
            Schema::connection('common_master')->create('social_medias', function (Blueprint $table) {
                $table->uuid('id')->primary();
                $table->string('username')->nullable();
                $table->string('url')->nullable();
                $table->uuid('social_mediaable_id');
                $table->string('social_mediaable_type');
                $table->uuid('created_by')->nullable();
                $table->uuid('modified_by')->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
            DB::statement('ALTER TABLE ONLY common_master.social_medias ALTER COLUMN id SET DEFAULT uuid_generate_v4();');
        }
    }

    /**
    * Reverse the migrations.
    *
    * @return void
    */
    public function down()
    {
        if (Schema::connection('common_master')->hasTable('social_medias')) {
            Schema::connection('common_master')->drop('social_medias');
        }
    }
}
