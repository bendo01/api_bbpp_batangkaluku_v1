<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InstitutionReferenceCategoriesTable extends Migration
{
    /**
    * Run the migrations.
    *
    * @return void
    */
    public function up()
    {
        if (!Schema::connection('institution_reference')->hasTable('categories')) {
            Schema::connection('institution_reference')->create('categories', function (Blueprint $table) {
                $table->uuid('id')->primary();
                $table->string('code');
                $table->string('name');
                $table->string('slug')->nullable();
                $table->uuid('created_by')->nullable();
                $table->uuid('modified_by')->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
            DB::statement('ALTER TABLE ONLY institution_reference.categories ALTER COLUMN id SET DEFAULT uuid_generate_v4();');
        }
    }

    /**
    * Reverse the migrations.
    *
    * @return void
    */
    public function down()
    {
        if (Schema::connection('institution_reference')->hasTable('categories')) {
            Schema::connection('institution_reference')->drop('categories');
        }
    }
}
