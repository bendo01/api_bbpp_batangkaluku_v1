<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InstitutionMasterInstitutionsTable extends Migration
{
    /**
    * Run the migrations.
    *
    * @return void
    */
    public function up()
    {
        if (!Schema::connection('institution_master')->hasTable('institutions')) {
            Schema::connection('institution_master')->create('institutions', function (Blueprint $table) {
                $table->uuid('id')->primary();
                $table->string('code');
                $table->string('name');
                $table->string('abbreviation')->nullable();
                $table->boolean('is_active')->default(false);
                $table->uuid('id_non_education')->nullable();
                $table->uuid('type_id')->nullable();
                $table->uuid('category_id')->nullable();
                $table->string('slug')->nullable();
                $table->uuid('country_id')->nullable();
                $table->uuid('created_by')->nullable();
                $table->uuid('modified_by')->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
            DB::statement('ALTER TABLE ONLY institution_master.institutions ALTER COLUMN id SET DEFAULT uuid_generate_v4();');
        }
    }

    /**
    * Reverse the migrations.
    *
    * @return void
    */
    public function down()
    {
        if (Schema::connection('institution_master')->hasTable('institutions')) {
            Schema::connection('institution_master')->drop('institutions');
        }
    }
}
