<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InstitutionMasterCoordinatorsTable extends Migration
{
    /**
    * Run the migrations.
    *
    * @return void
    */
    public function up()
    {
        if (!Schema::connection('institution_master')->hasTable('coordinators')) {
            Schema::connection('institution_master')->create('coordinators', function (Blueprint $table) {
                $table->uuid('id')->primary();
                $table->uuid('institution_id');
                $table->uuid('coordinator_type_id');
                $table->uuid('created_by')->nullable();
                $table->uuid('modified_by')->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
            DB::statement('ALTER TABLE ONLY institution_master.coordinators ALTER COLUMN id SET DEFAULT uuid_generate_v4();');
        }
    }

    /**
    * Reverse the migrations.
    *
    * @return void
    */
    public function down()
    {
        if (Schema::connection('institution_master')->hasTable('coordinators')) {
            Schema::connection('institution_master')->drop('coordinators');
        }
    }
}
