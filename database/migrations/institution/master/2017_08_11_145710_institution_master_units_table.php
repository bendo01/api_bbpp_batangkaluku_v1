<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InstitutionMasterUnitsTable extends Migration
{
    /**
    * Run the migrations.
    *
    * @return void
    */
    public function up()
    {
        if (!Schema::connection('institution_master')->hasTable('units')) {
            Schema::connection('institution_master')->create('units', function (Blueprint $table) {
                $table->uuid('id')->primary();
                $table->string('code');
                $table->string('name');
                $table->boolean('is_active')->default(false);
                $table->string('abbreviation')->nullable();
                $table->string('slug')->nullable();
                $table->uuid('unit_type_id');
                $table->uuid('institution_id')->nullable();
                $table->text('description')->nullable();
                $table->uuid('created_by')->nullable();
                $table->uuid('modified_by')->nullable();
                $table->timestamps();
                $table->softDeletes();
                $table->integer('_lft')->default(0);
                $table->integer('_rgt')->default(0);
                $table->uuid('parent_id')->nullable();
            });
            DB::statement('ALTER TABLE ONLY institution_master.units ALTER COLUMN id SET DEFAULT uuid_generate_v4();');
        }
    }

    /**
    * Reverse the migrations.
    *
    * @return void
    */
    public function down()
    {
        if (Schema::connection('institution_master')->hasTable('institutions')) {
            Schema::connection('institution_master')->drop('institutions');
        }
    }
}
