<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLocationProvincesTable extends Migration
{
    /**
    * Run the migrations.
    *
    * @return void
    */
    public function up()
    {
        if (!Schema::connection('location')->hasTable('provinces')) {
            Schema::connection('location')->create('provinces', function (Blueprint $table) {
                $table->uuid('id')->primary();
                $table->string('code')->nullable();
                $table->string('name');
                $table->string('dikti_code')->nullable();
                $table->string('dikti_name')->nullable();
                $table->uuid('country_id');
                $table->string('epsbed_code')->nullable();
                $table->text('description')->nullable();
                $table->string('state_ministry_code')->nullable();
                $table->string('state_ministry_full_code')->nullable();
                $table->string('state_ministry_name')->nullable();
                $table->string('state_post_department_code')->nullable();
                $table->string('postal_code_start_range')->nullable();
                $table->string('postal_code_end_range')->nullable();
                $table->string('validation_code')->nullable();
                $table->string('slug')->nullable();
                $table->string('alt_slug')->nullable();
                $table->uuid('created_by')->nullable();
                $table->uuid('modified_by')->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
            DB::statement('ALTER TABLE ONLY location.provinces ALTER COLUMN id SET DEFAULT uuid_generate_v4();');
        }
    }

    /**
    * Reverse the migrations.
    *
    * @return void
    */
    public function down()
    {
        if (Schema::connection('location')->hasTable('provinces')) {
            Schema::connection('location')->drop('provinces');
        }
    }
}
