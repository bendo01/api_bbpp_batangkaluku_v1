<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLocationRegenciesTable extends Migration
{
    /**
    * Run the migrations.
    *
    * @return void
    */
    public function up()
    {
        if (!Schema::connection('location')->hasTable('regencies')) {
            Schema::connection('location')->create('regencies', function (Blueprint $table) {
                $table->uuid('id')->primary();
                $table->string('code')->nullable();
                $table->string('name');
                $table->uuid('province_id');
                $table->uuid('regency_type_id');
                $table->string('dikti_code')->nullable();
                $table->string('dikti_name')->nullable();
                $table->string('epsbed_code')->nullable();
                $table->string('state_ministry_code')->nullable();
                $table->string('state_ministry_full_code')->nullable();
                $table->string('state_ministry_name')->nullable();
                $table->string('state_post_department_code')->nullable();
                $table->string('postal_code_start_range')->nullable();
                $table->string('postal_code_end_range')->nullable();
                $table->string('validation_code')->nullable();
                $table->string('agriculture_department_code')->nullable();
                $table->string('agriculture_department_name')->nullable();
                $table->boolean('is_capital')->default(false);
                $table->string('slug')->nullable();
                $table->string('alt_slug')->nullable();
                $table->text('description')->nullable();
                $table->uuid('created_by')->nullable();
                $table->uuid('modified_by')->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
            DB::statement('ALTER TABLE ONLY location.regencies ALTER COLUMN id SET DEFAULT uuid_generate_v4();');
        }
    }

    /**
    * Reverse the migrations.
    *
    * @return void
    */
    public function down()
    {
        if (Schema::connection('location')->hasTable('regencies')) {
            Schema::connection('location')->drop('regencies');
        }
    }
}
