<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLocationCountriesTable extends Migration
{
    /**
    * Run the migrations.
    *
    * @return void
    */
    public function up()
    {
        if (!Schema::connection('location')->hasTable('countries')) {
            Schema::connection('location')->create('countries', function (Blueprint $table) {
                $table->uuid('id')->primary();
                $table->string('code');
                $table->string('name');
                $table->string('alpha2_code');
                $table->string('alpha3_code');
                $table->string('iso3166_2_code');
                $table->string('dikti_code');
                $table->uuid('continent_id')->nullable();
                $table->uuid('region_id')->nullable();
                $table->string('slug')->nullable();
                $table->uuid('created_by')->nullable();
                $table->uuid('modified_by')->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
            DB::statement('ALTER TABLE ONLY location.countries ALTER COLUMN id SET DEFAULT uuid_generate_v4();');
        }
    }

    /**
    * Reverse the migrations.
    *
    * @return void
    */
    public function down()
    {
        if (Schema::connection('location')->hasTable('countries')) {
            Schema::connection('location')->drop('countries');
        }
    }
}
