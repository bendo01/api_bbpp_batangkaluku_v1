<?php
declare (strict_types = 1);
namespace Whiteboks\Http\Requests\Common\Reference\Education\Education;

use Illuminate\Foundation\Http\FormRequest;

class Store extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'code'         => 'required|string',
            'name'         => 'required|string',
            'alphabetic'   => 'nullable|string',
            'abbreviation' => 'nullable|string',
            'stage_id'     => 'required|string',
            'group_id'     => 'required|string',
            'category_id'  => 'required|string',
            'type_id'      => 'required|string',
            'institution'  => 'required|boolean',
            'individual'   => 'required|boolean',
        ];
    }
}
