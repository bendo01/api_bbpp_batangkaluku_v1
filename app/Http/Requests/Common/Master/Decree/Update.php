<?php
declare (strict_types = 1);
namespace Whiteboks\Http\Requests\Common\Master\Decree;

use Illuminate\Foundation\Http\FormRequest;

class Update extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'title'       => 'required|string',
            'code'        => 'nullable|string',
            'description' => 'nullable|string',
            'start_date'  => 'nullable|date_format:d-m-Y',
            'end_date'    => 'nullable|date_format:d-m-Y',
            'type_id'     => 'required|string',
        ];
    }
}
