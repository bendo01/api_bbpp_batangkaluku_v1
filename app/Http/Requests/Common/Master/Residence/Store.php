<?php
declare (strict_types = 1);
namespace Whiteboks\Http\Requests\Common\Master\Residence;

use Illuminate\Foundation\Http\FormRequest;

class Store extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'address'                  => 'required|string',
            'code'                     => 'nullable|string',
            'name'                     => 'nullable|string',
            'citizens_association'     => 'nullable|integer',
            'neighborhood_association' => 'nullable|integer',
            'regency_id'               => 'required|string',
            'sub_district_id'          => 'required|string',
            'village_id'               => 'required|string',
            'resident_type_id'         => 'required|string',
            'category_id'              => 'required|string',
            'type_id'                  => 'required|string',
            'latitude'                 => 'nullable|numeric',
            'longitude'                => 'nullable|numeric',
            'zoom'                     => 'nullable|integer',
        ];
    }
}
