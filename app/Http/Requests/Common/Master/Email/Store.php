<?php
declare (strict_types = 1);
namespace Whiteboks\Http\Requests\Common\Master\Email;

use Illuminate\Foundation\Http\FormRequest;

class Store extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'address'    => 'required|string',
            'is_primary' => 'required|boolean',
        ];
    }
}
