<?php
declare (strict_types = 1);
namespace Whiteboks\Http\Requests\Common\Master\ResidenceRoom;

use Illuminate\Foundation\Http\FormRequest;

class Update extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'code'         => 'nullable|string',
            'name'         => 'required|string',
            'floor'        => 'nullable|string',
            'long'         => 'nullable|numeric',
            'wide'         => 'nullable|numeric',
            'high'         => 'nullable|numeric',
            'latitude'     => 'nullable|numeric',
            'longitude'    => 'nullable|numeric',
            'zoom'         => 'nullable|integer',
            'room_type_id' => 'required|string',
        ];
    }
}
