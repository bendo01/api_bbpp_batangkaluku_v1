<?php
declare (strict_types = 1);
namespace Whiteboks\Http\Requests\Common\Master\StructuralPosition;

use Illuminate\Foundation\Http\FormRequest;

class Store extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'individual_id' => 'required|string',
            'role_id'       => 'required|string',
            'start_date'    => 'required|date_format:d-m-Y',
            'end_date'      => 'nullable|date_format:d-m-Y',
            'decree_number' => 'nullable|string',
            'decree_date'   => 'nullable|date_format:d-m-Y',
        ];
    }
}
