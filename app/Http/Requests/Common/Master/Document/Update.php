<?php
declare (strict_types = 1);
namespace Whiteboks\Http\Requests\Common\Master\Document;

use Illuminate\Foundation\Http\FormRequest;

class Update extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'title'   => 'nullable|string|required_without:url',
            'url'     => 'nullable|url|required_without:title',
            'type_id' => 'required|string',
        ];
    }
}
