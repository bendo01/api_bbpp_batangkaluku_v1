<?php
declare (strict_types = 1);
namespace Whiteboks\Http\Requests\Location\Country;

use Illuminate\Foundation\Http\FormRequest;

class Store extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'code'           => 'required|string',
            'name'           => 'required|string',
            'alpha2_code'    => 'required|string',
            'alpha3_code'    => 'required|string',
            'iso3166_2_code' => 'required|string',
            'dikti_code'     => 'required|string',
            'continent_id'   => 'nullable|string',
            'region_id'      => 'nullable|string',
        ];
    }
}
