<?php
declare (strict_types = 1);
namespace Whiteboks\Http\Requests\Location\SubDistrict;

use Illuminate\Foundation\Http\FormRequest;

class Store extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'code'                       => 'nullable|string',
            'name'                       => 'required|string',
            'regency_id'                 => 'required|string',
            'dikti_code'                 => 'nullable|string',
            'dikti_name'                 => 'nullable|string',
            'state_ministry_code'        => 'nullable|string',
            'state_ministry_full_code'   => 'nullable|string',
            'state_ministry_name'        => 'nullable|string',
            'state_post_department_code' => 'nullable|string',
            'postal_code_start_range'    => 'nullable|string',
            'postal_code_end_range'      => 'nullable|string',
            'validation_code'            => 'nullable|string',
            'description'                => 'nullable|string',
        ];
    }
}
