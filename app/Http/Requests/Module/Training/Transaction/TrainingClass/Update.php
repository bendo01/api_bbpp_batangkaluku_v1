<?php
declare (strict_types = 1);
namespace Whiteboks\Http\Requests\Module\Training\Transaction\TrainingClass;

use Illuminate\Foundation\Http\FormRequest;

class Update extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
    * Get the validation rules that apply to the request.
    *
    * @return array
    */
    public function rules(): array
    {
        return [
            'code'          => 'required',
            'name'          => 'required',
            'duration_hour' => 'required',
            'duration_day'  => 'required',
            'expanse'       => 'required',
        ];
    }
}
