<?php
declare (strict_types = 1);
namespace Whiteboks\Http\Requests\Module\Training\Transaction\TrainingClassCourse;

use Illuminate\Foundation\Http\FormRequest;

class Store extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
    * Get the validation rules that apply to the request.
    *
    * @return array
    */
    public function rules(): array
    {
        return [
            'training_class_id' => 'required',
            'course_id'         => 'required',
        ];
    }
}
