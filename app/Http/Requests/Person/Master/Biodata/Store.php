<?php
declare (strict_types = 1);
namespace Whiteboks\Http\Requests\Person\Master\Biodata;

use Illuminate\Foundation\Http\FormRequest;

class Store extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'height'                => 'required|integer',
            'weight'                => 'required|integer',
            'positive_blood_rhesus' => 'required|boolean',
            'blood_type_id'         => 'required|string',
            'hair_color_id'         => 'required|string',
            'hair_type_id'          => 'required|string',
            'eye_color_type_id'     => 'required|string',
            'individual_id'         => 'required|string',
        ];
    }
}
