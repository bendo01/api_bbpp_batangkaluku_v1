<?php
// declare(strict_types = 1);
namespace Whiteboks\Http\Requests\Person\Master\Individual;

use Whiteboks\Model\Location\SubDistrict;
use Illuminate\Foundation\Http\FormRequest;
use Whiteboks\Rules\Person\CitizenshipCode;
use Illuminate\Contracts\Validation\Validator;

class Register extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
             'code'                     => ['required', 'regex:/^[0-9]+$/', 'size:16', 'unique:person_master.individuals,code'],
             'name'                     => 'required|max:255',
             'birth_date'               => 'required|date_format:d-m-Y',
             'birth_place'              => 'required|max:255',
             'gender_id'                => 'required|uuid',
             'religion_id'              => 'required|uuid',
             'occupation_id'            => 'required|uuid',
             'profession_id'            => 'required|uuid',
             'education_id'             => 'required|uuid',
             'marital_status_id'        => 'required|uuid',
             'deceased'                 => 'required|boolean',
             'country_id'               => 'required|uuid',
             'address'                  => 'required',
             'phone_number'             => 'required|numeric',
             'citizens_association'     => 'required|integer|min:0',
             'neighborhood_association' => 'required|integer|min:0',
             'province_id'              => 'required|uuid',
             'regency_id'               => 'required|uuid',
             'sub_district_id'          => 'required|uuid',
             'village_id'               => 'required|uuid',
             'email'                    => 'required|email',
             'password'                 => 'required',
             'g_recaptcha_response'     => 'required|captcha'
         ];
    }

    public function messages(): array
    {
        return [
             'code.required'                     => 'Nomor Induk Kependudukan Wajib Diisi',
             'code.size'                         => 'Panjang Nomor Induk Kependudukan Tidak Valid',
             'code.regex'                        => 'Nomor Induk Kependudukan Dalam format Angka',
             'code.unique'                       => 'Nomor Induk Kependudukan Sudah Ada',
             'name.required'                     => 'Nama Wajib Diisi',
             'name.max'                          => 'Panjang Nama Maksimum 255 Karakter',
             'birth_date.required'               => 'Tanggal Lahir Wajib Diisi',
             'birth_date.date_format'            => 'Format Tanggal Lahir Tidak Sesuai',
             'birth_place.required'              => 'Tempat Lahir Wajib Diisi',
             'birth_place.max'                   => 'Panjang Tempat Lahir Maksimum 255 Karakter',
             'gender_id.required'                => 'Wajib Diisi',
             'gender_id.uuid'                    => 'Isian Tidak Sesuai',
             'religion_id.required'              => 'Wajib Diisi',
             'religion_id.uuid'                  => 'Isian Tidak Sesuai',
             'occupation_id.required'            => 'Wajib Diisi',
             'occupation_id.uuid'                => 'Isian Tidak Sesuai',
             'profession_id.required'            => 'Wajib Diisi',
             'profession_id.uuid'                => 'Isian Tidak Sesuai',
             'education_id.required'             => 'Wajib Diisi',
             'education_id.uuid'                 => 'Isian Tidak Sesuai',
             'marital_status_id.required'        => 'Wajib Diisi',
             'marital_status_id.uuid'            => 'Isian Tidak Sesuai',
             'deceased.required'                 => 'Wajib Diisi',
             'deceased.boolean'                  => 'Isian Tidak Sesuai',
             'country_id.required'               => 'Wajib Diisi',
             'country_id.uuid'                   => 'Isian Tidak Sesuai',
             'address.required'                  => 'Wajib Diisi',
             'phone_number.required'             => 'Wajib Diisi',
             'phone_number.numeric'              => 'Isian Dalam Tipe Angka',
             'citizens_association.required'     => 'Wajib Diisi',
             'citizens_association.integer'      => 'Isian Dalam Tipe Angka',
             'citizens_association.min'          => 'Angka Minimum adalah 0',
             'neighborhood_association.required' => 'Wajib Diisi',
             'neighborhood_association.integer'  => 'Isian Dalam Tipe Angka',
             'neighborhood_association.min'      => 'Angka Minimum adalah 0',
             'province_id.required'              => 'Wajib Diisi',
             'province_id.uuid'                  => 'Isian Tidak Sesuai',
             'regency_id.required'               => 'Wajib Diisi',
             'regency_id.uuid'                   => 'Isian Tidak Sesuai',
             'sub_district_id.required'          => 'Wajib Diisi',
             'sub_district_id.uuid'              => 'Isian Tidak Sesuai',
             'village_id.required'               => 'Wajib Diisi',
             'village_id.uuid'                   => 'Isian Tidak Sesuai',
             'email.required'                    => 'Wajib Diisi',
             'email.email'                       => 'required|email',
             'password.required'                 => 'Wajib Diisi',
             'g_recaptcha_response.required'     => 'Wajib Diisi',
             'g_recaptcha_response.captcha'      => 'Isian Captcha Tidak Sesuai'
         ];
    }

    public function individualCodeValidation(): bool
    {
        $returned = false;
        if (!empty($this->get('code'))) {
            $tempArray = str_split($this->get('code'), 2);
            $validateString = $tempArray[0].$tempArray[1].$tempArray[2];
            $returned = (bool)SubDistrict::where('validation_code', $validateString)->count();
        }

        return $returned;
    }

    public function individualCodeDateOfBirthValidation(): bool
    {
        $returned  = false;
        $count     = 0;
        if (!empty($this->get('code')) && strlen($this->get('code')) == 16) {
            $tempArray = str_split($this->get('code'), 2);
            if ($this->get('gender_id') == '8fd69ced-ac03-41eb-ac4c-47c99373a122') {
                $tempArray[3]-=40;
            }
            if (intval($tempArray[3]) < 10 && strlen($tempArray[3]) == 1) {
                $tempArray[3] = '0'.$tempArray[3];
            }
            $tempDateArray = explode('-', $this->get('birth_date'));
            $birtDayFromCode = $tempArray[3].'-'.$tempArray[4].'-'.$tempDateArray[2];
            if ($birtDayFromCode == $this->get('birth_date')) {
                $returned = true;
            }
        }
        return $returned;
    }

    /**
    * Get the validator instance for the request and
    * add attach callbacks to be run after validation
    * is completed.
    *
    * @return \Illuminate\Contracts\Validation\Validator
    */
    protected function getValidatorInstance()
    {
        return parent::getValidatorInstance()->after(function ($validator) {
            // Call the after method of the FormRequest (see below)
            $this->after($validator);
        });
    }

    /**
    * Attach callbacks to be run after validation is completed.
    *
    * @param  \Illuminate\Contracts\Validation\Validator $validator
    * @return callback
    */
    public function after($validator)
    {
        if (!$this->individualCodeValidation()) {
            $validator->errors()->add('code', 'Nomor Induk Kependudukan Anda Tidak Valid');
        }
        if (!$this->individualCodeDateOfBirthValidation()) {
            $validator->errors()->add('code', 'Nomor Induk Kependudukan Anda Tidak Valid');
            $validator->errors()->add('birth_date', 'Tanggal Lahir Anda Tidak Valid');
        }
    }
}
