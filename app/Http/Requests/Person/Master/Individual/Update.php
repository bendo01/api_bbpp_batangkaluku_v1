<?php
declare (strict_types = 1);
namespace Whiteboks\Http\Requests\Person\Master\Individual;

use Illuminate\Foundation\Http\FormRequest;

class Update extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'code'                   => 'required|string',
            'name'                   => 'required|string',
            'birth_date'             => 'required|string',
            'birth_place'            => 'required|string',
            'birth_place_id'         => 'nullable|string',
            'gender_id'              => 'required|string',
            'religion_id'            => 'required|string',
            'occupation_id'          => 'required|string',
            'education_id'           => 'required|string',
            'income_id'              => 'required|string',
            'identification_type_id' => 'required|string',
            'marital_status_id'      => 'required|string',
            'profession_id'          => 'required|string',
            'deceased'               => 'required|boolean',
            'country_id'             => 'nullable|string',
        ];
    }
}
