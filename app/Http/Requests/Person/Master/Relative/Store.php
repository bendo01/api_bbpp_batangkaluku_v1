<?php
declare (strict_types = 1);
namespace Whiteboks\Http\Requests\Person\Master\Relative;

use Illuminate\Foundation\Http\FormRequest;

class Store extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'individual_id'    => 'required|string',
            'relative_id'      => 'required|string',
            'relative_type_id' => 'required|string',
        ];
    }
}
