<?php
declare (strict_types = 1);
namespace Whiteboks\Http\Controllers\Person;

use Ramsey\Uuid\Uuid;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Hash;
use Whiteboks\Http\Controllers\Controller;
use Whiteboks\Http\Requests\Person\Master\Individual\Register as RegisterRequest;

use Whiteboks\Model\Auth\User;
use Whiteboks\Model\Common\Master\Phone;
use Whiteboks\Model\Common\Master\Residence;
use Whiteboks\Model\Person\Master\Individual;

class RegisterController extends Controller
{
    public function register(RegisterRequest $request) : JsonResponse
    {
        $individual                         = new Individual();
        $individual->code                   = $request->get('code');
        $individual->name                   = $request->get('name');
        $individual->birth_date             = $request->get('birth_date');
        $individual->birth_place            = $request->get('birth_place');
        $individual->gender_id              = $request->get('gender_id');
        $individual->religion_id            = $request->get('religion_id');
        $individual->occupation_id          = $request->get('occupation_id');
        $individual->education_id           = $request->get('education_id');
        $individual->identification_type_id = '3d59fc95-b07d-46ad-95ff-206b7e7f253f';
        $individual->marital_status_id      = $request->get('marital_status_id');
        $individual->profession_id          = $request->get('profession_id');
        $individual->deceased               = false;
        $individual->country_id             = 'cc9ec5bc-7ab9-4326-a0cd-7760b5dae1f4';
        $individual->save();

        $user                = new User();
        $user->name          = $individual->name;
        $user->email         = $request->get('email');
        $user->password      = Hash::make($request->get('password'));
        $user->active        = true;
        $user->confirmed     = true;
        $user->individual_id = $individual->id;
        $user->save();

        $individual->residencies()->create([
            'address'                  => $request->get('address'),
            'citizens_association'     => $request->get('citizens_association'),
            'neighborhood_association' => $request->get('neighborhood_association'),
            'province_id'              => $request->get('province_id'),
            'regency_id'               => $request->get('regency_id'),
            'sub_district_id'          => $request->get('sub_district_id'),
            'village_id'               => $request->get('village_id'),
        ]);
        return response()->json(['code' => 200, 'message'=>'Data Berhasil Disimpan Diserver']);
    }
}
