<?php
declare (strict_types = 1);
namespace Whiteboks\Model\Auth;

use Whiteboks\Model\Auth\Role;
use Whiteboks\Model\Auth\Ability;
use Laravel\Passport\HasApiTokens;
use Whiteboks\Lib\ModelTrait\UuidForKey;
use Illuminate\Database\Eloquent\SoftDeletes;
use Whiteboks\Model\Person\Master\Individual;
use Whiteboks\Overrides\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class User extends Authenticatable
{
    use UuidForKey, SoftDeletes, Notifiable, HasApiTokens;
    public $incrementing  = false;
    protected $table      = 'authentification.users';
    protected $connection = 'authentification';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts      = [
        'id'             => 'string',
        'name'           => 'string',
        'email'          => 'string',
        'password'       => 'string',
        'active'         => 'boolean',
        'individual_id'  => 'string',
        'remember_token' => 'string',
        'created_at'     => 'datetime',
        'updated_at'     => 'datetime',
        'deleted_at'     => 'datetime',
    ];

    public function individual(): BelongsTo
    {
        return $this->belongsTo(Individual::class, 'individual_id');
    }

    public function abilities(): BelongsToMany
    {
        return $this->belongsToMany(Ability::class, 'ability_user', 'abilty_id', 'user_id');
    }

    public function roles(): BelongsToMany
    {
        return $this->belongsToMany(Ability::class, 'role_user', 'role_id', 'user_id');
    }
}
