<?php
declare (strict_types = 1);
namespace Whiteboks\Model\Auth;

use Whiteboks\Model\Auth\Role;
use Whiteboks\Model\Auth\User;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Whiteboks\Lib\ModelTrait\UuidForKey;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Ability extends Model
{
    use UuidForKey, SoftDeletes;

    public $incrementing  = false;
    protected $table      = 'authentification.abilities';
    protected $connection = 'authentification';
    protected $fillable   = ['name', 'title'];
    protected $guarded    = ['id', 'created_at', 'updated_at', 'deleted_at'];
    protected $casts      = [
        'id'          => 'string',
        'title'       => 'string',
        'name'        => 'string',
        'created_by'  => 'string',
        'modified_by' => 'string',
        'created_at'  => 'datetime',
        'updated_at'  => 'datetime',
        'deleted_at'  => 'datetime',
    ];

    public function users(): BelongsToMany
    {
        return $this->belongsToMany(User::class, 'ability_user', 'ability_id', 'user_id');
    }

    public function roles(): BelongsToMany
    {
        return $this->belongsToMany(Role::class, 'ability_user', 'role_id', 'user_id');
    }
}
