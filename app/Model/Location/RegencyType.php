<?php
declare (strict_types = 1);
namespace Whiteboks\Model\Location;

use Illuminate\Database\Eloquent\Model;
use Whiteboks\Lib\ModelTrait\UuidForKey;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Relations\HasMany;

class RegencyType extends Model
{
    use UuidForKey, SoftDeletes;

    public $incrementing  = false;
    protected $dates      = ['deleted_at'];
    protected $table      = 'location.regency_types';
    protected $connection = 'location';
    protected $guarded    = ['id', 'created_at', 'updated_at', 'deleted_at'];
    protected $casts = [
        'id'          => 'string',
        'code'        => 'string',
        'name'        => 'string',
        'created_by'  => 'string',
        'modified_by' => 'string',
        'created_at'  => 'datetime',
        'updated_at'  => 'datetime',
        'deleted_at'  => 'datetime',
    ];
    public function regencies(): HasMany
    {
        return $this->hasMany(Regency::class, 'regency_type_id');
    }
}
