<?php
declare (strict_types = 1);
namespace Whiteboks\Model\Location;

use Illuminate\Database\Eloquent\Model;
use Whiteboks\Lib\ModelTrait\UuidForKey;
use Illuminate\Database\Eloquent\SoftDeletes;

use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

use Whiteboks\Model\Module\Research\Transaction\Publication;
use Whiteboks\Model\Module\Research\Transaction\ScienceForum;

use Whiteboks\Model\Common\Master\Residence;

class Regency extends Model
{

    use UuidForKey, SoftDeletes;

    public $incrementing  = false;
    protected $dates      = ['deleted_at'];
    protected $table      = 'location.regencies';
    protected $connection = 'location';
    protected $guarded    = ['id', 'created_at', 'updated_at', 'deleted_at'];
    protected $casts      = [
        'id'                          => 'string',
        'code'                        => 'string',
        'name'                        => 'string',
        'dikti_code'                  => 'string',
        'dikti_name'                  => 'string',
        'epsbed_code'                 => 'string',
        'state_ministry_code'         => 'string',
        'state_ministry_full_code'    => 'string',
        'state_ministry_name'         => 'string',
        'state_post_department_code'  => 'string',
        'postal_code_start_range'     => 'string',
        'postal_code_end_range'       => 'string',
        'validation_code'             => 'string',
        'agriculture_department_code' => 'string',
        'epsbed_code'                 => 'string',
        'agriculture_department_name' => 'boolean',
        'slug'                        => 'string',
        'alt_slug'                    => 'string',
        'description'                 => 'string',
        'province_id'                 => 'string',
        'regency_type_id'             => 'string',
        'created_by'                  => 'string',
        'modified_by'                 => 'string',
        'created_at'                  => 'datetime',
        'updated_at'                  => 'datetime',
        'deleted_at'                  => 'datetime',
    ];
    public function province(): BelongsTo
    {
        return $this->belongsTo(Province::class, 'province_id');
    }
    public function regencyType(): BelongsTo
    {
        return $this->belongsTo(RegencyType::class, 'regency_type_id');
    }
    public function subDistricts(): HasMany
    {
        return $this->hasMany(SubDistrict::class, 'regency_id');
    }
    public function publications(): HasMany
    {
        return $this->hasMany(Publication::class, 'regency_id');
    }
    public function scienceForums(): HasMany
    {
        return $this->hasMany(ScienceForum::class, 'regency_id');
    }

    public function residencies(): HasMany
    {
        return $this->hasMany(Residence::class, 'regency_id');
    }
}
