<?php
declare (strict_types = 1);
namespace Whiteboks\Model\Location;

use Illuminate\Database\Eloquent\Model;
use Whiteboks\Lib\ModelTrait\UuidForKey;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

use Whiteboks\Model\Common\Master\Residence;

class Village extends Model
{
    use UuidForKey, SoftDeletes;

    public $incrementing  = false;
    protected $dates      = ['deleted_at'];
    protected $table      = 'location.villages';
    protected $connection = 'location';
    protected $guarded    = ['id', 'created_at', 'updated_at', 'deleted_at'];
    protected $casts = [
        'id'              => 'string',
        'code'            => 'string',
        'name'            => 'string',
        'sub_district_id' => 'string',
        'created_by'      => 'string',
        'modified_by'     => 'string',
        'created_at'      => 'datetime',
        'updated_at'      => 'datetime',
        'deleted_at'      => 'datetime',
    ];
    public function subDistrict(): BelongsTo
    {
        return $this->belongsTo(SubDistrict::class, 'sub_district_id');
    }
    public function residencies(): HasMany
    {
        return $this->hasMany(Residence::class, 'village_id');
    }
}
