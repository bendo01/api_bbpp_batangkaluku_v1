<?php
declare (strict_types = 1);
namespace Whiteboks\Model\Location;

use Illuminate\Database\Eloquent\Model;
use Whiteboks\Lib\ModelTrait\UuidForKey;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Whiteboks\Model\Module\Research\Transaction\Publication;

class Country extends Model
{
    use UuidForKey, SoftDeletes;

    public $incrementing  = false;
    protected $dates      = ['deleted_at'];
    protected $table      = 'location.countries';
    protected $connection = 'location';
    protected $guarded    = ['id', 'created_at', 'updated_at', 'deleted_at'];
    protected $casts      = [
        'id'             => 'string',
        'code'           => 'string',
        'name'           => 'string',
        'alpha2_code'    => 'string',
        'alpha3_code'    => 'string',
        'iso3166_2_code' => 'string',
        'dikti_code'     => 'string',
        'slug'           => 'string',
        'continent_id'   => 'string',
        'region_id'      => 'string',
        'created_by'     => 'string',
        'modified_by'    => 'string',
        'created_at'     => 'datetime',
        'updated_at'     => 'datetime',
        'deleted_at'     => 'datetime',
    ];

    public function continents(): BelongsTo
    {
        return $this->belongsTo(Continent::class, 'continent_id');
    }

    public function regions(): BelongsTo
    {
        return $this->belongsTo(Region::class, 'region_id');
    }

    public function provinces(): HasMany
    {
        return $this->hasMany(Province::class, 'country_id');
    }
}
