<?php
declare (strict_types = 1);
namespace Whiteboks\Model\Institution\Master;

use Illuminate\Database\Eloquent\Model;
use Whiteboks\Lib\ModelTrait\UuidForKey;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

use Whiteboks\Model\Institution\Reference\Type;
use Whiteboks\Model\Institution\Reference\Category;
use Whiteboks\GraphQL\Type\Module\Training\Transaction\TrainingClass;
use Whiteboks\Model\Person\Transaction\Biodata\Education as EducationHistory;

class Institution extends Model
{
    use UuidForKey, SoftDeletes;

    public $incrementing  = false;
    protected $dates      = ['deleted_at'];
    protected $table      = 'institution_master.institutions';
    protected $connection = 'institution_master';
    protected $guarded    = ['id', 'created_at', 'updated_at', 'deleted_at'];
    protected $casts = [
        'id'           => 'string',
        'code'         => 'string',
        'name'         => 'string',
        'abbreviation' => 'string',
        'is_active'    => 'boolean',
        'type_id'      => 'string',
        'category_id'  => 'string',
        'slug'         => 'string',
        'country_id'   => 'string',
        'created_by'   => 'string',
        'modified_by'  => 'string',
        'created_at'   => 'datetime',
        'updated_at'   => 'datetime',
        'deleted_at'   => 'datetime',
    ];
    public function type(): BelongsTo
    {
        return $this->belongsTo(Type::class, 'type_id');
    }
    public function category(): BelongsTo
    {
        return $this->belongsTo(Category::class, 'category_id');
    }
    public function units(): HasMany
    {
        return $this->hasMany(Unit::class, 'institution_id');
    }
    public function trainingClasses(): HasMany
    {
        return $this->hasMany(TrainingClass::class, 'institution_id');
    }
    public function educationHistories(): HasMany
    {
        return $this->hasMany(EducationHistory::class, 'institution_id');
    }
}
