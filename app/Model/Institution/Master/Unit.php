<?php
declare (strict_types = 1);
namespace Whiteboks\Model\Institution\Master;

use Kalnoy\Nestedset\NodeTrait;
use Illuminate\Database\Eloquent\Model;
use Whiteboks\Lib\ModelTrait\UuidForKey;
use Illuminate\Database\Eloquent\SoftDeletes;

use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

use Whiteboks\Model\Institution\Reference\UnitType;
use Whiteboks\Model\Module\Research\Transaction\Partner;
use Whiteboks\Model\Module\Research\Master\PublicationMedia;
use Whiteboks\Model\Module\Research\Transaction\EmploymentContract;
use Whiteboks\Model\Module\Academic\CourseDepartment\Transaction\Lecturer\HomeBase;
use Whiteboks\Model\Module\Academic\CourseDepartment\Master\CourseDepartment\CourseDepartment;

class Unit extends Model
{
    use UuidForKey, SoftDeletes, NodeTrait;

    public $incrementing  = false;
    protected $dates      = ['deleted_at'];
    protected $table      = 'institution_master.units';
    protected $connection = 'institution_master';
    protected $guarded    = ['id', 'created_at', 'updated_at', 'deleted_at', '_lft', '_rgt'];
    protected $casts = [
        'id'             => 'string',
        'code'           => 'string',
        'name'           => 'string',
        'abbreviation'   => 'string',
        'is_active'      => 'boolean',
        'unit_type_id'   => 'string',
        'parent_id'      => 'string',
        'institution_id' => 'string',
        '_lft'           => 'integer',
        '_rgt'           => 'integer',
        'created_by'     => 'string',
        'modified_by'    => 'string',
        'created_at'     => 'datetime',
        'updated_at'     => 'datetime',
        'deleted_at'     => 'datetime',
    ];

    public function unitType(): BelongsTo
    {
        return $this->belongsTo(UnitType::class, 'unit_type_id');
    }

    public function courseDepartment(): HasOne
    {
        return $this->belongsTo(CourseDepartment::class, 'unit_id');
    }

    public function institution(): BelongsTo
    {
        return $this->belongsTo(Institution::class, 'institution_id');
    }
    public function homeBases(): HasMany
    {
        return $this->hasMany(HomeBase::class, 'unit_id');
    }
    public function publicationMedias(): HasMany
    {
        return $this->hasMany(PublicationMedia::class, 'unit_id');
    }
    public function partners(): HasMany
    {
        return $this->hasMany(Partner::class, 'unit_id');
    }
    public function employmentContracts(): HasMany
    {
        return $this->hasMany(Partner::class, 'unit_id');
    }
}
