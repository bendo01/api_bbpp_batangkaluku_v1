<?php
declare (strict_types = 1);
namespace Whiteboks\Model\Institution\Master;

use Illuminate\Database\Eloquent\Model;
use Whiteboks\Lib\ModelTrait\UuidForKey;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Whiteboks\Model\Institution\Reference\CoordinatorType;

class Coordinator extends Model
{
    use UuidForKey, SoftDeletes;

    public $incrementing  = false;
    protected $dates      = ['deleted_at'];
    protected $table      = 'institution_master.coordinators';
    protected $connection = 'institution_master';
    protected $guarded    = ['id', 'created_at', 'updated_at', 'deleted_at'];
    protected $casts = [
        'id'                  => 'string',
        'institution_id'      => 'string',
        'coordinator_type_id' => 'string',
        'created_by'          => 'string',
        'modified_by'         => 'string',
        'created_at'          => 'datetime',
        'updated_at'          => 'datetime',
        'deleted_at'          => 'datetime',
    ];
    public function institution(): BelongsTo
    {
        return $this->belongsTo(Institution::class, 'institution_id');
    }
    public function coordinatorType(): BelongsTo
    {
        return $this->belongsTo(CoordinatorType::class, 'coordinator_type_id');
    }
}
