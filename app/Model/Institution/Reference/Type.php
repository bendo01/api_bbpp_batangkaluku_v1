<?php
declare (strict_types = 1);
namespace Whiteboks\Model\Institution\Reference;

use Illuminate\Database\Eloquent\Model;
use Whiteboks\Lib\ModelTrait\UuidForKey;
use Illuminate\Database\Eloquent\SoftDeletes;
use Whiteboks\Model\Institution\Master\Institution;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Type extends Model
{
    use UuidForKey, SoftDeletes;

    public $incrementing  = false;
    protected $dates      = ['deleted_at'];
    protected $table      = 'institution_reference.types';
    protected $connection = 'institution_reference';
    protected $guarded    = ['id', 'created_at', 'updated_at', 'deleted_at'];
    protected $casts = [
        'id'             => 'string',
        'code'           => 'string',
        'name'           => 'string',
        'slug'           => 'string',
        'created_by'     => 'string',
        'modified_by'    => 'string',
        'created_at'     => 'datetime',
        'updated_at'     => 'datetime',
        'deleted_at'     => 'datetime',
    ];
    public function institutions(): HasMany
    {
        return $this->hasMany(Institution::class, 'type_id');
    }
    public function unitTypes(): HasMany
    {
        return $this->hasMany(UnitType::class, 'type_id');
    }
}
