<?php
declare (strict_types = 1);
namespace Whiteboks\Model\Institution\Reference;

use Illuminate\Database\Eloquent\Model;
use Whiteboks\Lib\ModelTrait\UuidForKey;
use Illuminate\Database\Eloquent\SoftDeletes;
use Whiteboks\Model\Institution\Master\Coordinator;
use Illuminate\Database\Eloquent\Relations\HasMany;

class CoordinatorType extends Model
{
    use UuidForKey, SoftDeletes;

    public $incrementing  = false;
    protected $dates      = ['deleted_at'];
    protected $table      = 'institution_reference.coordinator_types';
    protected $connection = 'institution_reference';
    protected $guarded    = ['id', 'created_at', 'updated_at', 'deleted_at'];
    protected $casts = [
        'id'             => 'string',
        'code'           => 'string',
        'name'           => 'string',
        'slug'           => 'string',
        'created_by'     => 'string',
        'modified_by'    => 'string',
        'created_at'     => 'datetime',
        'updated_at'     => 'datetime',
        'deleted_at'     => 'datetime',
    ];
    public function coordinators(): HasMany
    {
        return $this->hasMany(Coordinator::class, 'coordinator_type_id');
    }
}
