<?php
declare (strict_types = 1);
namespace Whiteboks\Model\Common\Master;

// utility
use Illuminate\Database\Eloquent\Model;
use Whiteboks\Lib\ModelTrait\UuidForKey;
use Illuminate\Database\Eloquent\SoftDeletes;
// relationship type
use Illuminate\Database\Eloquent\Relations\MorphTo;

class Tag extends Model
{
    use UuidForKey, SoftDeletes;

    public $incrementing  = false;
    protected $dates      = ['deleted_at'];
    protected $table      = 'common_master.tags';
    protected $connection = 'common_master';
    protected $guarded    = ['id', 'created_at', 'updated_at', 'deleted_at'];
    protected $casts      = [
        'id'           => 'string',
        'title'        => 'string',
        'tagable_id'   => 'string',
        'tagable_type' => 'string',
        'created_by'   => 'string',
        'modified_by'  => 'string',
        'created_at'   => 'datetime',
        'updated_at'   => 'datetime',
        'deleted_at'   => 'datetime',
    ];

    public function tagable(): MorphTo
    {
        return $this->morphTo();
    }
}
