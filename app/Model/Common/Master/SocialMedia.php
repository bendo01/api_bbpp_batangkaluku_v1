<?php
declare (strict_types = 1);
namespace Whiteboks\Model\Common\Master;

// utility
use Illuminate\Database\Eloquent\Model;
use Whiteboks\Lib\ModelTrait\UuidForKey;
use Illuminate\Database\Eloquent\SoftDeletes;
// relationship type
use Illuminate\Database\Eloquent\Relations\MorphTo;

class SocialMedia extends Model
{
    use UuidForKey, SoftDeletes;

    public $incrementing  = false;
    protected $dates      = ['deleted_at'];
    protected $table      = 'common_master.social_medias';
    protected $connection = 'common_master';
    protected $guarded    = ['id', 'created_at', 'updated_at', 'deleted_at'];
    protected $casts      = [
        'id'                    => 'string',
        'username'              => 'string',
        'url'                   => 'string',
        'social_mediaable_id'   => 'string',
        'social_mediaable_type' => 'string',
        'created_by'            => 'string',
        'modified_by'           => 'string',
        'created_at'            => 'datetime',
        'updated_at'            => 'datetime',
        'deleted_at'            => 'datetime',
    ];

    public function socialMediaable(): MorphTo
    {
        return $this->morphTo();
    }
}
