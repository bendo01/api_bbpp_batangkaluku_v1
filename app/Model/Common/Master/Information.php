<?php
declare (strict_types = 1);
namespace Whiteboks\Model\Common\Master;

// utility
use Illuminate\Database\Eloquent\Model;
use Whiteboks\Lib\ModelTrait\UuidForKey;
use Illuminate\Database\Eloquent\SoftDeletes;
// relationship type
use Illuminate\Database\Eloquent\Relations\MorphTo;

class Information extends Model
{
    use UuidForKey, SoftDeletes;

    public $incrementing  = false;
    protected $dates      = ['deleted_at'];
    protected $table      = 'common_master.informations';
    protected $connection = 'common_master';
    protected $guarded    = ['id', 'created_at', 'updated_at', 'deleted_at'];
    protected $casts      = [
        'id'                   => 'string',
        'informationable_id'   => 'string',
        'informationable_type' => 'string',
        'vision'               => 'string',
        'mission'              => 'string',
        'intention'            => 'string',
        'objective'            => 'string',
        'created_by'           => 'string',
        'modified_by'          => 'string',
        'created_at'           => 'datetime',
        'updated_at'           => 'datetime',
        'deleted_at'           => 'datetime',
    ];

    public function informationable(): MorphTo
    {
        return $this->morphTo();
    }
}
