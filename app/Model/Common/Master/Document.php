<?php
declare (strict_types = 1);
namespace Whiteboks\Model\Common\Master;

use Illuminate\Database\Eloquent\Model;
use Whiteboks\Lib\ModelTrait\UuidForKey;
use Illuminate\Database\Eloquent\SoftDeletes;
// relationship type
use Illuminate\Database\Eloquent\Relations\MorphTo;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
// model relationship
use Whiteboks\Model\Common\Reference\Document\Type;

class Document extends Model
{
    use UuidForKey, SoftDeletes;

    public $incrementing  = false;
    protected $dates      = ['deleted_at'];
    protected $table      = 'common_master.documents';
    protected $connection = 'common_master';
    protected $guarded    = ['id', 'created_at', 'updated_at', 'deleted_at'];
    protected $casts      = [
        'id'                => 'string',
        'title'             => 'string',
        'filename'          => 'string',
        'type'              => 'string',
        'size'              => 'string',
        'url'               => 'string',
        'documentable_id'   => 'string',
        'documentable_type' => 'string',
        'type_id'           => 'string',
        'created_by'        => 'string',
        'modified_by'       => 'string',
        'created_at'        => 'datetime',
        'updated_at'        => 'datetime',
        'deleted_at'        => 'datetime',
    ];

    public function documentable(): MorphTo
    {
        return $this->morphTo();
    }

    public function type(): BelongsTo
    {
        return $this->belongsTo(Type::class, 'type_id');
    }
}
