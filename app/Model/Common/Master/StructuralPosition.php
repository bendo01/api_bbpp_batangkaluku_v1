<?php
declare (strict_types = 1);
namespace Whiteboks\Model\Common\Master;

// utility
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Whiteboks\Lib\ModelTrait\UuidForKey;
use Illuminate\Database\Eloquent\SoftDeletes;
use Whiteboks\Lib\ModelTrait\StartDateAndEndDateAccessorAndMutator;
// relationship type
use Illuminate\Database\Eloquent\Relations\MorphTo;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
// relationship model
use Whiteboks\Model\Auth\Role;
use Whiteboks\Model\Person\Master\Individual;

class StructuralPosition extends Model
{
    use UuidForKey, SoftDeletes, StartDateAndEndDateAccessorAndMutator;

    public $incrementing  = false;
    protected $dates      = ['deleted_at'];
    protected $table      = 'common_master.structural_positions';
    protected $connection = 'common_master';
    protected $guarded    = ['id', 'created_at', 'updated_at', 'deleted_at'];
    protected $casts      = [
        'id'                           => 'string',
        'individual_id'                => 'string',
        'role_id'                      => 'string',
        'decree_number'                => 'string',
        'structural_positionable_id'   => 'string',
        'structural_positionable_type' => 'string',
        'created_by'                   => 'string',
        'modified_by'                  => 'string',
        'created_at'                   => 'datetime',
        'updated_at'                   => 'datetime',
        'deleted_at'                   => 'datetime',
    ];

    public function setDecreeDateAttribute($value): void
    {
        $this->attributes['decree_date'] = null;
        if (!empty($value)) {
            $this->attributes['decree_date'] = Carbon::createFromFormat('d-m-Y', $value)->format('Y-m-d');
        }
    }

    public function getDecreeDateAttribute($value): ?Carbon
    {
        if (!empty($value)) {
            $returned = Carbon::createFromFormat('Y-m-d', $value)->format('d-m-Y');
            return $returned;
        }
    }

    public function role(): BelongsTo
    {
        return $this->belongsTo(Role::class, 'role_id');
    }

    public function individual(): BelongsTo
    {
        return $this->belongsTo(Individual::class, 'individual_id');
    }

    public function structuralPositionable(): MorphTo
    {
        return $this->morphTo();
    }
}
