<?php
declare (strict_types = 1);
namespace Whiteboks\Model\Common\Master;

// utility
use Illuminate\Database\Eloquent\Model;
use Whiteboks\Lib\ModelTrait\UuidForKey;
use Illuminate\Database\Eloquent\SoftDeletes;
// relationship type
use Illuminate\Database\Eloquent\Relations\MorphTo;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class ResidenceRoom extends Model
{
    use UuidForKey, SoftDeletes;

    public $incrementing  = false;
    protected $dates      = ['deleted_at'];
    protected $table      = 'common_master.residence_rooms';
    protected $connection = 'common_master';
    protected $guarded    = ['id', 'created_at', 'updated_at', 'deleted_at'];
    protected $casts      = [
        'id'           => 'string',
        'residence_id' => 'string',
        'code'         => 'string',
        'name'         => 'string',
        'floor'        => 'string',
        'long'         => 'decimal',
        'wide'         => 'double',
        'high'         => 'double',
        'latitude'     => 'double',
        'longitude'    => 'double',
        'zoom'         => 'integer',
        'created_by'   => 'string',
        'modified_by'  => 'string',
        'created_at'   => 'datetime',
        'updated_at'   => 'datetime',
        'deleted_at'   => 'datetime',
    ];

    public function residence(): BelongsTo
    {
        return $this->belongsTo(Resident::class, 'resident_id');
    }
}
