<?php
declare (strict_types = 1);
namespace Whiteboks\Model\Common\Master;

// utility
use Illuminate\Database\Eloquent\Model;
use Whiteboks\Lib\ModelTrait\UuidForKey;
use Illuminate\Database\Eloquent\SoftDeletes;
use Whiteboks\Lib\ModelTrait\StartDateAndEndDateAccessorAndMutator;
// relationship type
use Illuminate\Database\Eloquent\Relations\MorphTo;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
// model relationship
use Whiteboks\Model\Common\Reference\Decree\Type;

class Decree extends Model
{
    use UuidForKey, SoftDeletes, StartDateAndEndDateAccessorAndMutator;

    public $incrementing  = false;
    protected $dates      = ['deleted_at'];
    protected $table      = 'common_master.decrees';
    protected $connection = 'common_master';
    protected $guarded    = ['id', 'created_at', 'updated_at', 'deleted_at'];
    protected $casts      = [
        'id'              => 'string',
        'decreeable_id'   => 'string',
        'decreeable_type' => 'string',
        'title'           => 'string',
        'type_id'         => 'string',
        'code'            => 'string',
        'description'     => 'string',
        'created_by'      => 'string',
        'modified_by'     => 'string',
        'created_at'      => 'datetime',
        'updated_at'      => 'datetime',
    'deleted_at'      => 'datetime',
    ];
    public function decreeable(): MorphTo
    {
        return $this->morphTo();
    }

    public function type(): BelongsTo
    {
        return $this->belongsTo(Type::class, 'type_id');
    }
}
