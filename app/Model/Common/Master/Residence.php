<?php
declare (strict_types = 1);
namespace Whiteboks\Model\Common\Master;

// utility
use Illuminate\Database\Eloquent\Model;
use Whiteboks\Lib\ModelTrait\UuidForKey;
use Illuminate\Database\Eloquent\SoftDeletes;
// relationship type
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
// relationship model
use Whiteboks\Model\Location\Province;
use Whiteboks\Model\Location\Regency;
use Whiteboks\Model\Location\Village;
use Whiteboks\Model\Location\SubDistrict;
use Whiteboks\Model\Common\Reference\Residence\ResidentType;

class Residence extends Model
{
    use UuidForKey, SoftDeletes;

    public $incrementing  = false;
    protected $dates      = ['deleted_at'];
    protected $table      = 'common_master.residencies';
    protected $connection = 'common_master';
    protected $guarded    = ['id', 'created_at', 'updated_at', 'deleted_at'];
    protected $casts      = [
        'id'                       => 'string',
        'residenceable_id'         => 'string',
        'address'                  => 'string',
        'code'                     => 'string',
        'name'                     => 'string',
        'citizens_association'     => 'string',
        'neighborhood_association' => 'string',
        'province_id'              => 'string',
        'regency_id'               => 'string',
        'sub_district_id'          => 'string',
        'village_id'               => 'string',
        'regency_name'             => 'string',
        'sub_district_name'        => 'string',
        'village_name'             => 'string',
        'resident_type_id'         => 'string',
        'category_id'              => 'string',
        'type_id'                  => 'string',
        'latitude'                 => 'double',
        'longitude'                => 'double',
        'zoom'                     => 'integer',
        'created_by'               => 'string',
        'modified_by'              => 'string',
        'created_at'               => 'datetime',
        'updated_at'               => 'datetime',
        'deleted_at'               => 'datetime',
    ];

    public function residenceable(): MorphTo
    {
        return $this->morphTo();
    }

    public function province(): BelongsTo
    {
        return $this->belongsTo(Province::class, 'province_id');
    }

    public function regency(): BelongsTo
    {
        return $this->belongsTo(Regency::class, 'regency_id');
    }

    public function subDistrict(): BelongsTo
    {
        return $this->belongsTo(SubDistrict::class, 'sub_district_id');
    }

    public function village(): BelongsTo
    {
        return $this->belongsTo(Village::class, 'village_id');
    }

    public function residentType(): BelongsTo
    {
        return $this->belongsTo(ResidentType::class, 'village_id');
    }

    public function type(): BelongsTo
    {
        return $this->belongsTo(Type::class, 'type_id');
    }

    public function category(): BelongsTo
    {
        return $this->belongsTo(Category::class, 'category_id');
    }

    public function residenceRooms(): HasMany
    {
        return $this->hasMany(ResidentRoom::class, 'resident_id');
    }
}
