<?php
declare (strict_types = 1);
namespace Whiteboks\Model\Common\Master;

use Illuminate\Database\Eloquent\Model;
use Whiteboks\Lib\ModelTrait\UuidForKey;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Relations\MorphTo;

class Email extends Model
{
    use UuidForKey, SoftDeletes;

    public $incrementing  = false;
    protected $dates      = ['deleted_at'];
    protected $table      = 'common_master.emails';
    protected $connection = 'common_master';
    protected $guarded    = ['id', 'created_at', 'updated_at', 'deleted_at'];
    protected $casts      = [
        'id'             => 'string',
        'address'        => 'string',
        'emailable_id'   => 'string',
        'emailable_type' => 'string',
        'is_primary'     => 'boolean',
        'created_by'     => 'string',
        'modified_by'    => 'string',
        'created_at'     => 'datetime',
        'updated_at'     => 'datetime',
        'deleted_at'     => 'datetime',
    ];

    public function emailable(): MorphTo
    {
        return $this->morphTo();
    }
}
