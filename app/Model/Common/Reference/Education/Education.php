<?php
declare (strict_types = 1);
namespace Whiteboks\Model\Common\Reference\Education;

use Illuminate\Database\Eloquent\Model;
use Whiteboks\Lib\ModelTrait\UuidForKey;
use Illuminate\Database\Eloquent\SoftDeletes;

use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

use Whiteboks\Model\Person\Master\Individual;
use Whiteboks\Model\Common\Reference\Education\Education;
use Whiteboks\Model\Person\Transaction\BioData\Education as EducationHistory;
use Whiteboks\Model\Module\Academic\CourseDepartment\Master\CourseDepartment\CourseDepartment;

class Education extends Model
{
    use UuidForKey, SoftDeletes;

    public $incrementing  = false;
    protected $dates      = ['deleted_at'];
    protected $table      = 'common_reference_education.educations';
    protected $connection = 'common_reference_education';
    protected $guarded    = ['id', 'created_at', 'updated_at', 'deleted_at'];
    protected $casts      = [
        'id'           => 'string',
        'code'         => 'string',
        'name'         => 'string',
        'alphabetic'   => 'string',
        'abbreviation' => 'string',
        'stage_id'     => 'string',
        'group_id'     => 'string',
        'category_id'  => 'string',
        'type_id'      => 'string',
        'institution'  => 'boolean',
        'individual'   => 'boolean',
        'created_by'   => 'string',
        'modified_by'  => 'string',
        'created_at'   => 'datetime',
        'updated_at'   => 'datetime',
        'deleted_at'   => 'datetime',
    ];

    public function stage(): BelongsTo
    {
        return $this->belongsTo(Stage::class, 'stage_id');
    }

    public function group(): BelongsTo
    {
        return $this->belongsTo(Group::class, 'group_id');
    }

    public function category(): BelongsTo
    {
        return $this->belongsTo(Category::class, 'category_id');
    }

    public function type(): BelongsTo
    {
        return $this->belongsTo(Type::class, 'type_id');
    }

    public function educationHistories(): HasMany
    {
        return $this->hasMany(EducationHistory::class, 'education_id');
    }

    public function individuals(): HasMany
    {
        return $this->hasMany(Individual::class, 'education_id');
    }

    public function courseDepartments(): HasMany
    {
        return $this->hasMany(CourseDepartment::class, 'education_id');
    }
}
