<?php
declare (strict_types = 1);
namespace Whiteboks\Model\Common\Reference\Education;

use Illuminate\Database\Eloquent\Model;
use Whiteboks\Lib\ModelTrait\UuidForKey;
use Illuminate\Database\Eloquent\SoftDeletes;

use Illuminate\Database\Eloquent\Relations\HasMany;

use Whiteboks\Model\Module\Research\Master\PublicationMedia;
use Whiteboks\Model\Module\Academic\CourseDepartment\Master\CourseDepartment\CourseDepartment;

class ScienceGroup extends Model
{
    use UuidForKey, SoftDeletes;

    public $incrementing  = false;
    protected $dates      = ['deleted_at'];
    protected $table      = 'common_reference_education.science_groups';
    protected $connection = 'common_reference_education';
    protected $guarded    = ['id', 'created_at', 'updated_at', 'deleted_at'];
    protected $casts      = [
        'id'          => 'string',
        'code'        => 'string',
        'name'        => 'string',
        'created_by'  => 'string',
        'modified_by' => 'string',
        'created_at'  => 'datetime',
        'updated_at'  => 'datetime',
        'deleted_at'  => 'datetime',
    ];

    public function courseDepartments(): HasMany
    {
        return $this->hasMany(CourseDepartment::class, 'science_group_id');
    }
    public function publicationMedias(): HasMany
    {
        return $this->hasMany(PublicationMedia::class, 'science_group_id');
    }
}
