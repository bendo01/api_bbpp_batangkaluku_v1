<?php
declare (strict_types = 1);
namespace Whiteboks\Model\Common\Reference\Period;

use Illuminate\Database\Eloquent\Model;
use Whiteboks\Lib\ModelTrait\UuidForKey;
use Illuminate\Database\Eloquent\SoftDeletes;
use Whiteboks\Lib\ModelTrait\StartDateAndEndDateAccessorAndMutator;

use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Whiteboks\Model\Module\Research\Transaction\Funding;
use Whiteboks\Model\Module\Academic\College\Transaction\Calendar;
use Whiteboks\Model\Module\Academic\CourseDepartment\Master\CourseDepartment\CourseDepartment;


class EducationYear extends Model
{
    use UuidForKey, SoftDeletes, StartDateAndEndDateAccessorAndMutator;

    public $incrementing  = false;
    protected $dates      = ['deleted_at'];
    protected $table      = 'common_reference_period.education_years';
    protected $connection = 'common_reference_period';
    protected $guarded    = ['id', 'created_at', 'updated_at', 'deleted_at'];
    protected $casts      = [
        'id'                         => 'string',
        'name'                       => 'string',
        'year'                       => 'integer',
        'feeder_name'                => 'string',
        'thread'                     => 'integer',
        'education_year_category_id' => 'string',
        'expired_date'               => 'date',
        'created_by'                 => 'string',
        'modified_by'                => 'string',
        'created_at'                 => 'datetime',
        'updated_at'                 => 'datetime',
        'deleted_at'                 => 'datetime',
    ];

    public function educationYearCategory(): BelongsTo
    {
        return $this->belongsTo(EducationYearCategory::class, 'education_year_category_id');
    }

    public function courseDepartments(): HasMany
    {
        return $this->hasMany(CourseDepartment::class, 'education_year_id');
    }

    public function calendars(): HasMany
    {
        return $this->hasMany(Calendar::class, 'education_year_id');
    }
    public function fundings(): HasMany
    {
        return $this->hasMany(Funding::class, 'education_year_id');
    }
}
