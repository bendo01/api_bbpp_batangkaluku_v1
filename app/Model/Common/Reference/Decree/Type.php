<?php
declare (strict_types = 1);
namespace Whiteboks\Model\Common\Reference\Decree;

use Illuminate\Database\Eloquent\Model;
use Whiteboks\Lib\ModelTrait\UuidForKey;
use Illuminate\Database\Eloquent\SoftDeletes;
// relationship type
use Illuminate\Database\Eloquent\Relations\HasMany;
//relationship model
use Whiteboks\Model\Common\Master\Decree;

class Type extends Model
{
    use UuidForKey, SoftDeletes;

    public $incrementing  = false;
    protected $dates      = ['deleted_at'];
    protected $table      = 'common_reference_decree.types';
    protected $connection = 'common_reference_decree';
    protected $guarded    = ['id', 'created_at', 'updated_at', 'deleted_at'];
    protected $casts      = [
        'id'          => 'string',
        'code'        => 'string',
        'name'        => 'string',
        'created_by'  => 'string',
        'modified_by' => 'string',
        'created_at'  => 'datetime',
        'updated_at'  => 'datetime',
        'deleted_at'  => 'datetime',
    ];

    public function decrees(): HasMany
    {
        return $this->hasMany(Decree::class, 'type_id');
    }
}
