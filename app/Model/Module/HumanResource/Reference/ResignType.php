<?php
declare (strict_types = 1);
namespace Whiteboks\Model\Module\HumanResource\Reference;

use Illuminate\Database\Eloquent\Model;
use Whiteboks\Lib\ModelTrait\UuidForKey;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Whiteboks\Model\Module\HumanResource\Master\Employee;

class ResignType extends Model
{
    use UuidForKey, SoftDeletes;

    public $incrementing  = false;
    protected $dates      = ['deleted_at'];
    protected $table      = 'module_human_resource_reference.resign_types';
    protected $connection = 'module_human_resource_reference';
    protected $guarded    = ['id', 'created_at', 'updated_at', 'deleted_at'];
    protected $casts      = [
        'id'          => 'string',
        'code'        => 'string',
        'name'        => 'string',
        'created_by'  => 'string',
        'modified_by' => 'string',
        'created_at'  => 'datetime',
        'updated_at'  => 'datetime',
        'deleted_at'  => 'datetime',
    ];

    public function employees(): HasMany
    {
        return $this->hasMany(Employee::class, 'resign_type_id');
    }
}
