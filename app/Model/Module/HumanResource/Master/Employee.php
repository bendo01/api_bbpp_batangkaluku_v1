<?php
declare (strict_types = 1);
namespace Whiteboks\Model\Module\HumanResource\Master;

use Illuminate\Database\Eloquent\Model;
use Whiteboks\Lib\ModelTrait\UuidForKey;
use Illuminate\Database\Eloquent\SoftDeletes;

use Illuminate\Database\Eloquent\Relations\BelongsTo;

use Whiteboks\Model\Person\Master\Individual;
use Whiteboks\Model\Module\HumanResource\Reference\Status;
use Whiteboks\Model\Module\HumanResource\Reference\ResignType;
use Whiteboks\Model\Module\HumanResource\Reference\ContractType;

class Employee extends Model
{
    use UuidForKey, SoftDeletes;

    public $incrementing  = false;
    protected $dates      = ['deleted_at'];
    protected $table      = 'module_human_resource_master.employees';
    protected $connection = 'module_human_resource_master';
    protected $guarded    = ['id', 'created_at', 'updated_at', 'deleted_at'];
    protected $casts      = [
        'id'               => 'string',
        'code'             => 'string',
        'individual_id'    => 'string',
        'status_id'        => 'string',
        'contract_type_id' => 'string',
        'resign_type_id'   => 'string',
        'created_by'       => 'string',
        'modified_by'      => 'string',
        'created_at'       => 'datetime',
        'updated_at'       => 'datetime',
        'deleted_at'       => 'datetime',
    ];

    public function individual(): BelongsTo
    {
        return $this->belongsTo(Individual::class, 'individual_id');
    }

    public function status(): BelongsTo
    {
        return $this->belongsTo(Status::class, 'status_id');
    }

    public function contractType(): BelongsTo
    {
        return $this->belongsTo(ContractType::class, 'contract_type_id');
    }

    public function resignType(): BelongsTo
    {
        return $this->belongsTo(ResignType::class, 'resign_type_id');
    }
}
