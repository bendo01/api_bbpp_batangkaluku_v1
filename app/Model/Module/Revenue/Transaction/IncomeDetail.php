<?php
declare (strict_types = 1);
namespace Whiteboks\Model\Module\Revenue\Transaction;

use Illuminate\Database\Eloquent\Model;
use Whiteboks\Lib\ModelTrait\UuidForKey;
use Illuminate\Database\Eloquent\SoftDeletes;

class IncomeDetail extends Model
{
    use UuidForKey, SoftDeletes;

    public $incrementing  = false;
    protected $dates      = ['deleted_at'];
    protected $table      = 'module_revenue_transaction.income_details';
    protected $connection = 'module_revenue_transaction';
    protected $guarded    = ['id', 'created_at', 'updated_at', 'deleted_at'];
    protected $casts      = [
        'id'           => 'string',
        'title'        => 'string',
        'publish_date' => 'string',
        'amount'       => 'decimal',
        'income_id'    => 'string',
        'created_by'   => 'string',
        'modified_by'  => 'string',
        'created_at'   => 'datetime',
        'updated_at'   => 'datetime',
        'deleted_at'   => 'datetime',
    ];
}
