<?php
declare (strict_types = 1);
namespace Whiteboks\Model\Module\Inventory\Transaction;

use Illuminate\Database\Eloquent\Model;
use Whiteboks\Lib\ModelTrait\UuidForKey;
use Illuminate\Database\Eloquent\SoftDeletes;

class Placement extends Model
{
    use UuidForKey, SoftDeletes;

    public $incrementing  = false;
    protected $dates      = ['deleted_at'];
    protected $table      = 'module_inventory_transaction.placements';
    protected $connection = 'module_inventory_transaction';
    protected $guarded    = ['id', 'created_at', 'updated_at', 'deleted_at'];
    protected $casts      = [
        'id'               => 'string',
        'code'             => 'string',
        'name'             => 'string',
        'unit_id'          => 'string',
        'recident_room_id' => 'string',
        'catalog_id'       => 'string',
        'created_by'       => 'string',
        'modified_by'      => 'string',
        'created_at'       => 'datetime',
        'updated_at'       => 'datetime',
        'deleted_at'       => 'datetime',
    ];

}
