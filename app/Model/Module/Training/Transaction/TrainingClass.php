<?php
declare (strict_types = 1);
namespace Whiteboks\Model\Module\Training\Transaction;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Whiteboks\Lib\ModelTrait\UuidForKey;
use Illuminate\Database\Eloquent\SoftDeletes;

use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

use Whiteboks\Model\Person\Master\Individual;
use Whiteboks\Model\Module\Training\Master\Course;
use Whiteboks\Model\Institution\Master\Institution;
use Whiteboks\Model\Module\Training\Reference\LearningMaterial\Method;
use Whiteboks\Model\Module\Training\Reference\LearningMaterial\Category;

class TrainingClass extends Model
{
    use UuidForKey, SoftDeletes;

    public $incrementing  = false;
    protected $dates      = ['deleted_at'];
    protected $table      = 'module_training_transaction.training_classes';
    protected $connection = 'module_training_transaction';
    protected $guarded    = ['id', 'created_at', 'updated_at', 'deleted_at'];
    protected $casts      = [
        'id'             => 'string',
        'code'           => 'string',
        'name'           => 'string',
        'duration_hour'  => 'integer',
        'duration_day'   => 'integer',
        'expanse'        => 'decimal',
        'preparation'    => 'string',
        'evaluation'     => 'string',
        'implementation' => 'string',
        'institution_id' => 'string',
        'category_id'    => 'string',
        'created_by'     => 'string',
        'modified_by'    => 'string',
        'created_at'     => 'datetime',
        'updated_at'     => 'datetime',
        'deleted_at'     => 'datetime',
    ];

    public function setStartDateAttribute($value): void
    {
        if (!empty($value)) {
            $this->attributes['start_date'] = Carbon::createFromFormat('d-m-Y', $value)->format('Y-m-d');
        }
    }

    public function getStartDateAttribute($value): ?string
    {
        $returned = null;
        if (!empty($value)) {
            $returned = Carbon::createFromFormat('Y-m-d', $value)->format('d-m-Y');
        }
        return $returned;
    }

    public function setEndDateAttribute($value): void
    {
        if (!empty($value)) {
            $this->attributes['end_date'] = Carbon::createFromFormat('d-m-Y', $value)->format('Y-m-d');
        }
    }

    public function getEndDateAttribute($value): ?string
    {
        $returned = null;
        if (!empty($value)) {
            $returned = Carbon::createFromFormat('Y-m-d', $value)->format('d-m-Y');
        }
        return $returned;
    }

    public function category(): BelongsTo
    {
        return $this->belongsTo(Category::class, 'category_id');
    }

    public function institution(): BelongsTo
    {
        return $this->belongsTo(Institution::class, 'institution_id');
    }

    public function members(): BelongsToMany
    {
        return $this->belongsToMany(Individual::class, 'module_training_transaction.training_class_member', 'training_class_id', 'individual_id')->withTimestamps();
    }

    public function facilitators(): BelongsToMany
    {
        return $this->belongsToMany(Individual::class, 'module_training_transaction.training_class_facilitator', 'training_class_id', 'individual_id')->withTimestamps();
    }

    public function methods(): BelongsToMany
    {
        return $this->belongsToMany(Method::class, 'module_training_transaction.training_class_learning_material_method', 'training_class_id', 'method_id')->withTimestamps();
    }

    public function courses(): BelongsToMany
    {
        return $this->belongsToMany(Course::class, 'module_training_transaction.training_class_course', 'training_class_id', 'course_id')->withTimestamps();
    }
}
