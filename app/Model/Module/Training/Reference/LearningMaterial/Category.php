<?php
declare (strict_types = 1);
namespace Whiteboks\Model\Module\Training\Reference\LearningMaterial;

use Kalnoy\Nestedset\NodeTrait;
use Illuminate\Database\Eloquent\Model;
use Whiteboks\Lib\ModelTrait\UuidForKey;
use Illuminate\Database\Eloquent\SoftDeletes;

use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

use Whiteboks\Model\Module\Training\Master\Course;
use Whiteboks\Model\Module\Training\Transaction\TrainingClass;
use Whiteboks\Model\Module\Training\Reference\TrainingPackage;

class Category extends Model
{
    use NodeTrait, UuidForKey, SoftDeletes;

    public $incrementing  = false;
    protected $dates      = ['deleted_at'];
    protected $table      = 'module_training_reference_learning_material.categories';
    protected $connection = 'module_training_reference_learning_material';
    protected $guarded    = ['id', 'created_at', 'updated_at', 'deleted_at'];
    protected $casts      = [
        'id'                  => 'string',
        'code'                => 'string',
        'name'                => 'string',
        'description'         => 'string',
        'parent_id'           => 'string',
        'training_package_id' => 'string',
        '_lft'                => 'integer',
        '_rgt'                => 'integer',
        'created_by'          => 'string',
        'modified_by'         => 'string',
        'created_at'          => 'datetime',
        'updated_at'          => 'datetime',
        'deleted_at'          => 'datetime',
    ];

    public function trainingPackage(): BelongsTo
    {
        return $this->belongsTo(TrainingPackage::class, 'training_package_id');
    }

    public function trainingClasses(): HasMany
    {
        return $this->hasMany(TrainingClass::class, 'category_id');
    }

    public function courses(): HasMany
    {
        return $this->hasMany(Course::class, 'category_id');
    }
}
