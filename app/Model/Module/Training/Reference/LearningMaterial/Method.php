<?php
declare (strict_types = 1);
namespace Whiteboks\Model\Module\Training\Reference\LearningMaterial;

use Illuminate\Database\Eloquent\Model;
use Whiteboks\Lib\ModelTrait\UuidForKey;
use Illuminate\Database\Eloquent\SoftDeletes;

use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

use Whiteboks\Model\Module\Training\Transaction\TrainingClass;

class Method extends Model
{
    use UuidForKey, SoftDeletes;

    public $incrementing  = false;
    protected $dates      = ['deleted_at'];
    protected $table      = 'module_training_reference_learning_material.methods';
    protected $connection = 'module_training_reference_learning_material';
    protected $guarded    = ['id', 'created_at', 'updated_at', 'deleted_at'];
    protected $casts      = [
        'id'          => 'string',
        'code'        => 'string',
        'name'        => 'string',
        'description' => 'string',
        'created_by'  => 'string',
        'modified_by' => 'string',
        'created_at'  => 'datetime',
        'updated_at'  => 'datetime',
        'deleted_at'  => 'datetime',
    ];

    public function trainingClasses(): BelongsToMany
    {
        return $this->belongsToMany(TrainingClass::class, 'module_training_transaction.training_class_learning_material_method', 'method_id', 'training_class_id');
    }
}
