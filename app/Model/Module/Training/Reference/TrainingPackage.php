<?php
declare (strict_types = 1);
namespace Whiteboks\Model\Module\Training\Reference;

use Illuminate\Database\Eloquent\Model;
use Whiteboks\Lib\ModelTrait\UuidForKey;
use Illuminate\Database\Eloquent\SoftDeletes;

use Illuminate\Database\Eloquent\Relations\HasMany;
// use Illuminate\Database\Eloquent\Relations\BelongsTo;

use Whiteboks\Model\Module\Training\Reference\LearningMaterial\Category;

class TrainingPackage extends Model
{
    use UuidForKey, SoftDeletes;

    public $incrementing  = false;
    protected $dates      = ['deleted_at'];
    protected $table      = 'module_training_reference.training_packages';
    protected $connection = 'module_training_reference';
    protected $guarded    = ['id', 'created_at', 'updated_at', 'deleted_at'];
    protected $casts      = [
        'id'          => 'string',
        'code'        => 'string',
        'name'        => 'string',
        'description' => 'string',
        'created_by'  => 'string',
        'modified_by' => 'string',
        'created_at'  => 'datetime',
        'updated_at'  => 'datetime',
        'deleted_at'  => 'datetime',
    ];

    public function categories(): HasMany
    {
        return $this->hasMany(Category::class, 'training_package_id');
    }
}
