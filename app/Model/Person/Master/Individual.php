<?php
declare (strict_types = 1);
namespace Whiteboks\Model\Person\Master;

// library
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Model;
use Whiteboks\Lib\ModelTrait\UuidForKey;
use Illuminate\Database\Eloquent\SoftDeletes;
// relationship
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
// related model
use Whiteboks\Model\Auth\User;
use Whiteboks\Model\Auth\Role;
use Whiteboks\Model\Location\Country;
use Whiteboks\Model\Person\Master\Biodata;
use Whiteboks\Model\Person\Master\Relative;
use Whiteboks\Model\Common\Master\Residence;
use Whiteboks\Model\Person\Reference\Career\Income;
use Whiteboks\Model\Person\Reference\Biodata\Gender;
use Whiteboks\Model\Common\Master\StructuralPosition;
use Whiteboks\Model\Person\Reference\Biodata\Religion;
use Whiteboks\Model\Person\Reference\Career\Occupation;
use Whiteboks\Model\Person\Reference\Career\Profession;
use Whiteboks\Model\Common\Reference\Education\Education;
use Whiteboks\Model\Person\Reference\Biodata\MaritalStatus;
use Whiteboks\Model\Module\Training\Transaction\TrainingClass;
use Whiteboks\Model\Person\Reference\Biodata\IdentificationType;

class Individual extends Model
{
    use UuidForKey, SoftDeletes;
    public $incrementing  = false;
    protected $dates      = ['deleted_at'];
    protected $table      = 'person_master.individuals';
    protected $connection = 'person_master';
    protected $guarded    = ['id', 'created_at', 'updated_at', 'deleted_at'];
    protected $casts      = [
        'id'                     => 'string',
        'code'                   => 'string',
        'name'                   => 'string',
        'birth_place'            => 'string',
        'birth_place_id'         => 'string',
        'gender_id'              => 'string',
        'religion_id'            => 'string',
        'occupation_id'          => 'string',
        'education_id'           => 'string',
        'income_id'              => 'string',
        'identification_type_id' => 'string',
        'marital_status_id'      => 'string',
        'profession_id'          => 'string',
        'deceased'               => 'boolean',
        'country_id'             => 'string',
        'id_lecturer'            => 'string',
        'created_by'             => 'string',
        'modified_by'            => 'string',
        'created_at'             => 'datetime',
        'updated_at'             => 'datetime',
        'deleted_at'             => 'datetime',
    ];

    public function setBirthDateAttribute($value): void
    {
        if (!empty($value)) {
            $this->attributes['birth_date'] = Carbon::createFromFormat('d-m-Y', $value)->format('Y-m-d');
        }
    }

    public function getBirthDateAttribute($value): ?string
    {
        if (!empty($value)) {
            $returned = Carbon::createFromFormat('Y-m-d', $value)->format('d-m-Y');
            return $returned;
        }
    }

    public function country(): BelongsTo
    {
        return $this->belongsTo(Country::class, 'country_id');
    }

    public function education(): BelongsTo
    {
        return $this->belongsTo(Education::class, 'education_id');
    }

    public function income(): BelongsTo
    {
        return $this->belongsTo(Income::class, 'income_id');
    }

    public function identificationType(): BelongsTo
    {
        return $this->belongsTo(IdentificationType::class, 'identification_type_id');
    }

    public function gender(): BelongsTo
    {
        return $this->belongsTo(Gender::class, 'gender_id');
    }

    public function maritalStatus(): BelongsTo
    {
        return $this->belongsTo(MaritalStatus::class, 'marital_status_id');
    }

    public function religion(): BelongsTo
    {
        return $this->belongsTo(Religion::class, 'religion_id');
    }

    public function profession(): BelongsTo
    {
        return $this->belongsTo(Profession::class, 'profession_id');
    }

    public function occupation(): BelongsTo
    {
        return $this->belongsTo(Occupation::class, 'occupation_id');
    }

    public function user(): HasOne
    {
        return $this->hasOne(User::class, 'individual_id');
    }

    public function residencies()
    {
        return $this->morphMany(Residence::class, 'residenceable');
    }

    public function members(): BelongsToMany
    {
        return $this->belongsToMany(TrainingClass::class, 'module_training_transaction.training_class_member', 'individual_id', 'training_class_id')->withTimestamps();
    }

    public function facilitators(): BelongsToMany
    {
        return $this->belongsToMany(TrainingClass::class, 'module_training_transaction.training_class_facilitator', 'individual_id', 'training_class_id')->withTimestamps();
    }
}
