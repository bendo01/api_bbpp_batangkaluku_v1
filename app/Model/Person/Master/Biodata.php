<?php
declare (strict_types = 1);
namespace Whiteboks\Model\Person\Master;

use Illuminate\Database\Eloquent\Model;
use Whiteboks\Lib\ModelTrait\UuidForKey;
use Illuminate\Database\Eloquent\SoftDeletes;

use Illuminate\Database\Eloquent\Relations\BelongsTo;

use Whiteboks\Model\Person\Reference\BioData\HairType;
use Whiteboks\Model\Person\Reference\BioData\EyeColor;
use Whiteboks\Model\Person\Reference\BioData\HairColor;
use Whiteboks\Model\Person\Reference\BioData\BloodType;

class Biodata extends Model
{
    use UuidForKey, SoftDeletes;
    public $incrementing  = false;
    protected $dates      = ['deleted_at'];
    protected $table      = 'person_master.biodatas';
    protected $connection = 'person_master';
    protected $guarded    = ['id', 'created_at', 'updated_at', 'deleted_at'];
    protected $casts      = [
        'id'                    => 'string',
        'height'                => 'double',
        'weight'                => 'double',
        'positive_blood_rhesus' => 'boolean',
        'blood_type_id'         => 'string',
        'hair_type_id'          => 'string',
        'hair_color_id'         => 'string',
        'eye_color_type_id'     => 'string',
        'individual_id'         => 'string',
        'created_by'            => 'string',
        'modified_by'           => 'string',
        'created_at'            => 'datetime',
        'updated_at'            => 'datetime',
        'deleted_at'            => 'datetime',
    ];

    public function bloodType(): BelongsTo
    {
        return $this->belongsTo(BloodType::class, 'blood_type_id');
    }
    public function hairType(): BelongsTo
    {
        return $this->belongsTo(HairType::class, 'hair_type_id');
    }
    public function hairColor(): BelongsTo
    {
        return $this->belongsTo(HairColor::class, 'hair_color_id');
    }
    public function eyeColor(): BelongsTo
    {
        return $this->belongsTo(EyeColor::class, 'eye_color_id');
    }
    public function individual(): BelongsTo
    {
        return $this->belongsTo(Individual::class, 'individual_id');
    }
}
