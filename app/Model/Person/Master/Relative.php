<?php
declare (strict_types = 1);
namespace Whiteboks\Model\Person\Master;

use Illuminate\Database\Eloquent\Model;
use Whiteboks\Lib\ModelTrait\UuidForKey;
use Illuminate\Database\Eloquent\SoftDeletes;

use Illuminate\Database\Eloquent\Relations\BelongsTo;

use Whiteboks\Model\Person\Reference\BioData\RelativeType;

class Relative extends Model
{
    use UuidForKey, SoftDeletes;
    public $incrementing  = false;
    protected $dates      = ['deleted_at'];
    protected $table      = 'person_master.relatives';
    protected $connection = 'person_master';
    protected $guarded    = ['id', 'created_at', 'updated_at', 'deleted_at'];
    protected $casts      = [
        'id'               => 'string',
        'individual_id'    => 'string',
        'relative_id'      => 'string',
        'relative_type_id' => 'string',
        'created_by'       => 'string',
        'modified_by'      => 'string',
        'created_at'       => 'datetime',
        'updated_at'       => 'datetime',
        'deleted_at'       => 'datetime',
    ];

    public function getBackwardRelativeType(string $relativeTypeId): string
    {
        $returned = '';
        $relativeType = RelativeType::findOrFail($relativeTypeId);
        $backwardRelativeTypeCode = 0;
        $backwardRelativeType = null;
        if ($relativeType->code >= 1 || $relativeType->code <= 6) {
            $backwardRelativeTypeCode = 14;
        } elseif ($relativeType->code == 7) {
            $backwardRelativeTypeCode = 7;
        } elseif ($relativeType->code == 8) {
            $backwardRelativeTypeCode = 8;
        } elseif ($relativeType->code == 9) {
            $backwardRelativeTypeCode = 9;
        } elseif ($relativeType->code >= 10 || $relativeType->code <= 11) {
            $backwardRelativeTypeCode = 14;
        } elseif ($relativeType->code == 12) {
            $backwardRelativeTypeCode = 13;
        } elseif ($relativeType->code == 13) {
            $backwardRelativeTypeCode = 12;
        }
        $backwardRelativeType =  RelativeType::where('code', $backwardRelativeTypeCode)->firstOrFail();
        $returned = $backwardRelativeType->id;
        return $returned;
    }

    public function individual(): BelongsTo
    {
        return $this->belongsTo(Individual::class, 'individual_id');
    }
    public function relative(): BelongsTo
    {
        return $this->belongsTo(Individual::class, 'relative_id');
    }
    public function relativeType(): BelongsTo
    {
        return $this->belongsTo(RelativeType::class, 'relative_type_id');
    }
}
