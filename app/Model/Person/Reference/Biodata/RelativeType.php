<?php
declare (strict_types = 1);
namespace Whiteboks\Model\Person\Reference\Biodata;

use Illuminate\Database\Eloquent\Model;
use Whiteboks\Lib\ModelTrait\UuidForKey;
use Illuminate\Database\Eloquent\SoftDeletes;

use Whiteboks\Model\Person\Master\Relative;
use Illuminate\Database\Eloquent\Relations\HasMany;

use Whiteboks\Model\Person\Master\Relative;

class RelativeType extends Model
{
    use UuidForKey, SoftDeletes;

    public $incrementing  = false;
    protected $dates      = ['deleted_at'];
    protected $table      = 'person_reference_biodata.relative_types';
    protected $connection = 'person_reference_biodata';
    protected $guarded    = ['id', 'created_at', 'updated_at', 'deleted_at'];
    protected $casts      = [
        'id'          => 'string',
        'code'        => 'string',
        'name'        => 'string',
        'created_by'  => 'string',
        'modified_by' => 'string',
        'created_at'  => 'datetime',
        'updated_at'  => 'datetime',
        'deleted_at'  => 'datetime',
    ];

    public function relatives(): HasMany
    {
        return $this->hasMany(Relative::class, 'relative_type_id');
    }
}
