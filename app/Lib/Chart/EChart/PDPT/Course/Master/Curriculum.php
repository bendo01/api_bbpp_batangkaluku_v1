<?php

declare (strict_types = 1);

namespace Whiteboks\Lib\Chart\EChart\PDPT\Course\Master;

use Whiteboks\Lib\Chart\EChart\Type\Radar;
use Whiteboks\Lib\Chart\EChart\Type\MultiLine;
use Whiteboks\Model\PDPT\Course\Reference\Type;
use Whiteboks\Model\PDPT\Course\Reference\Group;
use Whiteboks\Model\PDPT\Course\Master\CurriculumDetail;
use Whiteboks\Model\PDPT\Course\Master\Curriculum as Model;
use Whiteboks\Model\PDPT\CourseDepartment\Master\CourseDepartment;

class Curriculum
{
    /*
    * To Do Max Performance Using SQL
    */
    public function getChartGroupHighestValue(string $id): int
    {
        $groups = Group::all();
        $model = Model::findOrFail($id);
        $curriculumDetails = new CurriculumDetail();
        $highestValue = 0;
        $value = 0;
        foreach ($groups as $group) {
            $value = $curriculumDetails->join('pdpt_course_master.courses', 'pdpt_course_master.courses.id', '=', 'pdpt_course_master.curriculum_details.course_id')
                              ->where('pdpt_course_master.courses.group_id', $group->id)
                              ->where('pdpt_course_master.curriculum_details.curriculum_id', $id)
                              ->count();
            if($value > $highestValue) {
                $highestValue = $value;
            }
        }
        return $highestValue;
    }

    public function getChartTypeHighestValue(string $id): int
    {
        $types = Type::all();
        $model = Model::findOrFail($id);
        $curriculumDetails = new CurriculumDetail();
        $highestValue = 0;
        $value = 0;
        foreach ($types as $type) {
            $value = $curriculumDetails->join('pdpt_course_master.courses', 'pdpt_course_master.courses.id', '=', 'pdpt_course_master.curriculum_details.course_id')
                              ->where('pdpt_course_master.courses.type_id', $type->id)
                              ->where('pdpt_course_master.curriculum_details.curriculum_id', $id)
                              ->count();
            if($value > $highestValue) {
                $highestValue = $value;
            }
        }
        return $highestValue;
    }

    public function getCourseGroupChart(string $id): Radar
    {
        $model = Model::findOrFail($id);
        $chart = new Radar();
        $chart->createSeriesItem('Kurikulum');
        $chart->addLegendData($model->name);
        $groups = Group::all();
        $curriculumDetails = new CurriculumDetail();
        $values = [];
        foreach ($groups as $group) {
            $value = $curriculumDetails->join('pdpt_course_master.courses', 'pdpt_course_master.courses.id', '=', 'pdpt_course_master.curriculum_details.course_id')->where('pdpt_course_master.courses.group_id', $group->id)->where('pdpt_course_master.curriculum_details.curriculum_id', $id)->count();
            array_push($values, $value);
            $chart->addPolarIndicator($group->aberration, $this->getChartGroupHighestValue($id));
        }
        $chart->addSeriesData($model->name, $values);
        return $chart;
    }

    public function getCourseTypeChart(string $id): Radar
    {
        $model = Model::findOrFail($id);
        $chart = new Radar();
        $chart->createSeriesItem('Kurikulum');
        $chart->addLegendData($model->name);
        $types = Type::all();
        $curriculumDetails = new CurriculumDetail();
        $values = [];
        foreach ($types as $type) {
            $value = $curriculumDetails->join('pdpt_course_master.courses', 'pdpt_course_master.courses.id', '=', 'pdpt_course_master.curriculum_details.course_id')->where('pdpt_course_master.courses.type_id', $type->id)->where('pdpt_course_master.curriculum_details.curriculum_id', $id)->count();
            array_push($values, $value);
            $chart->addPolarIndicator($type->code, $this->getChartTypeHighestValue($id));
        }
        $chart->addSeriesData($model->name, $values);
        return $chart;
    }

    public function getCourseGroupData(string $id): array
    {
        $groups = Group::all();
        $model = Model::findOrFail($id);
        $dataList = [];
        $curriculumDetails = new CurriculumDetail();
        foreach ($groups as $group) {
            $data = new \stdClass;
            $data->name = $group->name;
            $data->aberration = $group->aberration;
            $data->code = $group->code;
            $data->quantity = $curriculumDetails->join('pdpt_course_master.courses', 'pdpt_course_master.courses.id', '=', 'pdpt_course_master.curriculum_details.course_id')
                              ->where('pdpt_course_master.courses.group_id', $group->id)
                              ->where('pdpt_course_master.curriculum_details.curriculum_id', $id)
                              ->count();
            array_push($dataList, $data);
        }
        return $dataList;
    }

    public function getCourseTypeData(string $id): array
    {
        $types = Type::all();
        $model = Model::findOrFail($id);
        $dataList = [];
        $curriculumDetails = new CurriculumDetail();
        foreach ($types as $type) {
            $data = new \stdClass;
            $data->code = $type->code;
            $data->name = $type->name;
            $data->quantity = $curriculumDetails->join('pdpt_course_master.courses', 'pdpt_course_master.courses.id', '=', 'pdpt_course_master.curriculum_details.course_id')
                              ->where('pdpt_course_master.courses.type_id', $type->id)
                              ->where('pdpt_course_master.curriculum_details.curriculum_id', $id)
                              ->count();
            array_push($dataList, $data);
        }
        return $dataList;
    }

}
