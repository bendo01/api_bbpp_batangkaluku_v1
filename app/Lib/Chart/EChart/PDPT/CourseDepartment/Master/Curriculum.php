<?php

declare (strict_types = 1);

namespace Whiteboks\Lib\Chart\EChart\PDPT\CourseDepartment\Master;

use Whiteboks\Lib\Chart\EChart\Type\Radar;
use Whiteboks\Lib\Chart\EChart\Type\MultiLine;
use Whiteboks\Model\PDPT\Course\Reference\Type;
use Whiteboks\Model\PDPT\Course\Reference\Group;
use Whiteboks\Model\PDPT\Course\Master\CurriculumDetail;
use Whiteboks\Model\PDPT\Course\Master\Curriculum as Model;
use Whiteboks\Model\PDPT\General\Transaction\ReportingYear;
use Whiteboks\Model\PDPT\CourseDepartment\Master\CourseDepartment;

class Curriculum
{
    public function getCourseGroupChart(string $id): Radar
    {
        $courseDepartment = CourseDepartment::findOrFail($id);
        $chart = new Radar();
        $chart->createSeriesItem('Kurikulum '.$courseDepartment->name);
        $curriculumDatas = Model::where('course_department_id', $courseDepartment->id)->get();
        $groups = Group::all();
        foreach ($curriculumDatas as $curriculumData) {
            $chart->addLegendData($curriculumData->name);
            $curriculumDetails = new CurriculumDetail();
            $values = [];
            foreach ($groups as $group) {
                $value = $curriculumDetails->join('pdpt_course_master.courses', 'pdpt_course_master.courses.id', '=', 'pdpt_course_master.curriculum_details.course_id')->where('pdpt_course_master.courses.group_id', $group->id)->where('pdpt_course_master.curriculum_details.curriculum_id', $curriculumData->id)->count();
                array_push($values, $value);
            }
            $chart->addSeriesData($curriculumData->name, $values);
        }
        foreach ($groups as $group) {
            $chart->addPolarIndicator($group->aberration, $chart->getHighestValue());
        }
        return $chart;
    }

    public function getCourseTypeChart(string $id): Radar
    {
        $courseDepartment = CourseDepartment::findOrFail($id);
        $chart = new Radar();
        $chart->createSeriesItem('Kurikulum '.$courseDepartment->name);
        $curriculumDatas = Model::where('course_department_id', $courseDepartment->id)->get();
        $types = Type::all();
        foreach ($curriculumDatas as $curriculumData) {
            $chart->addLegendData($curriculumData->name);
            $curriculumDetails = new CurriculumDetail();
            $values = [];
            foreach ($types as $type) {
                $value = $curriculumDetails->join('pdpt_course_master.courses', 'pdpt_course_master.courses.id', '=', 'pdpt_course_master.curriculum_details.course_id')->where('pdpt_course_master.courses.type_id', $type->id)->where('pdpt_course_master.curriculum_details.curriculum_id', $curriculumData->id)->count();
                array_push($values, $value);
            }
            $chart->addSeriesData($curriculumData->name, $values);
        }
        foreach ($types as $type) {
            $chart->addPolarIndicator($type->code, $chart->getHighestValue());
        }
        return $chart;
    }
}
