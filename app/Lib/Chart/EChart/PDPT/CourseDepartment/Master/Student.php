<?php

declare (strict_types = 1);

namespace Whiteboks\Lib\Chart\EChart\PDPT\CourseDepartment\Master;

use DB;
use Illuminate\Support\Collection;
use Whiteboks\Lib\Chart\EChart\Type\Pie;
use Whiteboks\Lib\Chart\EChart\Type\Radar;
use Whiteboks\Lib\Chart\EChart\Type\PieRose;
use Whiteboks\Lib\Chart\EChart\Type\MultiLine;
use Whiteboks\Model\PDPT\Course\Reference\Type;
use Whiteboks\Model\PDPT\Course\Reference\Group;
use Whiteboks\Model\PDPT\Course\Master\Curriculum;
use Whiteboks\Model\Person\Reference\BioData\Gender;
use Whiteboks\Model\Person\Reference\BioData\Religion;
use Whiteboks\Model\Person\Reference\Career\Profession;
use Whiteboks\Model\PDPT\Course\Master\CurriculumDetail;
use Whiteboks\Model\PDPT\General\Transaction\ReportingYear;
use Whiteboks\Model\PDPT\Student\Master\Student as StudentModel;
use Whiteboks\Model\PDPT\Student\Reference\Study\Status as StudentStatus;
use Whiteboks\Model\PDPT\CourseDepartment\Master\CourseDepartment as Model;
use Whiteboks\Model\PDPT\Student\Transaction\Study\Activity as StudentActivity;
use Whiteboks\Model\PDPT\Student\Reference\Study\ResignStatus as StudentResignStatus;
use Whiteboks\Model\PDPT\Student\Reference\Study\RegistrationType as StudentRegistrationType;
use Whiteboks\Model\PDPT\CourseDepartment\Transaction\Activity\Activity as CourseDepartmentActivity;

class Student
{
    public function studentRegistrationChart(string $id): MultiLine
    {
        $model = Model::findOrFail($id);
        $chart = new MultiLine();
        $studentReportingYears = StudentModel::where('course_department_id', $model->id)->distinct('reporting_year_id')->get(['reporting_year_id'])->pluck('reporting_year_id')->toArray();
        $studentRegistrationTypeDatas = array_pluck(StudentRegistrationType::where('group', true)->get(['name', 'id']), 'name', 'id');
        $reportingYearDatas = array_pluck(ReportingYear::whereIn('id', $studentReportingYears)->orderBy('thread')->get(['name','id']), 'name', 'id');
        $studentRegistrationTypes = array_flatten($studentRegistrationTypeDatas);
        $reportingYears = array_flatten($reportingYearDatas);
        $chart->createLegendData($studentRegistrationTypes);
        $chart->createXAxis($reportingYears);
        foreach ($studentRegistrationTypeDatas as $keyStudentRegistrationTypeData=>$valueStudentRegistrationTypeData) {
            $name = $valueStudentRegistrationTypeData;
            $data = [];
            foreach ($reportingYearDatas as $keyReportingYearData=>$valueReportingYearData) {
                $count = StudentModel::where('course_department_id', $model->id)->where('reporting_year_id', $keyReportingYearData)->where('registration_type_id', $keyStudentRegistrationTypeData)->count();
                array_push($data, $count);
            }
            $chart->createSeries($name, $data);
        }
        return $chart;
    }

    public function studentStatusChart(string $id): Multiline
    {
        $model = Model::findOrFail($id);
        $chart = new MultiLine();
        $courseDepartmentActivityReportingYears = CourseDepartmentActivity::where('course_department_id', $model->id)->distinct('reporting_year_id')->get(['reporting_year_id'])->pluck('reporting_year_id')->toArray();
        $studentStatusDatas = array_pluck(StudentStatus::get(['name', 'id']), 'name', 'id');
        $studentStatuses = array_flatten($studentStatusDatas);
        $reportingYearDatas = array_pluck(ReportingYear::whereIn('id', $courseDepartmentActivityReportingYears)->orderBy('thread')->get(['name','id']), 'name', 'id');
        $reportingYears = array_flatten($reportingYearDatas);
        $chart->createLegendData($studentStatuses);
        $chart->createXAxis($reportingYears);
        foreach ($studentStatusDatas as $keyStudentStatusData => $valueStudentStatusData) {
            $name = $valueStudentStatusData;
            $data = [];
            foreach ($reportingYearDatas as $keyReportingYearData=>$valueReportingYearData) {
                $courseDepartmentActivity = CourseDepartmentActivity::where('reporting_year_id', $keyReportingYearData)->where('course_department_id', $model->id)->firstOrFail();
                $count = StudentActivity::where('course_department_activity_id', $courseDepartmentActivity->id)->where('status_id', $keyStudentStatusData)->count();
                array_push($data, $count);
            }
            $chart->createSeries($name, $data);
        }
        return $chart;
    }

    public function studentResignStatusChart(string $id): Multiline
    {

    }

    public function studentScholarshipChart(string $id): Multiline
    {

    }

    public function studentDemography(string $id): Collection
    {
        $model = Model::findOrFail($id);
        $results = DB::table('general_master.addresses')
        ->select(DB::raw('location.regencies.name AS map_name, location.regencies.code AS map_code, count(location.regencies.name) as total'))
        ->join('location.regencies', function ($join) {
            $join->on('location.regencies.id', '=', 'general_master.addresses.regency_id')->whereNotNull('location.regencies.code');
        })
        ->join('building_master.buildings', function ($join) {
            $join->on('building_master.buildings.id', '=', 'general_master.addresses.building_id')
                 ->where('building_master.buildings.buildingable_type', 'Whiteboks\Model\Person\Master\Individual');
        })
        ->join('person_master.individuals', function ($join) {
            $join->on('person_master.individuals.id', '=', 'building_master.buildings.buildingable_id');
        })
        ->join('pdpt_student_master.students', function ($join) use($model) {
            $join->on('person_master.individuals.id', '=', 'pdpt_student_master.students.individual_id')
                 ->where('pdpt_student_master.students.course_department_id', $model->id);
        })
        ->groupBy('location.regencies.name', 'location.regencies.code')
        ->orderBy('total')
        ->get();
        return $results;
    }

    public function studentGenderChart(string $id): Pie
    {
        $model = Model::findOrFail($id);
        $chart = new Pie();
        $chart->createSeries('Jenis Kelamin');
        $legends = [];
        $genders = Gender::all();
        foreach ($genders as $gender) {
            $student = new StudentModel();
            $count = $student->join('person_master.individuals', function ($join) use($gender) {
                        $join->on('person_master.individuals.id', '=', 'pdpt_student_master.students.individual_id')->where('person_master.individuals.gender_id', $gender->id);
                     })
                     ->where('course_department_id', $model->id)
                     ->count();
             if(!empty($count)) {
                 array_push($legends, $gender->name);
                 $chart->addData($gender->name, $count);
             }
        }
        if(!empty($legends)) {
            $chart->addLegends($legends);
        }
        return $chart;
    }

    public function studentProfessionChart(string $id): PieRose
    {
        $model = Model::findOrFail($id);
        $chart = new PieRose();
        $chart->createSeries('Profesi');
        $legends = [];
        $professions = Profession::all();
        foreach ($professions as $profession) {
            $student = new StudentModel();
            $count = $student->join('person_master.individuals', function ($join) use($profession) {
                        $join->on('person_master.individuals.id', '=', 'pdpt_student_master.students.individual_id')->where('person_master.individuals.profession_id', $profession->id);
                     })
                     ->where('course_department_id', $model->id)
                     ->count();
            if(!empty($count)) {
                array_push($legends, $profession->name);
                $chart->addData($profession->name, $count);
            }
        }
        if(!empty($legends)) {
            $chart->addLegends($legends);
        }
        return $chart;
    }

    public function studentReligionChart(string $id): PieRose
    {
        $model = Model::findOrFail($id);
        $chart = new PieRose();
        $chart->createSeries('Agama');
        $legends = [];
        $religions = Religion::all();
        foreach ($religions as $religion) {
            $student = new StudentModel();
            $count = $student->join('person_master.individuals', function ($join) use ($religion) {
                        $join->on('person_master.individuals.id', '=', 'pdpt_student_master.students.individual_id')->where('person_master.individuals.religion_id', $religion->id);
                     })
                     ->where('course_department_id', $model->id)
                     ->count();
            if(!empty($count)) {
                array_push($legends, $religion->name);
                $chart->addData($religion->name, $count);
            }
        }
        if(!empty($legends)) {
            $chart->addLegends($legends);
        }
        return $chart;
    }
}
