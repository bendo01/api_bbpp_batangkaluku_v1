<?php

declare (strict_types = 1);

namespace Whiteboks\Lib\Chart\EChart\PDPT\CourseDepartment\Transaction\Teach;

use DB;
use Whiteboks\Lib\Chart\EChart\Type\MultiLine;
use Whiteboks\Model\PDPT\General\Transaction\ReportingYear;
use Whiteboks\Model\PDPT\CourseDepartment\Transaction\Teach\TeachingActivity;
use Whiteboks\Model\PDPT\CourseDepartment\Transaction\Teach\ClassCode as Model;
use Whiteboks\Model\PDPT\CourseDepartment\Transaction\Activity\Activity as CourseDepartmentActivity;

class ClassCode
{
    public function teachingActivity(string $id): MultiLine
    {
        $model = Model::findOrFail($id);
        $chart = new MultiLine();
        $teachingActivityCourseDepartmentActivities = TeachingActivity::where('class_code_id', $model->id)->distinct('course_department_activity_id')->get(['course_department_activity_id'])->pluck('course_department_activity_id')->toArray();
        $courseDepartmentActivityReportingYears = CourseDepartmentActivity::whereIn('id', $teachingActivityCourseDepartmentActivities)->get(['reporting_year_id'])->pluck('reporting_year_id')->toArray();
        $reportingYearNames = ReportingYear::whereIn('id', $courseDepartmentActivityReportingYears)->orderBy('thread')->get(['name'])->pluck('name')->toArray();
        $reportingYears = ReportingYear::whereIn('id', $courseDepartmentActivityReportingYears)->orderBy('thread')->get();
        $chart->createXAxis($reportingYearNames);
        $chart->createLegendData(['Pengajaran']);
        $data = [];
        foreach ($reportingYears as $reportingYear) {
            $courseDepartmentActivity = CourseDepartmentActivity::where('course_department_id', $model->courseDepartmentActivity->courseDepartment->id)->where('reporting_year_id', $reportingYear->id)->firstOrFail();
            $count = TeachingActivity::where('course_department_activity_id', $courseDepartmentActivity->id)->where('class_code_id', $model->id)->count();
            array_push($data, $count);
        }
        $chart->createSeries('Pengajaran', $data);
        return $chart;
    }
}
