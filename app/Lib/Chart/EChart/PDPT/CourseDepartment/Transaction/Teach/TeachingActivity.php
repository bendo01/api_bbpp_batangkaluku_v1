<?php

declare (strict_types = 1);

namespace Whiteboks\Lib\Chart\EChart\PDPT\CourseDepartment\Transaction\Teach;

use DB;
use Whiteboks\Lib\Chart\EChart\Type\PieRose;
use Whiteboks\Model\PDPT\General\Transaction\ReportingYear;
use Whiteboks\Model\PDPT\CourseDepartment\Transaction\Teach\Grade;
use Whiteboks\Model\PDPT\Student\Transaction\Study\DetailActivity;
use Whiteboks\Model\PDPT\CourseDepartment\Transaction\Teach\TeachingActivity as Model;

class TeachingActivity
{
    public function grade(string $id): PieRose
    {
        $model = Model::findOrFail($id);
        $chart = new PieRose();
        $chart->createSeries('Bobot Nilai');
        $listGrades = DetailActivity::where('teaching_activity_id', $model->id)->distinct('grade_id')->get(['grade_id'])->pluck('grade_id')->toArray();
        $grades = Grade::whereIn('id', $listGrades)->orderBy('name')->get();
        $legends = [];

        foreach ($grades as $grade) {
            $count = DetailActivity::where('teaching_activity_id', $model->id)->where('grade_id', $grade->id)->count();
            if(!empty($count)) {
                if(empty($grade->name)) {                    
                    array_push($legends, 'Belum Ada Nilai');
                    $chart->addData('Belum Ada Nilai', $count);
                }
                else {
                    array_push($legends, $grade->name);
                    $chart->addData($grade->name, $count);
                }

            }
        }
        if(!empty($legends)) {
            $chart->addLegends($legends);
        }
        return $chart;
    }
}
