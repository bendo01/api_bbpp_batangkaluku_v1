<?php

declare (strict_types = 1);

namespace Whiteboks\Lib\Chart\EChart\PDPT\CourseDepartment\Transaction\Activity;

use DB;
use Illuminate\Support\Collection;
use Whiteboks\Lib\Chart\EChart\Type\Pie;
use Whiteboks\Lib\Chart\EChart\Type\Radar;
use Whiteboks\Lib\Chart\EChart\Type\PieRose;
use Whiteboks\Model\PDPT\Course\Reference\Type;
use Whiteboks\Model\PDPT\Course\Reference\Group;
use Whiteboks\Lib\Chart\EChart\Type\VerticalBar;
use Whiteboks\Model\Person\Reference\BioData\Gender;
use Whiteboks\Model\Person\Reference\BioData\Religion;
use Whiteboks\Model\Person\Reference\Career\Profession;
use Whiteboks\Model\PDPT\Course\Master\CurriculumDetail;
use Whiteboks\Model\PDPT\General\Transaction\ReportingYear;
use Whiteboks\Model\PDPT\Student\Master\Student as StudentModel;
use Whiteboks\Model\PDPT\CourseDepartment\Master\CourseDepartment;
use Whiteboks\Model\PDPT\Student\Reference\Study\Status as StudentStatus;
use Whiteboks\Model\PDPT\Student\Transaction\Study\Activity as StudentActivity;
use Whiteboks\Model\PDPT\CourseDepartment\Transaction\Activity\Activity as Model;
use Whiteboks\Model\PDPT\Student\Reference\Study\ResignStatus as StudentResignStatus;
use Whiteboks\Model\PDPT\Student\Reference\Study\RegistrationType as StudentRegistrationType;

class Student
{
    public function studentRegisteredDemography(string $id): VerticalBar
    {
        $model = Model::findOrFail($id);
        $chart = new VerticalBar();
        $results = DB::table('general_master.addresses')
        ->select(DB::raw('location.regencies.name AS regency_name, location.regencies.code AS regency_code, count(location.regencies.name) as total'))
        ->join('location.regencies', function ($join) {
            $join->on('location.regencies.id', '=', 'general_master.addresses.regency_id')->whereNotNull('location.regencies.code');
        })
        ->join('building_master.buildings', function ($join) {
            $join->on('building_master.buildings.id', '=', 'general_master.addresses.building_id')
                 ->where('building_master.buildings.buildingable_type', 'Whiteboks\Model\Person\Master\Individual');
        })
        ->join('person_master.individuals', function ($join) {
            $join->on('person_master.individuals.id', '=', 'building_master.buildings.buildingable_id');
        })
        ->join('pdpt_student_master.students', function ($join) use($model) {
            $join->on('pdpt_student_master.students.individual_id', '=', 'person_master.individuals.id')
            ->where('pdpt_student_master.students.course_department_id', $model->course_department_id)
            ->where('pdpt_student_master.students.reporting_year_id', $model->reporting_year_id);
        })
        ->groupBy('location.regencies.name', 'location.regencies.code')
        ->orderBy('total', 'DESC')
        ->get();
        $chart->createLegend($model->name);
        $yAxisData = [];
        $seriesData = [];
        foreach ($results as $result) {
            array_push($yAxisData, $result->regency_name);
            array_push($seriesData, $result->total);
        }
        $chart->createYAxis($yAxisData);
        $chart->addSeries($model->name, $seriesData);
        return $chart;
    }

    public function studentRegisteredGender(string $id): Pie
    {
        $model = Model::findOrFail($id);
        $chart = new Pie();
        $chart->createSeries('Jenis Kelamin');
        $legends = [];
        $genders = Gender::all();
        foreach ($genders as $gender) {
            $student = new StudentModel();
            $count = $student->join('person_master.individuals', function ($join) use($gender) {
                        $join->on('person_master.individuals.id', '=', 'pdpt_student_master.students.individual_id')
                        ->where('person_master.individuals.gender_id', $gender->id);
                     })
                     ->where('course_department_id', $model->course_department_id)
                     ->where('reporting_year_id', $model->reporting_year_id)
                     ->count();
             if(!empty($count)) {
                 array_push($legends, $gender->name);
                 $chart->addData($gender->name, $count);
             }
        }
        if(!empty($legends)) {
            $chart->addLegends($legends);
        }
        return $chart;
    }

    public function studentRegisteredProfession(string $id): PieRose
    {
        $model = Model::findOrFail($id);
        $chart = new PieRose();
        $chart->createSeries('Profesi');
        $legends = [];
        $professions = Profession::all();
        foreach ($professions as $profession) {
            $student = new StudentModel();
            $count = $student->join('person_master.individuals', function ($join) use($profession) {
                        $join->on('person_master.individuals.id', '=', 'pdpt_student_master.students.individual_id')->where('person_master.individuals.profession_id', $profession->id);
                     })
                     ->where('course_department_id', $model->course_department_id)
                     ->where('reporting_year_id', $model->reporting_year_id)
                     ->count();
            if(!empty($count)) {
                array_push($legends, $profession->name);
                $chart->addData($profession->name, $count);
            }
        }
        if(!empty($legends)) {
            $chart->addLegends($legends);
        }
        return $chart;
    }

    public function studentRegisteredReligion(string $id): PieRose
    {
        $model = Model::findOrFail($id);
        $chart = new PieRose();
        $chart->createSeries('Agama');
        $legends = [];
        $religions = Religion::all();
        foreach ($religions as $religion) {
            $student = new StudentModel();
            $count = $student->join('person_master.individuals', function ($join) use ($religion) {
                        $join->on('person_master.individuals.id', '=', 'pdpt_student_master.students.individual_id')->where('person_master.individuals.religion_id', $religion->id);
                     })
                     ->where('course_department_id', $model->course_department_id)
                     ->where('reporting_year_id', $model->reporting_year_id)
                     ->count();
            if(!empty($count)) {
                array_push($legends, $religion->name);
                $chart->addData($religion->name, $count);
            }
        }
        if(!empty($legends)) {
            $chart->addLegends($legends);
        }
        return $chart;
    }
}
