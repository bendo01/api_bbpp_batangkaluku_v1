<?php

declare (strict_types = 1);

namespace Whiteboks\Lib\Chart\EChart\Individual\Master;

use Whiteboks\Lib\Chart\EChart\Type\Radar;
use Whiteboks\Lib\Chart\EChart\Type\MultiLine;
use Whiteboks\Model\PDPT\Course\Reference\Type;
use Whiteboks\Model\PDPT\Course\Reference\Group;
use Whiteboks\Model\Person\Master\Individual as Model;
use Whiteboks\Model\PDPT\Student\Transaction\Study\DetailActivity;

class Individual
{
    public function getGradeIndex(string $id): MultiLine
    {
        $model = Model::findOrFail($id);
        $chart = new MultiLine();
        $chart->createLegendData(['IPS', 'IPK']);
        if($model->students->count()) {
            $nameTemporary = 'IPS';
            $dataTemporary = [];
            $nameCumulative = 'IPK';
            $dataCumulative = [];
            $datacreateXAxis = [];
            foreach ($model->getStudentsOrderByReportingYear() as $student) {
                if($student->activities->count()) {
                    $activities = $student->getActivitiesOrderByReportingYear();
                    if(!empty($activities)) {
                        foreach ($activities as $activity) {
                            array_push($datacreateXAxis, $activity->courseDepartmentActivity->reportingYear->feeder_name.'-'.$activity->student->courseDepartment->education->abbreviation);
                            array_push($dataTemporary, $activity->cumulative_index);
                            array_push($dataCumulative, $activity->grand_cumulative_index);
                        }
                    }
                }
            }
            $chart->createXAxis($datacreateXAxis);
            $chart->createSeries($nameTemporary, $dataTemporary);
            $chart->createSeries($nameCumulative, $dataCumulative);
        }
        return $chart;

    }

    public function getCredit(string $id): MultiLine
    {
        $model = Model::findOrFail($id);
        $chart = new MultiLine();
        $chart->createLegendData(['SEMESTER', 'KUMULATIF']);
        if($model->students->count()) {
            $nameTemporary = 'SEMESTER';
            $dataTemporary = [];
            $nameCumulative = 'KUMULATIF';
            $dataCumulative = [];
            $datacreateXAxis = [];
            foreach ($model->getStudentsOrderByReportingYear() as $student) {
                if($student->activities->count()) {
                    $activities = $student->getActivitiesOrderByReportingYear();
                    if(!empty($activities)) {
                        foreach ($activities as $activity) {
                            array_push($datacreateXAxis, $activity->courseDepartmentActivity->reportingYear->feeder_name.'-'.$activity->student->courseDepartment->education->abbreviation);
                            array_push($dataTemporary, $activity->total_credit);
                            array_push($dataCumulative, $activity->grand_total_credit);
                        }
                    }
                }
            }
            $chart->createXAxis($datacreateXAxis);
            $chart->createSeries($nameTemporary, $dataTemporary);
            $chart->createSeries($nameCumulative, $dataCumulative);
        }
        return $chart;

    }

    public function getRadarChartHighestValue(\stdClass $data): float
    {
        $highestValue = 0;
        foreach ($data->data as $listData) {
            foreach ($listData->value as $value) {
                if($value > $highestValue) {
                    $highestValue = $value;
                }
            }
        }
        return $highestValue;
    }

    //public function getCourseTypeChart(string $id) : \stdClass
    public function getCourseTypeChart(string $id): Radar
    {
        $model = Model::findOrFail($id);
        $chart = new Radar();
        $chart->createSeriesItem('Tipe Matakuliah');
        if($model->students->count()) {
            $types = Type::all();
            foreach ($model->students as $student) {
                $chart->addLegendData($student->courseDepartment->education->abbreviation);
                $seriesItemDataName = $student->courseDepartment->education->abbreviation;
                $seriesItemDataValues = [];
                $detailActivity = new DetailActivity();
                foreach ($types as $type) {
                    array_push($seriesItemDataValues, $detailActivity->calculateCurriculumTypeGrade($student->id, $type->id));
                }
                $chart->addSeriesData($seriesItemDataName, $seriesItemDataValues);
            }
            foreach ($types as $type) {
                $chart->addPolarIndicator($type->code, 4);
            }
        }
        return $chart;
    }

    public function getCourseGroupChart(string $id) : Radar
    {
        $model = Model::findOrFail($id);        
        $chart = new Radar();
        $chart->createSeriesItem('Kelompok Matakuliah');
        if($model->students->count()) {
            $groups = Group::all();
            foreach ($model->students as $student) {
                $chart->addLegendData($student->courseDepartment->education->abbreviation);
                $seriesItemDataName = $student->courseDepartment->education->abbreviation;
                $seriesItemDataValues = [];
                $detailActivity = new DetailActivity();
                foreach ($groups as $group) {
                    array_push($seriesItemDataValues, $detailActivity->calculateCurriculumGroupGrade($student->id, $group->id));
                }
                $chart->addSeriesData($seriesItemDataName, $seriesItemDataValues);
            }
            foreach ($groups as $group) {
                $chart->addPolarIndicator($group->aberration, 4);
            }
        }
        return $chart;
    }

    public function getCourseGroupData(string $id): array
    {
        $groups = Group::all();
        $model = Model::findOrFail($id);
        $dataList = [];
        foreach ($groups as $group) {
            $data = new \stdClass;
            $data->name = $group->name;
            $data->aberration = $group->aberration;
            array_push($dataList, $data);
        }
        return $dataList;
    }

    public function getCourseTypeData(string $id): array
    {
        $types = Type::all();
        $model = Model::findOrFail($id);
        $dataList = [];
        foreach ($types as $type) {
            $data = new \stdClass;
            $data->code = $type->code;
            $data->name = $type->name;
            array_push($dataList, $data);
        }
        return $dataList;
    }
}
