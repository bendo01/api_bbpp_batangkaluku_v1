<?php

declare (strict_types = 1);

namespace Whiteboks\Lib\Chart\EChart\ELearning;

use DB;
use Whiteboks\Model\ELearning\Exam;
use Whiteboks\Model\ELearning\Grade;
use Whiteboks\Model\ELearning\Answer;
use Whiteboks\Model\ELearning\Question;
use Whiteboks\Lib\Chart\EChart\Type\Pie;
use Whiteboks\Model\ELearning\GradeIndex;
use Whiteboks\Model\ELearning\Participant;
use Whiteboks\Model\ELearning\ExamPackage;
use Whiteboks\Lib\Chart\EChart\Type\Scatter;
use Whiteboks\Model\PDPT\Student\Master\Student;

class ExamPackageResult
{
    public function answersCategoryByCorrectAnswer(string $examPackageId, string $examId): Pie
    {
        $questionLists = array_flatten(Question::where('exam_id', $examId)->get(['id'])->toArray());
        $chart = new Pie();
        $chart->createSeries('Kumulatif Jawaban Ujian');
        $legends = [];
        array_push($legends, 'Benar');
        $chart->addData('Benar', Answer::whereIn('question_id', $questionLists)->where('exam_package_id', $examPackageId)->where('correct_answer', true)->count());
        array_push($legends, 'Salah');
        $chart->addData('Salah', Answer::whereIn('question_id', $questionLists)->where('exam_package_id', $examPackageId)->where('correct_answer', false)->count());
        if(!empty($legends)) {
            $chart->addLegends($legends);
        }
        return $chart;
    }

    public function answersByParticipant(string $participantId): Pie
    {
        $participant   = Participant::findOrFail($participantId);
        $examLists     = DB::table('e_learning.exam_exam_package')->where('exam_package_id', $participant->exam_package_id)->pluck('exam_id')->toArray();
        $examIds       = array_flatten(Exam::whereIn('id', $examLists)->where('exam_type_id', '!=', '2519778a-6fa0-49bd-a072-c0ab75741931')->get(['id'])->toArray());
        $questionLists = array_flatten(Question::whereIn('exam_id', $examIds)->get(['id'])->toArray());
        $chart = new Pie();
        $chart->createSeries('Kumulatif Jawaban Ujian');
        $legends = [];
        array_push($legends, 'Benar');
        $chart->addData('Benar', Answer::whereIn('question_id', $questionLists)->where('exam_package_id', $participant->exam_package_id)->where('student_id', $participant->student_id)->where('correct_answer', true)->count());
        array_push($legends, 'Salah');
        $chart->addData('Salah', Answer::whereIn('question_id', $questionLists)->where('exam_package_id', $participant->exam_package_id)->where('student_id', $participant->student_id)->where('correct_answer', false)->count());
        if(!empty($legends)) {
            $chart->addLegends($legends);
        }
        return $chart;
    }

    public function examParticipantResults(string $examPackageId): Pie
    {
        $chart = new Pie();
        $chart->createSeries('Nilai Ujian');
        $legends = [];
        $participants = Participant::where('exam_package_id', $examPackageId)->get();
        $temps = [];
        foreach ($participants as $participant) {
            //$string = 'Nilai '.$participant->getExamResults();
            $temps[$participant->getExamResults()][] = $participant;
        }
        ksort($temps);
        foreach ($temps as $key => $value) {
            $title = $key.' Poin';
            array_push($legends, $title);
            $chart->addData($title, count($value));
        }
        if(!empty($legends)) {
            $chart->addLegends($legends);
        }
        return $chart;
    }

    public function examParticipantGradeResult(string $examPackageId, string $gradeId): Pie
    {
        $grade = Grade::findOrFail($gradeId);
        $chart = new Pie();
        $chart->createSeries('Grade Ujian');
        $legends = [];
        $participants = Participant::where('exam_package_id', $examPackageId)->get();
        $temps = [];
        foreach ($grade->gradeIndexs as $gradeIndex) {
            array_push($legends, $gradeIndex->name.' ( '.$gradeIndex->minimum.'-'.$gradeIndex->maximum.' )');
            $chart->addData($gradeIndex->name.' ( '.$gradeIndex->minimum.'-'.$gradeIndex->maximum.' )', $gradeIndex->participants()->count());
        }
        if(!empty($legends)) {
            $chart->addLegends($legends);
        }
        return $chart;
    }

    public function examParticipantTotalResults(string $examPackageId): Pie
    {
        $chart = new Pie();
        $chart->createSeries('Kumulatif Nilai Ujian');
        $legends = [];
        $participants = Participant::where('exam_package_id', $examPackageId)->get();
        $temps = [];
        foreach ($participants as $participant) {
            //$string = 'Nilai '.$participant->getExamResults();
            $temps[$participant->total_point][] = $participant;
        }
        ksort($temps);
        foreach ($temps as $key => $value) {
            $title = $key.' Total Poin';
            array_push($legends, $title);
            $chart->addData($title, count($value));
        }
        if(!empty($legends)) {
            $chart->addLegends($legends);
        }
        return $chart;
    }

    public function examParticipantTimeConsume(string $examPackageId): Pie
    {
        $chart = new Pie();
        $chart->createSeries('Kumulatif Waktu Ujian');
        $legends = [];
        $participants = Participant::where('exam_package_id', $examPackageId)->get();
        $temps = [];
        foreach ($participants as $participant) {
            $seconds = $participant->updated_at->diffInSeconds($participant->created_at);
            $roundedSecondToNearestMinute = round($seconds/60)*60;
            $roundedMinute = $roundedSecondToNearestMinute/60;
            $temps[$roundedMinute][] = $roundedMinute;
        }
        ksort($temps);
        foreach ($temps as $key => $value) {
            $title = $key.' Menit';
            array_push($legends, $title);
            $chart->addData($title, count($value));
        }
        if(!empty($legends)) {
            $chart->addLegends($legends);
        }
        return $chart;
    }

    public function examParticipantTimeLine(string $examPackageId): Scatter
    {
        $chart = new Scatter();
        $examPackage = ExamPackage::findOrFail($examPackageId);
        $dataRowCreatedAt = array_flatten($examPackage->answers()->orderBy('created_at')->get(['created_at'])->toArray());
        $dataRowUpdatedAt = array_flatten($examPackage->answers()->orderBy('updated_at')->get(['updated_at'])->toArray());
        $xAxis = array_values(array_unique(array_merge($dataRowCreatedAt, $dataRowUpdatedAt)));
        $chart->createXAxis($xAxis);
        $questionIds = array_flatten(DB::table('e_learning.answers')->select(DB::raw('distinct question_id'))->where('exam_package_id', $examPackageId)->pluck('question_id')->toArray());
        $questions = Question::whereIn('id', $questionIds)->orderBy('created_at')->get();
        $yAxis = [];
        foreach ($questions as $question) {
            $yAxis[] = $question->exam->code.'-'.$question->code;
        }
        $chart->createYAxis($yAxis);

        $legend = [];
        //created data
        $max = 1;
        $series = [];
        $seriesName = 'Jawaban';
        $i = 0;
        foreach ($xAxis as $xAxisData) {
            foreach ($questions as $question) {
                $count = Answer::where('question_id', $question->id)->where('created_at', $xAxisData)->count();
                if($count > $max) {
                    $max = $count;
                }
                if($count > 0) {
                    $series[$i] = [$xAxisData, $question->exam->code.'-'.$question->code, $count];
                }
            }
            $i++;
        }

        $legend[] = $seriesName;
        $chart->createSeries($seriesName, $series);

        //updated data
        $series = [];
        $seriesName = 'Pembaharuan Jawaban';
        $i = 0;
        foreach ($xAxis as $xAxisData) {
            foreach ($questions as $question) {
                $count = Answer::whereRaw('e_learning.answers.created_at != e_learning.answers.updated_at')->where('updated_at', $xAxisData)->count();
                //$count = Answer::where('updated_at', $xAxisData)->count();
                if($count > $max) {
                    $max = $count;
                }
                if($count > 0) {
                    $series[$i] = [$xAxisData, $question->exam->code.'-'.$question->code, ($count*10)];
                }
            }
            if(isset($series[$i]) && count($series[$i])) {
                $i++;
            }

        }
        if(count($series)) {
            $legend[] = $seriesName;
            $chart->createSeries($seriesName, $series);
        }
        $chart->createLegendData($legend);
        $chart->setMaxValue($max);
        //$chart->setSplitNumber(2);
        return $chart;
    }

    public function answersCategoryByCorrectAnswerClassCode(string $examPackageId, string $examId,string $classCodeId): Pie
    {
        $questionLists = array_flatten(Question::where('exam_id', $examId)->get(['id'])->toArray());
        $studentIds = Student::where('class_code_id', $classCodeId)->pluck('id');
        $chart = new Pie();
        $chart->createSeries('Kumulatif Jawaban Ujian');
        $legends = [];
        array_push($legends, 'Benar');
        $chart->addData('Benar', Answer::whereIn('question_id', $questionLists)->whereIn('student_id', $studentIds)->where('exam_package_id', $examPackageId)->where('correct_answer', true)->count());
        array_push($legends, 'Salah');
        $chart->addData('Salah', Answer::whereIn('question_id', $questionLists)->whereIn('student_id', $studentIds)->where('exam_package_id', $examPackageId)->where('correct_answer', false)->count());
        if(!empty($legends)) {
            $chart->addLegends($legends);
        }
        return $chart;
    }

    public function examParticipantResultByClassCode(string $examPackageId, string $classCodeId): Pie
    {
        $chart = new Pie();
        $chart->createSeries('Kumulatif Nilai Ujian');
        $legends = [];
        $studentIds = Student::where('class_code_id', $classCodeId)->pluck('id');
        $participants = Participant::whereIn('student_id', $studentIds)->where('exam_package_id', $examPackageId)->get();
        $temps = [];
        foreach ($participants as $participant) {
            //$string = 'Nilai '.$participant->getExamResults();
            $temps[$participant->total_point][] = $participant;
        }
        ksort($temps);
        foreach ($temps as $key => $value) {
            $title = $key.' Total Poin';
            array_push($legends, $title);
            $chart->addData($title, count($value));
        }
        if(!empty($legends)) {
            $chart->addLegends($legends);
        }
        return $chart;
    }

    public function examParticipantTimeConsumeByClassCode(string $examPackageId, string $classCodeId): Pie
    {
        $chart = new Pie();
        $chart->createSeries('Kumulatif Waktu Ujian');
        $legends = [];
        $studentIds = Student::where('class_code_id', $classCodeId)->pluck('id');
        $participants = Participant::whereIn('student_id', $studentIds)->where('exam_package_id', $examPackageId)->get();
        $temps = [];
        foreach ($participants as $participant) {
            $seconds = $participant->updated_at->diffInSeconds($participant->created_at);
            $roundedSecondToNearestMinute = round($seconds/60)*60;
            $roundedMinute = $roundedSecondToNearestMinute/60;
            $temps[$roundedMinute][] = $roundedMinute;
        }
        ksort($temps);
        foreach ($temps as $key => $value) {
            $title = $key.' Menit';
            array_push($legends, $title);
            $chart->addData($title, count($value));
        }
        if(!empty($legends)) {
            $chart->addLegends($legends);
        }
        return $chart;
    }

    public function examParticipantTimeAnswerHistoryByClassCode(string $examPackageId, string $classCodeId): Scatter
    {
        $studentIds = Student::where('class_code_id', $classCodeId)->pluck('id');
        //dd($studentIds);
        $chart = new Scatter();
        $examPackage = ExamPackage::findOrFail($examPackageId);
        $dataRowCreatedAt = array_flatten($examPackage->answers()->whereIn('student_id', $studentIds)->orderBy('created_at')->get(['created_at'])->toArray());
        $dataRowUpdatedAt = array_flatten($examPackage->answers()->whereIn('student_id', $studentIds)->orderBy('updated_at')->get(['updated_at'])->toArray());
        $xAxis = array_values(array_unique(array_merge($dataRowCreatedAt, $dataRowUpdatedAt)));
        $chart->createXAxis($xAxis);
        $questionIds = array_flatten(DB::table('e_learning.answers')->select(DB::raw('distinct question_id'))->where('exam_package_id', $examPackageId)->pluck('question_id')->toArray());
        $questions = Question::whereIn('id', $questionIds)->orderBy('created_at')->get();
        $yAxis = [];
        foreach ($questions as $question) {
            $yAxis[] = $question->exam->code.'-'.$question->code;
        }
        $chart->createYAxis($yAxis);

        $legend = [];
        //created data
        $max = 1;
        $series = [];
        $seriesName = 'Jawaban';
        $i = 0;
        foreach ($xAxis as $xAxisData) {
            foreach ($questions as $question) {
                $count = Answer::where('question_id', $question->id)->where('created_at', $xAxisData)->count();
                if($count > $max) {
                    $max = $count;
                }
                if($count > 0) {
                    $series[$i] = [$xAxisData, $question->exam->code.'-'.$question->code, $count];
                }
            }
            $i++;
        }

        $legend[] = $seriesName;
        $chart->createSeries($seriesName, $series);

        //updated data
        $series = [];
        $seriesName = 'Pembaharuan Jawaban';
        $i = 0;
        foreach ($xAxis as $xAxisData) {
            foreach ($questions as $question) {
                $count = Answer::whereRaw('e_learning.answers.created_at != e_learning.answers.updated_at')->where('updated_at', $xAxisData)->count();
                //$count = Answer::where('updated_at', $xAxisData)->count();
                if($count > $max) {
                    $max = $count;
                }
                if($count > 0) {
                    $series[$i] = [$xAxisData, $question->exam->code.'-'.$question->code, ($count*10)];
                }
            }
            if(isset($series[$i]) && count($series[$i])) {
                $i++;
            }

        }
        if(count($series)) {
            $legend[] = $seriesName;
            $chart->createSeries($seriesName, $series);
        }
        $chart->createLegendData($legend);
        $chart->setMaxValue($max);
        //$chart->setSplitNumber(2);
        return $chart;
    }

    public function examParticipantClassCodeGradeResult(string $examPackageId, string $gradeId , string $classCodeId): Pie
    {
        $grade = Grade::findOrFail($gradeId);
        $chart = new Pie();
        $chart->createSeries('Grade Ujian');
        $legends = [];
        $studentIds = Student::where('class_code_id', $classCodeId)->pluck('id');
        $participants = Participant::whereIn('student_id', $studentIds)->where('exam_package_id', $examPackageId)->pluck('id')->toArray();
        $temps = [];
        foreach ($grade->gradeIndexs as $gradeIndex) {
            array_push($legends,  $gradeIndex->name.' ( '.$gradeIndex->minimum.'-'.$gradeIndex->maximum.' )');
            $chart->addData( $gradeIndex->name.' ( '.$gradeIndex->minimum.'-'.$gradeIndex->maximum.' )', DB::table('e_learning.grade_index_participant')->whereIn('participant_id', $participants)->where('grade_index_id', $gradeIndex->id)->count());
        }
        if(!empty($legends)) {
            $chart->addLegends($legends);
        }
        return $chart;
    }
}
