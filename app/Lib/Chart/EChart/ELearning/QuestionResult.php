<?php

declare (strict_types = 1);

namespace Whiteboks\Lib\Chart\EChart\ELearning;

use DB;
use Whiteboks\Model\ELearning\Answer;
use Whiteboks\Model\ELearning\Choice;
use Whiteboks\Model\ELearning\Question;
use Whiteboks\Lib\Chart\EChart\Type\Pie;

class QuestionResult
{
    public function answerFrequency(string $questionId, string $examPackageId): Pie
    {
        $choiceIds = DB::table('e_learning.question_choice')->where('question_id', $questionId)->pluck('choice_id')->toArray();
        $choices = Choice::whereIn('id', $choiceIds)->get();
        $chart = new Pie();
        $chart->createSeries('Kumulatif Jawaban Angket');
        $legends = [];
        foreach ($choices as $examChoice) {
            $count = Answer::where('choice_id', $examChoice->id)->where('question_id', $questionId)->where('exam_package_id', $examPackageId)->count();
            array_push($legends, $examChoice->choice);
            $chart->addData($examChoice->choice, $count);
        }
        $chart->addLegends($legends);
        return $chart;
    }
}
