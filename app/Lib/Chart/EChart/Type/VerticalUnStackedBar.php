<?php

declare (strict_types = 1);

namespace Whiteboks\Lib\Chart\EChart\Type;

class VerticalUnStackedBar
{
    public $title;
    public $tooltip;
    public $legend;
    public $toolbox;
    public $calculable;
    public $xAxis;
    public $yAxis;
    public $series;

    public function __construct()
    {
        $this->title = new \stdClass;
        $this->title->show = false;
        $this->tooltip = new \stdClass;
        $this->tooltip->trigger = 'axis';
        $this->tooltip->axisPointer = new \stdClass;
        $this->tooltip->axisPointer->type = 'shadow';
        $this->legend = new \stdClass;
        $this->legend->data = [];
        $this->toolbox = new \stdClass;
        $this->toolbox->show = false;
        $this->calculable = true;
        $this->xAxis = new \stdClass;
        $this->yAxis = new \stdClass;
        $this->grid = new \stdClass;
        $this->series = [];
        $this->createXAxis();
    }

    public function createGrid(int $left = 3, int $right = 4, int $bottom = 3, bool $containLabel = true): bool
    {
        $this->grid->left = $left.'%';
        $this->grid->right = $right.'%';
        $this->grid->bottom = $bottom.'%';
        $this->grid->containLabel = $containLabel;
        return true;
    }

    public function createLegend(string $name): bool
    {
        array_push($this->legend->data, $name);
        return true;
    }

    public function createXAxis(): bool
    {
        $xAxis = new \stdClass;
        $xAxis->type = 'value';
        $xAxis->boundaryGap = [0, 0.01];
        $this->xAxis = $xAxis;
        return true;
    }

    public function createYAxis(array $data): bool
    {
        $yAxis = new \stdClass;
        $yAxis->type = 'category';
        $yAxis->data = $data;
        $this->yAxis = $yAxis;
        return true;
    }

    public function addSeries(string $name, array $data): bool
    {
        $series = new \stdClass;
        $series->name = $name;
        $series->type = 'bar';
        $series->data = $data;
        array_push($this->series, $series);
        return true;
    }
}
