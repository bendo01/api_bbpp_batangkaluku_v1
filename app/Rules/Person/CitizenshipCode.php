<?php
declare (strict_types = 1);
namespace Whiteboks\Rules\Person;

use Whiteboks\Model\Location\SubDistrict;
use Illuminate\Contracts\Validation\Rule;

class CitizenshipCode implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value): bool
    {
        $tempArray = str_split($value, 2);
        $validateString = $tempArray[0].$tempArray[1].$tempArray[2];
        return (bool)SubDistrict::where('validation_code', $validateString)->count();
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message(): string
    {
        return 'Nomor Induk Kependudukan Anda Tidak Valid';
    }
}
