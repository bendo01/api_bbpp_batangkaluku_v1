<?php
declare (strict_types = 1);
namespace Whiteboks\Rules\Person;

use Illuminate\Contracts\Validation\Rule;
use Whiteboks\Model\Person\Reference\Biodata\Gender;

class CitizenshipDateOfBirth implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value): bool
    {
        $returned  = false;
        $count     = 0;
        $tempArray = str_split($this->get('code'), 2);
        if ($this->get('gender_id') == '8fd69ced-ac03-41eb-ac4c-47c99373a122') {
            $tempArray[3]-=40;
        }
        if (intval($tempArray[3]) < 10 && strlen($tempArray[3]) == 1) {
            $tempArray[3] = '0'.$tempArray[3];
        }
        $tempDateArray = explode('-', $this->get('birth_date'));
        $birtDayFromCode = $tempArray[3].'-'.$tempArray[4].'-'.$tempDateArray[2];
        if ($birtDayFromCode == $this->get('birth_date')) {
            $returned = true;
        }
        return $returned;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message(): string
    {
        return 'Tanggal Lahir Anda Tidak Valid';
    }
}
