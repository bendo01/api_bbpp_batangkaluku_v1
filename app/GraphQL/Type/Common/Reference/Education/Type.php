<?php
declare (strict_types = 1);

namespace Whiteboks\GraphQL\Type\Common\Reference\Education;

use GraphQL\Type\Definition\Type as GraphQLDataType;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Type as GraphQLType;

class Type extends GraphQLType
{
    protected $attributes = [
        'name' => 'CommonReferenceEducationType',
        'description' => 'A type'
    ];

    public function fields()
    {
        return [
            'id' => [
                'type' => GraphQLDataType::nonNull(GraphQLDataType::string()),
                'description' => 'The id of Model'
            ],
            'code' => [
                'type' => GraphQLDataType::nonNull(GraphQLDataType::string()),
                'description' => 'The code of Model'
            ],
            'name' => [
                'type' => GraphQLDataType::nonNull(GraphQLDataType::string()),
                'description' => 'The name of Model'
            ],
            'created_by' => [
                'type' => GraphQLDataType::string(),
                'description' => 'User Id Whom Created Data'
            ],
            'modified_by' => [
                'type' => GraphQLDataType::string(),
                'description' => 'User Id Whom Modified Data'
            ],
            'created_at' => [
                'type' => GraphQLDataType::string(),
                'description' => 'Time Created Data'
            ],
            'updated_at' => [
                'type' => GraphQLDataType::string(),
                'description' => 'Time Updated Data'
            ],
        ];
    }
}
