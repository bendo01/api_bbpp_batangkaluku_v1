<?php
declare (strict_types = 1);

namespace Whiteboks\GraphQL\Type\Common\Reference\Decree;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Type as GraphQLType;

class DecreeType extends GraphQLType
{
    protected $attributes = [
        'name' => 'CommonReferenceDecreeDecreeType',
        'description' => 'A type'
    ];

    public function fields()
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'The id of field'
            ],
            'code' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'The code of field'
            ],
            'name' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'The name of field'
            ],
            'created_by' => [
                'type' => Type::string(),
                'description' => 'User Id Whom Created Data'
            ],
            'modified_by' => [
                'type' => Type::string(),
                'description' => 'User Id Whom Modified Data'
            ],
            'created_at' => [
                'type' => Type::string(),
                'description' => 'Time Created Data'
            ],
            'updated_at' => [
                'type' => Type::string(),
                'description' => 'Time Updated Data'
            ],
        ];
    }
}
