<?php

namespace Whiteboks\GraphQL\Type\Common\Master;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Type as GraphQLType;

use Whiteboks\Model\Common\Master\SocialMedia as Model;


class SocialMedia extends GraphQLType
{
    protected $attributes = [
        'name' => 'CommonMasterSocialMedia',
        'description' => 'A type',
        'model' => Model::class, // define model for users type
    ];

    public function fields()
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'The id of decrees'
            ],
            'username' => [
                'type' => Type::string(),
                'description' => 'username field'
            ],
            'url' => [
                'type' => Type::string(),
                'description' => 'url field'
            ],
            'social_mediaable_id' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'social_mediaable_id field'
            ],
            'social_mediaable_type' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'social_mediaable_type field'
            ],
            'created_by' => [
                'type' => Type::string(),
                'description' => 'User Id Whom Created Data decrees'
            ],
            'modified_by' => [
                'type' => Type::string(),
                'description' => 'User Id Whom Modified Data decrees'
            ],
            'created_at' => [
                'type' => Type::string(),
                'description' => 'Time Created Data decrees'
            ],
            'updated_at' => [
                'type' => Type::string(),
                'description' => 'Time Updated Data decrees'
            ],
        ];
    }
}
