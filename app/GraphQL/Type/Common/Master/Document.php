<?php
declare (strict_types = 1);
namespace Whiteboks\GraphQL\Type\Common\Master;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Type as GraphQLType;

use Whiteboks\Model\Common\Master\Document as Model;

class Document extends GraphQLType
{
    protected $attributes = [
        'name' => 'CommonMasterDocument',
        'description' => 'A Document',
        'model' => Model::class, // define model for users type
    ];

    public function fields()
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'The id of documents'
            ],
            'title' => [
                'type' => Type::string(),
                'description' => 'The title of documents'
            ],
            'filename' => [
                'type' => Type::string(),
                'description' => 'The name of documents file'
            ],
            'dir' => [
                'type' => Type::string(),
                'description' => 'The name of documents file'
            ],
            'type' => [
                'type' => Type::string(),
                'description' => 'The type of documents'
            ],
            'size' => [
                'type' => Type::string(),
                'description' => 'The size of documents type'
            ],
            'url' => [
                'type' => Type::string(),
                'description' => 'The url of the documents'
            ],
            'documentable_id' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'The id documentable items'
            ],
            'documentable_type' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'The type documentable items'
            ],
            'type_id' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'The id of documents type'
            ],
            'created_by' => [
                'type' => Type::string(),
                'description' => 'User Id Whom Created Data documents'
            ],
            'modified_by' => [
                'type' => Type::string(),
                'description' => 'User Id Whom Modified Data documents'
            ],
            'created_at' => [
                'type' => Type::string(),
                'description' => 'Time Created Data documents'
            ],
            'updated_at' => [
                'type' => Type::string(),
                'description' => 'Time Updated Data documents'
            ],
        ];
    }
}
