<?php

namespace Whiteboks\GraphQL\Type\Common\Master;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Type as GraphQLType;

use Whiteboks\Model\Common\Master\ResidenceRoom as Model;

class ResidenceRoom extends GraphQLType
{
    protected $attributes = [
        'name' => 'CommonMasterResidenceRoom',
        'description' => 'A type',
        'model' => Model::class, // define model for users type
    ];

    public function fields()
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'The id of decrees'
            ],
            'residence_id' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'residence_id field'
            ],
            'code' => [
                'type' => Type::string(),
                'description' => 'code field'
            ],
            'name' => [
                'type' => Type::string(),
                'description' => 'name field'
            ],
            'floor' => [
                'type' => Type::string(),
                'description' => 'floor field'
            ],
            'long' => [
                'type' => Type::string(),
                'description' => 'long field'
            ],
            'wide' => [
                'type' => Type::string(),
                'description' => 'wide field'
            ],
            'high' => [
                'type' => Type::string(),
                'description' => 'high field'
            ],
            'latitude' => [
                'type' => Type::string(),
                'description' => 'latitude field'
            ],
            'longitude' => [
                'type' => Type::string(),
                'description' => 'longitude field'
            ],
            'zoom' => [
                'type' => Type::int(),
                'description' => 'zoom field'
            ],
            'room_type_id' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'room_type_id field'
            ],
            'created_by' => [
                'type' => Type::string(),
                'description' => 'User Id Whom Created Data decrees'
            ],
            'modified_by' => [
                'type' => Type::string(),
                'description' => 'User Id Whom Modified Data decrees'
            ],
            'created_at' => [
                'type' => Type::string(),
                'description' => 'Time Created Data decrees'
            ],
            'updated_at' => [
                'type' => Type::string(),
                'description' => 'Time Updated Data decrees'
            ],
        ];
    }
}
