<?php

namespace Whiteboks\GraphQL\Type\Common\Master;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Type as GraphQLType;

use Whiteboks\Model\Common\Master\Residence as Model;

class Residence extends GraphQLType
{
    protected $attributes = [
        'name' => 'CommonMasterResidence',
        'description' => 'Common Master Residence',
        'model' => Model::class, // define model for users type
    ];

    public function fields()
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'The id of decrees'
            ],
            'residenceable_id' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'residenceable_id field'
            ],
            'residenceable_type' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'residenceable_type field'
            ],
            'address' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'address field'
            ],
            'code' => [
                'type' => Type::string(),
                'description' => 'code field'
            ],
            'name' => [
                'type' => Type::string(),
                'description' => 'name field'
            ],
            'citizens_association' => [
                'type' => Type::string(),
                'description' => 'citizens_association field'
            ],
            'neighborhood_association' => [
                'type' => Type::string(),
                'description' => 'neighborhood_association field'
            ],
            'province_id' => [
                'type' => Type::string(),
                'description' => 'province_id field'
            ],
            'regency_id' => [
                'type' => Type::string(),
                'description' => 'regency_id field'
            ],
            'sub_district_id' => [
                'type' => Type::string(),
                'description' => 'sub_district_id field'
            ],
            'village_id' => [
                'type' => Type::string(),
                'description' => 'village_id field'
            ],
            'regency_name' => [
                'type' => Type::string(),
                'description' => 'regency_name field'
            ],
            'sub_district_name' => [
                'type' => Type::string(),
                'description' => 'sub_district_name field'
            ],
            'village_name' => [
                'type' => Type::string(),
                'description' => 'village_name field'
            ],
            'resident_type_id' => [
                'type' => Type::string(),
                'description' => 'resident_type_id field'
            ],
            'category_id' => [
                'type' => Type::string(),
                'description' => 'category_id field'
            ],
            'type_id' => [
                'type' => Type::string(),
                'description' => 'type_id field'
            ],
            'latitude' => [
                'type' => Type::string(),
                'description' => 'latitude field'
            ],
            'longitude' => [
                'type' => Type::string(),
                'description' => 'longitude field'
            ],
            'zoom' => [
                'type' => Type::int(),
                'description' => 'zoom field'
            ],
            'created_by' => [
                'type' => Type::string(),
                'description' => 'User Id Whom Created Data decrees'
            ],
            'modified_by' => [
                'type' => Type::string(),
                'description' => 'User Id Whom Modified Data decrees'
            ],
            'created_at' => [
                'type' => Type::string(),
                'description' => 'Time Created Data decrees'
            ],
            'updated_at' => [
                'type' => Type::string(),
                'description' => 'Time Updated Data decrees'
            ],
            'province' => [
                'type' => GraphQL::type('LocationProvince'),
                'description' => 'The Type of this Regency'
            ],
            'regency' => [
                'type' => GraphQL::type('LocationRegency'),
                'description' => 'The Type of this Regency'
            ],
            'subDistrict' => [
                'type' => GraphQL::type('LocationSubDistrict'),
                'description' => 'The Type of this Sub District'
            ],
            'village' => [
                'type' => GraphQL::type('LocationVillage'),
                'description' => 'The Type of this village'
            ],
        ];
    }
}
