<?php

namespace Whiteboks\GraphQL\Type\Common\Master;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Type as GraphQLType;

class Tag extends GraphQLType
{
    protected $attributes = [
        'name' => 'CommonMasterTag',
        'description' => 'A type'
    ];

    public function fields()
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'The id of decrees'
            ],
            'title' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'title field'
            ],
            'tagable_id' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'tagable_id field'
            ],
            'tagable_type' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'tagable_type field'
            ],
            'created_by' => [
                'type' => Type::string(),
                'description' => 'User Id Whom Created Data decrees'
            ],
            'modified_by' => [
                'type' => Type::string(),
                'description' => 'User Id Whom Modified Data decrees'
            ],
            'created_at' => [
                'type' => Type::string(),
                'description' => 'Time Created Data decrees'
            ],
            'updated_at' => [
                'type' => Type::string(),
                'description' => 'Time Updated Data decrees'
            ],
        ];
    }
}
