<?php

namespace Whiteboks\GraphQL\Type\Common\Master;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Type as GraphQLType;


class StructuralPosition extends GraphQLType
{
    protected $attributes = [
        'name' => 'CommonMasterStructuralPosition',
        'description' => 'Common Master Structural Position',
    ];

    public function fields()
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'The id of decrees'
            ],
            'individual_id' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'individual_id field'
            ],
            'role_id' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'role_id field'
            ],
            'start_date' => [
                'type' => Type::string(),
                'description' => 'start_date field'
            ],
            'end_date' => [
                'type' => Type::string(),
                'description' => 'end_date field'
            ],
            'decree_number' => [
                'type' => Type::string(),
                'description' => 'decree_number field'
            ],
            'decree_date' => [
                'type' => Type::string(),
                'description' => 'decree_date field'
            ],
            'structural_positionable_id' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'structural_positionable_id field'
            ],
            'structural_positionable_type' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'structural_positionable_type field'
            ],
            'created_by' => [
                'type' => Type::string(),
                'description' => 'User Id Whom Created Data decrees'
            ],
            'modified_by' => [
                'type' => Type::string(),
                'description' => 'User Id Whom Modified Data decrees'
            ],
            'created_at' => [
                'type' => Type::string(),
                'description' => 'Time Created Data decrees'
            ],
            'updated_at' => [
                'type' => Type::string(),
                'description' => 'Time Updated Data decrees'
            ],
        ];
    }
}
