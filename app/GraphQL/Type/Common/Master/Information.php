<?php

namespace Whiteboks\GraphQL\Type\Common\Master;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Type as GraphQLType;

use Whiteboks\Model\Common\Master\Email as Model;

class Information extends GraphQLType
{
    protected $attributes = [
        'name' => 'CommonMasterInformation',
        'description' => 'Common Master Information',
        'model' => Model::class, // define model for users type
    ];

    public function fields()
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'The id of decrees'
            ],
            'informationable_id' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'informationable_id fields'
            ],
            'informationable_type' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'informationable_type fields'
            ],
            'vision' => [
                'type' => Type::string(),
                'description' => 'vision fields'
            ],
            'mission' => [
                'type' => Type::string(),
                'description' => 'mission fields'
            ],
            'intention' => [
                'type' => Type::string(),
                'description' => 'intention fields'
            ],
            'objective' => [
                'type' => Type::string(),
                'description' => 'objective fields'
            ],
            'created_by' => [
                'type' => Type::string(),
                'description' => 'User Id Whom Created Data decrees'
            ],
            'modified_by' => [
                'type' => Type::string(),
                'description' => 'User Id Whom Modified Data decrees'
            ],
            'created_at' => [
                'type' => Type::string(),
                'description' => 'Time Created Data decrees'
            ],
            'updated_at' => [
                'type' => Type::string(),
                'description' => 'Time Updated Data decrees'
            ],
        ];
    }
}
