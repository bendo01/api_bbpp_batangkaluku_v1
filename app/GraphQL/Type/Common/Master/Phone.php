<?php

namespace Whiteboks\GraphQL\Type\Common\Master;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Type as GraphQLType;

use Whiteboks\Model\Common\Master\Phone as Model;

class Phone extends GraphQLType
{
    protected $attributes = [
        'name' => 'CommonMasterPhone',
        'description' => 'Common Master Phone',
        'model' => Model::class, // define model for users type
    ];

    public function fields()
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'The id of decrees'
            ],
            'dial_number' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'dial_number field'
            ],
            'is_primary' => [
                'type' => Type::int(),
                'description' => 'is_primary field'
            ],
            'phoneable_id' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'phoneable_id field'
            ],
            'phoneable_type' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'phoneable_type field'
            ],
            'created_by' => [
                'type' => Type::string(),
                'description' => 'User Id Whom Created Data decrees'
            ],
            'modified_by' => [
                'type' => Type::string(),
                'description' => 'User Id Whom Modified Data decrees'
            ],
            'created_at' => [
                'type' => Type::string(),
                'description' => 'Time Created Data decrees'
            ],
            'updated_at' => [
                'type' => Type::string(),
                'description' => 'Time Updated Data decrees'
            ],
        ];
    }
}
