<?php
declare (strict_types = 1);
namespace Whiteboks\GraphQL\Type\Common\Master;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Type as GraphQLType;

use Whiteboks\Model\Common\Master\Email as Model;

class Email extends GraphQLType
{
    protected $attributes = [
        'name' => 'CommonMasterEmail',
        'description' => 'A type',
        'model' => Model::class, // define model for users type
    ];
    public function fields()
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'The id of emails'
            ],
            'address' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'The address of emails'
            ],
            'is_primary' => [
                'type' => Type::int(),
                'description' => 'is_primary field'
            ],
            'emailable_id' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'emailable_id field'
            ],
            'emailable_type' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'emailable_type field'
            ],
            'created_by' => [
                'type' => Type::string(),
                'description' => 'User Id Whom Created Data emails'
            ],
            'modified_by' => [
                'type' => Type::string(),
                'description' => 'User Id Whom Modified Data emails'
            ],
            'created_at' => [
                'type' => Type::string(),
                'description' => 'Time Created Data emails'
            ],
            'updated_at' => [
                'type' => Type::string(),
                'description' => 'Time Updated Data emails'
            ],
            'provinces' => [
                'type' => Type::listOf(GraphQL::type('Province')),
                'description' => 'List Of Provinces By Country',
                'always' => ['code', 'name', 'country_id'],
            ]
        ];
    }
}
