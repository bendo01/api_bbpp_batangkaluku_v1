<?php
declare (strict_types = 1);
namespace Whiteboks\GraphQL\Type\Common\Master;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Type as GraphQLType;

use Whiteboks\Model\Common\Master\Decree as Model;

class Decree extends GraphQLType
{
    protected $attributes = [
        'name' => 'CommonMasterDecree',
        'description' => 'Common Master Decree',
        'model' => Model::class,
    ];

    public function fields()
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'The id of decrees'
            ],
            'title' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'The title of decrees'
            ],
            'code' => [
                'type' => Type::string(),
                'description' => 'The code of the decrees'
            ],
            'description' => [
                'type' => Type::string(),
                'description' => 'The description of the decrees'
            ],
            'start_date' => [
                'type' => Type::string(),
                'description' => 'start_date field'
            ],
            'end_date' => [
                'type' => Type::string(),
                'description' => 'end_date field'
            ],
            'type_id' => [
                'type' => Type::string(),
                'description' => 'type_id field'
            ],
            'decreeable_id' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'The id of decreeable item'
            ],
            'decreeable_type' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'The type of decreeable item'
            ],
            'created_by' => [
                'type' => Type::string(),
                'description' => 'User Id Whom Created Data decrees'
            ],
            'modified_by' => [
                'type' => Type::string(),
                'description' => 'User Id Whom Modified Data decrees'
            ],
            'created_at' => [
                'type' => Type::string(),
                'description' => 'Time Created Data decrees'
            ],
            'updated_at' => [
                'type' => Type::string(),
                'description' => 'Time Updated Data decrees'
            ],
        ];
    }
}
