<?php
declare (strict_types = 1);
namespace Whiteboks\GraphQL\Type\Location;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Type as GraphQLType;

use Whiteboks\Model\Location\Village as Model;

class Village extends GraphQLType
{
    protected $attributes = [
        'name' => 'LocationVillage',
        'description' => 'Location Village Type',
        'model' => Model::class, // define model for users type
    ];
    public function fields(): array
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'The id of villages'
            ],
            'code' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'The code of villages'
            ],
            'name' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'The name of villages'
            ],
            'slug' => [
                'type' => Type::string(),
                'description' => 'The slug of villages'
            ],
            'alt_slug' => [
                'type' => Type::string(),
                'description' => 'The alt_slug of villages'
            ],
            'state_ministry_code' => [
                'type' => Type::string(),
                'description' => 'The state_ministry_code of villages'
            ],
            'state_ministry_full_code' => [
                'type' => Type::string(),
                'description' => 'The state_ministry_full_code of villages'
            ],
            'state_post_department_code' => [
                'type' => Type::string(),
                'description' => 'The state_post_department_code of villages'
            ],
            'postal_code_start_range' => [
                'type' => Type::string(),
                'description' => 'The postal_code_start_range of villages'
            ],
            'postal_code_end_range' => [
                'type' => Type::string(),
                'description' => 'The postal_code_end_range of villages'
            ],
            'state_ministry_name' => [
                'type' => Type::string(),
                'description' => 'The state_ministry_name of villages'
            ],
            'dikti_name' => [
                'type' => Type::string(),
                'description' => 'The dikti_name of villages'
            ],
            'created_by' => [
                'type' => Type::string(),
                'description' => 'User Id Whom Craeted Data villages'
            ],
            'modified_by' => [
                'type' => Type::string(),
                'description' => 'User Id Whom Modified Data villages'
            ],
            'created_at' => [
                'type' => Type::string(),
                'description' => 'Time Created Data villages'
            ],
            'updated_at' => [
                'type' => Type::string(),
                'description' => 'Time Updated Data villages'
            ],
            'deleted_at' => [
                'type' => Type::string(),
                'description' => 'Time Deleted Data villages'
            ],
            'sub_district_id' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'The Sub District id of villages'
            ],
            'subDistrict' => [
                'type' => GraphQL::type('LocationSubDistrict'),
                'description' => 'The Sub District of this Village'
            ],
            'residencies' => [
                'type' => Type::listOf(GraphQL::type('CommonMasterResidence')),
                'description' => 'List Of residencies'
            ]
        ];
    }
}
