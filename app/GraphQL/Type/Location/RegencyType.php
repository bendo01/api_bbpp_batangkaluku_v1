<?php
declare (strict_types = 1);
namespace Whiteboks\GraphQL\Type\Location;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Type as GraphQLType;

use Whiteboks\Model\Location\RegencyType as Model;

class RegencyType extends GraphQLType
{
    protected $attributes = [
        'name' => 'LocationRegencyType',
        'description' => 'Location Regency Type',
        'model' => Model::class, // define model for users type
    ];
    public function fields(): array
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'The id of regency type'
            ],
            'code' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'The code of regency type'
            ],
            'name' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'The name of the regency type'
            ],
            'created_by' => [
                'type' => Type::string(),
                'description' => 'User Id Whom Created Data regency type'
            ],
            'modified_by' => [
                'type' => Type::string(),
                'description' => 'User Id Whom Modified Data regency type'
            ],
            'created_at' => [
                'type' => Type::string(),
                'description' => 'Time Created Data regency type'
            ],
            'updated_at' => [
                'type' => Type::string(),
                'description' => 'Time Updated Data regency type'
            ],
            'regencies' => [
                'type' => Type::listOf(GraphQL::type('LocationRegency')),
                'description' => 'List Of Regencies By Type'
            ],
        ];
    }
}
