<?php
declare (strict_types = 1);
namespace Whiteboks\GraphQL\Type\Location;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Type as GraphQLType;

use Whiteboks\Model\Location\SubDistrict as Model;

class SubDistrict extends GraphQLType
{
    protected $attributes = [
        'name' => 'LocationSubDistrict',
        'description' => 'Location Sub District',
        'model' => Model::class, // define model for users type
    ];
    public function fields(): array
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'The id of sub district'
            ],
            'code' => [
                'type' => Type::string(),
                'description' => 'The code of sub district'
            ],
            'name' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'The name of the sub district'
            ],
            'slug' => [
                'type' => Type::string(),
                'description' => 'The slug of the sub district'
            ],
            'alt_slug' => [
                'type' => Type::string(),
                'description' => 'The alt slug of the sub district'
            ],
            'state_ministry_code' => [
                'type' => Type::string(),
                'description' => 'The state ministry code of the sub district'
            ],
            'state_ministry_full_code' => [
                'type' => Type::string(),
                'description' => 'The state ministry full code of the sub district'
            ],
            'state_post_department_code' => [
                'type' => Type::string(),
                'description' => 'The state post department code of the sub district'
            ],
            'postal_code_start_range' => [
                'type' => Type::string(),
                'description' => 'postal_code_start_range field'
            ],
            'postal_code_end_range' => [
                'type' => Type::string(),
                'description' => 'postal_code_end_range field'
            ],
            'state_ministry_name' => [
                'type' => Type::string(),
                'description' => 'The state ministry name of the sub district'
            ],
            'dikti_code' => [
                'type' => Type::string(),
                'description' => 'The dikti code of the sub district'
            ],
            'dikti_name' => [
                'type' => Type::string(),
                'description' => 'The dikti name of the sub district'
            ],
            'validation_code' => [
                'type' => Type::string(),
                'description' => 'The validation code of the regency'
            ],
            'agriculture_department_code' => [
                'type' => Type::string(),
                'description' => 'The agriculture department code of the sub district'
            ],
            'created_by' => [
                'type' => Type::string(),
                'description' => 'User Id Whom Created Data sub district'
            ],
            'modified_by' => [
                'type' => Type::string(),
                'description' => 'User Id Whom Modified Data sub district'
            ],
            'created_at' => [
                'type' => Type::string(),
                'description' => 'Time Created Data sub district'
            ],
            'updated_at' => [
                'type' => Type::string(),
                'description' => 'Time Updated Data sub district'
            ],
            'regency_id' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'The regency id of the sub district'
            ],
            'regency' => [
                'type' => GraphQL::type('LocationRegency'),
                'description' => 'The Regency of this Sub District'
            ],
            'villages' => [
                'type' => Type::listOf(GraphQL::type('LocationVillage')),
                'description' => 'List Of Village By Sub District'
            ],
            'residencies' => [
                'type' => Type::listOf(GraphQL::type('CommonMasterResidence')),
                'description' => 'List Of residencies'
            ]
        ];
    }
}
