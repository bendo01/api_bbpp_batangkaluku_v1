<?php
declare (strict_types = 1);
namespace Whiteboks\GraphQL\Type\Location;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Type as GraphQLType;

use Whiteboks\Model\Location\Country as Model;

class Country extends GraphQLType
{
    protected $attributes = [
        'name' => 'LocationCountry',
        'description' => 'Location Country',
        'model' => Model::class, // define model for users type
    ];
    public function fields(): array
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'The id of countries'
            ],
            'code' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'The code of countries'
            ],
            'name' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'The name of the countries'
            ],
            'alpha2_code' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'The alpha2 code of the countries'
            ],
            'alpha3_code' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'The alpha3 code of the countries'
            ],
            'iso3166_2_code' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'The iso3166 2 code of the countries'
            ],
            'dikti_code' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'The name of the countries'
            ],
            'slug' => [
                'type' => Type::string(),
                'description' => 'The slug of the countries'
            ],
            'continent_id' => [
                'type' => Type::string(),
                'description' => 'The continent id of the countries'
            ],
            'region_id' => [
                'type' => Type::string(),
                'description' => 'The region id of the countries'
            ],
            'created_by' => [
                'type' => Type::string(),
                'description' => 'User Id Whom Created Data countries'
            ],
            'modified_by' => [
                'type' => Type::string(),
                'description' => 'User Id Whom Modified Data countries'
            ],
            'created_at' => [
                'type' => Type::string(),
                'description' => 'Time Created Data countries'
            ],
            'updated_at' => [
                'type' => Type::string(),
                'description' => 'Time Updated Data countries'
            ],
            'provinces' => [
                'type' => Type::listOf(GraphQL::type('LocationProvince')),
                'description' => 'List Of Provinces By Country'
            ],
        ];
    }
}
