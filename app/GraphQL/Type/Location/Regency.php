<?php
declare (strict_types = 1);
namespace Whiteboks\GraphQL\Type\Location;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Type as GraphQLType;

use Whiteboks\Model\Location\Regency as Model;

class Regency extends GraphQLType
{
    protected $attributes = [
        'name' => 'LocationRegency',
        'description' => 'Location Regency',
        'model' => Model::class, // define model for users type
    ];
    public function fields(): array
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'The id of regency'
            ],
            'code' => [
                'type' => Type::string(),
                'description' => 'The code of regency'
            ],
            'name' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'The name of the regency'
            ],
            'dikti_code' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'The dikti code of the regency'
            ],
            'epsbed_code' => [
                'type' => Type::string(),
                'description' => 'The epsbed code of the regency'
            ],
            'slug' => [
                'type' => Type::string(),
                'description' => 'The slug of the regency'
            ],
            'description' => [
                'type' => Type::string(),
                'description' => 'The description of the regency'
            ],
            'alt_slug' => [
                'type' => Type::string(),
                'description' => 'The alt slug of the regency'
            ],
            'state_ministry_code' => [
                'type' => Type::string(),
                'description' => 'The state ministry code of the regency'
            ],
            'state_ministry_full_code' => [
                'type' => Type::string(),
                'description' => 'The state ministry full code of the regency'
            ],
            'state_post_department_code' => [
                'type' => Type::string(),
                'description' => 'The state post department code of the regency'
            ],
            'state_ministry_name' => [
                'type' => Type::string(),
                'description' => 'The state ministry name of the regency'
            ],
            'dikti_name' => [
                'type' => Type::string(),
                'description' => 'The dikti name of the regency'
            ],
            'validation_code' => [
                'type' => Type::string(),
                'description' => 'The validation code of the regency'
            ],
            'agriculture_department_code' => [
                'type' => Type::string(),
                'description' => 'The agriculture department code of the regency'
            ],
            'agriculture_department_name' => [
                'type' => Type::string(),
                'description' => 'The agriculture department name of the regency'
            ],
            'is_capital' => [
                'type' => Type::int(),
                'description' => 'is_capital field'
            ],
            'created_by' => [
                'type' => Type::string(),
                'description' => 'User Id Whom Created Data regency'
            ],
            'modified_by' => [
                'type' => Type::string(),
                'description' => 'User Id Whom Modified Data regency'
            ],
            'created_at' => [
                'type' => Type::string(),
                'description' => 'Time Created Data regency'
            ],
            'updated_at' => [
                'type' => Type::string(),
                'description' => 'Time Updated Data regency'
            ],
            'province_id' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'The province id of the regency'
            ],
            'province' => [
                'type' => GraphQL::type('LocationProvince'),
                'description' => 'The Province of this Regency'
            ],
            'regency_type_id' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'The regency type id of the regency'
            ],
            'regencyType' => [
                'type' => GraphQL::type('LocationRegencyType'),
                'description' => 'The Type of this Regency'
            ],
            'subDistricts' => [
                'type' => Type::listOf(GraphQL::type('LocationSubDistrict')),
                'description' => 'List Of Regencies By Province'
            ],
            'residencies' => [
                'type' => Type::listOf(GraphQL::type('CommonMasterResidence')),
                'description' => 'List Of residencies'
            ]
        ];
    }
}
