<?php
declare (strict_types = 1);
namespace Whiteboks\GraphQL\Type\Location;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Type as GraphQLType;

use Whiteboks\Model\Location\Province as Model;

class Province extends GraphQLType
{
    protected $attributes = [
        'name' => 'LocationProvince',
        'description' => 'Location Province',
        'model' => Model::class, // define model for Province
    ];
    public function fields(): array
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'The id of province'
            ],
            'code' => [
                'type' => Type::string(),
                'description' => 'The code of province'
            ],
            'name' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'The name of the province'
            ],
            'dikti_code' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'The dikti code of the province'
            ],
            'epsbed_code' => [
                'type' => Type::string(),
                'description' => 'The epsbed code of the province'
            ],
            'slug' => [
                'type' => Type::string(),
                'description' => 'The slug of the province'
            ],
            'description' => [
                'type' => Type::string(),
                'description' => 'The description of the province'
            ],
            'alt_slug' => [
                'type' => Type::string(),
                'description' => 'The alt slug of the province'
            ],
            'state_ministry_code' => [
                'type' => Type::string(),
                'description' => 'The state ministry code of the province'
            ],
            'state_ministry_full_code' => [
                'type' => Type::string(),
                'description' => 'The state ministry full code of the province'
            ],
            'state_post_department_code' => [
                'type' => Type::string(),
                'description' => 'The state post department code of the province'
            ],
            'state_ministry_name' => [
                'type' => Type::string(),
                'description' => 'The state ministry name of the province'
            ],
            'dikti_name' => [
                'type' => Type::string(),
                'description' => 'The dikti name of the province'
            ],
            'validation_code' => [
                'type' => Type::string(),
                'description' => 'The validation code of the province'
            ],
            'created_by' => [
                'type' => Type::string(),
                'description' => 'User Id Whom Created Data province'
            ],
            'modified_by' => [
                'type' => Type::string(),
                'description' => 'User Id Whom Modified Data province'
            ],
            'created_at' => [
                'type' => Type::string(),
                'description' => 'Time Created Data province'
            ],
            'updated_at' => [
                'type' => Type::string(),
                'description' => 'Time Updated Data province'
            ],
            'country_id' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'The country id of the province'
            ],
            'country' => [
                'type' => GraphQL::type('LocationCountry'),
                'description' => 'The Country of this province'
            ],
            'regencies' => [
                'type' => Type::listOf(GraphQL::type('LocationRegency')),
                'description' => 'List Of Regencies By Province'
            ],
            'residencies' => [
                'type' => Type::listOf(GraphQL::type('CommonMasterResidence')),
                'description' => 'List Of residencies'
            ]
        ];
    }
}
