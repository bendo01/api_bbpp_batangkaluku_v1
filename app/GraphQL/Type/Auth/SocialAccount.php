<?php
declare (strict_types = 1);

namespace Whiteboks\GraphQL\Type\Auth;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Type as GraphQLType;

class SocialAccount extends GraphQLType
{
    protected $attributes = [
        'name' => 'AuthSocialAccount',
        'description' => 'A type'
    ];

    public function fields()
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'id field'
            ],
            'user_id' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'user_id field'
            ],
            'provider' => [
                'type' => Type::string(),
                'description' => 'provider field'
            ],
            'provider_user_id' => [
                'type' => Type::string(),
                'description' => 'provider_user_id field'
            ],
            'created_by' => [
                'type' => Type::string(),
                'description' => 'User Id Whom Created Data'
            ],
            'modified_by' => [
                'type' => Type::string(),
                'description' => 'User Id Whom Modified Data'
            ],
            'created_at' => [
                'type' => Type::string(),
                'description' => 'Time Created Data'
            ],
            'updated_at' => [
                'type' => Type::string(),
                'description' => 'Time Updated Data'
            ],
        ];
    }
}
