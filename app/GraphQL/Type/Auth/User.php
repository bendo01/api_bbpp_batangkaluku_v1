<?php
declare (strict_types = 1);

namespace Whiteboks\GraphQL\Type\Auth;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Type as GraphQLType;

class User extends GraphQLType
{
    protected $attributes = [
        'name' => 'AuthUser',
        'description' => 'A type'
    ];

    public function fields()
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'id field'
            ],
            'name' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'name field'
            ],
            'email' => [
                'type' => Type::string(),
                'description' => 'email field'
            ],
            'password' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'password field'
            ],
            'active' => [
                'type' => Type::nonNull(Type::boolean()),
                'description' => 'active field'
            ],
            'confirmed' => [
                'type' => Type::nonNull(Type::boolean()),
                'description' => 'confirmed field'
            ],
            'is_admin' => [
                'type' => Type::nonNull(Type::boolean()),
                'description' => 'confirmed field'
            ],
            'individual_id' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'individual_id field'
            ],
            'created_by' => [
                'type' => Type::string(),
                'description' => 'User Id Whom Created Data'
            ],
            'modified_by' => [
                'type' => Type::string(),
                'description' => 'User Id Whom Modified Data'
            ],
            'created_at' => [
                'type' => Type::string(),
                'description' => 'Time Created Data'
            ],
            'updated_at' => [
                'type' => Type::string(),
                'description' => 'Time Updated Data'
            ],
            'individual' => [
                'type' => GraphQL::type('PersonMasterIndividual'),
                'description' => 'Individual'
            ],
        ];
    }
}
