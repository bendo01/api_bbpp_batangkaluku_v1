<?php
declare (strict_types = 1);

namespace Whiteboks\GraphQL\Type\Module\HumanResource\Master;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Type as GraphQLType;

use Whiteboks\Model\Module\HumanResource\Master\Employee as Model;

class Employee extends GraphQLType
{
    protected $attributes = [
        'name' => 'ModuleHumanResourceMasterEmployee',
        'description' => 'Module Human Resource Master Employee',
        'model' => Model::class, // define model for users type
    ];

    public function fields()
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'The id of Model'
            ],
            'code' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'code field'
            ],
            'individual_id' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'individual_id field'
            ],
            'status_id' => [
                'type' => Type::string(),
                'description' => 'status_id field'
            ],
            'contract_type_id' => [
                'type' => Type::string(),
                'description' => 'contract_type_id field'
            ],
            'unit_id' => [
                'type' => Type::string(),
                'description' => 'unit_id field'
            ],
            'institution_id' => [
                'type' => Type::string(),
                'description' => 'institution_id field'
            ],
            'resign_type_id' => [
                'type' => Type::string(),
                'description' => 'resign_type_id field'
            ],
            'visa' => [
                'type' => Type::int(),
                'description' => 'visa field'
            ],
            'created_by' => [
                'type' => Type::string(),
                'description' => 'User Id Whom Created Data'
            ],
            'modified_by' => [
                'type' => Type::string(),
                'description' => 'User Id Whom Modified Data'
            ],
            'created_at' => [
                'type' => Type::string(),
                'description' => 'Time Created Data'
            ],
            'updated_at' => [
                'type' => Type::string(),
                'description' => 'Time Updated Data'
            ],
        ];
    }
}
