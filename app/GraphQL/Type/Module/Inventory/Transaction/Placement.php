<?php
declare (strict_types = 1);

namespace Whiteboks\GraphQL\Type\Module\Inventory\Transaction;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Type as GraphQLType;

use Whiteboks\Model\Module\Inventory\Transaction\Placement as Model;

class Placement extends GraphQLType
{
    protected $attributes = [
        'name' => 'ModuleInventoryTransactionPlacement',
        'description' => 'Module Inventory Transaction Placement',
        'model' => Model::class, // define model for users type
    ];

    public function fields()
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'The id of Model'
            ],
            'code' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'code field'
            ],
            'name' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'name field'
            ],
            'unit_id' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'unit_id field'
            ],
            'recident_room_id' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'recident_room_id field'
            ],
            'catalog_id' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'catalog_id field'
            ],
            'condition_id' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'condition_id field'
            ],
            'created_by' => [
                'type' => Type::string(),
                'description' => 'User Id Whom Created Data'
            ],
            'modified_by' => [
                'type' => Type::string(),
                'description' => 'User Id Whom Modified Data'
            ],
            'created_at' => [
                'type' => Type::string(),
                'description' => 'Time Created Data'
            ],
            'updated_at' => [
                'type' => Type::string(),
                'description' => 'Time Updated Data'
            ],
        ];
    }
}
