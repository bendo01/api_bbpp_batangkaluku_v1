<?php
declare (strict_types = 1);

namespace Whiteboks\GraphQL\Type\Module\Inventory\Master;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Type as GraphQLType;

use Whiteboks\Model\Module\Inventory\Master\Catalog as Model;

class Catalog extends GraphQLType
{
    protected $attributes = [
        'name' => 'ModuleInventoryMasterCatalog',
        'description' => 'Module Inventory Master Catalog',
        'model' => Model::class, // define model for users type
    ];

    public function fields()
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'The id of Model'
            ],
            'code' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'code field'
            ],
            'name' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'name field'
            ],
            'type_id' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'type_id field'
            ],
            'description' => [
                'type' => Type::string(),
                'description' => 'description field'
            ],
            'created_by' => [
                'type' => Type::string(),
                'description' => 'User Id Whom Created Data'
            ],
            'modified_by' => [
                'type' => Type::string(),
                'description' => 'User Id Whom Modified Data'
            ],
            'created_at' => [
                'type' => Type::string(),
                'description' => 'Time Created Data'
            ],
            'updated_at' => [
                'type' => Type::string(),
                'description' => 'Time Updated Data'
            ],
        ];
    }
}
