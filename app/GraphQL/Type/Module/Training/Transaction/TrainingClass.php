<?php
declare (strict_types = 1);
namespace Whiteboks\GraphQL\Type\Module\Training\Transaction;

use GraphQL\Type\Definition\Type;
use Illuminate\Support\Facades\DB;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Type as GraphQLType;

use Whiteboks\Model\Module\Training\Transaction\TrainingClass as Model;

class TrainingClass extends GraphQLType
{
    protected $attributes = [
        'name' => 'ModuleTrainingTransactionTrainingClass',
        'description' => 'Module Training Transaction Training Class',
        'model' => Model::class, // define model for users type
    ];

    public function fields()
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'The id of Model'
            ],
            'code' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'code id of Model'
            ],
            'name' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'name id of Model'
            ],
            'duration_hour' => [
                'type' => Type::int(),
                'description' => 'duration_hour id of Model'
            ],
            'duration_day' => [
                'type' => Type::int(),
                'description' => 'duration_day id of Model'
            ],
            'start_date' => [
                'type' => Type::string(),
                'description' => 'start_date field'
            ],
            'end_date' => [
                'type' => Type::string(),
                'description' => 'end_date field'
            ],
            'expanse' => [
                'type' => Type::string(),
                'description' => 'expanse field'
            ],
            'preparation' => [
                'type' => Type::string(),
                'description' => 'preparation field'
            ],
            'implementation' => [
                'type' => Type::string(),
                'description' => 'implementation field'
            ],
            'evaluation' => [
                'type' => Type::string(),
                'description' => 'evaluation field'
            ],
            'institution_id' => [
                'type' => Type::string(),
                'description' => 'institution_id field'
            ],
            'category_id' => [
                'type' => Type::string(),
                'description' => 'institution_id field'
            ],
            'created_by' => [
                'type' => Type::string(),
                'description' => 'User Id Whom Created Data'
            ],
            'modified_by' => [
                'type' => Type::string(),
                'description' => 'User Id Whom Modified Data'
            ],
            'created_at' => [
                'type' => Type::string(),
                'description' => 'Time Created Data'
            ],
            'updated_at' => [
                'type' => Type::string(),
                'description' => 'Time Updated Data'
            ],
            'institution' => [
                'type' => GraphQL::type('InstitutionMasterInstitution'),
                'description' => 'The institution'
            ],
            'category' => [
                'type' => GraphQL::type('ModuleTrainingReferenceLearningMaterialCategory'),
                'description' => 'category'
            ],
            'facilitators' => [
                'type' => Type::listOf(GraphQL::type('PersonMasterIndividual')),
                'description' => 'class facilitators',
                'selectable' => false
            ],
            'members' => [
                'type' => Type::listOf(GraphQL::type('PersonMasterIndividual')),
                'description' => 'members class',
                'selectable' => false
            ],
            'courses' => [
                'type' => Type::listOf(GraphQL::type('ModuleTrainingMasterCourse')),
                'description' => 'courses class',
                'selectable' => false
            ],
        ];
    }
}
