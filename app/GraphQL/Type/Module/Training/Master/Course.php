<?php
declare (strict_types = 1);
namespace Whiteboks\GraphQL\Type\Module\Training\Master;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Type as GraphQLType;

use Whiteboks\Model\Module\Training\Master\Course as Model;

class Course extends GraphQLType
{
    protected $attributes = [
        'name' => 'ModuleTrainingMasterCourse',
        'description' => 'Module Training Master Course',
        'model' => Model::class, // define model for users type
    ];

    public function fields()
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'The id of Model'
            ],
            'code' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'code field'
            ],
            'name' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'name field'
            ],
            'category_id' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'category_id field'
            ],
            'description' => [
                'type' => Type::string(),
                'description' => 'description field'
            ],
            'created_by' => [
                'type' => Type::string(),
                'description' => 'User Id Whom Created Data'
            ],
            'modified_by' => [
                'type' => Type::string(),
                'description' => 'User Id Whom Modified Data'
            ],
            'created_at' => [
                'type' => Type::string(),
                'description' => 'Time Created Data'
            ],
            'updated_at' => [
                'type' => Type::string(),
                'description' => 'Time Updated Data'
            ],
            'category' => [
                'type' => GraphQL::type('ModuleTrainingReferenceLearningMaterialCategory'),
                'description' => 'category course Package'
            ],
        ];
    }
}
