<?php
declare (strict_types = 1);
namespace Whiteboks\GraphQL\Type\Module\Training\Reference;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Type as GraphQLType;

use Whiteboks\Model\Module\Training\Reference\TrainingPackage as Model;

class TrainingPackage extends GraphQLType
{
    protected $attributes = [
        'name' => 'ModuleTrainingReferenceTrainingPackage',
        'description' => 'Module Training Reference TrainingPackage',
        'model' => Model::class, // define model for users type
    ];

    public function fields(): array
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'The id of Model'
            ],
            'code' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'code id of Model'
            ],
            'name' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'name id of Model'
            ],
            'description' => [
                'type' => Type::string(),
                'description' => 'name id of Model'
            ],
            'created_by' => [
                'type' => Type::string(),
                'description' => 'User Id Whom Created Data'
            ],
            'modified_by' => [
                'type' => Type::string(),
                'description' => 'User Id Whom Modified Data'
            ],
            'created_at' => [
                'type' => Type::string(),
                'description' => 'Time Created Data'
            ],
            'updated_at' => [
                'type' => Type::string(),
                'description' => 'Time Updated Data'
            ],
            'categories' => [
                'type' => Type::listOf(GraphQL::type('ModuleTrainingReferenceLearningMaterialCategory')),
                'description' => 'List Of Training Package',
                'always' => ['id', 'code', 'name'],
            ]
        ];
    }
}
