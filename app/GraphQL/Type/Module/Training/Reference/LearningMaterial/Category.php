<?php
declare (strict_types = 1);
namespace Whiteboks\GraphQL\Type\Module\Training\Reference\LearningMaterial;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Type as GraphQLType;

use Whiteboks\Model\Module\Training\Reference\LearningMaterial\Category as Model;

class Category extends GraphQLType
{
    protected $attributes = [
        'name' => 'ModuleTrainingReferenceLearningMaterialCategory',
        'description' => 'Module Training Reference Learning Material Category',
        'model' => Model::class, // define model for users type
    ];

    public function fields()
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'The id of Model'
            ],
            'code' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'code id of Model'
            ],
            'name' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'name id of Model'
            ],
            'training_package_id' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'training package id of Model'
            ],
            'parent_id' => [
                'type' => Type::string(),
                'description' => 'parent id of Model'
            ],
            'description' => [
                'type' => Type::string(),
                'description' => 'name id of Model'
            ],
            'created_by' => [
                'type' => Type::string(),
                'description' => 'User Id Whom Created Data'
            ],
            'modified_by' => [
                'type' => Type::string(),
                'description' => 'User Id Whom Modified Data'
            ],
            'created_at' => [
                'type' => Type::string(),
                'description' => 'Time Created Data'
            ],
            'updated_at' => [
                'type' => Type::string(),
                'description' => 'Time Updated Data'
            ],
            'trainingPackage' => [
                'type' => GraphQL::type('ModuleTrainingReferenceTrainingPackage'),
                'description' => 'Training Package'
            ],
            'trainingClasses' => [
                'type' => Type::listOf(GraphQL::type('ModuleTrainingTransactionTrainingClass')),
                'description' => 'List Of Training Package'
            ],
            'courses' => [
                'type' => Type::listOf(GraphQL::type('ModuleTrainingMasterCourse')),
                'description' => 'List Of Training Package'
            ]
        ];
    }
}
