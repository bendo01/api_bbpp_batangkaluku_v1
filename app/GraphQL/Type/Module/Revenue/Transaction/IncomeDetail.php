<?php
declare (strict_types = 1);
namespace Whiteboks\GraphQL\Type\Module\Revenue\Transaction;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Type as GraphQLType;

class IncomeDetail extends GraphQLType
{
    protected $attributes = [
        'name' => 'MoudleRevenueTransactionIncomeDetail',
        'description' => 'Moudle Revenue Transaction Income Detail'
    ];

    public function fields()
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'The id of Model'
            ],
            'title' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'The title of Model'
            ],
            'publish_date' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'The publish_date of Model'
            ],
            'amount' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'The amount of Model'
            ],
            'income_id' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'The income_id of Model'
            ],
            'created_by' => [
                'type' => Type::string(),
                'description' => 'User Id Whom Created Data'
            ],
            'modified_by' => [
                'type' => Type::string(),
                'description' => 'User Id Whom Modified Data'
            ],
            'created_at' => [
                'type' => Type::string(),
                'description' => 'Time Created Data'
            ],
            'updated_at' => [
                'type' => Type::string(),
                'description' => 'Time Updated Data'
            ],
        ];
    }
}
