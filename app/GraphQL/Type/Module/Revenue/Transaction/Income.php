<?php
declare (strict_types = 1);
namespace Whiteboks\GraphQL\Type\Module\Revenue\Transaction;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Type as GraphQLType;

class Income extends GraphQLType
{
    protected $attributes = [
        'name' => 'MoudleRevenueTransactionIncome',
        'description' => 'Moudle Revenue Transaction Income'
    ];

    public function fields()
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'The id of Model'
            ],
            'title' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'The title of Model'
            ],
            'year' => [
                'type' => Type::nonNull(Type::int()),
                'description' => 'The year of Model'
            ],
            'amount' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'The amount of Model'
            ],
            'unit_id' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'The unit_id of Model'
            ],
            'created_by' => [
                'type' => Type::string(),
                'description' => 'User Id Whom Created Data'
            ],
            'modified_by' => [
                'type' => Type::string(),
                'description' => 'User Id Whom Modified Data'
            ],
            'created_at' => [
                'type' => Type::string(),
                'description' => 'Time Created Data'
            ],
            'updated_at' => [
                'type' => Type::string(),
                'description' => 'Time Updated Data'
            ],
        ];
    }
}
