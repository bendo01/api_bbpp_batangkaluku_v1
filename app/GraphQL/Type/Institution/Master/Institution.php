<?php
declare (strict_types = 1);

namespace Whiteboks\GraphQL\Type\Institution\Master;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Type as GraphQLType;

class Institution extends GraphQLType
{
    protected $attributes = [
        'name' => 'InstitutionMasterInstitution',
        'description' => 'Institution Master Institution'
    ];

    public function fields()
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'The id of Model'
            ],
            'code' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'code field'
            ],
            'name' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'name field'
            ],
            'abbreviation' => [
                'type' => Type::string(),
                'description' => 'abbreviation field'
            ],
            'is_active' => [
                'type' => Type::boolean(),
                'description' => 'is_active field'
            ],
            'id_non_education' => [
                'type' => Type::string(),
                'description' => 'id_non_education field'
            ],
            'type_id' => [
                'type' => Type::string(),
                'description' => 'type_id field'
            ],
            'category_id' => [
                'type' => Type::string(),
                'description' => 'type_id field'
            ],
            'slug' => [
                'type' => Type::string(),
                'description' => 'slug field'
            ],
            'country_id' => [
                'type' => Type::string(),
                'description' => 'country_id field'
            ],
            'created_by' => [
                'type' => Type::string(),
                'description' => 'User Id Whom Created Data'
            ],
            'modified_by' => [
                'type' => Type::string(),
                'description' => 'User Id Whom Modified Data'
            ],
            'created_at' => [
                'type' => Type::string(),
                'description' => 'Time Created Data'
            ],
            'updated_at' => [
                'type' => Type::string(),
                'description' => 'Time Updated Data'
            ],
            'category' => [
                'type' => GraphQL::type('InstitutionReferenceCategory'),
                'description' => 'The Category of this Institution',
            ],
            'type' => [
                'type' => GraphQL::type('InstitutionReferenceInstitutionType'),
                'description' => 'The Type of this Institution',
            ],
        ];
    }
}
