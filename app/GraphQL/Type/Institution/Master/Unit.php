<?php
declare (strict_types = 1);

namespace Whiteboks\GraphQL\Type\Institution\Master;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Type as GraphQLType;

class Unit extends GraphQLType
{
    protected $attributes = [
        'name' => 'InstitutionMasterUnit',
        'description' => 'Institution Master Unit'
    ];

    public function fields()
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'The id of Model'
            ],
            'code' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'code field'
            ],
            'name' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'name field'
            ],
            'is_active' => [
                'type' => Type::int(),
                'description' => 'is_active field'
            ],
            'abbreviation' => [
                'type' => Type::string(),
                'description' => 'abbreviation field'
            ],
            'slug' => [
                'type' => Type::string(),
                'description' => 'slug field'
            ],
            'unit_type_id' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'unit_type_id field'
            ],
            'institution_id' => [
                'type' => Type::string(),
                'description' => 'institution_id field'
            ],
            'description' => [
                'type' => Type::string(),
                'description' => 'description field'
            ],
            'created_by' => [
                'type' => Type::string(),
                'description' => 'User Id Whom Created Data'
            ],
            'modified_by' => [
                'type' => Type::string(),
                'description' => 'User Id Whom Modified Data'
            ],
            'created_at' => [
                'type' => Type::string(),
                'description' => 'Time Created Data'
            ],
            'updated_at' => [
                'type' => Type::string(),
                'description' => 'Time Updated Data'
            ],
            '_lft' => [
                'type' => Type::nonNull(Type::int()),
                'description' => '_lft field'
            ],
            '_rgt' => [
                'type' => Type::nonNull(Type::int()),
                'description' => '_rgt field'
            ],
            'parent_id' => [
                'type' => Type::nonNull(Type::int()),
                'description' => 'parent_id field'
            ],
        ];
    }
}
