<?php
declare (strict_types = 1);

namespace Whiteboks\GraphQL\Type\Institution\Master;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Type as GraphQLType;

class Coordinator extends GraphQLType
{
    protected $attributes = [
        'name' => 'InstitutionMasterCoordinator',
        'description' => 'Institution Master Coordinator'
    ];

    public function fields()
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'The id of Model'
            ],
            'institution_id' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'institution_id field'
            ],
            'coordinator_type_id' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'coordinator_type_id field'
            ],
            'created_by' => [
                'type' => Type::string(),
                'description' => 'User Id Whom Created Data'
            ],
            'modified_by' => [
                'type' => Type::string(),
                'description' => 'User Id Whom Modified Data'
            ],
            'created_at' => [
                'type' => Type::string(),
                'description' => 'Time Created Data'
            ],
            'updated_at' => [
                'type' => Type::string(),
                'description' => 'Time Updated Data'
            ],
        ];
    }
}
