<?php
declare (strict_types = 1);
namespace Whiteboks\GraphQL\Type\Person\Reference\Career;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Type as GraphQLType;

class CivilServantBracket extends GraphQLType
{
    protected $attributes = [
        'name' => 'PersonReferenceCareerCivilServantBracket',
        'description' => 'A type'
    ];

    public function fields()
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'The id of Model'
            ],
            'code' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'The code of Model'
            ],
            'name' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'The name of Model'
            ],
            'group' => [
                'type' => Type::string(),
                'description' => 'The group of Model'
            ],
            'group_name' => [
                'type' => Type::string(),
                'description' => 'The group_name of Model'
            ],
            'rank' => [
                'type' => Type::int(),
                'description' => 'The rank of Model'
            ],
            'created_by' => [
                'type' => Type::string(),
                'description' => 'User Id Whom Created Data'
            ],
            'modified_by' => [
                'type' => Type::string(),
                'description' => 'User Id Whom Modified Data'
            ],
            'created_at' => [
                'type' => Type::string(),
                'description' => 'Time Created Data'
            ],
            'updated_at' => [
                'type' => Type::string(),
                'description' => 'Time Updated Data'
            ],
        ];
    }
}
