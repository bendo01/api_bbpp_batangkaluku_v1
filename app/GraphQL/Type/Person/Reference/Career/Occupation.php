<?php
declare (strict_types = 1);
namespace Whiteboks\GraphQL\Type\Person\Reference\Career;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Type as GraphQLType;

class Occupation extends GraphQLType
{
    protected $attributes = [
        'name' => 'PersonReferenceCareerOccupation',
        'description' => 'Person Reference Career Occupation'
    ];

    public function fields()
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'The id of Model'
            ],
            'code' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'The code of Model'
            ],
            'name' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'The name of Model'
            ],
            'created_by' => [
                'type' => Type::string(),
                'description' => 'User Id Whom Created Data'
            ],
            'modified_by' => [
                'type' => Type::string(),
                'description' => 'User Id Whom Modified Data'
            ],
            'created_at' => [
                'type' => Type::string(),
                'description' => 'Time Created Data'
            ],
            'updated_at' => [
                'type' => Type::string(),
                'description' => 'Time Updated Data'
            ],
        ];
    }
}
