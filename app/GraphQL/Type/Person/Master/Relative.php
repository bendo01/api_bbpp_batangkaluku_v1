<?php
declare (strict_types = 1);

namespace Whiteboks\GraphQL\Type\Person\Master;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Type as GraphQLType;

class Relative extends GraphQLType
{
    protected $attributes = [
        'name' => 'PersonMasterRelative',
        'description' => 'Person Master Relative'
    ];

    public function fields()
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'The id of Model'
            ],
            'individual_id' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'The individual_id of Model'
            ],
            'relative_id' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'The relative_id of Model'
            ],
            'relative_type_id' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'The relative_type_id of Model'
            ],
            'created_by' => [
                'type' => Type::string(),
                'description' => 'User Id Whom Created Data'
            ],
            'modified_by' => [
                'type' => Type::string(),
                'description' => 'User Id Whom Modified Data'
            ],
            'created_at' => [
                'type' => Type::string(),
                'description' => 'Time Created Data'
            ],
            'updated_at' => [
                'type' => Type::string(),
                'description' => 'Time Updated Data'
            ],
        ];
    }
}
