<?php
declare (strict_types = 1);

namespace Whiteboks\GraphQL\Type\Person\Master;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Type as GraphQLType;

class Individual extends GraphQLType
{
    protected $attributes = [
        'name' => 'PersonMasterIndividual',
        'description' => 'Person Master Individual'
    ];

    public function fields()
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'The id of Model'
            ],
            'code' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'code field'
            ],
            'name' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'name field'
            ],
            'birth_date' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'birth_date field'
            ],
            'birth_place' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'birth_place field'
            ],
            'birth_place_id' => [
                'type' => Type::string(),
                'description' => 'birth_place_id field'
            ],
            'gender_id' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'gender_id field'
            ],
            'religion_id' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'religion_id field'
            ],
            'occupation_id' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'occupation_id field'
            ],
            'education_id' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'education_id field'
            ],
            'income_id' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'income_id field'
            ],
            'identification_type_id' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'identification_type_id field'
            ],
            'marital_status_id' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'marital_status_id field'
            ],
            'profession_id' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'profession_id field'
            ],
            'deceased' => [
                'type' => Type::nonNull(Type::boolean()),
                'description' => 'deceased field'
            ],
            'country_id' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'country_id field'
            ],
            'created_by' => [
                'type' => Type::string(),
                'description' => 'User Id Whom Created Data'
            ],
            'modified_by' => [
                'type' => Type::string(),
                'description' => 'User Id Whom Modified Data'
            ],
            'created_at' => [
                'type' => Type::string(),
                'description' => 'Time Created Data'
            ],
            'updated_at' => [
                'type' => Type::string(),
                'description' => 'Time Updated Data'
            ],
            'gender' => [
                'type' => GraphQL::type('PersonReferenceBiodataGender'),
                'description' => 'Gender'
            ],
            'identificationType' => [
                'type' => GraphQL::type('PersonReferenceBiodataIdentificationType'),
                'description' => 'Identification Type'
            ],
            'maritalStatus' => [
                'type' => GraphQL::type('PersonReferenceBiodataMaritalStatus'),
                'description' => 'Identification Type'
            ],
            'occupation' => [
                'type' => GraphQL::type('PersonReferenceCareerOccupation'),
                'description' => 'occupation Type'
            ],
            'profession' => [
                'type' => GraphQL::type('PersonReferenceCareerProfession'),
                'description' => 'profession Type'
            ],
            'religion' => [
                'type' => GraphQL::type('PersonReferenceBiodataReligion'),
                'description' => 'religion Type'
            ],
            'education' => [
                'type' => GraphQL::type('CommonReferenceEducationEducation'),
                'description' => 'education Type'
            ],
            'user' => [
                'type' => GraphQL::type('AuthUser'),
                'description' => 'education Type'
            ],
            'members' => [
                'type' => Type::listOf(GraphQL::type('ModuleTrainingTransactionTrainingClass')),
                'description' => 'member class',
                'selectable' => false
            ],
            'facilitators' => [
                'type' => Type::listOf(GraphQL::type('ModuleTrainingTransactionTrainingClass')),
                'description' => 'facilitator class',
                'selectable' => false
            ],
            'residencies' => [
                'type' => Type::listOf(GraphQL::type('CommonMasterResidence')),
                'description' => 'residences',
                'selectable' => false
            ],
        ];
    }
}
