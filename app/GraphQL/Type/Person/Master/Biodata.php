<?php
declare (strict_types = 1);

namespace Whiteboks\GraphQL\Type\Person\Master;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Type as GraphQLType;

class Biodata extends GraphQLType
{
    protected $attributes = [
        'name' => 'PersonMasterBiodata',
        'description' => 'Person Master Biodata'
    ];

    public function fields()
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'The id of Model'
            ],
            'height' => [
                'type' => Type::nonNull(Type::int()),
                'description' => 'weight field'
            ],
            'weight' => [
                'type' => Type::nonNull(Type::int()),
                'description' => 'weight field'
            ],
            'positive_blood_rhesus' => [
                'type' => Type::nonNull(Type::int()),
                'description' => 'positive_blood_rhesus field'
            ],
            'blood_type_id' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'blood_type_id field'
            ],
            'hair_type_id' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'hair_type_id field'
            ],
            'hair_color_id' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'hair_color_id field'
            ],
            'eye_color_type_id' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'eye_color_type_id field'
            ],
            'individual_id' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'individual_id field'
            ],
            'created_by' => [
                'type' => Type::string(),
                'description' => 'User Id Whom Created Data'
            ],
            'modified_by' => [
                'type' => Type::string(),
                'description' => 'User Id Whom Modified Data'
            ],
            'created_at' => [
                'type' => Type::string(),
                'description' => 'Time Created Data'
            ],
            'updated_at' => [
                'type' => Type::string(),
                'description' => 'Time Updated Data'
            ],
        ];
    }
}
