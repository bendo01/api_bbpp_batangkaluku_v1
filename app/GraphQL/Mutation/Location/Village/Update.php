<?php
declare (strict_types = 1);
namespace Whiteboks\GraphQL\Mutation\Location\Village;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;

use Whiteboks\Model\Location\Village as Model;

class Update extends Mutation
{
    protected $attributes = [
        'name' => 'UpdateLocationVillage',
        'description' => 'Update Location Village',
        'model' => Model::class, // define model for users type
    ];
    public function type()
    {
        return GraphQL::type('LocationVillage');
    }
    public function args(): array
    {
        return [
            'id' => [
                'name' => 'id',
                'type' => Type::nonNull(Type::string())
            ],
            'code' => [
                'name' => 'code',
                'type' => Type::nonNull(Type::string())
            ],
            'name' => [
                'name' => 'name',
                'type' => Type::nonNull(Type::string())
            ],
            'sub_district_id' => [
                'name' => 'sub_district_id',
                'type' => Type::nonNull(Type::string())
            ],
            'slug' => [
                'name' => 'slug',
                'type' => Type::string()
            ],
            'alt_slug' => [
                'name' => 'alt_slug',
                'type' => Type::string()
            ],
            'state_ministry_code' => [
                'name' => 'state_ministry_code',
                'type' => Type::string()
            ],
            'state_ministry_full_code' => [
                'name' => 'state_ministry_full_code',
                'type' => Type::string()
            ],
            'state_post_department_code' => [
                'name' => 'state_post_department_code',
                'type' => Type::string()
            ],
            'state_ministry_name' => [
                'name' => 'state_ministry_name',
                'type' => Type::string()
            ],
            'dikti_name' => [
                'name' => 'dikti_name',
                'type' => Type::string()
            ],
        ];
    }
    public function rules(): array
    {
        return [
            'id' => ['required'],
            'code' => ['required'],
            'name' => ['required'],
            'sub_district_id' => ['required'],
        ];
    }
    public function resolve($root, $args): Model
    {
        $model = Model::find($args['id']);
        if (!$model) {
            return null;
        }
        $model->code = $args['code'];
        $model->name = $args['name'];
        $model->sub_district_id = $args['sub_district_id'];
        if (isset($args['slug']) && !empty($args['slug'])) {
            $model->slug = $args['slug'];
        }
        if (isset($args['alt_slug']) && !empty($args['alt_slug'])) {
            $model->alt_slug = $args['alt_slug'];
        }
        if (isset($args['state_ministry_code']) && !empty($args['state_ministry_code'])) {
            $model->state_ministry_code = $args['state_ministry_code'];
        }
        if (isset($args['state_ministry_full_code']) && !empty($args['state_ministry_full_code'])) {
            $model->state_ministry_full_code = $args['state_ministry_full_code'];
        }
        if (isset($args['state_post_department_code']) && !empty($args['state_post_department_code'])) {
            $model->state_post_department_code = $args['state_post_department_code'];
        }
        if (isset($args['state_ministry_name']) && !empty($args['state_ministry_name'])) {
            $model->state_ministry_name = $args['state_ministry_name'];
        }
        if (isset($args['dikti_name']) && !empty($args['dikti_name'])) {
            $model->dikti_name = $args['dikti_name'];
        }
        $model->save();
        return $model;
    }
}
