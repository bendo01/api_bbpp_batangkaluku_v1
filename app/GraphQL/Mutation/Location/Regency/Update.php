<?php
declare (strict_types = 1);
namespace Whiteboks\GraphQL\Mutation\Location\Regency;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use Whiteboks\Model\Location\Regency as Model;

class Update extends Mutation
{
    protected $attributes = [
        'name' => 'UpdateLocationRegency',
        'description' => 'Update Location Regency',
        'model' => Model::class, // define model for users type
    ];
    public function type()
    {
        return GraphQL::type('LocationRegency');
    }
    public function args(): array
    {
        return [
            'id' => [
                'name' => 'id',
                'type' => Type::nonNull(Type::string())
            ],
            'code' => [
                'name' => 'code',
                'type' => Type::string()
            ],
            'name' => [
                'name' => 'name',
                'type' => Type::nonNull(Type::string())
            ],
            'dikti_code' => [
                'name' => 'dikti_code',
                'type' => Type::nonNull(Type::string())
            ],
            'province_id' => [
                'name' => 'province_id',
                'type' => Type::nonNull(Type::string())
            ],
        ];
    }
    public function rules(): array
    {
        return [
            'id' => ['required'],
            'code' => ['required'],
            'name' => ['required'],
            'country_id' => ['required'],
            'dikti_code' => ['required'],
        ];
    }
    public function resolve($root, $args): Model
    {
        $model = Model::find($args['id']);
        if (!$model) {
            return null;
        }
        $model->name = $args['name'];
        $model->province_id = $args['province_id'];
        $model->regency_type_id = $args['regency_type_id'];
        if (isset($args['code']) && !empty($args['code'])) {
            $model->code = $args['code'];
        }
        if (isset($args['slug']) && !empty($args['slug'])) {
            $model->slug = $args['slug'];
        }
        if (isset($args['description']) && !empty($args['description'])) {
            $model->description = $args['description'];
        }
        if (isset($args['alt_slug']) && !empty($args['alt_slug'])) {
            $model->alt_slug = $args['alt_slug'];
        }
        if (isset($args['state_ministry_code']) && !empty($args['state_ministry_code'])) {
            $model->state_ministry_code = $args['state_ministry_code'];
        }
        if (isset($args['state_ministry_full_code']) && !empty($args['state_ministry_full_code'])) {
            $model->state_ministry_full_code = $args['state_ministry_full_code'];
        }
        if (isset($args['state_post_department_code']) && !empty($args['state_post_department_code'])) {
            $model->state_post_department_code = $args['state_post_department_code'];
        }
        if (isset($args['state_ministry_name']) && !empty($args['state_ministry_name'])) {
            $model->state_ministry_name = $args['state_ministry_name'];
        }
        if (isset($args['dikti_name']) && !empty($args['dikti_name'])) {
            $model->dikti_name = $args['dikti_name'];
        }
        if (isset($args['validation_code']) && !empty($args['validation_code'])) {
            $model->validation_code = $args['validation_code'];
        }
        if (isset($args['agriculture_department_code']) && !empty($args['agriculture_department_code'])) {
            $model->agriculture_department_code = $args['agriculture_department_code'];
        }
        $model->save();
        return $model;
    }
}
