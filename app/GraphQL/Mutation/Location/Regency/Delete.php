<?php
declare (strict_types = 1);
namespace Whiteboks\GraphQL\Mutation\Location\Regency;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use Whiteboks\Model\Location\Regency as Model;

class Delete extends Mutation
{
    protected $attributes = [
        'name' => 'LocationRegencyDelete',
        'description' => 'Location Regency Delete',
        'model' => Model::class, // define model for users type
    ];

    public function type()
    {
        return GraphQL::type('LocationRegency');
    }

    public function args(): array
    {
        return [
            'id' => [
                'name' => 'id',
                'type' => Type::nonNull(Type::string())
            ],
        ];
    }

    public function rules(): array
    {
        return [
            'id' => ['required'],
        ];
    }

    public function resolve($root, $args): Model
    {
        $model = Model::find($args['id']);
        if (!$model) {
            return null;
        }
        $model->delete();
        return $model;
    }
}
