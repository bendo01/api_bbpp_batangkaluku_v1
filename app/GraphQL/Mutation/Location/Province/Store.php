<?php
declare (strict_types = 1);
namespace Whiteboks\GraphQL\Mutation\Location\Province;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use Whiteboks\Model\Location\Province as Model;

class Store extends Mutation
{
    protected $attributes = [
        'name' => 'StoreLocationProvince',
        'description' => 'Store Location Province',
        'model' => Model::class, // define model for users type
    ];
    public function type()
    {
        return GraphQL::type('LocationProvince');
    }
    public function args(): array
    {
        return [
            'code' => [
                'name' => 'code',
                'type' => Type::nonNull(Type::string())
            ],
            'name' => [
                'name' => 'name',
                'type' => Type::nonNull(Type::string())
            ],
            'dikti_code' => [
                'name' => 'dikti_code',
                'type' => Type::nonNull(Type::string())
            ],
            'country_id' => [
                'name' => 'continent_id',
                'type' => Type::nonNull(Type::string())
            ],
            'epsbed_code' => [
                'name' => 'epsbed_code',
                'type' => Type::string()
            ],
            'slug' => [
                'name' => 'slug',
                'type' => Type::string()
            ],
            'description' => [
                'name' => 'description',
                'type' => Type::string()
            ],
            'alt_slug' => [
                'name' => 'alt_slug',
                'type' => Type::string()
            ],
            'state_ministry_code' => [
                'name' => 'state_ministry_code',
                'type' => Type::string()
            ],
            'state_ministry_full_code' => [
                'name' => 'state_ministry_full_code',
                'type' => Type::string()
            ],
            'state_post_department_code' => [
                'name' => 'state_post_department_code',
                'type' => Type::string()
            ],
            'state_ministry_name' => [
                'name' => 'state_ministry_name',
                'type' => Type::string()
            ],
            'dikti_name' => [
                'name' => 'dikti_name',
                'type' => Type::string()
            ],
            'validation_code' => [
                'name' => 'validation_code',
                'type' => Type::string()
            ],
        ];
    }
    public function rules(): array
    {
        return [
            'code' => ['required'],
            'name' => ['required'],
            'country_id' => ['required'],
            'dikti_code' => ['required'],
        ];
    }
    public function resolve($root, $args): Model
    {
        $model = new Model;
        $model->code = $args['code'];
        $model->name = $args['name'];
        $model->country_id = $args['country_id'];
        $model->dikti_code = $args['dikti_code'];
        if (isset($args['epsbed_code']) && !empty($args['epsbed_code'])) {
            $model->epsbed_code = $args['epsbed_code'];
        }
        if (isset($args['slug']) && !empty($args['slug'])) {
            $model->slug = $args['slug'];
        }
        if (isset($args['description']) && !empty($args['description'])) {
            $model->description = $args['description'];
        }
        if (isset($args['alt_slug']) && !empty($args['alt_slug'])) {
            $model->alt_slug = $args['alt_slug'];
        }
        if (isset($args['state_ministry_code']) && !empty($args['state_ministry_code'])) {
            $model->state_ministry_code = $args['state_ministry_code'];
        }
        if (isset($args['state_ministry_full_code']) && !empty($args['state_ministry_full_code'])) {
            $model->state_ministry_full_code = $args['state_ministry_full_code'];
        }
        if (isset($args['state_post_department_code']) && !empty($args['state_post_department_code'])) {
            $model->state_post_department_code = $args['state_post_department_code'];
        }
        if (isset($args['state_ministry_name']) && !empty($args['state_ministry_name'])) {
            $model->state_ministry_name = $args['state_ministry_name'];
        }
        if (isset($args['dikti_name']) && !empty($args['dikti_name'])) {
            $model->dikti_name = $args['dikti_name'];
        }
        if (isset($args['validation_code']) && !empty($args['validation_code'])) {
            $model->validation_code = $args['validation_code'];
        }
        $model->save();
        return $model;
    }
}
