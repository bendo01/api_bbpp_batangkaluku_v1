<?php
declare (strict_types = 1);
namespace Whiteboks\GraphQL\Mutation\Location\SubDistrict;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use Whiteboks\Model\Location\SubDistrict as Model;

class Delete extends Mutation
{
    protected $attributes = [
        'name' => 'LocationSubDistrictDelete',
        'description' => 'Location Sub District Delete',
        'model' => Model::class, // define model for users type
    ];

    public function type()
    {
        return GraphQL::type('LocationSubDistrict');
    }

    public function args(): array
    {
        return [
            'id' => [
                'name' => 'id',
                'type' => Type::nonNull(Type::string())
            ],
        ];
    }

    public function rules(): array
    {
        return [
            'id' => ['required'],
        ];
    }

    public function resolve($root, $args): Model
    {
        $model = Model::find($args['id']);
        if (!$model) {
            return null;
        }
        $model->delete();
        return $model;
    }
}
