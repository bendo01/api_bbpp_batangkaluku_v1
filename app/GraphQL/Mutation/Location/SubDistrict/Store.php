<?php
declare (strict_types = 1);
namespace Whiteboks\GraphQL\Mutation\Location\SubDistrict;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;

use Whiteboks\Model\Location\SubDistrict as Model;

class Store extends Mutation
{
    protected $attributes = [
        'name' => 'StoreLocationSubDistrict',
        'description' => 'Store Location SubDistrict',
        'model' => Model::class, // define model for users type
    ];
    public function type()
    {
        return GraphQL::type('LocationSubDistrict');
    }
    public function args(): array
    {
        return [
            'code' => [
                'name' => 'code',
                'type' => Type::string()
            ],
            'name' => [
                'name' => 'name',
                'type' => Type::nonNull(Type::string())
            ],
            'dikti_code' => [
                'name' => 'dikti_code',
                'type' => Type::string()
            ],
            'regency_id' => [
                'name' => 'regency_id',
                'type' => Type::nonNull(Type::string())
            ],
            'slug' => [
                'name' => 'slug',
                'type' => Type::string()
            ],
            'alt_slug' => [
                'name' => 'alt_slug',
                'type' => Type::string()
            ],
            'state_ministry_code' => [
                'name' => 'state_ministry_code',
                'type' => Type::string()
            ],
            'state_ministry_code' => [
                'name' => 'state_ministry_code',
                'type' => Type::string()
            ],
            'state_ministry_full_code' => [
                'name' => 'state_ministry_full_code',
                'type' => Type::string()
            ],
            'state_post_department_code' => [
                'name' => 'state_post_department_code',
                'type' => Type::string()
            ],
            'state_ministry_name' => [
                'name' => 'state_ministry_name',
                'type' => Type::string()
            ],
            'dikti_name' => [
                'name' => 'dikti_name',
                'type' => Type::string()
            ],
            'validation_code' => [
                'name' => 'validation_code',
                'type' => Type::string()
            ],
            'agriculture_department_name' => [
                'name' => 'agriculture_department_name',
                'type' => Type::string()
            ],
        ];
    }
    public function rules(): array
    {
        return [
            'code' => ['code'],
            'name' => ['required'],
            'regency_id' => ['required'],
        ];
    }
    public function resolve($root, $args): Model
    {
        $model = new Model;
        $model->name = $args['name'];
        $model->regency_id = $args['regency_id'];
        if (isset($args['code']) && !empty($args['code'])) {
            $model->code = $args['code'];
        }
        if (isset($args['slug']) && !empty($args['slug'])) {
            $model->slug = $args['slug'];
        }
        if (isset($args['alt_slug']) && !empty($args['alt_slug'])) {
            $model->alt_slug = $args['alt_slug'];
        }
        if (isset($args['state_ministry_code']) && !empty($args['state_ministry_code'])) {
            $model->state_ministry_code = $args['state_ministry_code'];
        }
        if (isset($args['state_ministry_full_code']) && !empty($args['state_ministry_full_code'])) {
            $model->state_ministry_full_code = $args['state_ministry_full_code'];
        }
        if (isset($args['state_post_department_code']) && !empty($args['state_post_department_code'])) {
            $model->state_post_department_code = $args['state_post_department_code'];
        }
        if (isset($args['state_ministry_name']) && !empty($args['state_ministry_name'])) {
            $model->state_ministry_name = $args['state_ministry_name'];
        }
        if (isset($args['dikti_name']) && !empty($args['dikti_name'])) {
            $model->dikti_name = $args['dikti_name'];
        }
        if (isset($args['validation_code']) && !empty($args['validation_code'])) {
            $model->validation_code = $args['validation_code'];
        }
        if (isset($args['agriculture_department_code']) && !empty($args['agriculture_department_code'])) {
            $model->agriculture_department_code = $args['agriculture_department_code'];
        }
        $model->save();
        return $model;
    }
}
