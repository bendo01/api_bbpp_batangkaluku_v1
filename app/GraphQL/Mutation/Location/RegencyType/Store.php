<?php
declare (strict_types = 1);
namespace Whiteboks\GraphQL\Mutation\Location\RegencyType;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use Whiteboks\Model\Location\RegencyType as Model;

class Store extends Mutation
{
    protected $attributes = [
        'name' => 'StoreLocationRegencyType',
        'description' => 'Store Location Regency Type',
        'model' => Model::class, // define model for users type
    ];
    public function type()
    {
        return GraphQL::type('LocationRegencyType');
    }
    public function args(): array
    {
        return [
            'code' => [
                'name' => 'code',
                'type' => Type::nonNull(Type::string())
            ],
            'name' => [
                'name' => 'name',
                'type' => Type::nonNull(Type::string())
            ],

        ];
    }
    public function rules(): array
    {
        return [
            'code' => ['code'],
            'name' => ['required'],
        ];
    }
    public function resolve($root, $args): Model
    {
        $model = new Model;
        $model->code = $args['code'];
        $model->name = $args['name'];
        $model->save();
        return $model;
    }
}
