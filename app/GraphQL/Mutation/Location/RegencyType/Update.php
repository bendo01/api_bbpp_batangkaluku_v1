<?php
declare (strict_types = 1);
namespace Whiteboks\GraphQL\Mutation\Location\RegencyType;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use Whiteboks\Model\Location\RegencyType as Model;

class Update extends Mutation
{
    protected $attributes = [
        'name' => 'UpdateLocationRegency',
        'description' => 'Update Location Regency Type',
        'model' => Model::class, // define model for users type
    ];
    public function type()
    {
        return GraphQL::type('LocationRegencyType');
    }
    public function args(): array
    {
        return [
            'id' => [
                'name' => 'id',
                'type' => Type::nonNull(Type::string())
            ],
            'code' => [
                'name' => 'code',
                'type' => Type::nonNull(Type::string())
            ],
            'name' => [
                'name' => 'name',
                'type' => Type::nonNull(Type::string())
            ],
        ];
    }
    public function rules(): array
    {
        return [
            'id' => ['required'],
            'code' => ['required'],
            'name' => ['required'],
        ];
    }
    public function resolve($root, $args): Model
    {
        $model = Model::find($args['id']);
        if (!$model) {
            return null;
        }
        $model->code = $args['code'];
        $model->name = $args['name'];
        $model->save();
        return $model;
    }
}
