<?php
declare (strict_types = 1);
namespace Whiteboks\GraphQL\Mutation\Location\Country;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use Whiteboks\Model\Location\Country as Model;

class Update extends Mutation
{
    protected $attributes = [
        'name' => 'UpdateLocationCountry',
        'description' => 'Update Location Country',
        'model' => Model::class, // define model for users type
    ];
    public function type()
    {
        return GraphQL::type('LocationCountry');
    }
    public function args(): array
    {
        return [
            'id' => [
                'name' => 'id',
                'type' => Type::nonNull(Type::string())
            ],
            'code' => [
                'name' => 'code',
                'type' => Type::nonNull(Type::string())
            ],
            'name' => [
                'name' => 'name',
                'type' => Type::nonNull(Type::string())
            ],
            'alpha2_code' => [
                'name' => 'alpha2_code',
                'type' => Type::nonNull(Type::string())
            ],
            'alpha3_code' => [
                'name' => 'alpha3_code',
                'type' => Type::nonNull(Type::string())
            ],
            'dikti_code' => [
                'name' => 'iso3166_2_code',
                'type' => Type::nonNull(Type::string())
            ],
            'continent_id' => [
                'name' => 'continent_id',
                'type' => Type::string()
            ],
            'region_id' => [
                'name' => 'region_id',
                'type' => Type::string()
            ],
            'slug' => [
                'name' => 'slug',
                'type' => Type::string()
            ],
        ];
    }
    public function rules(): array
    {
        return [
            'id' => ['required'],
            'code' => ['required'],
            'name' => ['required'],
            'alpha2_code' => ['required'],
            'alpha3_code' => ['required'],
            'iso3166_2_code' => ['required'],
            'dikti_code' => ['required'],
        ];
    }
    public function resolve($root, $args): Model
    {
        $model = Model::find($args['id']);
        if (!$model) {
            return null;
        }
        $model->name = $args['name'];
        $model->alpha2_code = $args['alpha2_code'];
        $model->alpha3_code = $args['alpha3_code'];
        $model->iso3166_2_code = $args['iso3166_2_code'];
        $model->dikti_code = $args['dikti_code'];
        if (isset($args['continent_id']) && !empty($args['continent_id'])) {
            $model->continent_id = $args['continent_id'];
        }
        if (isset($args['region_id']) && !empty($args['region_id'])) {
            $model->region_id = $args['region_id'];
        }
        if (isset($args['slug']) && !empty($args['slug'])) {
            $model->slug = $args['slug'];
        }
        $model->save();
        return $model;
    }
}
