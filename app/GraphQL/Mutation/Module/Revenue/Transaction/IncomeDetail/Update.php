<?php
declare (strict_types = 1);
namespace Whiteboks\GraphQL\Mutation\Module\Revenue\Transaction\IncomeDetail;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use Whiteboks\Model\Module\Revenue\Transaction\IncomeDetail as Model;

class Update extends Mutation
{
    protected $attributes = [
        'name' => 'ModuleRevenueTransactionIncomeDetailUpdate',
        'description' => 'Module Revenue Transaction Income Detail Update',
        'model' => Model::class,
    ];

    public function type()
    {
        return GraphQL::type('ModuleRevenueTransactionIncomeDetail');
    }

    public function args(): array
    {
        return [
            'id' => [
                'name' => 'id',
                'type' => Type::nonNull(Type::string())
            ],
            'title' => [
                'name' => 'title',
                'type' => Type::nonNull(Type::string())
            ],
            'publish_date' => [
                'name' => 'publish_date',
                'type' => Type::nonNull(Type::string())
            ],
            'amount' => [
                'name' => 'amount',
                'type' => Type::nonNull(Type::string())
            ],
            'income_id' => [
                'name' => 'income_id',
                'type' => Type::nonNull(Type::string())
            ],
        ];
    }
    public function rules(): array
    {
        return [
            'id'           => ['required'],
            'title'        => ['required'],
            'publish_date' => ['required'],
            'amount'       => ['required'],
            'income_id'    => ['required'],
        ];
    }
    public function resolve($root, $args): Model
    {
        $model = Model::find($args['id']);
        if (!$model) {
            return null;
        }
        $model->title   = $args['title'];
        $model->publish_date    = $args['publish_date'];
        $model->amount  = $args['amount'];
        $model->income_id = $args['income_id'];
        $model->save();
        return $model;
    }
}
