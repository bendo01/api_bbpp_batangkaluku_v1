<?php
declare (strict_types = 1);
namespace Whiteboks\GraphQL\Mutation\Module\Revenue\Transaction\IncomeDetail;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use Whiteboks\Model\Module\Revenue\Transaction\IncomeDetail as Model;

class Delete extends Mutation
{
    protected $attributes = [
        'name' => 'ModuleRevenueTransactionIncomeDetailDelete',
        'description' => 'Module Revenue Transaction Income Detail Delete',
        'model' => Model::class,
    ];

    public function type()
    {
        return GraphQL::type('ModuleRevenueTransactionIncomeDetail');
    }

    public function args(): array
    {
        return [
            'id' => [
                'name' => 'id',
                'type' => Type::nonNull(Type::string())
            ],
        ];
    }
    public function rules(): array
    {
        return [
            'id' => ['required'],
        ];
    }
    public function resolve($root, $args): Model
    {
        $model = Model::find($args['id']);
        if (!$model) {
            return null;
        }
        $model->delete();
        return $model;
    }
}
