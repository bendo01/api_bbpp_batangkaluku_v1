<?php
declare (strict_types = 1);
namespace Whiteboks\GraphQL\Mutation\Module\Revenue\Transaction\Income;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use Whiteboks\Model\Module\Revenue\Transaction\Income as Model;

class Update extends Mutation
{
    protected $attributes = [
        'name' => 'ModuleRevenueTransactionIncomeUpdate',
        'description' => 'Module Revenue Transaction Income Update',
        'model' => Model::class,
    ];

    public function type()
    {
        return GraphQL::type('ModuleRevenueTransactionIncome');
    }

    public function args(): array
    {
        return [
            'id' => [
                'name' => 'id',
                'type' => Type::nonNull(Type::string())
            ],
            'title' => [
                'name' => 'title',
                'type' => Type::nonNull(Type::string())
            ],
            'year' => [
                'name' => 'year',
                'type' => Type::nonNull(Type::int())
            ],
            'amount' => [
                'name' => 'amount',
                'type' => Type::nonNull(Type::string())
            ],
            'unit_id' => [
                'name' => 'unit_id',
                'type' => Type::nonNull(Type::string())
            ],
        ];
    }
    public function rules(): array
    {
        return [
            'id'      => ['required'],
            'title'   => ['required'],
            'year'    => ['required'],
            'amount'  => ['required'],
            'unit_id' => ['required'],
        ];
    }
    public function resolve($root, $args): Model
    {
        $model = Model::find($args['id']);
        if (!$model) {
            return null;
        }
        $model->title   = $args['title'];
        $model->year    = $args['year'];
        $model->amount  = $args['amount'];
        $model->unit_id = $args['unit_id'];
        $model->save();
        return $model;
    }
}
