<?php
declare (strict_types = 1);
namespace Whiteboks\GraphQL\Mutation\Module\Revenue\Transaction\Income;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use Whiteboks\Model\Module\Revenue\Transaction\Income as Model;

class Delete extends Mutation
{
    protected $attributes = [
        'name' => 'ModuleRevenueTransactionIncomeDelete',
        'description' => 'Module Revenue Transaction Income Delete',
        'model' => Model::class,
    ];

    public function type(): array
    {
        return GraphQL::type('ModuleRevenueTransactionIncome');
    }

    public function args()
    {
        return [
            'id' => [
                'name' => 'id',
                'type' => Type::nonNull(Type::string())
            ],
        ];
    }

    public function rules(): array
    {
        return [
            'id' => ['required'],
        ];
    }
    public function resolve($root, $args): Model
    {
        $model = Model::find($args['id']);
        if (!$model) {
            return null;
        }
        $model->delete();
        return $model;
    }
}
