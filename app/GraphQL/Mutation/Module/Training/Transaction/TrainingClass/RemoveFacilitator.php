<?php
declare (strict_types = 1);
namespace Whiteboks\GraphQL\Mutation\Module\Training\Transaction\TrainingClass;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use Whiteboks\Model\Module\Training\Transaction\TrainingClass as Model;

class RemoveFacilitator extends Mutation
{
    protected $attributes = [
        'name' => 'TrainingTransactionTrainingClassAddFacilitator',
        'description' => 'Training Transaction Training Class Remove Facilitator',
        'model' => Model::class,
    ];

    public function type()
    {
        return GraphQL::type('ModuleTrainingTransactionTrainingClass');
    }

    public function args(): array
    {
        return [
            'id' => [
                'name' => 'id',
                'type' => Type::nonNull(Type::string())
            ],
            'individual_id' => [
                'name' => 'individual_id',
                'type' => Type::nonNull(Type::string())
            ],
        ];
    }

    public function rules(): array
    {
        return [
            'id' => ['required'],
            'individual_id' => ['required'],
        ];
    }
    public function resolve($root, $args): Model
    {
        $model = Model::find($args['id']);
        if (!$model) {
            return null;
        }
        $model->facilitators()->detach($args['individual_id']);
        return $model;
    }
}
