<?php
declare (strict_types = 1);
namespace Whiteboks\GraphQL\Mutation\Module\Training\Transaction\TrainingClass;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use Whiteboks\Model\Module\Training\Master\Course;
use Whiteboks\Model\Module\Training\Transaction\TrainingClass as Model;

class Store extends Mutation
{
    protected $attributes = [
        'name' => 'ModuleTrainingTransactionTrainingClassStore',
        'description' => 'Module Training Transaction Training Class Store',
        'model' => Model::class,
    ];

    public function type()
    {
        return GraphQL::type('ModuleTrainingTransactionTrainingClass');
    }

    public function args(): array
    {
        return [
            'code' => [
                'name' => 'code',
                'type' => Type::nonNull(Type::string())
            ],
            'name' => [
                'name' => 'name',
                'type' => Type::nonNull(Type::string())
            ],
            'expanse' => [
                'name' => 'expanse',
                'type' => Type::nonNull(Type::string())
            ],
            'duration_hour' => [
                'name' => 'duration_hour',
                'type' => Type::nonNull(Type::int())
            ],
            'duration_day' => [
                'name' => 'duration_day',
                'type' => Type::int()
            ],
            'start_date' => [
                'name' => 'start_date',
                'type' => Type::nonNull(Type::string())
            ],
            'end_date' => [
                'name' => 'end_date',
                'type' => Type::nonNull(Type::string())
            ],
            'preparation' => [
                'name' => 'preparation',
                'type' => Type::string()
            ],
            'implementation' => [
                'name' => 'implementation',
                'type' => Type::string()
            ],
            'evaluation' => [
                'name' => 'evaluation',
                'type' => Type::string()
            ],
            'institution_id' => [
                'name' => 'institution_id',
                'type' => Type::string()
            ],
            'category_id' => [
                'name' => 'category_id',
                'type' => Type::string()
            ],
        ];
    }
    public function rules(): array
    {
        return [
            'code'           => ['required'],
            'name'           => ['required'],
            'institution_id' => ['required'],
            'category_id'    => ['required'],
            'expanse'        => ['required', 'numeric'],
            'duration_hour'  => ['required', 'integer'],
            'start_date'     => ['required', 'date_format:d-m-Y'],
            'end_date'       => ['required', 'date_format:d-m-Y'],
        ];
    }
    public function resolve($root, $args): Model
    {
        $model                 = new Model;
        $model->code           = $args['code'];
        $model->name           = $args['name'];
        $model->expanse        = $args['expanse'];
        $model->duration_hour  = $args['duration_hour'];
        $model->duration_day   = 0;
        $model->start_date     = $args['start_date'];
        $model->end_date       = $args['end_date'];
        $model->institution_id = $args['institution_id'];
        $model->category_id    = $args['category_id'];
        $model->preparation    = null;
        if (isset($args['preparation']) && !empty($args['preparation']) && $args['preparation'] !== 'null') {
            $model->preparation = $args['preparation'];
        }
        $model->implementation = null;
        if (isset($args['implementation']) && !empty($args['implementation']) && $args['implementation'] !== 'null') {
            $model->implementation = $args['implementation'];
        }
        $model->implementation = null;
        if (isset($args['evaluation']) && !empty($args['evaluation']) && $args['evaluation'] !== 'null') {
            $model->evaluation = $args['evaluation'];
        }
        $model->save();
        $courses = Course::where('category_id', $model->category_id)->get();
        foreach($courses as $course) {
            $model->courses()->attach($course->id);
        }
        return $model;
    }
}
