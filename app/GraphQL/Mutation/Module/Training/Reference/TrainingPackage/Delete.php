<?php
declare (strict_types = 1);
namespace Whiteboks\GraphQL\Mutation\Module\Training\Reference\TrainingPackage;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use Whiteboks\Model\Module\Training\Reference\TrainingPackage as Model;

class Delete extends Mutation
{
    protected $attributes = [
        'name'        => 'ModuleTrainingReferenceTrainingPackageDelete',
        'description' => 'Module Training Reference TrainingPackage Delete',
        'model'       => Model::class,
    ];

    public function type()
    {
        return GraphQL::type('ModuleTrainingReferenceTrainingPackage');
    }

    public function args(): array
    {
        return [
            'id' => [
                'name' => 'id',
                'type' => Type::nonNull(Type::string())
            ],
        ];
    }
    public function rules(): array
    {
        return [
            'id' => ['required'],
        ];
    }
    public function resolve($root, $args): Model
    {
        $model = Model::find($args['id']);
        if (!$model) {
            return null;
        }
        $model->delete();
        return $model;
    }
}
