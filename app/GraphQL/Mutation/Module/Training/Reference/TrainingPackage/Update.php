<?php
declare (strict_types = 1);
namespace Whiteboks\GraphQL\Mutation\Module\Training\Reference\TrainingPackage;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use Whiteboks\Model\Module\Training\Reference\TrainingPackage as Model;

class Update extends Mutation
{
    protected $attributes = [
        'name'        => 'ModuleTrainingReferenceTrainingPackageUpdate',
        'description' => 'Module Training Reference TrainingPackage Update',
        'model'       => Model::class,
    ];

    public function type()
    {
        return GraphQL::type('ModuleTrainingReferenceTrainingPackage');
    }

    public function args()
    {
        return [
            'id' => [
                'name' => 'id',
                'type' => Type::nonNull(Type::string())
            ],
            'code' => [
                'name' => 'code',
                'type' => Type::nonNull(Type::string())
            ],
            'name' => [
                'name' => 'name',
                'type' => Type::nonNull(Type::string())
            ],
            'description' => [
                'name' => 'description',
                'type' => Type::string()
            ],
        ];
    }
    public function rules(): array
    {
        return [
            'id' => ['required'],
            'code' => ['required'],
            'name' => ['required'],
        ];
    }
    public function resolve($root, $args): Model
    {
        $model = Model::find($args['id']);
        if (!$model) {
            return null;
        }
        $model->code        = $args['code'];
        $model->name        = $args['name'];
        $model->description = null;
        if ((isset($args['description']) && !empty($args['description'])) || $args['description'] !== 'null') {
            $model->description = $args['description'];
        }
        $model->save();
        return $model;
    }
}
