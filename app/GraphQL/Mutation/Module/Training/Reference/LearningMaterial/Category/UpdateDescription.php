<?php
declare (strict_types = 1);
namespace Whiteboks\GraphQL\Mutation\Module\Training\Reference\LearningMaterial\Category;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use Whiteboks\Model\Module\Training\Reference\LearningMaterial\Category as Model;

class UpdateDescription extends Mutation
{
    protected $attributes = [
        'name'        => 'ModuleTrainingReferenceLearningMaterialCategoryUpdateDescription',
        'description' => 'Module Training Reference Learning Material Category Update Description',
        'model'       => Model::class,
    ];

    public function type()
    {
        return GraphQL::type('ModuleTrainingReferenceLearningMaterialCategory');
    }

    public function args()
    {
        return [
            'id' => [
                'name' => 'id',
                'type' => Type::nonNull(Type::string())
            ],
            'description' => [
                'name' => 'description',
                'type' => Type::string()
            ],
        ];
    }
    public function rules(): array
    {
        return [
            'id'   => ['required'],
        ];
    }
    public function resolve($root, $args): Model
    {
        $model = Model::find($args['id']);
        if (!$model) {
            return null;
        }
        $model->description = null;
        if (isset($args['description']) && !empty($args['description']) && $args['description'] !== 'null') {
            $model->description = $args['description'];
        }
        $model->save();
        return $model;
    }
}
