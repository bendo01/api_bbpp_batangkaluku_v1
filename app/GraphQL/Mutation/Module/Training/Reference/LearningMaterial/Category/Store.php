<?php
declare (strict_types = 1);
namespace Whiteboks\GraphQL\Mutation\Module\Training\Reference\LearningMaterial\Category;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use Whiteboks\Model\Module\Training\Reference\LearningMaterial\Category as Model;

class Store extends Mutation
{
    protected $attributes = [
        'name'        => 'ModuleTrainingReferenceLearningMaterialCategoryStore',
        'description' => 'Module Training Reference Learning Material Category Store',
        'model'       => Model::class,
    ];

    public function type()
    {
        return GraphQL::type('ModuleTrainingReferenceLearningMaterialCategory');
    }

    public function args(): array
    {
        return [
            'code' => [
                'name' => 'code',
                'type' => Type::nonNull(Type::string())
            ],
            'name' => [
                'name' => 'name',
                'type' => Type::nonNull(Type::string())
            ],
            'training_package_id' => [
                'name' => 'training_package_id',
                'type' => Type::nonNull(Type::string())
            ],
            'parent_id' => [
                'name' => 'parent_id',
                'type' => Type::string()
            ],
            'description' => [
                'name' => 'description',
                'type' => Type::string()
            ],
        ];
    }
    public function rules(): array
    {
        return [
            'code' => ['required'],
            'name' => ['required'],
        ];
    }
    public function resolve($root, $args): Model
    {
        $model = new Model;
        $model->code = $args['code'];
        $model->name = $args['name'];
        $model->training_package_id = $args['training_package_id'];
        $model->parent_id = null;
        if (isset($args['parent_id']) && !empty($args['parent_id']) && $args['parent_id'] !== 'null') {
            $model->parent_id = $args['parent_id'];
        }
        $model->description = null;
        if (isset($args['description']) && !empty($args['description']) && $args['description'] !== 'null') {
            $model->description = $args['description'];
        }
        $model->save();
        return $model;
    }
}
