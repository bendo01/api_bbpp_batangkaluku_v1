<?php
declare (strict_types = 1);
namespace Whiteboks\GraphQL\Mutation\Module\Training\Reference\Facilitator;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use Whiteboks\Model\Module\Training\Reference\Facilitator as Model;

class Store extends Mutation
{
    protected $attributes = [
        'name'        => 'ModuleTrainingReferenceFacilitatorStore',
        'description' => 'Module Training Reference Facilitator Store',
        'model'       => Model::class,
    ];

    public function type()
    {
        return GraphQL::type('ModuleTrainingReferenceFacilitator');
    }

    public function args(): array
    {
        return [
            'code' => [
                'name' => 'code',
                'type' => Type::nonNull(Type::string())
            ],
            'name' => [
                'name' => 'name',
                'type' => Type::nonNull(Type::string())
            ],
            'description' => [
                'name' => 'description',
                'type' => Type::string()
            ],
        ];
    }
    public function rules(): array
    {
        return [
            'code' => ['required'],
            'name' => ['required'],
        ];
    }
    public function resolve($root, $args): Model
    {
        $model              = new Model;
        $model->code        = $args['code'];
        $model->name        = $args['name'];
        $model->description = null;
        if (isset($args['description']) && !empty($args['description'])) {
            $model->description = $args['description'];
        }
        $model->save();
        return $model;
    }
}
