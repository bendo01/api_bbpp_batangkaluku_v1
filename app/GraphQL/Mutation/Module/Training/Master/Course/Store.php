<?php
declare (strict_types = 1);
namespace Whiteboks\GraphQL\Mutation\Module\Training\Master\Course;

use GraphQL;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use Rebing\GraphQL\Support\SelectFields;

use Whiteboks\Model\Module\Training\Master\Course as Model;

class Store extends Mutation
{
    protected $attributes = [
        'name' => 'Store',
        'description' => 'A mutation'
    ];

    public function type()
    {
        return GraphQL::type('ModuleTrainingMasterCourse');
    }

    public function args()
    {
        return [
            'code' => [
                'name' => 'code',
                'type' => Type::nonNull(Type::string())
            ],
            'name' => [
                'name' => 'name',
                'type' => Type::nonNull(Type::string())
            ],
            'category_id' => [
                'name' => 'category_id',
                'type' => Type::nonNull(Type::string())
            ],
            'description' => [
                'name' => 'description',
                'type' => Type::string()
            ],
        ];
    }

    public function rules(): array
    {
        return [
            'code'        => ['required'],
            'name'        => ['required'],
            'category_id' => ['required'],
        ];
    }

    public function resolve($root, $args, SelectFields $fields, ResolveInfo $info)
    {
        $model              = new Model();
        $model->code        = $args['code'];
        $model->name        = $args['name'];
        $model->category_id = $args['category_id'];
        $model->description = null;
        if (isset($args['description']) && !empty($args['description'])) {
            $model->description = $args['description'];
        }
        $model->save();
        return $model;
    }
}
