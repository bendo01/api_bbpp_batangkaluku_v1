<?php
declare (strict_types = 1);
namespace Whiteboks\GraphQL\Mutation\Module\Training\Master\Course;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use Whiteboks\Model\Module\Training\Master\Course as Model;

class Update extends Mutation
{
    protected $attributes = [
        'name' => 'ModuleTrainingMasterCourseStore',
        'description' => 'Module Training Master Course Store',
        'model' => Model::class,
    ];

    public function type()
    {
        return GraphQL::type('ModuleTrainingMasterCourse');
    }

    public function args(): array
    {
        return [
            'id' => [
                'name' => 'id',
                'type' => Type::nonNull(Type::string())
            ],
            'code' => [
                'name' => 'code',
                'type' => Type::nonNull(Type::string())
            ],
            'name' => [
                'name' => 'name',
                'type' => Type::nonNull(Type::string())
            ],
            'category_id' => [
                'name' => 'category_id',
                'type' => Type::nonNull(Type::string())
            ],
            'description' => [
                'name' => 'description',
                'type' => Type::string()
            ],
        ];
    }
    public function rules(): array
    {
        return [
            'id'          => ['required'],
            'code'        => ['required'],
            'name'        => ['required'],
            'category_id' => ['required'],
        ];
    }
    public function resolve($root, $args): Model
    {
        $model = Model::find($args['id']);
        if (!$model) {
            return null;
        }
        $model->code        = $args['code'];
        $model->name        = $args['name'];
        $model->category_id = $args['category_id'];
        $model->description = null;
        if (isset($args['description']) && !empty($args['description'])) {
            $model->description = $args['description'];
        }
        $model->save();
        return $model;
    }
}
