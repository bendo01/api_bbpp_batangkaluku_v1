<?php
declare (strict_types = 1);
namespace Whiteboks\GraphQL\Mutation\Module\HumanResource\Reference\ContractType;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use Whiteboks\Model\Module\HumanResource\Reference\ContractType as Model;

class Delete extends Mutation
{
    protected $attributes = [
        'name' => 'ModuleHumanResourceReferenceContractTypeDelete',
        'description' => 'Module Human Resource Reference Contract Type Delete',
        'model' => Model::class,
    ];

    public function type()
    {
      return GraphQL::type('ModuleHumanResourceReferenceContractType');
    }

    public function args()
    {
        return [
            'id' => [
                'name' => 'id',
                'type' => Type::nonNull(Type::string())
            ],
        ];
    }

    public function rules(): array
    {
        return [
            'id' => ['required'],
        ];
    }

    public function resolve($root, $args): Model
    {
        $model = Model::find($args['id']);
        if (!$model) {
            return null;
        }
        $model->delete();
        return $model;
    }
}
