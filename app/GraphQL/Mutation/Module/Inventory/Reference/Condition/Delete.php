<?php
declare (strict_types = 1);
namespace Whiteboks\GraphQL\Mutation\Module\Inventory\Reference\Condition;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use Whiteboks\Model\Module\Inventory\Reference\Condition as Model;

class Delete extends Mutation
{
    protected $attributes = [
        'name' => 'ModuleInventoryReferenceConditionDelete',
        'description' => 'Module Inventory Reference Condition Delete',
        'model' => Model::class,
    ];

    public function type()
    {
      return GraphQL::type('ModuleInventoryReferenceCondition');
    }

    public function args()
    {
        return [
            'id' => [
                'name' => 'id',
                'type' => Type::nonNull(Type::string())
            ],
        ];
    }

    public function rules(): array
    {
        return [
            'id' => ['required'],
        ];
    }

    public function resolve($root, $args): Model
    {
        $model = Model::find($args['id']);
        if (!$model) {
            return null;
        }
        $model->delete();
        return $model;
    }
}
