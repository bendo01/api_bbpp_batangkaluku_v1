<?php
declare (strict_types = 1);

namespace Whiteboks\GraphQL\Mutation\Institution\Reference\Category;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use Whiteboks\Model\Institution\Reference\Category as Model;

class Store extends Mutation
{
    protected $attributes = [
        'name' => 'Store',
        'description' => 'A mutation',
        'model' => Model::class, // define model for users type
    ];

    public function type()
    {
        return GraphQL::type('InstitutionReferenceCategory');
    }

    public function args()
    {
        return [
            'code' => [
                'name' => 'code',
                'type' => Type::nonNull(Type::string())
            ],
            'name' => [
                'name' => 'name',
                'type' => Type::nonNull(Type::string())
            ],
        ];
    }

    public function rules(): array
    {
        return [
            'code' => ['required'],
            'name' => ['required'],
        ];
    }
    public function resolve($root, $args): Model
    {
        $model                  = new Model;
        $model->code   = $args['code'];
        $model->name   = $args['name'];
        $model->save();
        return $model;
    }
}
