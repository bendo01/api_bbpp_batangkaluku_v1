<?php
declare (strict_types = 1);

namespace Whiteboks\GraphQL\Mutation\Institution\Reference\CoordinatorType;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;

class Update extends Mutation
{
    protected $attributes = [
        'name' => 'Update',
        'description' => 'A mutation'
    ];

    public function type()
    {
        return Type::listOf(Type::string());
    }

    public function args()
    {
        return [

        ];
    }

    public function resolve($root, $args, SelectFields $fields, ResolveInfo $info)
    {
        $select = $fields->getSelect();
        $with = $fields->getRelations();

        return [];
    }
}
