<?php
declare (strict_types = 1);

namespace Whiteboks\GraphQL\Mutation\Institution\Master\Institution;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use Whiteboks\Model\Institution\Master\Institution as Model;

class Update extends Mutation
{
    protected $attributes = [
        'name' => 'Update',
        'description' => 'A mutation',
        'model' => Model::class, // define model for users type
    ];

    public function type()
    {
        return GraphQL::type('InstitutionMasterInstitution');
    }

    public function args()
    {
        return [
            'id' => [
                'name' => 'id',
                'type' => Type::nonNull(Type::string())
            ],
            'code' => [
                'name' => 'code',
                'type' => Type::nonNull(Type::string())
            ],
            'name' => [
                'name' => 'name',
                'type' => Type::nonNull(Type::string())
            ],
            'is_active' => [
                'name' => 'is_active',
                'type' => Type::nonNull(Type::boolean())
            ],
            'type_id' => [
                'name' => 'type_id',
                'type' => Type::nonNull(Type::string())
            ],
            'category_id' => [
                'name' => 'category_id',
                'type' => Type::nonNull(Type::string())
            ],
        ];
    }

    public function rules(): array
    {
        return [
            'id'          => ['required'],
            'code'        => ['required'],
            'name'        => ['required'],
            'is_active'   => ['required'],
            'type_id'     => ['required'],
            'category_id' => ['required'],
        ];
    }

    public function resolve($root, $args): Model
    {
        $model = Model::findOrFail($args['id']);
        $model->id          = $args['id'];
        $model->code        = $args['code'];
        $model->name        = $args['name'];
        $model->is_active   = $args['is_active'];
        $model->type_id     = $args['type_id'];
        $model->category_id = $args['category_id'];
        $model->save();
        return $model;
    }
}
