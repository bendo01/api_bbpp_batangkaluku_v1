<?php
declare (strict_types = 1);

namespace Whiteboks\GraphQL\Mutation\Auth\SocialAccount;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;

class Store extends Mutation
{
    protected $attributes = [
        'name' => 'AuthSocialAccountStore',
        'description' => 'A mutation'
    ];

    public function type()
    {
        return Type::listOf(Type::string());
    }

    public function args()
    {
        return [

        ];
    }

    public function resolve($root, $args, SelectFields $fields, ResolveInfo $info)
    {
        $select = $fields->getSelect();
        $with = $fields->getRelations();

        return [];
    }
}
