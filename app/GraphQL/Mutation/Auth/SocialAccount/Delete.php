<?php
declare(strict_types = 1);

namespace Whiteboks\GraphQL\Mutation\Auth\SocialAccount;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;

class Delete extends Mutation
{
    protected $attributes = [
        'name' => 'AuthSocialAccountDelete',
        'description' => 'Auth Social Account Delete',
        'model' => Model::class,
    ];

    public function type()
    {
        return GraphQL::type('AuthSocialAccount');
    }

    public function args()
    {
        return [
          'id' => [
              'name' => 'id',
              'type' => Type::nonNull(Type::string())
          ],
        ];
    }

    public function rules(): array
    {
        return [
            'id' => ['required'],
        ];
    }

    public function resolve($root, $args): Model
    {
        $model = Model::find($args['id']);
        if (!$model) {
            return null;
        }
        $model->delete();
        return $model;
    }
}
