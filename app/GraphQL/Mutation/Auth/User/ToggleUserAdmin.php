<?php
declare (strict_types = 1);

namespace Whiteboks\GraphQL\Mutation\Auth\User;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;

use Illuminate\Support\Facades\Hash;
use Whiteboks\Model\Auth\User as Model;

class ToggleUserAdmin extends Mutation
{
    protected $attributes = [
        'name' => 'AuthUserToogleUserAdmin',
        'description' => 'A mutation',
        'model' => Model::class, // define model for users type
    ];

    public function type()
    {
        return GraphQL::type('AuthUser');
    }

    public function args()
    {
        return [
            'id' => [
                'name' => 'id',
                'type' => Type::nonNull(Type::string())
            ],
        ];
    }

    public function rules(): array
    {
        return [
            'id' => ['required'],
        ];
    }

    public function resolve($root, $args): Model
    {
        $model = Model::find($args['id']);
        if (!$model) {
            return null;
        }
        $model->is_admin = !$model->is_admin;
        $model->save();
        return $model;
    }
}
