<?php
declare (strict_types = 1);

namespace Whiteboks\GraphQL\Mutation\Auth\User;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;

use Illuminate\Support\Facades\Hash;
use Whiteboks\Model\Auth\User as Model;

class Update extends Mutation
{
    protected $attributes = [
        'name' => 'AuthUserUpdate',
        'description' => 'A mutation',
        'model' => Model::class, // define model for users type
    ];

    public function type()
    {
        return GraphQL::type('AuthUser');
    }

    public function args()
    {
        return [
            'id' => [
                'name' => 'id',
                'type' => Type::nonNull(Type::string())
            ],
            'name' => [
                'name' => 'name',
                'type' => Type::string()
            ],
            'email' => [
                'name' => 'email',
                'type' => Type::string()
            ],
            'password' => [
                'name' => 'password',
                'type' => Type::string()
            ],
            'active' => [
                'name' => 'active',
                'type' => Type::boolean()
            ],
            'confirmed' => [
                'name' => 'confirmed',
                'type' => Type::boolean()
            ],
            'is_admin' => [
                'name' => 'is_admin',
                'type' => Type::boolean()
            ],
        ];
    }

    public function rules(): array
    {
        return [
            'id'        => ['required'],
            'email'     => ['email'],
            'active'    => ['boolean'],
            'confirmed' => ['boolean'],
            'is_admin'  => ['boolean'],
        ];
    }

    public function resolve($root, $args): Model
    {
        $model = Model::find($args['id']);
        if (!$model) {
            return null;
        }
        if (isset($args['name']) && !empty($args['name'])) {
            $model->name = $args['name'];
        }
        if (isset($args['email']) && !empty($args['email'])) {
            $model->email = $args['email'];
        }
        if (isset($args['password']) && !empty($args['password'])) {
            $model->password = Hash::make($args['password']);
        }
        if (isset($args['active']) && !empty($args['active'])) {
            $model->active = $args['active'];
        }
        if (isset($args['confirmed']) && !empty($args['confirmed'])) {
            $model->confirmed = $args['confirmed'];
        }
        if (isset($args['is_admin']) && !empty($args['is_admin'])) {
            $model->is_admin = $args['is_admin'];
        }
        $model->save();
        return $model;
    }
}
