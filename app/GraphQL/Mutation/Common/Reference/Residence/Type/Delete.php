<?php
declare (strict_types = 1);

namespace Whiteboks\GraphQL\Mutation\Common\Reference\Residence\Type;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use Whiteboks\Model\Common\Reference\Residence\Type as Model;

class Delete extends Mutation
{
    protected $attributes = [
        'name' => 'CommonReferenceResidenceTypeDelete',
        'description' => 'Common Reference Residence Type Delete',
        'model' => Model::class,
    ];

    public function type()
    {
        return GraphQL::type('CommonReferenceResidenceType');
    }

    public function args()
    {
        return [
            'id' => [
                'name' => 'id',
                'type' => Type::nonNull(Type::string())
            ],
        ];
    }

    public function rules(): array
    {
        return [
            'id' => ['required'],
        ];
    }

    public function resolve($root, $args): Model
    {
        $model = Model::find($args['id']);
        if (!$model) {
            return null;
        }
        $model->delete();
        return $model;
    }
}
