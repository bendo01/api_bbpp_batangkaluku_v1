<?php
declare (strict_types = 1);

namespace Whiteboks\GraphQL\Mutation\Common\Reference\Residence\ResidentType;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;

class Store extends Mutation
{
    protected $attributes = [
        'name' => 'Store',
        'description' => 'A mutation'
    ];

    public function type()
    {
        return Type::listOf(Type::string());
    }

    public function args()
    {
        return [

        ];
    }

    public function resolve($root, $args, SelectFields $fields, ResolveInfo $info)
    {
        $select = $fields->getSelect();
        $with = $fields->getRelations();

        return [];
    }
}
