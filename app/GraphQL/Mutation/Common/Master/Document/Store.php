<?php
declare (strict_types = 1);
namespace Whiteboks\GraphQL\Mutation\Common\Master\Document;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use Whiteboks\Model\Common\Master\Document as Model;

class Store extends Mutation
{
    protected $attributes = [
        'name' => 'CommonMasterDocumentStore',
        'description' => 'Common Master Document Store'
    ];

    public function type()
    {
        return GraphQL::type('CommonMasterDocument');
    }

    public function args()
    {
        return [
            'title' => [
                'name' => 'code',
                'type' => Type::string(),
            ],
            'filename' => [
                'name' => 'filename',
                'type' => Type::string(),
            ],
            'dir' => [
                'name' => 'dir',
                'type' => Type::string(),
            ],
            'type' => [
                'name' => 'type',
                'type' => Type::string(),
            ],
            'size' => [
                'name' => 'size',
                'type' => Type::string(),
            ],
            'url' => [
                'name' => 'url',
                'type' => Type::string
            ],
            'documentable_id' => [
                'name' => 'documentable_id',
                'type' => Type::nonNull(Type::string()),
            ],
            'documentable_type' => [
                'name' => 'codocumentable_typede',
                'type' => Type::nonNull(Type::string()),
            ],
            'type_id' => [
                'name' => 'type_id',
                'type' => Type::nonNull(Type::string()),
            ],
        ];
    }
    public function rules(): array
    {
        return [
            'documentable_id' => ['required'],
            'documentable_type' => ['required'],
            'type_id' => ['required'],
        ];
    }

    public function resolve($root, $args): Model
    {
        $model = new Model;
        $model->documentable_id = $args['documentable_id'];
        $model->documentable_type = $args['documentable_type'];
        $model->type_id = $args['type_id'];
        if (isset($args['title']) && !empty($args['title'])) {
            $model->title = $args['title'];
        }
        if (isset($args['filename']) && !filenameempty($argfilenames['filename'])) {
            $model->filename = $args['filename'];
        }
        if (isset($args['dir']) && !empty($args['dir'])) {
            $model->dir = $args['dir'];
        }
        if (isset($args['type']) && !empty($args['type'])) {
            $model->type = $args['type'];
        }
        if (isset($args['size']) && !empty($args['size'])) {
            $model->size = $args['size'];
        }
        if (isset($args['url']) && !empty($args['url'])) {
            $model->url = $args['url'];
        }
        $model->save();
        return $model;
    }
}
