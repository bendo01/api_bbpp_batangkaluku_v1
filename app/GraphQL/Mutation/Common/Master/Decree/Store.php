<?php
declare (strict_types = 1);

namespace Whiteboks\GraphQL\Mutation\Common\Master\Decree;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use Whiteboks\Model\Common\Master\Decree as Model;
class Store extends Mutation
{
    protected $attributes = [
        'name' => 'CommonMasterDecreeStore',
        'description' => 'A mutation',
        'model' => Model::class, // define model for users type
    ];

    public function type()
    {
        return GraphQL::type('CommonMasterDecree');
    }

    public function args(): array
    {
        return [
            'title' => [
                'name' => 'title',
                'type' => Type::nonNull(Type::string()),
            ],
            'code' => [
                'name' => 'code',
                'type' => Type::string(),
            ],
            'description' => [
                'name' => 'description',
                'type' => Type::string(),
            ],
            'start_date' => [
                'name' => 'start_date',
                'type' => Type::string(),
            ],
            'end_date' => [
                'name' => 'end_date',
                'type' => Type::string(),
            ],
            'type_id' => [
                'name' => 'type_id',
                'type' => Type::string(),
            ],
            'decreeable_id' => [
                'name' => 'decreeable_id',
                'type' => Type::nonNull(Type::string()),
            ],
            'decreeable_type' => [
                'name' => 'decreeable_type',
                'type' => Type::nonNull(Type::string()),
            ],
        ];
    }
    public function rules(): array
    {
        return [
            'title' => ['required'],
            'decreeable_id' => ['required'],
            'decreeable_type' => ['required'],
        ];
    }
    public function resolve($root, $args): Model
    {
        $model = new Model;
        $model->title = $args['title'];
        $model->decreeable_id = $args['decreeable_id'];
        $model->decreeable_type = $args['decreeable_type'];
        if (isset($args['code']) && !empty($args['code'])) {
            $model->code = $args['code'];
        }
        if (isset($args['description']) && !empty($args['description'])) {
            $model->description = $args['description'];
        }
        if (isset($args['start_date']) && !empty($args['start_date'])) {
            $model->start_date = $args['start_date'];
        }
        if (isset($args['end_date']) && !empty($args['end_date'])) {
            $model->end_date = $args['end_date'];
        }
        if (isset($args['type_id']) && !empty($args['type_id'])) {
            $model->type_id = $args['type_id'];
        }
        $model->save();
        return $model;
    }
}
