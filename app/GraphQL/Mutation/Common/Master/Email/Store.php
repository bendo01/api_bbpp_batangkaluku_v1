<?php
declare (strict_types = 1);

namespace Whiteboks\GraphQL\Mutation\Common\Master\Email;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use Whiteboks\Model\Common\Master\Email as Model;

class Store extends Mutation
{
    protected $attributes = [
        'name' => 'CommonMasterEmailStore',
        'description' => 'Common Master Email Store'
    ];

    public function type()
    {
        return GraphQL::type('CommonMasterEmail');
    }

    public function args(): array
    {
        return [
            'address' => [
                'name' => 'address',
                'type' => Type::nonNull(Type::string()),
            ],
            'is_primary' => [
                'name' => 'is_primary',
                'type' => Type::int(),
            ],
            'emailable_id' => [
                'name' => 'emailable_id',
                'type' => Type::nonNull(Type::string()),
            ],
            'emailable_type' => [
                'name' => 'emailable_type',
                'type' => Type::nonNull(Type::string()),
            ],
        ];
    }
    public function rules(): array
    {
        return [
            'address' => ['required'],
            'emailable_id' => ['required'],
            'emailable_type' => ['required'],
        ];
    }

    public function resolve($root, $args): Model
    {
        $model = new Model;
        $model->address = $args['address'];
        $model->emailable_id = $args['emailable_id'];
        $model->emailable_type = $args['emailable_type'];

        if (isset($args['is_primary']) && !empty($args['is_primary'])) {
            $model->is_primary = $args['is_primary'];
        }
        $model->save();
        return $model;
    }
}
