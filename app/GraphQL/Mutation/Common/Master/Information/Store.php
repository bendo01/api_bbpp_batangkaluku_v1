<?php
declare (strict_types = 1);

namespace Whiteboks\GraphQL\Mutation\Common\Master\Information;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use Whiteboks\Model\Common\Master\Information as Model;

class Store extends Mutation
{
    protected $attributes = [
        'name' => 'CommonMasterInformationStore',
        'description' => 'Common Master Information Store',
        'model' => Model::class, // define model for users type
    ];

    public function type()
    {
        return GraphQL::type('CommonMasterInformation');
    }

    public function args(): array
    {
        return [
            'informationable_id' => [
                'name' => 'informationable_id',
                'type' => Type::nonNull(Type::string()),
            ],
            'informationable_type' => [
                'name' => 'informationable_type',
                'type' => Type::nonNull(Type::string()),
            ],
            'vision' => [
                'name' => 'vision',
                'type' => Type::string(),
            ],
            'mission' => [
                'name' => 'mission',
                'type' => Type::string(),
            ],
            'intention' => [
                'name' => 'intention',
                'type' => Type::string(),
            ],
            'objective' => [
                'name' => 'objective',
                'type' => Type::string(),
            ],
        ];
    }
    public function rules(): array
    {
        return [
            'informationable_id' => ['required'],
            'informationable_type' => ['required'],
        ];
    }
    public function resolve($root, $args): Model
    {
        $model = new Model;
        $model->informationable_id = $args['informationable_id'];
        $model->informationable_type = $args['informationable_type'];
        if (isset($args['vision']) && !empty($args['vision'])) {
            $model->vision = $args['vision'];
        }
        if (isset($args['mission']) && !empty($args['mission'])) {
            $model->mission = $args['mission'];
        }
        if (isset($args['intention']) && !empty($args['intention'])) {
            $model->intention = $args['intention'];
        }
        if (isset($args['objective']) && !empty($args['objective'])) {
            $model->objective = $args['objective'];
        }
        $model->save();
        return $model;
    }
}
