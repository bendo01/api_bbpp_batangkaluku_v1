<?php
declare (strict_types = 1);

namespace Whiteboks\GraphQL\Mutation\Common\Master\Residence;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;

use Whiteboks\Model\Common\Master\Residence as Model;

class Update extends Mutation
{
    protected $attributes = [
        'name' => 'CommonMasterResidenceUpdate',
        'description' => 'A mutation',
        'model' => Model::class,
    ];

    public function type()
    {
        return GraphQL::type('CommonMasterResidence');
    }

    public function args()
    {
        return [
            'id' => [
                'name' => 'id',
                'type' => Type::nonNull(Type::string())
            ],
            'address' => [
                'name' => 'address',
                'type' => Type::nonNull(Type::string())
            ],
            'citizens_association' => [
                'name' => 'citizens_association',
                'type' => Type::nonNull(Type::string())
            ],
            'neighborhood_association' => [
                'name' => 'neighborhood_association',
                'type' => Type::nonNull(Type::string())
            ],
            'province_id' => [
                'name' => 'province_id',
                'type' => Type::nonNull(Type::string())
            ],
            'regency_id' => [
                'name' => 'regency_id',
                'type' => Type::nonNull(Type::string())
            ],
            'sub_district_id' => [
                'name' => 'sub_district_id',
                'type' => Type::nonNull(Type::string())
            ],
            'village_id' => [
                'name' => 'village_id',
                'type' => Type::nonNull(Type::string())
            ],
        ];
    }
    public function rules(): array
    {
        return [
            'id'                       => ['required'],
            'address'                  => ['required'],
            'citizens_association'     => ['required'],
            'neighborhood_association' => ['required'],
            'province_id'              => ['required'],
            'regency_id'               => ['required'],
            'sub_district_id'          => ['required'],
            'village_id'               => ['required'],
        ];
    }

    public function resolve($root, $args): Model
    {
        $model = Model::find($args['id']);
        if (!$model) {
            return null;
        }
        $model->address                  = $args['address'];
        $model->citizens_association     = $args['citizens_association'];
        $model->neighborhood_association = $args['neighborhood_association'];
        $model->province_id              = $args['province_id'];
        $model->regency_id               = $args['regency_id'];
        $model->sub_district_id          = $args['sub_district_id'];
        $model->village_id               = $args['village_id'];
        $model->save();
        return $model;
    }
}
