<?php
declare (strict_types = 1);

namespace Whiteboks\GraphQL\Mutation\Common\Master\Phone;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use Whiteboks\Model\Common\Master\Phone as Model;

class Store extends Mutation
{
    protected $attributes = [
        'name' => 'CommonMasterPhoneStore',
        'description' => 'Common Master Phone Store',
        'model' => Model::class, // define model for users type
    ];

    public function type()
    {
        return GraphQL::type('CommonMasterPhone');
    }

    public function args(): array
    {
        return [
            'dial_number' => [
                'name' => 'iso3166_2_code',
                'type' => Type::nonNull(Type::string()),
            ],
            'is_primary' => [
                'name' => 'iso3166_2_code',
                'type' => Type::int(),
            ],
            'phoneable_id' => [
                'name' => 'iso3166_2_code',
                'type' => Type::nonNull(Type::string()),
            ],
            'phoneable_type' => [
                'name' => 'iso3166_2_code',
                'type' => Type::nonNull(Type::string()),
            ],
        ];
    }
    public function rules(): array
    {
        return [
            'dial_number' => ['required'],
            'phoneable_id' => ['required'],
            'phoneable_type' => ['required'],
        ];
    }
    public function resolve($root, $args): Model
    {
        $model = new Model;
        $model->dial_number = $args['dial_number'];
        $model->phoneable_id = $args['phoneable_id'];
        $model->phoneable_type = $args['phoneable_type'];
        if (isset($args['is_primary']) && !empty($args['is_primary'])) {
            $model->is_primary = $args['is_primary'];
        }
        $model->save();
        return $model;
    }
}
