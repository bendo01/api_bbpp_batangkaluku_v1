<?php
declare (strict_types = 1);

namespace Whiteboks\GraphQL\Mutation\Person\Master\Relative;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use Whiteboks\Model\Person\Master\Relative as Model;

class Store extends Mutation
{
    protected $attributes = [
        'name'        => 'PersonMasterRelativeStore',
        'description' => 'A mutation of Person Master Relative Store',
        'model'       => Model::class,
    ];

    public function type()
    {
        return GraphQL::type('PersonMasterRelative');
    }

    public function args(): array
    {
        return [
            'individual_id' => [
                'name' => 'individual_id',
                'type' => Type::nonNull(Type::string())
            ],
            'relative_id' => [
                'name' => 'relative_id',
                'type' => Type::nonNull(Type::string())
            ],
            'relative_type_id' => [
                'name' => 'relative_type_id',
                'type' => Type::nonNull(Type::string())
            ],
        ];
    }

    public function rules(): array
    {
        return [
            'individual_id'    => ['required'],
            'relative_id'      => ['required'],
            'relative_type_id' => ['required'],
        ];
    }
    public function resolve($root, $args): Model
    {
        $model                  = new Model;
        $model->individual_id    = $args['individual_id'];
        $model->relative_id      = $args['relative_id'];
        $model->relative_type_id = $args['relative_type_id'];
        $model->save();
        return $model;
    }
}
