<?php
declare (strict_types = 1);

namespace Whiteboks\GraphQL\Mutation\Person\Master\Biodata;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use Whiteboks\Model\Person\Master\Biodata as Model;

class Store extends Mutation
{
    protected $attributes = [
        'name'        => 'PersonMasterBiodataStore',
        'description' => 'Person Master Biodata Store',
        'model'       => Model::class,
    ];

    public function type()
    {
        return GraphQL::type('PersonMasterBiodata');
    }

    public function args(): array
    {
        return [
            'height' => [
                'name' => 'height',
                'type' => Type::nonNull(Type::string())
            ],
            'weight' => [
                'name' => 'weight',
                'type' => Type::nonNull(Type::string())
            ],
            'positive_blood_rhesus' => [
                'name' => 'positive_blood_rhesus',
                'type' => Type::nonNull(Type::int())
            ],
            'blood_type_id' => [
                'name' => 'blood_type_id',
                'type' => Type::nonNull(Type::string())
            ],
            'hair_type_id' => [
                'name' => 'hair_type_id',
                'type' => Type::nonNull(Type::string())
            ],
            'hair_color_id' => [
                'name' => 'hair_color_id',
                'type' => Type::nonNull(Type::string())
            ],
            'eye_color_type_id' => [
                'name' => 'eye_color_type_id',
                'type' => Type::nonNull(Type::string())
            ],
            'individual_id' => [
                'name' => 'individual_id',
                'type' => Type::nonNull(Type::string())
            ],
        ];
    }

    public function rules(): array
    {
        return [
            'height'                => ['required'],
            'weight'                => ['required'],
            'positive_blood_rhesus' => ['required'],
            'blood_type_id'         => ['required'],
            'hair_type_id'          => ['required'],
            'hair_color_id'         => ['required'],
            'eye_color_type_id'     => ['required'],
            'individual_id'         => ['required'],
        ];
    }
    public function resolve($root, $args): Model
    {
        $model                        = new Model;
        $model->height                = $args['height'];
        $model->weight                = $args['weight'];
        $model->positive_blood_rhesus = $args['positive_blood_rhesus'];
        $model->blood_type_id         = $args['blood_type_id'];
        $model->hair_type_id          = $args['hair_type_id'];
        $model->hair_color_id         = $args['hair_color_id'];
        $model->eye_color_type_id     = $args['eye_color_type_id'];
        $model->individual_id         = $args['individual_id'];
        $model->save();
        return $model;
    }
}
