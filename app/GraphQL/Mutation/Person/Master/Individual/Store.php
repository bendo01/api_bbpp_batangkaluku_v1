<?php
declare (strict_types = 1);

namespace Whiteboks\GraphQL\Mutation\Person\Master\Individual;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use Whiteboks\Model\Person\Master\Individual as Model;

class Store extends Mutation
{
    protected $attributes = [
        'name'        => 'PersonMasterIndividualStore',
        'description' => 'A mutation of Person Master Individual Store',
        'model'       => Model::class,
    ];

    public function type()
    {
        return GraphQL::type('PersonMasterIndividual');
    }

    public function args(): array
    {
        return [
            'code' => [
                'name' => 'code',
                'type' => Type::nonNull(Type::string())
            ],
            'name' => [
                'name' => 'name',
                'type' => Type::nonNull(Type::string())
            ],
            'birth_date' => [
                'name' => 'birth_date',
                'type' => Type::nonNull(Type::string())
            ],
            'birth_place' => [
                'name' => 'birth_place',
                'type' => Type::nonNull(Type::string())
            ],
            'gender_id' => [
                'name' => 'gender_id',
                'type' => Type::nonNull(Type::string())
            ],
            'religion_id' => [
                'name' => 'religion_id',
                'type' => Type::nonNull(Type::string())
            ],
            'occupation_id' => [
                'name' => 'occupation_id',
                'type' => Type::nonNull(Type::string())
            ],
            'education_id' => [
                'name' => 'education_id',
                'type' => Type::nonNull(Type::string())
            ],
            'income_id' => [
                'name' => 'income_id',
                'type' => Type::nonNull(Type::string())
            ],
            'identification_type_id' => [
                'name' => 'identification_type_id',
                'type' => Type::nonNull(Type::string())
            ],
            'marital_status_id' => [
                'name' => 'marital_status_id',
                'type' => Type::nonNull(Type::string())
            ],
            'profession_id' => [
                'name' => 'profession_id',
                'type' => Type::nonNull(Type::string())
            ],
            'deceased' => [
                'name' => 'deceased',
                'type' => Type::nonNull(Type::int())
            ],
            'birth_place_id' => [
                'name' => 'birth_place_id',
                'type' => Type::string()
            ],
            'front_title' => [
                'name' => 'front_title',
                'type' => Type::string()
            ],
            'behind_title' => [
                'name' => 'behind_title',
                'type' => Type::string()
            ],
            'full_name' => [
                'name' => 'full_name',
                'type' => Type::string()
            ],
            'country_id' => [
                'name' => 'country_id',
                'type' => Type::string()
            ],
        ];
    }

    public function rules(): array
    {
        return [
            'code'                   => ['required'],
            'name'                   => ['required'],
            'birth_date'             => ['required'],
            'birth_place'            => ['required'],
            'gender_id'              => ['required'],
            'religion_id'            => ['required'],
            'occupation_id'          => ['required'],
            'education_id'           => ['required'],
            'income_id'              => ['required'],
            'identification_type_id' => ['required'],
            'marital_status_id'      => ['required'],
            'profession_id'          => ['required'],
            'deceased'               => ['required'],
        ];
    }
    public function resolve($root, $args): Model
    {
        $model                  = new Model;
        $model->code                   = $args['code'];
        $model->name                   = $args['name'];
        $model->birth_date             = $args['birth_date'];
        $model->birth_place            = $args['birth_place'];
        $model->gender_id              = $args['gender_id'];
        $model->religion_id            = $args['religion_id'];
        $model->occupation_id          = $args['occupation_id'];
        $model->education_id           = $args['education_id'];
        $model->income_id              = $args['income_id'];
        $model->identification_type_id = $args['identification_type_id'];
        $model->marital_status_id      = $args['marital_status_id'];
        $model->profession_id          = $args['profession_id'];
        $model->deceased               = $args['deceased'];
        $model->save();
        return $model;
    }
}
