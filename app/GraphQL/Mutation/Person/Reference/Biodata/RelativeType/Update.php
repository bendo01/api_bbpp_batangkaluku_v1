<?php
declare (strict_types = 1);

namespace Whiteboks\GraphQL\Mutation\Person\Reference\Biodata\RelativeType;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use Whiteboks\Model\Person\Reference\Biodata\RelativeType as Model;

class Update extends Mutation
{
    protected $attributes = [
        'name' => 'PersonReferenceBiodataRelativeTypeUpdate',
        'description' => 'Person Reference Biodata Relative Type Update',
        'model' => Model::class,
    ];

    public function type()
    {
        return GraphQL::type('PersonReferenceBiodataRelativeType');
    }

    public function args(): array
    {
        return [
            'id' => [
                'name' => 'id',
                'type' => Type::nonNull(Type::string())
            ],
            'code' => [
                'name' => 'code',
                'type' => Type::nonNull(Type::string())
            ],
            'name' => [
                'name' => 'name',
                'type' => Type::nonNull(Type::string())
            ],
        ];
    }

    public function rules(): array
    {
        return [
            'id'   => ['required'],
            'code' => ['required'],
            'name' => ['required'],
        ];
    }

    public function resolve($root, $args): Model
    {
        $model = Model::find($args['id']);
        if (!$model) {
            return null;
        }
        $model->code = $args['code'];
        $model->name = $args['name'];

        $model->save();
        return $model;
    }
}
