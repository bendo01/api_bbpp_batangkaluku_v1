<?php
declare (strict_types = 1);

namespace Whiteboks\GraphQL\Mutation\Person\Reference\Biodata\IdentificationType;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use Whiteboks\Model\Person\Reference\Biodata\IdentificationType as Model;

class Store extends Mutation
{
    protected $attributes = [
        'name' => 'PersonReferenceBiodataIdentificationTypeStore',
        'description' => 'A mutation of Person Reference Biodata Identification Type Store'
    ];

    public function type()
    {
        return GraphQL::type('PersonReferenceBiodataIdentificationType');
    }

    public function args(): array
    {
        return [
            'code' => [
                'name' => 'code',
                'type' => Type::nonNull(Type::string())
            ],
            'name' => [
                'name' => 'name',
                'type' => Type::nonNull(Type::string())
            ],
        ];
    }

    public function rules(): array
    {
        return [
            'code' => ['required'],
            'name' => ['required'],
        ];
    }
    public function resolve($root, $args): Model
    {
        $model       = new Model;
        $model->code = $args['code'];
        $model->name = $args['name'];
        $model->save();
        return $model;
    }
}
