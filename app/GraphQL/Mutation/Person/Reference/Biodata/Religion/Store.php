<?php
declare (strict_types = 1);

namespace Whiteboks\GraphQL\Mutation\Person\Reference\Biodata\Religion;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use Whiteboks\Model\Person\Reference\Biodata\Religion as Model;

class Store extends Mutation
{
    protected $attributes = [
        'name' => 'PersonReferenceBiodataReligionStore',
        'description' => 'A mutation of Person Reference Biodata Religion Store'
    ];

    public function type()
    {
        return GraphQL::type('PersonReferenceBiodataReligion');
    }

    public function args(): array
    {
        return [
            'code' => [
                'name' => 'code',
                'type' => Type::nonNull(Type::string())
            ],
            'name' => [
                'name' => 'name',
                'type' => Type::nonNull(Type::string())
            ]
        ];
    }

    public function rules(): array
    {
        return [
            'code'       => ['required'],
            'name'       => ['required'],
        ];
    }
    public function resolve($root, $args): Model
    {
        $model             = new Model;
        $model->code       = $args['code'];
        $model->name       = $args['name'];
        $model->save();
        return $model;
    }
}
