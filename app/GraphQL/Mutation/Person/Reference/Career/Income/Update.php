<?php
declare (strict_types = 1);

namespace Whiteboks\GraphQL\Mutation\Person\Reference\Career\Income;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use Whiteboks\Model\Mutation\Person\Reference\Career\Income as Model;

class Update extends Mutation
{
    protected $attributes = [
        'name' => 'PersonReferenceCareerIncomeUpdate',
        'description' => 'Person Reference Career Income Update',
        'model' => Model::class,
    ];

    public function type()
    {
        return GraphQL::type('PersonReferenceCareerIncome');
    }

    public function args(): array
    {
        return [
            'id' => [
                'name' => 'id',
                'type' => Type::nonNull(Type::string())
            ],
            'code' => [
                'name' => 'code',
                'type' => Type::nonNull(Type::string())
            ],
            'name' => [
                'name' => 'name',
                'type' => Type::nonNull(Type::string())
            ],
            'feeder_code' => [
                'name' => 'feeder_code',
                'type' => Type::nonNull(Type::string())
            ],
            'minimum' => [
                'name' => 'minimum',
                'type' => Type::nonNull(Type::int())
            ],
            'maximum' => [
                'name' => 'maximum',
                'type' => Type::int()
            ],
        ];
    }

    public function rules(): array
    {
        return [
            'id'          => ['required'],
            'code'        => ['required'],
            'name'        => ['required'],
            'feeder_code' => ['required'],
            'minimum'     => ['required'],
        ];
    }

    public function resolve($root, $args): Model
    {
        $model = Model::find($args['id']);
        if (!$model) {
            return null;
        }
        $model->code       = $args['code'];
        $model->name       = $args['name'];
        $model->feeder_key = $args['feeder_key'];
        $model->minimum    = $args['minimum'];

        if (isset($args['maximum']) && !empty($args['maximum'])) {
            $model->maximum = $args['maximum'];
        }
        $model->save();
        return $model;
    }
}
