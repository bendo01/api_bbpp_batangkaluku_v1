<?php
declare (strict_types = 1);

namespace Whiteboks\GraphQL\Mutation\Person\Reference\Career\Income;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use Whiteboks\Model\Person\Reference\Career\Income as Model;

class Store extends Mutation
{
    protected $attributes = [
        'name' => 'PersonReferenceCareerIncomeStore',
        'description' => 'A mutation of Person Reference Career Income Store'
    ];

    public function type()
    {
        return GraphQL::type('PersonReferenceCareerIncome');
    }

    public function args(): array
    {
        return [
            'code' => [
                'name' => 'code',
                'type' => Type::nonNull(Type::string())
            ],
            'name' => [
                'name' => 'name',
                'type' => Type::nonNull(Type::string())
            ],
            'feeder_code' => [
                'name' => 'feeder_code',
                'type' => Type::nonNull(Type::string())
            ],
            'minimum' => [
                'name' => 'minimum',
                'type' => Type::nonNull(Type::string())
            ],
            'maximum' => [
                'name' => 'maximum',
                'type' => Type::string()
            ],
        ];
    }

    public function rules(): array
    {
        return [
            'code'        => ['required'],
            'name'        => ['required'],
            'feeder_code' => ['required'],
            'minimum'     => ['required'],
        ];
    }
    public function resolve($root, $args): Model
    {
        $model              = new Model;
        $model->code        = $args['code'];
        $model->name        = $args['name'];
        $model->feeder_code = $args['feeder_code'];
        $model->minimum     = $args['minimum'];
        if (isset($args['maximum']) && !empty($args['maximum'])) {
            $model->maximum = $args['maximum'];
        }
        $model->save();
        return $model;
    }
}
