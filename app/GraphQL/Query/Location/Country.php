<?php
declare(strict_types = 1);
namespace Whiteboks\GraphQL\Query\Location;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Query;
use Illuminate\Support\Facades\Log;
use Rebing\GraphQL\Support\SelectFields;
use GraphQL\Type\Definition\ResolveInfo;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Illuminate\Pagination\LengthAwarePaginator;

use Whiteboks\Model\Location\Country as Model;

class Country extends Query
{
    protected $attributes = [
        'name' => 'LocationCountry',
        'description' => 'A query of country'
    ];
    public function type()
    {
        return GraphQL::paginate('LocationCountry');
    }
    // arguments to filter query
    public function args(): array
    {
        return [
            'id' => [
                'name' => 'id',
                'type' => Type::string(),
                'description' => 'id of countries searching with equals'
            ],
            'code' => [
                'name' => 'code',
                'type' => Type::string(),
                'description' => 'code of countries searching with like'
            ],
            'name' => [
                'name' => 'name',
                'type' => Type::string(),
                'description' => 'name of countries searching with like'
            ],
            'alpha2_code' => [
                'name' => 'alpha2_code',
                'type' => Type::string(),
                'description' => 'alpha2_code of countries searching with like'
            ],
            'alpha3_code' => [
                'name' => 'alpha3_code',
                'type' => Type::string(),
                'description' => 'alpha3_code of countries searching with like'
            ],
            'iso3166_2_code' => [
                'name' => 'iso3166_2_code',
                'type' => Type::string(),
                'description' => 'iso3166_2_code of countries searching with like'
            ],
            'dikti_code' => [
                'name' => 'dikti_code',
                'type' => Type::string(),
                'description' => 'dikti_code of countries searching with like'
            ],
            'continent_id' => [
                'name' => 'continent_id',
                'type' => Type::string(),
                'description' => 'continent_id of countries searching with like'
            ],
            'region_id' => [
                'name' => 'region_id',
                'type' => Type::string(),
                'description' => 'region_id of countries searching with like'
            ],
            'slug' => [
                'name' => 'slug',
                'type' => Type::string(),
                'description' => 'slug of countries searching with like'
            ],
            'created_by' => [
                'name' => 'created_by',
                'type' => Type::string(),
                'description' => 'created_by of countries searching with like'
            ],
            'modified_by' => [
                'name' => 'modified_by',
                'type' => Type::string(),
                'description' => 'modified_by of countries searching with like'
            ],
            'created_at' => [
                'name' => 'created_at',
                'type' => Type::string(),
                'description' => 'created_at of residence rooms searching with equals'
            ],
            'updated_at' => [
                'name' => 'updated_at',
                'type' => Type::string(),
                'description' => 'updated_at of residence rooms searching with equals'
            ],
            'limit' => [
                'name' => 'limit',
                'type' => Type::int(),
                'description' => 'Limit Pagination'
            ],
            'page' => [
                'type' => Type::int(),
                'description' => 'Current Page Pagination'
            ],
            'orderBy' => [
                'name' => 'orderBy',
                'type' => Type::string(),
                'description' => 'sort field'
            ],
            'sortOrder' => [
                'name' => 'sortOrder',
                'type' => Type::string(),
                'description' => 'sort order'
            ],
        ];
    }

    public function resolve($root, array $args, SelectFields $fields): LengthAwarePaginator
    {
        $limit = 10;
        $page = 1;
        $orderBy = 'created_at';
        $sortOrder = 'asc';
        $query = Model::query();

        if (isset($args['id']) && !empty($args['id'])) {
            $query->where('id', $args['id']);
        }
        if (isset($args['code']) && !empty($args['code'])) {
            $query->orWhere('code', 'like', '%'.$args['code'].'%');
        }
        if (isset($args['name']) && !empty($args['name'])) {
            $query->orWhere('name', 'like', '%'.$args['name'].'%');
        }
        if (isset($args['alpha2_code']) && !empty($args['alpha2_code'])) {
            $query->orWhere('alpha2_code', $args['alpha2_code']);
        }
        if (isset($args['alpha3_code']) && !empty($args['alpha3_code'])) {
            $query->orWhere('alpha3_code', $args['alpha3_code']);
        }
        if (isset($args['iso3166_2_code']) && !empty($args['iso3166_2_code'])) {
            $query->orWhere('iso3166_2_code', $args['iso3166_2_code']);
        }
        if (isset($args['dikti_code']) && !empty($args['dikti_code'])) {
            $query->orWhere('dikti_code', $args['dikti_code']);
        }
        if (isset($args['continent_id']) && !empty($args['continent_id'])) {
            $query->where('continent_id', $args['continent_id']);
        }
        if (isset($args['region_id']) && !empty($args['region_id'])) {
            $query->where('region_id', $args['region_id']);
        }
        if (isset($args['slug']) && !empty($args['slug'])) {
            $query->where('slug', 'like', '%'.$args['slug'].'%');
        }
        if (isset($args['created_by']) && !empty($args['created_by'])) {
            $query->where('created_by', $args['created_by']);
        }
        if (isset($args['modified_by']) && !empty($args['modified_by'])) {
            $query->where('modified_by', $args['modified_by']);
        }
        if (isset($args['created_at']) && !empty($args['created_at'])) {
            $query->where('created_at', $args['created_at']);
        }
        if (isset($args['updated_at']) && !empty($args['updated_at'])) {
            $query->where('updated_at', $args['updated_at']);
        }
        if (isset($args['limit']) && !empty($args['limit'])) {
            $limit = intval($args['limit']);
        }
        if (isset($args['page']) && !empty($args['page'])) {
            $page = intval($args['page']);
        }
        if (isset($args['orderBy']) && !empty($args['orderBy'])) {
            $orderBy = $args['orderBy'];
        }
        if (isset($args['sortOrder']) && !empty($args['sortOrder'])) {
            $sortOrder = $args['sortOrder'];
        }
        $select = $fields->getSelect();
        $with = $fields->getRelations();
        array_pop($with);
        return $query->select($select)->orderBy($orderBy, $sortOrder)->with($with)->paginate($limit, ['*'], 'page', $page);
    }
}
