<?php
declare(strict_types = 1);
namespace Whiteboks\GraphQL\Query\Location;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Query;
use Illuminate\Support\Facades\Log;
use Rebing\GraphQL\Support\SelectFields;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Illuminate\Pagination\LengthAwarePaginator;

use Whiteboks\Model\Location\Regency as Model;

class Regency extends Query
{
    protected $attributes = [
        'name' => 'LocationRegency',
        'description' => 'A query of location Regency'
    ];
    public function type()
    {
        return GraphQL::paginate('LocationRegency');
    }
    // arguments to filter query
    public function args(): array
    {
        return [
            'id' => [
                'name' => 'id',
                'type' => Type::string(),
                'description' => 'id of regencies searching with equals'
            ],
            'code' => [
                'name' => 'code',
                'type' => Type::string(),
                'description' => 'code of regencies searching with equals'
            ],
            'name' => [
                'name' => 'name',
                'type' => Type::string(),
                'description' => 'name of regencies searching with like'
            ],
            'province_id' => [
                'name' => 'province_id',
                'type' => Type::string(),
                'description' => 'province_id of regencies searching with equals'
            ],
            'regency_type_id' => [
                'name' => 'regency_type_id',
                'type' => Type::string(),
                'description' => 'regency_type_id of regencies searching with equals'
            ],
            'dikti_code' => [
                'name' => 'dikti_code',
                'type' => Type::string(),
                'description' => 'dikti code of regencies searching with equals'
            ],
            'dikti_name' => [
                'name' => 'dikti_name',
                'type' => Type::string(),
                'description' => 'dikti_name of regencies searching with equals'
            ],
            'epsbed_code' => [
                'name' => 'epsbed_code',
                'type' => Type::string(),
                'description' => 'epsbed_code of regencies searching with equals'
            ],
            'state_ministry_code' => [
                'name' => 'state_ministry_code',
                'type' => Type::string(),
                'description' => 'state_ministry_code of regencies searching with equals'
            ],
            'state_ministry_full_code' => [
                'name' => 'state_ministry_full_code',
                'type' => Type::string(),
                'description' => 'state_ministry_full_code of regencies searching with equals'
            ],
            'state_ministry_name' => [
                'name' => 'state_ministry_name',
                'type' => Type::string(),
                'description' => 'state_ministry_name of regencies searching with equals'
            ],
            'state_post_department_code' => [
                'name' => 'state_post_department_code',
                'type' => Type::string(),
                'description' => 'state_post_department_code of regencies searching with equals'
            ],
            'postal_code_start_range' => [
                'name' => 'postal_code_start_range',
                'type' => Type::string(),
                'description' => 'postal_code_start_range of regencies searching with equals'
            ],
            'postal_code_end_range' => [
                'name' => 'postal_code_end_range',
                'type' => Type::string(),
                'description' => 'postal_code_end_range of regencies searching with equals'
            ],
            'validation_code' => [
                'name' => 'validation_code',
                'type' => Type::string(),
                'description' => 'validation_code of regencies searching with equals'
            ],
            'agriculture_department_code' => [
                'name' => 'agriculture_department_code',
                'type' => Type::string(),
                'description' => 'agriculture_department_code of regencies searching with equals'
            ],
            'agriculture_department_name' => [
                'name' => 'agriculture_department_name',
                'type' => Type::string(),
                'description' => 'agriculture_department_name of regencies searching with equals'
            ],
            'is_capital' => [
                'name' => 'is_capital',
                'type' => Type::int(),
                'description' => 'is regency capital or not'
            ],
            'slug' => [
                'name' => 'slug',
                'type' => Type::string(),
                'description' => 'slug of regencies searching with equals'
            ],
            'alt_slug' => [
                'name' => 'alt_slug',
                'type' => Type::string(),
                'description' => 'alt_slug of regencies searching with equals'
            ],
            'description' => [
                'name' => 'description',
                'type' => Type::string(),
                'description' => 'description of regencies searching with equals'
            ],
            'created_by' => [
                'name' => 'created_by',
                'type' => Type::string(),
                'description' => 'created_by of regencies searching with equals'
            ],
            'modified_by' => [
                'name' => 'modified_by',
                'type' => Type::string(),
                'description' => 'modified_by of regencies searching with equals'
            ],
            'created_at' => [
                'name' => 'created_at',
                'type' => Type::string(),
                'description' => 'created_at of residence rooms searching with equals'
            ],
            'updated_at' => [
                'name' => 'updated_at',
                'type' => Type::string(),
                'description' => 'updated_at of residence rooms searching with equals'
            ],
            'limit' => [
                'name' => 'limit',
                'type' => Type::int(),
                'description' => 'Limit Pagination'
            ],
            'page' => [
                'type' => Type::int(),
                'description' => 'Current Page Pagination'
            ],
            'orderBy' => [
                'name' => 'orderBy',
                'type' => Type::string(),
                'description' => 'sort field'
            ],
            'sortOrder' => [
                'name' => 'sortOrder',
                'type' => Type::string(),
                'description' => 'sort order'
            ],
        ];
    }

    public function resolve($root, array $args, SelectFields $fields): LengthAwarePaginator
    {
        $limit = 10;
        $page = 1;
        $orderBy = 'name';
        $sortOrder = 'asc';
        $query = Model::query();

        if (isset($args['id']) && !empty($args['id'])) {
            $query->where('id', $args['id']);
        }
        if (isset($args['code']) && !empty($args['code'])) {
            $query->where('code', 'like', '%'.$args['code'].'%');
        }
        if (isset($args['name']) && !empty($args['name'])) {
            $query->where('name', 'like', '%'.$args['name'].'%');
        }
        if (isset($args['province_id']) && !empty($args['province_id'])) {
            $query->where('province_id', $args['province_id']);
        }
        if (isset($args['regency_type_id']) && !empty($args['regency_type_id'])) {
            $query->where('regency_type_id', $args['regency_type_id']);
        }
        if (isset($args['dikti_code']) && !empty($args['dikti_code'])) {
            $query->where('dikti_code', $args['dikti_code']);
        }
        if (isset($args['dikti_name']) && !empty($args['dikti_name'])) {
            $query->where('dikti_name', 'like', '%'.$args['dikti_name'].'%');
        }
        if (isset($args['epsbed_code']) && !empty($args['epsbed_code'])) {
            $query->where('epsbed_code', $args['epsbed_code']);
        }
        if (isset($args['state_ministry_code']) && !empty($args['state_ministry_code'])) {
            $query->where('state_ministry_code', $args['state_ministry_code']);
        }
        if (isset($args['state_ministry_full_code']) && !empty($args['state_ministry_full_code'])) {
            $query->where('state_ministry_full_code', 'like', '%'.$args['state_ministry_full_code'].'%');
        }
        if (isset($args['state_ministry_name']) && !empty($args['state_ministry_name'])) {
            $query->where('state_ministry_name', 'like', '%'.$args['state_ministry_name'].'%');
        }
        if (isset($args['state_post_department_code']) && !empty($args['state_post_department_code'])) {
            $query->where('state_post_department_code', $args['state_post_department_code']);
        }
        if (isset($args['postal_code_start_range']) && !empty($args['postal_code_start_range'])) {
            $query->where('postal_code_start_range', 'like', '%'.$args['postal_code_start_range'].'%');
        }
        if (isset($args['postal_code_end_range']) && !empty($args['postal_code_end_range'])) {
            $query->where('postal_code_end_range', 'like', '%'.$args['postal_code_end_range'].'%');
        }
        if (isset($args['validation_code']) && !empty($args['validation_code'])) {
            $query->where('validation_code', $args['validation_code']);
        }
        if (isset($args['agriculture_department_code']) && !empty($args['agriculture_department_code'])) {
            $query->where('agriculture_department_code', $args['agriculture_department_code']);
        }
        if (isset($args['agriculture_department_name']) && !empty($args['agriculture_department_name'])) {
            $query->where('agriculture_department_name', 'like', '%'.$args['agriculture_department_name'].'%');
        }
        if (isset($args['is_capital']) && !empty($args['is_capital'])) {
            $query->where('is_capital', $args['is_capital']);
        }
        if (isset($args['slug']) && !empty($args['slug'])) {
            $query->where('slug', 'like', '%'.$args['slug'].'%');
        }
        if (isset($args['alt_slug']) && !empty($args['alt_slug'])) {
            $query->where('alt_slug', 'like', '%'.$args['alt_slug'].'%');
        }
        if (isset($args['description']) && !empty($args['description'])) {
            $query->where('description', 'like', '%'.$args['description'].'%');
        }
        if (isset($args['created_by']) && !empty($args['created_by'])) {
            $query->where('created_by', $args['created_by']);
        }
        if (isset($args['modified_by']) && !empty($args['modified_by'])) {
            $query->where('modified_by', $args['modified_by']);
        }
        if (isset($args['created_at']) && !empty($args['created_at'])) {
            $query->where('created_at', $args['created_at']);
        }
        if (isset($args['updated_at']) && !empty($args['updated_at'])) {
            $query->where('updated_at', $args['updated_at']);
        }
        if (isset($args['limit']) && !empty($args['limit'])) {
            $limit = intval($args['limit']);
        }
        if (isset($args['page']) && !empty($args['page'])) {
            $page = intval($args['page']);
        }
        if (isset($args['orderBy']) && !empty($args['orderBy'])) {
            $orderBy = $args['orderBy'];
        }
        if (isset($args['sortOrder']) && !empty($args['sortOrder'])) {
            $sortOrder = $args['sortOrder'];
        }
        $select = $fields->getSelect();
        $with = $fields->getRelations();
        array_pop($with);
        return $query->with($with)->select($select)->orderBy($orderBy, $sortOrder)->paginate($limit, ['*'], 'page', $page);
    }
}
