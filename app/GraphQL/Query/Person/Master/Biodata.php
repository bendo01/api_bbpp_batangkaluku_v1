<?php
declare (strict_types = 1);

namespace Whiteboks\GraphQL\Query\Person\Master;

use Rebing\GraphQL\Support\Query;
use GraphQL\Type\Definition\Type;
use Illuminate\Support\Facades\Log;
use GraphQL\Type\Definition\ResolveInfo;
use Rebing\GraphQL\Support\SelectFields;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Illuminate\Pagination\LengthAwarePaginator;

use Whiteboks\Model\Person\Master\Biodata as Model;

class Biodata extends Query
{
    protected $attributes = [
        'name' => 'PersonMasterBiodata',
        'description' => 'A query of Person Biodata'
    ];

    public function type()
    {
        return GraphQL::paginate('PersonMasterBiodata');
    }

    public function args()
    {
        return [
            'id' => [
                'name' => 'id',
                'type' => Type::string(),
                'description' => 'id of biodatas searching with equals'
            ],
            'height' => [
                'name' => 'height',
                'type' => Type::string(),
                'description' => 'height of biodatas searching with equals'
            ],
            'weight' => [
                'name' => 'weight',
                'type' => Type::string(),
                'description' => 'weight of biodatas searching with equals'
            ],
            'positive_blood_rhesus' => [
                'name' => 'positive_blood_rhesus',
                'type' => Type::nonNull(Type::int()),
                'description' => 'positive_blood_rhesus of biodatas searching with equals'
            ],
            'blood_type_id' => [
                'name' => 'blood_type_id',
                'type' => Type::string(),
                'description' => 'blood_type_id of biodatas searching with equals'
            ],
            'hair_type_id' => [
                'name' => 'hair_type_id',
                'type' => Type::string(),
                'description' => 'hair_type_id of biodatas searching with equals'
            ],
            'hair_color_id' => [
                'name' => 'hair_color_id',
                'type' => Type::string(),
                'description' => 'hair_color_id of biodatas searching with equals'
            ],
            'eye_color_type_id' => [
                'name' => 'eye_color_type_id',
                'type' => Type::string(),
                'description' => 'eye_color_type_id of biodatas searching with equals'
            ],
            'individual_id' => [
                'name' => 'individual_id',
                'type' => Type::string(),
                'description' => 'individual_id of biodatas searching with equals'
            ],
            'created_by' => [
                'name' => 'created_by',
                'type' => Type::string(),
                'description' => 'created_by of biodatas searching with equals'
            ],
            'modified_by' => [
                'name' => 'modified_by',
                'type' => Type::string(),
                'description' => 'modified_by of biodatas searching with equals'
            ],
            'created_at' => [
                'name' => 'created_at',
                'type' => Type::string(),
                'description' => 'created_at of biodatas searching with equals'
            ],
            'updated_at' => [
                'name' => 'updated_at',
                'type' => Type::string(),
                'description' => 'updated_at of biodatas searching with equals'
            ],
            'limit' => [
                'name' => 'limit',
                'type' => Type::int(),
                'description' => 'Limit Pagination'
            ],
            'page' => [
                'type' => Type::int(),
                'description' => 'Current Page Pagination'
            ],
            'orderBy' => [
                'name' => 'orderBy',
                'type' => Type::string(),
                'description' => 'sort field'
            ],
            'sortOrder' => [
                'name' => 'sortOrder',
                'type' => Type::string(),
                'description' => 'sort order'
            ],
        ];
    }

    public function resolve($root, array $args, SelectFields $fields): LengthAwarePaginator
    {
        $limit = 10;
        $page = 1;
        $orderBy = 'created_at';
        $sortOrder = 'asc';
        $query = Model::query();
        if (isset($args['id']) && !empty($args['id'])) {
            $query->where('id', $args['id']);
        }
        if (isset($args['height']) && !empty($args['height'])) {
            $query->where('height', $args['height']);
        }
        if (isset($args['weight']) && !empty($args['weight'])) {
            $query->where('weight', $args['weight']);
        }
        if (isset($args['positive_blood_rhesus']) && !empty($args['positive_blood_rhesus'])) {
            $query->where('positive_blood_rhesus', $args['positive_blood_rhesus']);
        }
        if (isset($args['blood_type_id']) && !empty($args['blood_type_id'])) {
            $query->where('blood_type_id', $args['blood_type_id']);
        }
        if (isset($args['hair_type_id']) && !empty($args['hair_type_id'])) {
            $query->where('hair_type_id', $args['hair_type_id']);
        }
        if (isset($args['hair_color_id']) && !empty($args['hair_color_id'])) {
            $query->where('hair_color_id', $args['hair_color_id']);
        }
        if (isset($args['eye_color_type_id']) && !empty($args['eye_color_type_id'])) {
            $query->where('eye_color_type_id', $args['eye_color_type_id']);
        }
        if (isset($args['individual_id']) && !empty($args['individual_id'])) {
            $query->where('individual_id', $args['individual_id']);
        }
        if (isset($args['created_by']) && !empty($args['created_by'])) {
            $query->where('created_by', $args['created_by']);
        }
        if (isset($args['modified_by']) && !empty($args['modified_by'])) {
            $query->where('modified_by', $args['modified_by']);
        }
        if (isset($args['created_at']) && !empty($args['created_at'])) {
            $query->where('created_at', $args['created_at']);
        }
        if (isset($args['updated_at']) && !empty($args['updated_at'])) {
            $query->where('updated_at', $args['updated_at']);
        }
        if (isset($args['limit']) && !empty($args['limit'])) {
            $limit = intval($args['limit']);
        }
        if (isset($args['page']) && !empty($args['page'])) {
            $page = intval($args['page']);
        }
        if (isset($args['orderBy']) && !empty($args['orderBy'])) {
            $orderBy = $args['orderBy'];
        }
        if (isset($args['sortOrder']) && !empty($args['sortOrder'])) {
            $sortOrder = $args['sortOrder'];
        }
        $select = $fields->getSelect();
        $with = $fields->getRelations();
        array_pop($with);
        return $query->select($select)->orderBy($orderBy, $sortOrder)->with($with)->paginate($limit, ['*'], 'page', $page);

    }
}
