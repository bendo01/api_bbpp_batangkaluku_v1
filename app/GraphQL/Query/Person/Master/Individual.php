<?php
declare (strict_types = 1);

namespace Whiteboks\GraphQL\Query\Person\Master;

use Rebing\GraphQL\Support\Query;
use GraphQL\Type\Definition\Type;
use Illuminate\Support\Facades\Log;
use GraphQL\Type\Definition\ResolveInfo;
use Rebing\GraphQL\Support\SelectFields;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Illuminate\Pagination\LengthAwarePaginator;

use Whiteboks\Model\Person\Master\Individual as Model;

class Individual extends Query
{
    protected $attributes = [
        'name' => 'PersonMasterIndividual',
        'description' => 'A query of Person Individual'
    ];

    public function type()
    {
        return GraphQL::paginate('PersonMasterIndividual');
    }

    public function args()
    {
        return [
            'id' => [
                'name' => 'id',
                'type' => Type::string(),
                'description' => 'id of individuals searching with equals'
            ],
            'code' => [
                'name' => 'code',
                'type' => Type::string(),
                'description' => 'code of individuals searching with equals'
            ],
            'name' => [
                'name' => 'name',
                'type' => Type::string(),
                'description' => 'name of individuals searching with equals'
            ],
            'birth_date' => [
                'name' => 'birth_date',
                'type' => Type::string(),
                'description' => 'birth_date of individuals searching with equals'
            ],
            'birth_place' => [
                'name' => 'birth_place',
                'type' => Type::string(),
                'description' => 'birth_place of individuals searching with equals'
            ],
            'birth_place_id' => [
                'name' => 'birth_place_id',
                'type' => Type::string(),
                'description' => 'birth_place_id of individuals searching with equals'
            ],
            'gender_id' => [
                'name' => 'gender_id',
                'type' => Type::string(),
                'description' => 'gender_id of individuals searching with equals'
            ],
            'religion_id' => [
                'name' => 'religion_id',
                'type' => Type::string(),
                'description' => 'religion_id of individuals searching with equals'
            ],
            'occupation_id' => [
                'name' => 'occupation_id',
                'type' => Type::string(),
                'description' => 'occupation_id of individuals searching with equals'
            ],
            'education_id' => [
                'name' => 'education_id',
                'type' => Type::string(),
                'description' => 'education_id of individuals searching with equals'
            ],
            'income_id' => [
                'name' => 'income_id',
                'type' => Type::string(),
                'description' => 'income_id of individuals searching with equals'
            ],
            'identification_type_id' => [
                'name' => 'identification_type_id',
                'type' => Type::string(),
                'description' => 'identification_type_id of individuals searching with equals'
            ],
            'marital_status_id' => [
                'name' => 'marital_status_id',
                'type' => Type::string(),
                'description' => 'marital_status_id of individuals searching with equals'
            ],
            'profession_id' => [
                'name' => 'profession_id',
                'type' => Type::string(),
                'description' => 'profession_id of individuals searching with equals'
            ],
            'deceased' => [
                'name' => 'deceased',
                'type' => Type::boolean(),
                'description' => 'deceased of individuals searching with equals'
            ],
            'country_id' => [
                'name' => 'country_id',
                'type' => Type::string(),
                'description' => 'country_id of individuals searching with equals'
            ],
            'created_by' => [
                'name' => 'created_by',
                'type' => Type::string(),
                'description' => 'created_by of individuals searching with equals'
            ],
            'modified_by' => [
                'name' => 'modified_by',
                'type' => Type::string(),
                'description' => 'modified_by of individuals searching with equals'
            ],
            'created_at' => [
                'name' => 'created_at',
                'type' => Type::string(),
                'description' => 'created_at of individuals searching with equals'
            ],
            'updated_at' => [
                'name' => 'updated_at',
                'type' => Type::string(),
                'description' => 'updated_at of individuals searching with equals'
            ],
            'limit' => [
                'name' => 'limit',
                'type' => Type::int(),
                'description' => 'Limit Pagination'
            ],
            'page' => [
                'type' => Type::int(),
                'description' => 'Current Page Pagination'
            ],
            'orderBy' => [
                'name' => 'orderBy',
                'type' => Type::string(),
                'description' => 'sort field'
            ],
            'sortOrder' => [
                'name' => 'sortOrder',
                'type' => Type::string(),
                'description' => 'sort order'
            ],
        ];
    }

    public function resolve($root, array $args, SelectFields $fields): LengthAwarePaginator
    {
        $limit = 10;
        $page = 1;
        $orderBy = 'created_at';
        $sortOrder = 'asc';
        $query = Model::query();
        if (isset($args['id']) && !empty($args['id'])) {
            $query->where('id', $args['id']);
        }
        if (isset($args['code']) && !empty($args['code'])) {
            $query->where('code', $args['code']);
        }
        if (isset($args['name']) && !empty($args['name'])) {
            $query->where('name', $args['name']);
        }
        if (isset($args['birth_date']) && !empty($args['birth_date'])) {
            $query->where('birth_date', $args['birth_date']);
        }
        if (isset($args['birth_place']) && !empty($args['birth_place'])) {
            $query->where('birth_place', $args['birth_place']);
        }
        if (isset($args['birth_place_id']) && !empty($args['birth_place_id'])) {
            $query->where('birth_place_id', $args['birth_place_id']);
        }
        if (isset($args['gender_id']) && !empty($args['gender_id'])) {
            $query->where('gender_id', $args['gender_id']);
        }
        if (isset($args['religion_id']) && !empty($args['religion_id'])) {
            $query->where('religion_id', $args['religion_id']);
        }
        if (isset($args['occupation_id']) && !empty($args['occupation_id'])) {
            $query->where('occupation_id', $args['occupation_id']);
        }
        if (isset($args['education_id']) && !empty($args['education_id'])) {
            $query->where('education_id', $args['education_id']);
        }
        if (isset($args['income_id']) && !empty($args['income_id'])) {
            $query->where('income_id', $args['income_id']);
        }
        if (isset($args['identification_type_id']) && !empty($args['identification_type_id'])) {
            $query->where('identification_type_id', $args['identification_type_id']);
        }
        if (isset($args['marital_status_id']) && !empty($args['marital_status_id'])) {
            $query->where('marital_status_id', $args['marital_status_id']);
        }
        if (isset($args['profession_id']) && !empty($args['profession_id'])) {
            $query->where('profession_id', $args['profession_id']);
        }
        if (isset($args['deceased']) && !empty($args['deceased'])) {
            $query->where('deceased', $args['deceased']);
        }
        if (isset($args['country_id']) && !empty($args['country_id'])) {
            $query->where('country_id', $args['country_id']);
        }
        if (isset($args['created_by']) && !empty($args['created_by'])) {
            $query->where('created_by', $args['created_by']);
        }
        if (isset($args['modified_by']) && !empty($args['modified_by'])) {
            $query->where('modified_by', $args['modified_by']);
        }
        if (isset($args['created_at']) && !empty($args['created_at'])) {
            $query->where('created_at', $args['created_at']);
        }
        if (isset($args['updated_at']) && !empty($args['updated_at'])) {
            $query->where('updated_at', $args['updated_at']);
        }
        if (isset($args['limit']) && !empty($args['limit'])) {
            $limit = intval($args['limit']);
        }
        if (isset($args['page']) && !empty($args['page'])) {
            $page = intval($args['page']);
        }
        if (isset($args['orderBy']) && !empty($args['orderBy'])) {
            $orderBy = $args['orderBy'];
        }
        if (isset($args['sortOrder']) && !empty($args['sortOrder'])) {
            $sortOrder = $args['sortOrder'];
        }
        $select = $fields->getSelect();
        $with = $fields->getRelations();
        array_pop($with);
        return $query->select($select)->orderBy($orderBy, $sortOrder)->with($with)->paginate($limit, ['*'], 'page', $page);

    }
}
