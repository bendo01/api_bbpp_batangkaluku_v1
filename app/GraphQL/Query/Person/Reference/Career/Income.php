<?php
declare (strict_types = 1);

namespace Whiteboks\GraphQL\Query\Person\Reference\Career;

use Rebing\GraphQL\Support\Query;
use GraphQL\Type\Definition\Type;
use Illuminate\Support\Facades\Log;
use GraphQL\Type\Definition\ResolveInfo;
use Rebing\GraphQL\Support\SelectFields;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Illuminate\Pagination\LengthAwarePaginator;

use Whiteboks\Model\Person\Reference\Career\Income as Model;

class Income extends Query
{
    protected $attributes = [
        'name' => 'PersonReferenceCareerIncome',
        'description' => 'A query of Carrer Income'
    ];

    public function type()
    {
        return GraphQL::paginate('PersonReferenceCareerIncome');
    }

    public function args()
    {
        return [
            'id' => [
                'name' => 'id',
                'type' => Type::string(),
                'description' => 'id of incomes searching with equals'
            ],
            'code' => [
                'name' => 'code',
                'type' => Type::string(),
                'description' => 'code of incomes searching with equals'
            ],
            'name' => [
                'name' => 'name',
                'type' => Type::string(),
                'description' => 'name of incomes searching with equals'
            ],
            'feeder_code' => [
                'name' => 'feeder_code',
                'type' => Type::string(),
                'description' => 'feeder_code of incomes searching with equals'
            ],
            'minimum' => [
                'name' => 'minimum',
                'type' => Type::string(),
                'description' => 'minimum of incomes searching with equals'
            ],
            'maximum' => [
                'name' => 'maximum',
                'type' => Type::string(),
                'description' => 'maximum of incomes searching with equals'
            ],
            'created_by' => [
                'name' => 'created_by',
                'type' => Type::string(),
                'description' => 'created_by of incomes searching with equals'
            ],
            'modified_by' => [
                'name' => 'modified_by',
                'type' => Type::string(),
                'description' => 'modified_by of incomes searching with equals'
            ],
            'created_at' => [
                'name' => 'created_at',
                'type' => Type::string(),
                'description' => 'created_at of incomes searching with equals'
            ],
            'updated_at' => [
                'name' => 'updated_at',
                'type' => Type::string(),
                'description' => 'updated_at of incomes searching with equals'
            ],
            'limit' => [
                'name' => 'limit',
                'type' => Type::int(),
                'description' => 'Limit Pagination'
            ],
            'page' => [
                'type' => Type::int(),
                'description' => 'Current Page Pagination'
            ],
            'orderBy' => [
                'name' => 'orderBy',
                'type' => Type::string(),
                'description' => 'sort field'
            ],
            'sortOrder' => [
                'name' => 'sortOrder',
                'type' => Type::string(),
                'description' => 'sort order'
            ],
        ];
    }

    public function resolve($root, array $args, SelectFields $fields): LengthAwarePaginator
    {
        $limit = 10;
        $page = 1;
        $orderBy = 'created_at';
        $sortOrder = 'asc';
        $query = Model::query();
        if (isset($args['id']) && !empty($args['id'])) {
            $query->where('id', $args['id']);
        }
        if (isset($args['code']) && !empty($args['code'])) {
            $query->where('code', $args['code']);
        }
        if (isset($args['name']) && !empty($args['name'])) {
            $query->where('name', $args['name']);
        }
        if (isset($args['feeder_code']) && !empty($args['feeder_code'])) {
            $query->where('feeder_code', $args['feeder_code']);
        }
        if (isset($args['minimum']) && !empty($args['minimum'])) {
            $query->where('minimum', $args['minimum']);
        }
        if (isset($args['maximum']) && !empty($args['maximum'])) {
            $query->where('maximum', $args['maximum']);
        }
        if (isset($args['created_by']) && !empty($args['created_by'])) {
            $query->where('created_by', $args['created_by']);
        }
        if (isset($args['modified_by']) && !empty($args['modified_by'])) {
            $query->where('modified_by', $args['modified_by']);
        }
        if (isset($args['created_at']) && !empty($args['created_at'])) {
            $query->where('created_at', $args['created_at']);
        }
        if (isset($args['updated_at']) && !empty($args['updated_at'])) {
            $query->where('updated_at', $args['updated_at']);
        }
        if (isset($args['limit']) && !empty($args['limit'])) {
            $limit = intval($args['limit']);
        }
        if (isset($args['page']) && !empty($args['page'])) {
            $page = intval($args['page']);
        }
        if (isset($args['orderBy']) && !empty($args['orderBy'])) {
            $orderBy = $args['orderBy'];
        }
        if (isset($args['sortOrder']) && !empty($args['sortOrder'])) {
            $sortOrder = $args['sortOrder'];
        }
        $select = $fields->getSelect();
        $with = $fields->getRelations();
        array_pop($with);
        return $query->select($select)->orderBy($orderBy, $sortOrder)->with($with)->paginate($limit, ['*'], 'page', $page);

    }
}
