<?php
declare (strict_types = 1);

namespace Whiteboks\GraphQL\Query\Auth;

use Rebing\GraphQL\Support\Query;
use GraphQL\Type\Definition\Type;
use Illuminate\Support\Facades\Log;
use GraphQL\Type\Definition\ResolveInfo;
use Rebing\GraphQL\Support\SelectFields;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Illuminate\Pagination\LengthAwarePaginator;

use Whiteboks\Model\Auth\SocialAccount as Model;

class SocialAccount extends Query
{
    protected $attributes = [
        'name' => 'AuthSocialAccount',
        'description' => 'A query of Auth Social Account'
    ];

    public function type()
    {
        return GraphQL::paginate('AuthSocialAccount');
    }

    public function args()
    {
        return [
            'id' => [
                'name' => 'id',
                'type' => Type::string(),
                'description' => 'id of social accounts searching with equals'
            ],
            'user_id' => [
                'name' => 'user_id',
                'type' => Type::string(),
                'description' => 'user_id of social accounts searching with equals'
            ],
            'provider' => [
                'name' => 'provider',
                'type' => Type::string(),
                'description' => 'provider of social accounts searching with equals'
            ],
            'provider_user_id' => [
                'name' => 'provider_user_id',
                'type' => Type::string(),
                'description' => 'provider_user_id of social accounts searching with equals'
            ],
            'created_at' => [
                'name' => 'created_at',
                'type' => Type::string(),
                'description' => 'created_at of social accounts searching with equals'
            ],
            'updated_at' => [
                'name' => 'updated_at',
                'type' => Type::string(),
                'description' => 'updated_at of social accounts searching with equals'
            ],
            'limit' => [
                'name' => 'limit',
                'type' => Type::int(),
                'description' => 'Limit Pagination'
            ],
            'page' => [
                'type' => Type::int(),
                'description' => 'Current Page Pagination'
            ],
            'orderBy' => [
                'name' => 'orderBy',
                'type' => Type::string(),
                'description' => 'sort field'
            ],
            'sortOrder' => [
                'name' => 'sortOrder',
                'type' => Type::string(),
                'description' => 'sort order'
            ],
        ];
    }

    public function resolve($root, array $args, SelectFields $fields): LengthAwarePaginator
    {
        $limit = 10;
        $page = 1;
        $orderBy = 'name';
        $sortOrder = 'asc';
        $query = Model::query();

        if (isset($args['id']) && !empty($args['id'])) {
            $query->where('id', $args['id']);
        }
        if (isset($args['user_id']) && !empty($args['user_id'])) {
            $query->where('user_id', $args['user_id']);
        }
        if (isset($args['provider']) && !empty($args['provider'])) {
            $query->where('provider', $args['provider']);
        }
        if (isset($args['provider_user_id']) && !empty($args['provider_user_id'])) {
            $query->where('provider_user_id', $args['provider_user_id']);
        }
        if (isset($args['created_at']) && !empty($args['created_at'])) {
            $query->where('created_at', $args['created_at']);
        }
        if (isset($args['updated_at']) && !empty($args['updated_at'])) {
            $query->where('updated_at', $args['updated_at']);
        }
        if (isset($args['limit']) && !empty($args['limit'])) {
            $limit = intval($args['limit']);
        }
        if (isset($args['page']) && !empty($args['page'])) {
            $page = intval($args['page']);
        }
        if (isset($args['orderBy']) && !empty($args['orderBy'])) {
            $orderBy = $args['orderBy'];
        }
        if (isset($args['sortOrder']) && !empty($args['sortOrder'])) {
            $sortOrder = $args['sortOrder'];
        }
        $select = $fields->getSelect();
        $with = $fields->getRelations();
        array_pop($with);
        return $query->with($with)->select($select)->orderBy($orderBy, $sortOrder)->paginate($limit, ['*'], 'page', $page);
    }
}
