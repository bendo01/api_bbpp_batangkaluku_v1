<?php
declare (strict_types = 1);

namespace Whiteboks\GraphQL\Query\Auth;

use Rebing\GraphQL\Support\Query;
use GraphQL\Type\Definition\Type;
use Illuminate\Support\Facades\Log;
use GraphQL\Type\Definition\ResolveInfo;
use Rebing\GraphQL\Support\SelectFields;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Illuminate\Pagination\LengthAwarePaginator;

use Whiteboks\Model\Auth\User as Model;

class User extends Query
{
    protected $attributes = [
        'name' => 'AuthUser',
        'description' => 'A query of Auth User'
    ];

    public function type()
    {
        return GraphQL::paginate('AuthUser');
    }

    public function args()
    {
        return [
            'id' => [
                'name' => 'id',
                'type' => Type::string(),
                'description' => 'id of users searching with equals'
            ],
            'name' => [
                'name' => 'name',
                'type' => Type::string(),
                'description' => 'name of users searching with equals'
            ],
            'email' => [
                'name' => 'email',
                'type' => Type::string(),
                'description' => 'email of users searching with equals'
            ],
            'password' => [
                'name' => 'password',
                'type' => Type::string(),
                'description' => 'password of users searching with equals'
            ],
            'active' => [
                'name' => 'active',
                'type' => Type::boolean(),
                'description' => 'active of users searching with equals'
            ],
            'confirmed' => [
                'name' => 'confirmed',
                'type' => Type::boolean(),
                'description' => 'confirmed of users searching with equals'
            ],
            'is_admin' => [
                'name' => 'confirmed',
                'type' => Type::boolean(),
                'description' => 'confirmed of users searching with equals'
            ],
            'individual_id' => [
                'name' => 'individual_id',
                'type' => Type::string(),
                'description' => 'individual_id of users searching with equals'
            ],
            'created_at' => [
                'name' => 'created_at',
                'type' => Type::string(),
                'description' => 'created_at of users searching with equals'
            ],
            'updated_at' => [
                'name' => 'updated_at',
                'type' => Type::string(),
                'description' => 'updated_at of users searching with equals'
            ],
            'limit' => [
                'name' => 'limit',
                'type' => Type::int(),
                'description' => 'Limit Pagination'
            ],
            'page' => [
                'type' => Type::int(),
                'description' => 'Current Page Pagination'
            ],
            'orderBy' => [
                'name' => 'orderBy',
                'type' => Type::string(),
                'description' => 'sort field'
            ],
            'sortOrder' => [
                'name' => 'sortOrder',
                'type' => Type::string(),
                'description' => 'sort order'
            ],
        ];
    }

    public function resolve($root, array $args, SelectFields $fields): LengthAwarePaginator
    {
        $limit = 10;
        $page = 1;
        $orderBy = 'name';
        $sortOrder = 'asc';
        $query = Model::query();

        if (isset($args['id']) && !empty($args['id'])) {
            $query->where('id', $args['id']);
        }
        if (isset($args['name']) && !empty($args['name'])) {
            $query->where('name', $args['name']);
        }
        if (isset($args['email']) && !empty($args['email'])) {
            $query->where('email', $args['email']);
        }
        if (isset($args['password']) && !empty($args['password'])) {
            $query->where('password', $args['password']);
        }
        if (isset($args['active']) && !empty($args['active'])) {
            $query->where('active', $args['active']);
        }
        if (isset($args['confirmed']) && !empty($args['confirmed'])) {
            $query->where('confirmed', $args['confirmed']);
        }
        if (isset($args['is_admin']) && !empty($args['is_admin'])) {
            $query->where('is_admin', $args['is_admin']);
        }
        if (isset($args['individual_id']) && !empty($args['individual_id'])) {
            $query->where('individual_id', $args['individual_id']);
        }
        if (isset($args['created_at']) && !empty($args['created_at'])) {
            $query->where('created_at', $args['created_at']);
        }
        if (isset($args['updated_at']) && !empty($args['updated_at'])) {
            $query->where('updated_at', $args['updated_at']);
        }
        if (isset($args['limit']) && !empty($args['limit'])) {
            $limit = intval($args['limit']);
        }
        if (isset($args['page']) && !empty($args['page'])) {
            $page = intval($args['page']);
        }
        if (isset($args['orderBy']) && !empty($args['orderBy'])) {
            $orderBy = $args['orderBy'];
        }
        if (isset($args['sortOrder']) && !empty($args['sortOrder'])) {
            $sortOrder = $args['sortOrder'];
        }
        $select = $fields->getSelect();
        $with = $fields->getRelations();
        array_pop($with);
        return $query->with($with)->select($select)->orderBy($orderBy, $sortOrder)->paginate($limit, ['*'], 'page', $page);
    }
}
