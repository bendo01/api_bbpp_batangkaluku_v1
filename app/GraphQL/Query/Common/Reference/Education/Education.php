<?php
declare (strict_types = 1);
namespace Whiteboks\GraphQL\Query\Common\Reference\Education;

use Rebing\GraphQL\Support\Query;
use GraphQL\Type\Definition\Type;
use Illuminate\Support\Facades\Log;
use GraphQL\Type\Definition\ResolveInfo;
use Rebing\GraphQL\Support\SelectFields;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Illuminate\Pagination\LengthAwarePaginator;

use Whiteboks\Model\Common\Reference\Education\Education as Model;

class Education extends Query
{
    protected $attributes = [
        'name' => 'CommonReferenceEducationEducation',
        'description' => 'A query of Common Reference Education'
    ];

    public function type()
    {
        return GraphQL::paginate('CommonReferenceEducationEducation');
    }

    public function args()
    {
        return [
            'id' => [
                'name' => 'id',
                'type' => Type::string(),
                'description' => 'id of educations searching with equals'
            ],
            'code' => [
                'name' => 'code',
                'type' => Type::string(),
                'description' => 'code of educations searching with equals'
            ],
            'name' => [
                'name' => 'name',
                'type' => Type::string(),
                'description' => 'name of educations searching with equals'
            ],
            'alphabetic' => [
                'name' => 'alphabetic',
                'type' => Type::string(),
                'description' => 'alphabetic of educations searching with equals'
            ],
            'abbreviation' => [
                'name' => 'abbreviation',
                'type' => Type::string(),
                'description' => 'abbreviation of educations searching with equals'
            ],
            'stage_id' => [
                'name' => 'stage_id',
                'type' => Type::string(),
                'description' => 'stage_id of educations searching with equals'
            ],
            'group_id' => [
                'name' => 'group_id',
                'type' => Type::string(),
                'description' => 'group_id of educations searching with equals'
            ],
            'category_id' => [
                'name' => 'category_id',
                'type' => Type::string(),
                'description' => 'category_id of educations searching with equals'
            ],
            'type_id' => [
                'name' => 'type_id',
                'type' => Type::string(),
                'description' => 'type_id of educations searching with equals'
            ],
            'institution' => [
                'name' => 'institution',
                'type' => Type::int(),
                'description' => 'institution check'
            ],
            'individual' => [
                'name' => 'individual',
                'type' => Type::int(),
                'description' => 'individual check'
            ],
            'created_by' => [
                'name' => 'created_by',
                'type' => Type::string(),
                'description' => 'created_by of educations searching with equals'
            ],
            'modified_by' => [
                'name' => 'modified_by',
                'type' => Type::string(),
                'description' => 'modified_by of educations searching with equals'
            ],
            'created_at' => [
                'name' => 'created_at',
                'type' => Type::string(),
                'description' => 'created_at of educations searching with equals'
            ],
            'updated_at' => [
                'name' => 'updated_at',
                'type' => Type::string(),
                'description' => 'updated_at of educations searching with equals'
            ],
            'limit' => [
                'name' => 'limit',
                'type' => Type::int(),
                'description' => 'Limit Pagination'
            ],
            'page' => [
                'type' => Type::int(),
                'description' => 'Current Page Pagination'
            ],
            'orderBy' => [
                'name' => 'orderBy',
                'type' => Type::string(),
                'description' => 'sort field'
            ],
            'sortOrder' => [
                'name' => 'sortOrder',
                'type' => Type::string(),
                'description' => 'sort order'
            ],
        ];
    }

    public function resolve($root, array $args, SelectFields $fields): LengthAwarePaginator
    {
        $limit = 10;
        $page = 1;
        $orderBy = 'created_at';
        $sortOrder = 'asc';
        $query = Model::query();
        if (isset($args['id']) && !empty($args['id'])) {
            $query->where('id', $args['id']);
        }
        if (isset($args['code']) && !empty($args['code'])) {
            $query->where('code', $args['code']);
        }
        if (isset($args['name']) && !empty($args['name'])) {
            $query->where('name', $args['name']);
        }
        if (isset($args['alphabetic']) && !empty($args['alphabetic'])) {
            $query->where('alphabetic', 'like', '%'.$args['alphabetic'].'%');
        }
        if (isset($args['abbreviation']) && !empty($args['abbreviation'])) {
            $query->where('abbreviation', 'like', '%'.$args['abbreviation'].'%');
        }
        if (isset($args['stage_id']) && !empty($args['stage_id'])) {
            $query->where('stage_id', $args['stage_id']);
        }
        if (isset($args['group_id']) && !empty($args['group_id'])) {
            $query->where('group_id', $args['group_id']);
        }
        if (isset($args['category_id']) && !empty($args['category_id'])) {
            $query->where('category_id', $args['category_id']);
        }
        if (isset($args['type_id']) && !empty($args['type_id'])) {
            $query->where('type_id', $args['type_id']);
        }
        if (isset($args['institution']) && !empty($args['institution'])) {
            $query->where('institution', $args['institution']);
        }
        if (isset($args['individual']) && !empty($args['individual'])) {
            $query->where('individual', $args['individual']);
        }
        if (isset($args['created_by']) && !empty($args['created_by'])) {
            $query->where('created_by', $args['created_by']);
        }
        if (isset($args['modified_by']) && !empty($args['modified_by'])) {
            $query->where('modified_by', $args['modified_by']);
        }
        if (isset($args['created_at']) && !empty($args['created_at'])) {
            $query->where('created_at', $args['created_at']);
        }
        if (isset($args['updated_at']) && !empty($args['updated_at'])) {
            $query->where('updated_at', $args['updated_at']);
        }
        if (isset($args['limit']) && !empty($args['limit'])) {
            $limit = intval($args['limit']);
        }
        if (isset($args['page']) && !empty($args['page'])) {
            $page = intval($args['page']);
        }
        if (isset($args['orderBy']) && !empty($args['orderBy'])) {
            $orderBy = $args['orderBy'];
        }
        if (isset($args['sortOrder']) && !empty($args['sortOrder'])) {
            $sortOrder = $args['sortOrder'];
        }
        $select = $fields->getSelect();
        $with = $fields->getRelations();
        array_pop($with);
        return $query->select($select)->orderBy($orderBy, $sortOrder)->with($with)->paginate($limit, ['*'], 'page', $page);

    }
}
