<?php
declare (strict_types = 1);

namespace Whiteboks\GraphQL\Query\Common\Reference\Period;

use Rebing\GraphQL\Support\Query;
use GraphQL\Type\Definition\Type;
use Illuminate\Support\Facades\Log;
use GraphQL\Type\Definition\ResolveInfo;
use Rebing\GraphQL\Support\SelectFields;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Illuminate\Pagination\LengthAwarePaginator;

use Whiteboks\Model\Common\Reference\Period\EducationYear as Model;

class EducationYear extends Query
{
    protected $attributes = [
        'name' => 'CommonReferencePeriodEducationYear',
        'description' => 'A query of Common Reference Period Education Year'
    ];

    public function type()
    {
        return GraphQL::paginate('CommonReferencePeriodEducationYear');
    }

    public function args()
    {
        return [
            'id' => [
                'name' => 'id',
                'type' => Type::string(),
                'description' => 'id of education years searching with equals'
            ],
            'name' => [
                'name' => 'name',
                'type' => Type::string(),
                'description' => 'name of education years searching with equals'
            ],
            'start_date' => [
                'name' => 'start_date',
                'type' => Type::string(),
                'description' => 'start_date of education years searching with equals'
            ],
            'end_date' => [
                'name' => 'end_date',
                'type' => Type::string(),
                'description' => 'end_date of education years searching with equals'
            ],
            'expired_date' => [
                'name' => 'expired_date',
                'type' => Type::string(),
                'description' => 'expired_date of education years searching with equals'
            ],
            'year' => [
                'name' => 'year',
                'type' => Type::nonNull(Type::int()),
                'description' => 'year education'
            ],
            'feeder_name' => [
                'name' => 'feeder_name',
                'type' => Type::string(),
                'description' => 'feeder_name of education years searching with equals'
            ],
            'thread' => [
                'name' => 'thread',
                'type' => Type::nonNull(Type::int()),
                'description' => 'thread education'
            ],
            'education_year_category_id' => [
                'name' => 'education_year_category_id',
                'type' => Type::string(),
                'description' => 'education_year_category_id of education years searching with equals'
            ],
            'created_by' => [
                'name' => 'created_by',
                'type' => Type::string(),
                'description' => 'created_by of education years searching with equals'
            ],
            'modified_by' => [
                'name' => 'modified_by',
                'type' => Type::string(),
                'description' => 'modified_by of education years searching with equals'
            ],
            'created_at' => [
                'name' => 'created_at',
                'type' => Type::string(),
                'description' => 'created_at of education years searching with equals'
            ],
            'updated_at' => [
                'name' => 'updated_at',
                'type' => Type::string(),
                'description' => 'updated_at of education years searching with equals'
            ],
            'limit' => [
                'name' => 'limit',
                'type' => Type::int(),
                'description' => 'Limit Pagination'
            ],
            'page' => [
                'type' => Type::int(),
                'description' => 'Current Page Pagination'
            ],
            'orderBy' => [
                'name' => 'orderBy',
                'type' => Type::string(),
                'description' => 'sort field'
            ],
            'sortOrder' => [
                'name' => 'sortOrder',
                'type' => Type::string(),
                'description' => 'sort order'
            ],
        ];
    }

    public function resolve($root, array $args, SelectFields $fields): LengthAwarePaginator
    {
        $limit = 10;
        $page = 1;
        $orderBy = 'created_at';
        $sortOrder = 'asc';
        $query = Model::query();
        if (isset($args['id']) && !empty($args['id'])) {
            $query->where('id', $args['id']);
        }
        if (isset($args['name']) && !empty($args['name'])) {
            $query->where('name', $args['name']);
        }
        if (isset($args['start_date']) && !empty($args['start_date'])) {
            $query->where('start_date', $args['start_date']);
        }
        if (isset($args['end_date']) && !empty($args['end_date'])) {
            $query->where('end_date', $args['end_date']);
        }
        if (isset($args['expired_date']) && !empty($args['expired_date'])) {
            $query->where('expired_date', $args['expired_date']);
        }
        if (isset($args['year']) && !empty($args['year'])) {
            $query->where('year', $args['year']);
        }
        if (isset($args['feeder_name']) && !empty($args['feeder_name'])) {
            $query->where('feeder_name', $args['feeder_name']);
        }
        if (isset($args['thread']) && !empty($args['thread'])) {
            $query->where('thread', $args['thread']);
        }
        if (isset($args['education_year_category_id']) && !empty($args['education_year_category_id'])) {
            $query->where('education_year_category_id', $args['education_year_category_id']);
        }
        if (isset($args['created_by']) && !empty($args['created_by'])) {
            $query->where('created_by', $args['created_by']);
        }
        if (isset($args['modified_by']) && !empty($args['modified_by'])) {
            $query->where('modified_by', $args['modified_by']);
        }
        if (isset($args['created_at']) && !empty($args['created_at'])) {
            $query->where('created_at', $args['created_at']);
        }
        if (isset($args['updated_at']) && !empty($args['updated_at'])) {
            $query->where('updated_at', $args['updated_at']);
        }
        if (isset($args['limit']) && !empty($args['limit'])) {
            $limit = intval($args['limit']);
        }
        if (isset($args['page']) && !empty($args['page'])) {
            $page = intval($args['page']);
        }
        if (isset($args['orderBy']) && !empty($args['orderBy'])) {
            $orderBy = $args['orderBy'];
        }
        if (isset($args['sortOrder']) && !empty($args['sortOrder'])) {
            $sortOrder = $args['sortOrder'];
        }
        $select = $fields->getSelect();
        $with = $fields->getRelations();
        array_pop($with);
        return $query->select($select)->orderBy($orderBy, $sortOrder)->with($with)->paginate($limit, ['*'], 'page', $page);

    }
}
