<?php
declare (strict_types = 1);
namespace Whiteboks\GraphQL\Query\Common\Master;

use Rebing\GraphQL\Support\Query;
use GraphQL\Type\Definition\Type;
use Illuminate\Support\Facades\Log;
use GraphQL\Type\Definition\ResolveInfo;
use Rebing\GraphQL\Support\SelectFields;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Illuminate\Pagination\LengthAwarePaginator;

use Whiteboks\Model\Common\Master\Decree as Model;

class Decree extends Query
{
    protected $attributes = [
        'name' => 'CommonMasterDecree',
        'description' => 'A query of Common Master Decree'
    ];

    public function type()
    {
        return GraphQL::paginate('CommonMasterDecree');
    }

    public function args()
    {
        return [
            'id' => [
                'name' => 'id',
                'type' => Type::string(),
                'description' => 'id of decree searching with equals'
            ],
            'title' => [
                'name' => 'title',
                'type' => Type::string(),
                'description' => 'code of decree searching with equals'
            ],
            'code' => [
                'name' => 'code',
                'type' => Type::string(),
                'description' => 'code of decree searching with equals'
            ],
            'description' => [
                'name' => 'description',
                'type' => Type::string(),
                'description' => 'description of decree searching with equals'
            ],
            'start_date' => [
                'name' => 'start_date',
                'type' => Type::string(),
                'description' => 'start date of decree searching with equals'
            ],
            'end_date' => [
                'name' => 'end_date',
                'type' => Type::string(),
                'description' => 'end date of decree searching with equals'
            ],
            'type_id' => [
                'name' => 'type_id',
                'type' => Type::string(),
                'description' => 'type id of decree searching with equals'
            ],
            'decreeable_id' => [
                'name' => 'decreeable_id',
                'type' => Type::string(),
                'description' => 'decreeable id of decree searching with equals'
            ],
            'decreeable_type' => [
                'name' => 'decreeable_type',
                'type' => Type::string(),
                'description' => 'decreeable type of decree searching with equals'
            ],
            'created_by' => [
                'name' => 'created_by',
                'type' => Type::string(),
                'description' => 'created_by of decree searching with equals'
            ],
            'modified_by' => [
                'name' => 'modified_by',
                'type' => Type::string(),
                'description' => 'modified_by of decree searching with equals'
            ],
            'created_at' => [
                'name' => 'created_at',
                'type' => Type::string(),
                'description' => 'created_at of residence rooms searching with equals'
            ],
            'updated_at' => [
                'name' => 'updated_at',
                'type' => Type::string(),
                'description' => 'updated_at of residence rooms searching with equals'
            ],
            'limit' => [
                'name' => 'limit',
                'type' => Type::int(),
                'description' => 'Limit Pagination'
            ],
            'page' => [
                'type' => Type::int(),
                'description' => 'Current Page Pagination'
            ],
            'orderBy' => [
                'name' => 'orderBy',
                'type' => Type::string(),
                'description' => 'sort field'
            ],
            'sortOrder' => [
                'name' => 'sortOrder',
                'type' => Type::string(),
                'description' => 'sort order'
            ],
        ];
    }

    public function resolve($root, array $args, SelectFields $fields): LengthAwarePaginator
    {
        $limit = 10;
        $page = 1;
        $orderBy = 'created_at';
        $sortOrder = 'asc';
        $query = Model::query();
        if (isset($args['id']) && !empty($args['id'])) {
            $query->where('id', $args['id']);
        }
        if (isset($args['title']) && !empty($args['title'])) {
            $query->where('title', 'like', '%'.$args['title'].'%');
        }
        if (isset($args['code']) && !empty($args['code'])) {
            $query->where('code', $args['code']);
        }
        if (isset($args['description']) && !empty($args['description'])) {
            $query->where('description', 'like', '%'.$args['description'].'%');
        }
        if (isset($args['start_date']) && !empty($args['start_date'])) {
            $query->where('start_date', 'like', '%'.$args['start_date'].'%');
        }
        if (isset($args['end_date']) && !empty($args['end_date'])) {
            $query->where('end_date', 'like', '%'.$args['end_date'].'%');
        }
        if (isset($args['type_id']) && !empty($args['type_id'])) {
            $query->where('type_id', $args['type_id']);
        }
        if (isset($args['decreeable_id']) && !empty($args['decreeable_id'])) {
            $query->where('decreeable_id', $args['decreeable_id']);
        }
        if (isset($args['decreeable_type']) && !empty($args['decreeable_type'])) {
            $query->where('decreeable_type', 'like', '%'.$args['decreeable_type'].'%');
        }
        if (isset($args['created_by']) && !empty($args['created_by'])) {
            $query->where('created_by', $args['created_by']);
        }
        if (isset($args['modified_by']) && !empty($args['modified_by'])) {
            $query->where('modified_by', $args['modified_by']);
        }
        if (isset($args['created_at']) && !empty($args['created_at'])) {
            $query->where('created_at', $args['created_at']);
        }
        if (isset($args['updated_at']) && !empty($args['updated_at'])) {
            $query->where('updated_at', $args['updated_at']);
        }
        if (isset($args['limit']) && !empty($args['limit'])) {
            $limit = intval($args['limit']);
        }
        if (isset($args['page']) && !empty($args['page'])) {
            $page = intval($args['page']);
        }
        if (isset($args['orderBy']) && !empty($args['orderBy'])) {
            $orderBy = $args['orderBy'];
        }
        if (isset($args['sortOrder']) && !empty($args['sortOrder'])) {
            $sortOrder = $args['sortOrder'];
        }
        $select = $fields->getSelect();
        $with = $fields->getRelations();
        array_pop($with);
        return $query->select($select)->orderBy($orderBy, $sortOrder)->with($with)->paginate($limit, ['*'], 'page', $page);

    }
}
