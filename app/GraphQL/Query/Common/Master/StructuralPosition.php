<?php
declare (strict_types = 1);
namespace Whiteboks\GraphQL\Query\Common\Master;

use Rebing\GraphQL\Support\Query;
use GraphQL\Type\Definition\Type;
use Illuminate\Support\Facades\Log;
use GraphQL\Type\Definition\ResolveInfo;
use Rebing\GraphQL\Support\SelectFields;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Illuminate\Pagination\LengthAwarePaginator;

use Whiteboks\Model\Common\Master\StructuralPosition as Model;

class StructuralPosition extends Query
{
    protected $attributes = [
        'name' => 'CommonMasterStructuralPosition',
        'description' => 'A query of Common Master Structural Position'
    ];

    public function type()
    {
        return GraphQL::paginate('CommonMasterSocialMedia');
    }

    public function args()
    {
        return [
            'id' => [
                'name' => 'id',
                'type' => Type::string(),
                'description' => 'id of structural positions searching with equals'
            ],
            'individual_id' => [
                'name' => 'individual_id',
                'type' => Type::string(),
                'description' => 'individual_id of structural positions searching with equals'
            ],
            'role_id' => [
                'name' => 'role_id',
                'type' => Type::string(),
                'description' => 'role_id of structural positions searching with equals'
            ],
            'start_date' => [
                'name' => 'start_date',
                'type' => Type::string(),
                'description' => 'start_date of structural positions searching with equals'
            ],
            'end_date' => [
                'name' => 'end_date',
                'type' => Type::string(),
                'description' => 'end_date of structural positions searching with equals'
            ],
            'decree_number' => [
                'name' => 'decree_number',
                'type' => Type::string(),
                'description' => 'decree_number of structural positions searching with equals'
            ],
            'decree_date' => [
                'name' => 'decree_date',
                'type' => Type::string(),
                'description' => 'decree_date of structural positions searching with equals'
            ],
            'structural_positionable_id' => [
                'name' => 'structural_positionable_id',
                'type' => Type::string(),
                'description' => 'structural_positionable_id of structural positions searching with equals'
            ],
            'structural_positionable_type' => [
                'name' => 'structural_positionable_type',
                'type' => Type::string(),
                'description' => 'structural_positionable_type of structural positions searching with equals'
            ],
            'created_by' => [
                'name' => 'created_by',
                'type' => Type::string(),
                'description' => 'created_by of structural positions searching with equals'
            ],
            'modified_by' => [
                'name' => 'modified_by',
                'type' => Type::string(),
                'description' => 'modified_by of structural positions searching with equals'
            ],
            'created_at' => [
                'name' => 'created_at',
                'type' => Type::string(),
                'description' => 'created_at of structural positions searching with equals'
            ],
            'updated_at' => [
                'name' => 'updated_at',
                'type' => Type::string(),
                'description' => 'updated_at of structural positions searching with equals'
            ],
            'limit' => [
                'name' => 'limit',
                'type' => Type::int(),
                'description' => 'Limit Pagination'
            ],
            'page' => [
                'type' => Type::int(),
                'description' => 'Current Page Pagination'
            ],
            'orderBy' => [
                'name' => 'orderBy',
                'type' => Type::string(),
                'description' => 'sort field'
            ],
            'sortOrder' => [
                'name' => 'sortOrder',
                'type' => Type::string(),
                'description' => 'sort order'
            ],
        ];
    }

    public function resolve($root, array $args, SelectFields $fields): LengthAwarePaginator
    {
        $limit = 10;
        $page = 1;
        $orderBy = 'created_at';
        $sortOrder = 'asc';
        $query = Model::query();
        if (isset($args['id']) && !empty($args['id'])) {
            $query->where('id', $args['id']);
        }
        if (isset($args['individual_id']) && !empty($args['individual_id'])) {
            $query->where('individual_id', $args['individual_id']);
        }
        if (isset($args['role_id']) && !empty($args['role_id'])) {
            $query->where('role_id', $args['role_id']);
        }
        if (isset($args['start_date']) && !empty($args['start_date'])) {
            $query->where('start_date', 'like', '%'.$args['start_date'].'%');
        }
        if (isset($args['end_date']) && !empty($args['end_date'])) {
            $query->where('end_date', 'like', '%'.$args['end_date'].'%');
        }
        if (isset($args['decree_number']) && !empty($args['decree_number'])) {
            $query->where('decree_number', 'like', '%'.$args['decree_number'].'%');
        }
        if (isset($args['decree_date']) && !empty($args['decree_date'])) {
            $query->where('decree_date', 'like', '%'.$args['decree_date'].'%');
        }
        if (isset($args['structural_positionable_id']) && !empty($args['structural_positionable_id'])) {
            $query->where('structural_positionable_id', $args['structural_positionable_id']);
        }
        if (isset($args['structural_positionable_type']) && !empty($args['structural_positionable_type'])) {
            $query->where('structural_positionable_type', 'like', '%'.$args['structural_positionable_type'].'%');
        }
        if (isset($args['created_by']) && !empty($args['created_by'])) {
            $query->where('created_by', $args['created_by']);
        }
        if (isset($args['modified_by']) && !empty($args['modified_by'])) {
            $query->where('modified_by', $args['modified_by']);
        }
        if (isset($args['created_at']) && !empty($args['created_at'])) {
            $query->where('created_at', $args['created_at']);
        }
        if (isset($args['updated_at']) && !empty($args['updated_at'])) {
            $query->where('updated_at', $args['updated_at']);
        }
        if (isset($args['limit']) && !empty($args['limit'])) {
            $limit = intval($args['limit']);
        }
        if (isset($args['page']) && !empty($args['page'])) {
            $page = intval($args['page']);
        }
        if (isset($args['orderBy']) && !empty($args['orderBy'])) {
            $orderBy = $args['orderBy'];
        }
        if (isset($args['sortOrder']) && !empty($args['sortOrder'])) {
            $sortOrder = $args['sortOrder'];
        }
        $select = $fields->getSelect();
        $with = $fields->getRelations();
        array_pop($with);
        return $query->select($select)->orderBy($orderBy, $sortOrder)->with($with)->paginate($limit, ['*'], 'page', $page);

    }
}
