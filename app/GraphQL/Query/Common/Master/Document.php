<?php
declare (strict_types = 1);
namespace Whiteboks\GraphQL\Query\Common\Master;

use Rebing\GraphQL\Support\Query;
use GraphQL\Type\Definition\Type;
use Illuminate\Support\Facades\Log;
use GraphQL\Type\Definition\ResolveInfo;
use Rebing\GraphQL\Support\SelectFields;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Illuminate\Pagination\LengthAwarePaginator;

use Whiteboks\Model\Common\Master\Document as Model;

class Document extends Query
{
    protected $attributes = [
        'name' => 'CommonMasterDocument',
        'description' => 'A query of Common Master Document'
    ];

    public function type()
    {
        return GraphQL::paginate('CommonMasterDocument');
    }

    public function args()
    {
        return [
            'id' => [
                'name' => 'id',
                'type' => Type::string(),
                'description' => 'id of documents searching with equals'
            ],
            'title' => [
                'name' => 'title',
                'type' => Type::string(),
                'description' => 'title of documents searching with equals'
            ],
            'filename' => [
                'name' => 'filename',
                'type' => Type::string(),
                'description' => 'filename of documents searching with equals'
            ],
            'dir' => [
                'name' => 'dir',
                'type' => Type::string(),
                'description' => 'dir of documents searching with equals'
            ],
            'type' => [
                'name' => 'type',
                'type' => Type::string(),
                'description' => 'type of documents searching with equals'
            ],
            'size' => [
                'name' => 'size',
                'type' => Type::int(),
                'description' => 'size of documents searching with equals'
            ],
            'url' => [
                'name' => 'url',
                'type' => Type::string(),
                'description' => 'url of documents searching with equals'
            ],
            'documentable_id' => [
                'name' => 'documentable_id',
                'type' => Type::string(),
                'description' => 'documentable_id of documents searching with equals'
            ],
            'documentable_type' => [
                'name' => 'documentable_type',
                'type' => Type::string(),
                'description' => 'documentable_type of documents searching with equals'
            ],
            'type_id' => [
                'name' => 'type_id',
                'type' => Type::string(),
                'description' => 'type_id of documents searching with equals'
            ],
            'created_by' => [
                'name' => 'created_by',
                'type' => Type::string(),
                'description' => 'created_by of documents searching with equals'
            ],
            'modified_by' => ['orderBy' => [
                'name' => 'orderBy',
                'type' => Type::string(),
                'description' => 'sort field'
            ],
            'sortOrder' => [
                'name' => 'sortOrder',
                'type' => Type::string(),
                'description' => 'sort order'
            ],
                'name' => 'modified_by',
                'type' => Type::string(),
                'description' => 'modified_by of documents searching with equals'
            ],
            'created_at' => [
                'name' => 'created_at',
                'type' => Type::string(),
                'description' => 'created_at of residence rooms searching with equals'
            ],
            'updated_at' => [
                'name' => 'updated_at',
                'type' => Type::string(),
                'description' => 'updated_at of residence rooms searching with equals'
            ],
            'limit' => [
                'name' => 'limit',
                'type' => Type::int(),
                'description' => 'Limit Pagination'
            ],
            'page' => [
                'type' => Type::int(),
                'description' => 'Current Page Pagination'
            ],
            'orderBy' => [
                'name' => 'orderBy',
                'type' => Type::string(),
                'description' => 'sort field'
            ],
            'sortOrder' => [
                'name' => 'sortOrder',
                'type' => Type::string(),
                'description' => 'sort order'
            ],
        ];
    }

    public function resolve($root, array $args, SelectFields $fields): LengthAwarePaginator
    {
        $limit = 10;
        $page = 1;
        $orderBy = 'created_at';
        $sortOrder = 'asc';
        $query = Model::query();
        if (isset($args['id']) && !empty($args['id'])) {
            $query->where('id', $args['id']);
        }
        if (isset($args['title']) && !empty($args['title'])) {
            $query->where('title', 'like', '%'.$args['title'].'%');
        }
        if (isset($args['filename']) && !empty($args['filename'])) {
            $query->where('filename', 'like', '%'.$args['filename'].'%');
        }
        if (isset($args['dir']) && !empty($args['dir'])) {
            $query->where('dir', 'like', '%'.$args['dir'].'%');
        }
        if (isset($args['type']) && !empty($args['type'])) {
            $query->where('type', 'like', '%'.$args['type'].'%');
        }
        if (isset($args['type']) && !empty($args['type'])) {
            $query->where('type', 'like', '%'.$args['type'].'%');
        }
        if (isset($args['size']) && !empty($args['size'])) {
            $query->where('size', 'like', '%'.$args['size'].'%');
        }
        if (isset($args['url']) && !empty($args['url'])) {
            $query->where('url', 'like', '%'.$args['url'].'%');
        }
        if (isset($args['documentable_id']) && !empty($args['documentable_id'])) {
            $query->where('documentable_id', $args['documentable_id']);
        }
        if (isset($args['documentable_type']) && !empty($args['documentable_type'])) {
            $query->where('documentable_type', 'like', '%'.$args['documentable_type'].'%');
        }
        if (isset($args['type_id']) && !empty($args['type_id'])) {
            $query->where('type_id', $args['type_id']);
        }
        if (isset($args['created_by']) && !empty($args['created_by'])) {
            $query->where('created_by', $args['created_by']);
        }
        if (isset($args['modified_by']) && !empty($args['modified_by'])) {
            $query->where('modified_by', $args['modified_by']);
        }
        if (isset($args['created_at']) && !empty($args['created_at'])) {
            $query->where('created_at', $args['created_at']);
        }
        if (isset($args['updated_at']) && !empty($args['updated_at'])) {
            $query->where('updated_at', $args['updated_at']);
        }
        if (isset($args['limit']) && !empty($args['limit'])) {
            $limit = intval($args['limit']);
        }
        if (isset($args['page']) && !empty($args['page'])) {
            $page = intval($args['page']);
        }
        if (isset($args['orderBy']) && !empty($args['orderBy'])) {
            $orderBy = $args['orderBy'];
        }
        if (isset($args['sortOrder']) && !empty($args['sortOrder'])) {
            $sortOrder = $args['sortOrder'];
        }
        $select = $fields->getSelect();
        $with = $fields->getRelations();
        array_pop($with);
        return $query->select($select)->orderBy($orderBy, $sortOrder)->with($with)->paginate($limit, ['*'], 'page', $page);

    }
}
