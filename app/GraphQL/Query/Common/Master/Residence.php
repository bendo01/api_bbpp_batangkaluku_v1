<?php
declare (strict_types = 1);
namespace Whiteboks\GraphQL\Query\Common\Master;

use Rebing\GraphQL\Support\Query;
use GraphQL\Type\Definition\Type;
use Illuminate\Support\Facades\Log;
use GraphQL\Type\Definition\ResolveInfo;
use Rebing\GraphQL\Support\SelectFields;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Illuminate\Pagination\LengthAwarePaginator;

use Whiteboks\Model\Common\Master\Residence as Model;

class Residence extends Query
{
    protected $attributes = [
        'name' => 'CommonMasterResidence',
        'description' => 'A query of Common Master Residence'
    ];

    public function type()
    {
        return GraphQL::paginate('CommonMasterResidence');
    }

    public function args()
    {
        return [
            'id' => [
                'name' => 'id',
                'type' => Type::string(),
                'description' => 'id of residences searching with equals'
            ],
            'residenceable_id' => [
                'name' => 'residenceable_id',
                'type' => Type::string(),
                'description' => 'residenceable_id of residences searching with equals'
            ],
            'residenceable_type' => [
                'name' => 'residenceable_type',
                'type' => Type::string(),
                'description' => 'residenceable_type of residences searching with equals'
            ],
            'address' => [
                'name' => 'address',
                'type' => Type::string(),
                'description' => 'address of residences searching with equals'
            ],
            'code' => [
                'name' => 'code',
                'type' => Type::string(),
                'description' => 'code of residences searching with equals'
            ],
            'name' => [
                'name' => 'name',
                'type' => Type::string(),
                'description' => 'name of residences searching with equals'
            ],
            'citizens_association' => [
                'name' => 'citizens_association',
                'type' => Type::string(),
                'description' => 'citizens_association of residences searching with equals'
            ],
            'neighborhood_association' => [
                'name' => 'neighborhood_association',
                'type' => Type::string(),
                'description' => 'neighborhood_association of residences searching with equals'
            ],
            'regency_id' => [
                'name' => 'regency_id',
                'type' => Type::string(),
                'description' => 'regency_id of residences searching with equals'
            ],
            'sub_district_id' => [
                'name' => 'sub_district_id',
                'type' => Type::string(),
                'description' => 'sub_district_id of residences searching with equals'
            ],
            'village_id' => [
                'name' => 'village_id',
                'type' => Type::string(),
                'description' => 'village_id of residences searching with equals'
            ],
            'regency_name' => [
                'name' => 'regency_name',
                'type' => Type::string(),
                'description' => 'regency_name of residences searching with equals'
            ],
            'sub_district_name' => [
                'name' => 'sub_district_name',
                'type' => Type::string(),
                'description' => 'sub_district_name of residences searching with equals'
            ],
            'village_name' => [
                'name' => 'village_name',
                'type' => Type::string(),
                'description' => 'village_name of residences searching with equals'
            ],
            'resident_type_id' => [
                'name' => 'resident_type_id',
                'type' => Type::string(),
                'description' => 'resident_type_id of residences searching with equals'
            ],
            'category_id' => [
                'name' => 'category_id',
                'type' => Type::string(),
                'description' => 'category_id of residences searching with equals'
            ],
            'type_id' => [
                'name' => 'type_id',
                'type' => Type::string(),
                'description' => 'type_id of residences searching with equals'
            ],
            'latitude' => [
                'name' => 'latitude',
                'type' => Type::string(),
                'description' => 'latitude of residences searching with equals'
            ],
            'longitude' => [
                'name' => 'longitude',
                'type' => Type::string(),
                'description' => 'longitude of residences searching with equals'
            ],
            'zoom' => [
                'name' => 'zoom',
                'type' => Type::int(),
                'description' => 'Zoom'
            ],
            'created_by' => [
                'name' => 'created_by',
                'type' => Type::string(),
                'description' => 'created_by of residences searching with equals'
            ],
            'modified_by' => [
                'name' => 'modified_by',
                'type' => Type::string(),
                'description' => 'modified_by of residences searching with equals'
            ],
            'created_at' => [
                'name' => 'created_at',
                'type' => Type::string(),
                'description' => 'created_at of residence rooms searching with equals'
            ],
            'updated_at' => [
                'name' => 'updated_at',
                'type' => Type::string(),
                'description' => 'updated_at of residence rooms searching with equals'
            ],
            'limit' => [
                'name' => 'limit',
                'type' => Type::int(),
                'description' => 'Limit Pagination'
            ],
            'page' => [
                'type' => Type::int(),
                'description' => 'Current Page Pagination'
            ],
            'orderBy' => [
                'name' => 'orderBy',
                'type' => Type::string(),
                'description' => 'sort field'
            ],
            'sortOrder' => [
                'name' => 'sortOrder',
                'type' => Type::string(),
                'description' => 'sort order'
            ],
        ];
    }

    public function resolve($root, array $args, SelectFields $fields): LengthAwarePaginator
    {
        $limit = 10;
        $page = 1;
        $orderBy = 'created_at';
        $sortOrder = 'asc';
        $query = Model::query();
        if (isset($args['id']) && !empty($args['id'])) {
            $query->where('id', $args['id']);
        }
        if (isset($args['residenceable_id']) && !empty($args['residenceable_id'])) {
            $query->where('residenceable_id', $args['residenceable_id']);
        }
        if (isset($args['residenceable_type']) && !empty($args['residenceable_type'])) {
            $query->where('residenceable_type', $args['residenceable_type']);
        }
        if (isset($args['address']) && !empty($args['address'])) {
            $query->where('address', 'like', '%'.$args['address'].'%');
        }
        if (isset($args['code']) && !empty($args['code'])) {
            $query->where('code', $args['code']);
        }
        if (isset($args['name']) && !empty($args['name'])) {
            $query->where('name', 'like', '%'.$args['name'].'%');
        }
        if (isset($args['citizens_association']) && !empty($args['citizens_association'])) {
            $query->where('citizens_association', 'like', '%'.$args['citizens_association'].'%');
        }
        if (isset($args['neighborhood_association']) && !empty($args['neighborhood_association'])) {
            $query->where('neighborhood_association', 'like', '%'.$args['neighborhood_association'].'%');
        }
        if (isset($args['regency_id']) && !empty($args['regency_id'])) {
            $query->where('regency_id', $args['regency_id']);
        }
        if (isset($args['sub_district_id']) && !empty($args['sub_district_id'])) {
            $query->where('sub_district_id', $args['sub_district_id']);
        }
        if (isset($args['village_id']) && !empty($args['village_id'])) {
            $query->where('village_id', $args['village_id']);
        }
        if (isset($args['regency_name']) && !empty($args['regency_name'])) {
            $query->where('regency_name', 'like', '%'.$args['regency_name'].'%');
        }
        if (isset($args['sub_district_name']) && !empty($args['sub_district_name'])) {
            $query->where('sub_district_name', 'like', '%'.$args['sub_district_name'].'%');
        }
        if (isset($args['village_name']) && !empty($args['village_name'])) {
            $query->where('village_name', 'like', '%'.$args['village_name'].'%');
        }
        if (isset($args['resident_type_id']) && !empty($args['resident_type_id'])) {
            $query->where('resident_type_id', $args['resident_type_id']);
        }
        if (isset($args['category_id']) && !empty($args['category_id'])) {
            $query->where('category_id', $args['category_id']);
        }
        if (isset($args['type_id']) && !empty($args['type_id'])) {
            $query->where('type_id', $args['type_id']);
        }
        if (isset($args['latitude']) && !empty($args['latitude'])) {
            $query->where('latitude', $args['latitude']);
        }
        if (isset($args['longitude']) && !empty($args['longitude'])) {
            $query->where('longitude', $args['longitude']);
        }
        if (isset($args['zoom']) && !empty($args['zoom'])) {
            $query->where('zoom', $args['zoom']);
        }
        if (isset($args['created_by']) && !empty($args['created_by'])) {
            $query->where('created_by', $args['created_by']);
        }
        if (isset($args['modified_by']) && !empty($args['modified_by'])) {
            $query->where('modified_by', $args['modified_by']);
        }
        if (isset($args['created_at']) && !empty($args['created_at'])) {
            $query->where('created_at', $args['created_at']);
        }
        if (isset($args['updated_at']) && !empty($args['updated_at'])) {
            $query->where('updated_at', $args['updated_at']);
        }
        if (isset($args['limit']) && !empty($args['limit'])) {
            $limit = intval($args['limit']);
        }
        if (isset($args['page']) && !empty($args['page'])) {
            $page = intval($args['page']);
        }
        if (isset($args['orderBy']) && !empty($args['orderBy'])) {
            $orderBy = $args['orderBy'];
        }
        if (isset($args['sortOrder']) && !empty($args['sortOrder'])) {
            $sortOrder = $args['sortOrder'];
        }
        $select = $fields->getSelect();
        $with = $fields->getRelations();
        array_pop($with);
        return $query->select($select)->orderBy($orderBy, $sortOrder)->with($with)->paginate($limit, ['*'], 'page', $page);

    }
}
