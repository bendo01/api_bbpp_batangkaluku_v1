<?php
declare (strict_types = 1);
namespace Whiteboks\GraphQL\Query\Common\Master;

use Rebing\GraphQL\Support\Query;
use GraphQL\Type\Definition\Type;
use Illuminate\Support\Facades\Log;
use GraphQL\Type\Definition\ResolveInfo;
use Rebing\GraphQL\Support\SelectFields;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Illuminate\Pagination\LengthAwarePaginator;

use Whiteboks\Model\Common\Master\ResidenceRoom as Model;

class ResidenceRoom extends Query
{
    protected $attributes = [
        'name' => 'CommonMasterResidenceRoom',
        'description' => 'A query of Common Master Residence Room'
    ];

    public function type()
    {
        return GraphQL::paginate('CommonMasterResidenceRoom');
    }

    public function args()
    {
        return [
            'id' => [
                'name' => 'id',
                'type' => Type::string(),
                'description' => 'id of residence rooms searching with equals'
            ],
            'residence_id' => [
                'name' => 'residence_id',
                'type' => Type::string(),
                'description' => 'residence_id of residence rooms searching with equals'
            ],
            'code' => [
                'name' => 'code',
                'type' => Type::string(),
                'description' => 'code of residence rooms searching with equals'
            ],
            'name' => [
                'name' => 'name',
                'type' => Type::string(),
                'description' => 'name of residence rooms searching with equals'
            ],
            'floor' => [
                'name' => 'floor',
                'type' => Type::string(),
                'description' => 'floor of residence rooms searching with equals'
            ],
            'long' => [
                'name' => 'long',
                'type' => Type::string(),
                'description' => 'long of residence rooms searching with equals'
            ],
            'high' => [
                'name' => 'high',
                'type' => Type::string(),
                'description' => 'high of residence rooms searching with equals'
            ],
            'latitude' => [
                'name' => 'latitude',
                'type' => Type::string(),
                'description' => 'latitude of residence rooms searching with equals'
            ],
            'longitude' => [
                'name' => 'longitude',
                'type' => Type::string(),
                'description' => 'longitude of residence rooms searching with equals'
            ],
            'zoom' => [
                'name' => 'zoom',
                'type' => Type::int(),
                'description' => 'zoom'
            ],
            'room_type_id' => [
                'name' => 'room_type_id',
                'type' => Type::string(),
                'description' => 'room_type_id of residence rooms searching with equals'
            ],
            'created_by' => [
                'name' => 'created_by',
                'type' => Type::string(),
                'description' => 'created_by of residence rooms searching with equals'
            ],
            'modified_by' => [
                'name' => 'modified_by',
                'type' => Type::string(),
                'description' => 'modified_by of residence rooms searching with equals'
            ],
            'created_at' => [
                'name' => 'created_at',
                'type' => Type::string(),
                'description' => 'created_at of residence rooms searching with equals'
            ],
            'updated_at' => [
                'name' => 'updated_at',
                'type' => Type::string(),
                'description' => 'updated_at of residence rooms searching with equals'
            ],
            'limit' => [
                'name' => 'limit',
                'type' => Type::int(),
                'description' => 'Limit Pagination'
            ],
            'page' => [
                'type' => Type::int(),
                'description' => 'Current Page Pagination'
            ]
        ];
    }

    public function resolve($root, array $args, SelectFields $fields): LengthAwarePaginator
    {
        $limit = 10;
        $page = 1;
        $orderBy = 'created_at';
        $sortOrder = 'asc';
        $query = Model::query();
        if (isset($args['id']) && !empty($args['id'])) {
            $query->where('id', $args['id']);
        }
        if (isset($args['residence_id']) && !empty($args['residence_id'])) {
            $query->where('residence_id', $args['residence_id']);
        }
        if (isset($args['code']) && !empty($args['code'])) {
            $query->where('code', $args['code']);
        }
        if (isset($args['name']) && !empty($args['name'])) {
            $query->where('name', 'like', '%'.$args['name'].'%');
        }
        if (isset($args['floor']) && !empty($args['floor'])) {
            $query->where('floor', 'like', '%'.$args['floor'].'%');
        }
        if (isset($args['long']) && !empty($args['long'])) {
            $query->where('long', 'like', '%'.$args['long'].'%');
        }
        if (isset($args['wide']) && !empty($args['wide'])) {
            $query->where('wide', 'like', '%'.$args['wide'].'%');
        }
        if (isset($args['high']) && !empty($args['high'])) {
            $query->where('high', 'like', '%'.$args['high'].'%');
        }
        if (isset($args['latitude']) && !empty($args['latitude'])) {
            $query->where('latitude', 'like', '%'.$args['latitude'].'%');
        }
        if (isset($args['longitude']) && !empty($args['longitude'])) {
            $query->where('longitude', 'like', '%'.$args['longitude'].'%');
        }
        if (isset($args['zoom']) && !empty($args['zoom'])) {
            $query->where('zoom', $args['zoom']);
        }
        if (isset($args['longitude']) && !empty($args['longitude'])) {
            $query->where('longitude', 'like', '%'.$args['longitude'].'%');
        }
        if (isset($args['created_by']) && !empty($args['created_by'])) {
            $query->where('created_by', $args['created_by']);
        }
        if (isset($args['modified_by']) && !empty($args['modified_by'])) {
            $query->where('modified_by', $args['modified_by']);
        }
        if (isset($args['created_at']) && !empty($args['created_at'])) {
            $query->where('created_at', $args['created_at']);
        }
        if (isset($args['updated_at']) && !empty($args['updated_at'])) {
            $query->where('updated_at', $args['updated_at']);
        }
        if (isset($args['limit']) && !empty($args['limit'])) {
            $limit = intval($args['limit']);
        }
        if (isset($args['page']) && !empty($args['page'])) {
            $page = intval($args['page']);
        }
        if (isset($args['orderBy']) && !empty($args['orderBy'])) {
            $orderBy = $args['orderBy'];
        }
        if (isset($args['sortOrder']) && !empty($args['sortOrder'])) {
            $sortOrder = $args['sortOrder'];
        }
        $select = $fields->getSelect();
        $with = $fields->getRelations();
        array_pop($with);
        return $query->select($select)->orderBy($orderBy, $sortOrder)->with($with)->paginate($limit, ['*'], 'page', $page);
    }
}
