<?php
declare (strict_types = 1);
namespace Whiteboks\GraphQL\Query\Common\Master;

use Rebing\GraphQL\Support\Query;
use GraphQL\Type\Definition\Type;
use Illuminate\Support\Facades\Log;
use GraphQL\Type\Definition\ResolveInfo;
use Rebing\GraphQL\Support\SelectFields;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Illuminate\Pagination\LengthAwarePaginator;

use Whiteboks\Model\Common\Master\SocialMedia as Model;

class SocialMedia extends Query
{
    protected $attributes = [
        'name' => 'CommonMasterSocialMedia',
        'description' => 'A query of Common Master Social Media'
    ];

    public function type()
    {
        return GraphQL::paginate('CommonMasterSocialMedia');
    }

    public function args()
    {
        return [
            'id' => [
                'name' => 'id',
                'type' => Type::string(),
                'description' => 'id of social medias searching with equals'
            ],
            'username' => [
                'name' => 'username',
                'type' => Type::string(),
                'description' => 'username of social medias searching with equals'
            ],
            'url' => [
                'name' => 'url',
                'type' => Type::string(),
                'description' => 'url of social medias searching with equals'
            ],
            'social_mediaable_id' => [
                'name' => 'social_mediaable_id',
                'type' => Type::string(),
                'description' => 'social_mediaable_id of social medias searching with equals'
            ],
            'social_mediaable_type' => [
                'name' => 'social_mediaable_type',
                'type' => Type::string(),
                'description' => 'social_mediaable_type of social medias searching with equals'
            ],
            'created_by' => [
                'name' => 'created_by',
                'type' => Type::string(),
                'description' => 'created_by of social medias searching with equals'
            ],
            'modified_by' => [
                'name' => 'modified_by',
                'type' => Type::string(),
                'description' => 'modified_by of social medias searching with equals'
            ],
            'created_at' => [
                'name' => 'created_at',
                'type' => Type::string(),
                'description' => 'created_at of social medias searching with equals'
            ],
            'updated_at' => [
                'name' => 'updated_at',
                'type' => Type::string(),
                'description' => 'updated_at of social medias searching with equals'
            ],
            'limit' => [
                'name' => 'limit',
                'type' => Type::int(),
                'description' => 'Limit Pagination'
            ],
            'page' => [
                'type' => Type::int(),
                'description' => 'Current Page Pagination'
            ],
            'orderBy' => [
                'name' => 'orderBy',
                'type' => Type::string(),
                'description' => 'sort field'
            ],
            'sortOrder' => [
                'name' => 'sortOrder',
                'type' => Type::string(),
                'description' => 'sort order'
            ],
        ];
    }

    public function resolve($root, array $args, SelectFields $fields): LengthAwarePaginator
    {
        $limit = 10;
        $page = 1;
        $orderBy = 'created_at';
        $sortOrder = 'asc';
        $query = Model::query();
        if (isset($args['id']) && !empty($args['id'])) {
            $query->where('id', $args['id']);
        }
        if (isset($args['username']) && !empty($args['username'])) {
            $query->where('username', 'like', '%'.$args['username'].'%');
        }
        if (isset($args['url']) && !empty($args['url'])) {
            $query->where('url', 'like', '%'.$args['url'].'%');
        }
        if (isset($args['social_mediaable_id']) && !empty($args['social_mediaable_id'])) {
            $query->where('social_mediaable_id', $args['social_mediaable_id']);
        }
        if (isset($args['social_mediaable_type']) && !empty($args['social_mediaable_type'])) {
            $query->where('social_mediaable_type', $args['social_mediaable_type']);
        }
        if (isset($args['created_by']) && !empty($args['created_by'])) {
            $query->where('created_by', $args['created_by']);
        }
        if (isset($args['modified_by']) && !empty($args['modified_by'])) {
            $query->where('modified_by', $args['modified_by']);
        }
        if (isset($args['created_at']) && !empty($args['created_at'])) {
            $query->where('created_at', $args['created_at']);
        }
        if (isset($args['updated_at']) && !empty($args['updated_at'])) {
            $query->where('updated_at', $args['updated_at']);
        }
        if (isset($args['limit']) && !empty($args['limit'])) {
            $limit = intval($args['limit']);
        }
        if (isset($args['page']) && !empty($args['page'])) {
            $page = intval($args['page']);
        }
        if (isset($args['orderBy']) && !empty($args['orderBy'])) {
            $orderBy = $args['orderBy'];
        }
        if (isset($args['sortOrder']) && !empty($args['sortOrder'])) {
            $sortOrder = $args['sortOrder'];
        }
        $select = $fields->getSelect();
        $with = $fields->getRelations();
        array_pop($with);
        return $query->select($select)->orderBy($orderBy, $sortOrder)->with($with)->paginate($limit, ['*'], 'page', $page);

    }
}
