<?php
declare (strict_types = 1);

namespace Whiteboks\GraphQL\Query\Module\Inventory\Transaction;

use Rebing\GraphQL\Support\Query;
use GraphQL\Type\Definition\Type;
use Illuminate\Support\Facades\Log;
use GraphQL\Type\Definition\ResolveInfo;
use Rebing\GraphQL\Support\SelectFields;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Illuminate\Pagination\LengthAwarePaginator;

use Whiteboks\Model\Module\Inventory\Transaction\Placement as Model;

class Placement extends Query
{
    protected $attributes = [
        'name' => 'ModuleInventoryTransactionPlacement',
        'description' => 'A query of Inventory Placement'
    ];

    public function type()
    {
        return GraphQL::paginate('ModuleInventoryTransactionPlacement');
    }

    public function args()
    {
        return [
            'id' => [
                'name' => 'id',
                'type' => Type::string(),
                'description' => 'id of placements searching with equals'
            ],
            'code' => [
                'name' => 'code',
                'type' => Type::string(),
                'description' => 'code of placements searching with equals'
            ],
            'name' => [
                'name' => 'name',
                'type' => Type::string(),
                'description' => 'name of placements searching with equals'
            ],
            'unit_id' => [
                'name' => 'unit_id',
                'type' => Type::string(),
                'description' => 'unit_id of placements searching with equals'
            ],
            'recident_room_id' => [
                'name' => 'recident_room_id',
                'type' => Type::string(),
                'description' => 'recident_room_id of placements searching with equals'
            ],
            'catalog_id' => [
                'name' => 'catalog_id',
                'type' => Type::string(),
                'description' => 'catalog_id of placements searching with equals'
            ],
            'condition_id' => [
                'name' => 'condition_id',
                'type' => Type::string(),
                'description' => 'condition_id of placements searching with equals'
            ],
            'created_by' => [
                'name' => 'created_by',
                'type' => Type::string(),
                'description' => 'created_by of placements searching with equals'
            ],
            'modified_by' => [
                'name' => 'modified_by',
                'type' => Type::string(),
                'description' => 'modified_by of placements searching with equals'
            ],
            'created_at' => [
                'name' => 'created_at',
                'type' => Type::string(),
                'description' => 'created_at of placements searching with equals'
            ],
            'updated_at' => [
                'name' => 'updated_at',
                'type' => Type::string(),
                'description' => 'updated_at of placements searching with equals'
            ],
            'limit' => [
                'name' => 'limit',
                'type' => Type::int(),
                'description' => 'Limit Pagination'
            ],
            'page' => [
                'type' => Type::int(),
                'description' => 'Current Page Pagination'
            ],
            'orderBy' => [
                'name' => 'orderBy',
                'type' => Type::string(),
                'description' => 'sort field'
            ],
            'sortOrder' => [
                'name' => 'sortOrder',
                'type' => Type::string(),
                'description' => 'sort order'
            ],
        ];
    }

    public function resolve($root, array $args, SelectFields $fields): LengthAwarePaginator
    {
        $limit = 10;
        $page = 1;
        $orderBy = 'created_at';
        $sortOrder = 'asc';
        $query = Model::query();
        if (isset($args['id']) && !empty($args['id'])) {
            $query->where('id', $args['id']);
        }
        if (isset($args['code']) && !empty($args['code'])) {
            $query->where('code', $args['code']);
        }
        if (isset($args['name']) && !empty($args['name'])) {
            $query->where('name', $args['name']);
        }
        if (isset($args['unit_id']) && !empty($args['unit_id'])) {
            $query->where('unit_id', $args['unit_id']);
        }
        if (isset($args['recident_room_id']) && !empty($args['recident_room_id'])) {
            $query->where('recident_room_id', $args['recident_room_id']);
        }
        if (isset($args['catalog_id']) && !empty($args['catalog_id'])) {
            $query->where('catalog_id', $args['catalog_id']);
        }
        if (isset($args['condition_id']) && !empty($args['condition_id'])) {
            $query->where('condition_id', $args['condition_id']);
        }
        if (isset($args['created_by']) && !empty($args['created_by'])) {
            $query->where('created_by', $args['created_by']);
        }
        if (isset($args['modified_by']) && !empty($args['modified_by'])) {
            $query->where('modified_by', $args['modified_by']);
        }
        if (isset($args['created_at']) && !empty($args['created_at'])) {
            $query->where('created_at', $args['created_at']);
        }
        if (isset($args['updated_at']) && !empty($args['updated_at'])) {
            $query->where('updated_at', $args['updated_at']);
        }
        if (isset($args['limit']) && !empty($args['limit'])) {
            $limit = intval($args['limit']);
        }
        if (isset($args['page']) && !empty($args['page'])) {
            $page = intval($args['page']);
        }
        if (isset($args['orderBy']) && !empty($args['orderBy'])) {
            $orderBy = $args['orderBy'];
        }
        if (isset($args['sortOrder']) && !empty($args['sortOrder'])) {
            $sortOrder = $args['sortOrder'];
        }
        $select = $fields->getSelect();
        $with = $fields->getRelations();
        array_pop($with);
        return $query->select($select)->orderBy($orderBy, $sortOrder)->with($with)->paginate($limit, ['*'], 'page', $page);

    }
}
