<?php
declare (strict_types = 1);

namespace Whiteboks\GraphQL\Query\Module\HumanResource\Master;

use Rebing\GraphQL\Support\Query;
use GraphQL\Type\Definition\Type;
use Illuminate\Support\Facades\Log;
use GraphQL\Type\Definition\ResolveInfo;
use Rebing\GraphQL\Support\SelectFields;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Illuminate\Pagination\LengthAwarePaginator;

use Whiteboks\Model\Module\HumanResource\Master\Employee as Model;

class Employee extends Query
{
    protected $attributes = [
        'name' => 'ModuleHumanResourceMasterEmployee',
        'description' => 'A query of Employee'
    ];

    public function type()
    {
        return GraphQL::paginate('ModuleHumanResourceMasterEmployee');
    }

    public function args()
    {
        return [
            'id' => [
                'name' => 'id',
                'type' => Type::string(),
                'description' => 'id of employees searching with equals'
            ],
            'code' => [
                'name' => 'code',
                'type' => Type::string(),
                'description' => 'code of employees searching with equals'
            ],
            'individual_id' => [
                'name' => 'individual_id',
                'type' => Type::string(),
                'description' => 'individual_id of employees searching with equals'
            ],
            'status_id' => [
                'name' => 'status_id',
                'type' => Type::string(),
                'description' => 'status_id of employees searching with equals'
            ],
            'contract_type_id' => [
                'name' => 'contract_type_id',
                'type' => Type::string(),
                'description' => 'contract_type_id of employees searching with equals'
            ],
            'unit_id' => [
                'name' => 'unit_id',
                'type' => Type::string(),
                'description' => 'unit_id of employees searching with equals'
            ],
            'institution_id' => [
                'name' => 'institution_id',
                'type' => Type::string(),
                'description' => 'institution_id of employees searching with equals'
            ],
            'resign_type_id' => [
                'name' => 'resign_type_id',
                'type' => Type::string(),
                'description' => 'resign_type_id of employees searching with equals'
            ],
            'visa' => [
                'name' => 'visa',
                'type' => Type::int(),
                'description' => 'visa of employees searching with equals'
            ],
            'created_by' => [
                'name' => 'created_by',
                'type' => Type::string(),
                'description' => 'created_by of employees searching with equals'
            ],
            'modified_by' => [
                'name' => 'modified_by',
                'type' => Type::string(),
                'description' => 'modified_by of employees searching with equals'
            ],
            'created_at' => [
                'name' => 'created_at',
                'type' => Type::string(),
                'description' => 'created_at of employees searching with equals'
            ],
            'updated_at' => [
                'name' => 'updated_at',
                'type' => Type::string(),
                'description' => 'updated_at of employees searching with equals'
            ],
            'limit' => [
                'name' => 'limit',
                'type' => Type::int(),
                'description' => 'Limit Pagination'
            ],
            'page' => [
                'type' => Type::int(),
                'description' => 'Current Page Pagination'
            ],
            'orderBy' => [
                'name' => 'orderBy',
                'type' => Type::string(),
                'description' => 'sort field'
            ],
            'sortOrder' => [
                'name' => 'sortOrder',
                'type' => Type::string(),
                'description' => 'sort order'
            ],
        ];
    }

    public function resolve($root, array $args, SelectFields $fields): LengthAwarePaginator
    {
        $limit = 10;
        $page = 1;
        $orderBy = 'created_at';
        $sortOrder = 'asc';
        $query = Model::query();
        if (isset($args['id']) && !empty($args['id'])) {
            $query->where('id', $args['id']);
        }
        if (isset($args['code']) && !empty($args['code'])) {
            $query->where('code', $args['code']);
        }
        if (isset($args['individual_id']) && !empty($args['individual_id'])) {
            $query->where('individual_id', $args['individual_id']);
        }
        if (isset($args['status_id']) && !empty($args['status_id'])) {
            $query->where('status_id', $args['status_id']);
        }
        if (isset($args['contract_type_id']) && !empty($args['contract_type_id'])) {
            $query->where('contract_type_id', $args['contract_type_id']);
        }
        if (isset($args['unit_id']) && !empty($args['unit_id'])) {
            $query->where('unit_id', $args['unit_id']);
        }
        if (isset($args['institution_id']) && !empty($args['institution_id'])) {
            $query->where('institution_id', $args['institution_id']);
        }
        if (isset($args['resign_type_id']) && !empty($args['resign_type_id'])) {
            $query->where('resign_type_id', $args['resign_type_id']);
        }
        if (isset($args['visa']) && !empty($args['visa'])) {
            $query->where('visa', $args['visa']);
        }
        if (isset($args['created_by']) && !empty($args['created_by'])) {
            $query->where('created_by', $args['created_by']);
        }
        if (isset($args['modified_by']) && !empty($args['modified_by'])) {
            $query->where('modified_by', $args['modified_by']);
        }
        if (isset($args['created_at']) && !empty($args['created_at'])) {
            $query->where('created_at', $args['created_at']);
        }
        if (isset($args['updated_at']) && !empty($args['updated_at'])) {
            $query->where('updated_at', $args['updated_at']);
        }
        if (isset($args['limit']) && !empty($args['limit'])) {
            $limit = intval($args['limit']);
        }
        if (isset($args['page']) && !empty($args['page'])) {
            $page = intval($args['page']);
        }
        if (isset($args['orderBy']) && !empty($args['orderBy'])) {
            $orderBy = $args['orderBy'];
        }
        if (isset($args['sortOrder']) && !empty($args['sortOrder'])) {
            $sortOrder = $args['sortOrder'];
        }
        $select = $fields->getSelect();
        $with = $fields->getRelations();
        array_pop($with);
        return $query->select($select)->orderBy($orderBy, $sortOrder)->with($with)->paginate($limit, ['*'], 'page', $page);

    }
}
