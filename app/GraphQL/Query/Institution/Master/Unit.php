<?php
declare (strict_types = 1);

namespace Whiteboks\GraphQL\Query\Institution\Master;

use Rebing\GraphQL\Support\Query;
use GraphQL\Type\Definition\Type;
use Illuminate\Support\Facades\Log;
use GraphQL\Type\Definition\ResolveInfo;
use Rebing\GraphQL\Support\SelectFields;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Illuminate\Pagination\LengthAwarePaginator;

use Whiteboks\Model\Institution\Master\Unit as Model;

class Unit extends Query
{
    protected $attributes = [
        'name' => 'InstitutionMasterUnit',
        'description' => 'A query of Institution Master Unit'
    ];

    public function type()
    {
        return GraphQL::paginate('InstitutionMasterUnit');
    }

    public function args()
    {
        return [
            'id' => [
                'name' => 'id',
                'type' => Type::string(),
                'description' => 'id of units searching with equals'
            ],
            'code' => [
                'name' => 'code',
                'type' => Type::string(),
                'description' => 'code of units searching with equals'
            ],
            'name' => [
                'name' => 'name',
                'type' => Type::string(),
                'description' => 'name of units searching with equals'
            ],
            'is_active' => [
                'name' => 'is_active',
                'type' => Type::int(),
                'description' => 'is_active of units searching with equals'
            ],
            'abbreviation' => [
                'name' => 'abbreviation',
                'type' => Type::string(),
                'description' => 'abbreviation of units searching with equals'
            ],
            'slug' => [
                'name' => 'slug',
                'type' => Type::string(),
                'description' => 'slug of units searching with equals'
            ],
            'unit_type_id' => [
                'name' => 'unit_type_id',
                'type' => Type::string(),
                'description' => 'unit_type_id of units searching with equals'
            ],
            'institution_id' => [
                'name' => 'institution_id',
                'type' => Type::string(),
                'description' => 'institution_id of units searching with equals'
            ],
            'description' => [
                'name' => 'description',
                'type' => Type::string(),
                'description' => 'description of units searching with equals'
            ],
            'parent_id' => [
                'name' => 'parent_id',
                'type' => Type::string(),
                'description' => 'parent_id of units searching with equals'
            ],
            '_lft' => [
                'name' => '_lft',
                'type' => Type::int(),
                'description' => '_lft of units searching with equals'
            ],
            '_rgt' => [
                'name' => '_rgt',
                'type' => Type::int(),
                'description' => '_rgt of units searching with equals'
            ],
            'created_by' => [
                'name' => 'created_by',
                'type' => Type::string(),
                'description' => 'created_by of units searching with equals'
            ],
            'modified_by' => [
                'name' => 'modified_by',
                'type' => Type::string(),
                'description' => 'modified_by of units searching with equals'
            ],
            'created_at' => [
                'name' => 'created_at',
                'type' => Type::string(),
                'description' => 'created_at of units searching with equals'
            ],
            'updated_at' => [
                'name' => 'updated_at',
                'type' => Type::string(),
                'description' => 'updated_at of units searching with equals'
            ],
            'limit' => [
                'name' => 'limit',
                'type' => Type::int(),
                'description' => 'Limit Pagination'
            ],
            'page' => [
                'type' => Type::int(),
                'description' => 'Current Page Pagination'
            ],
            'orderBy' => [
                'name' => 'orderBy',
                'type' => Type::string(),
                'description' => 'sort field'
            ],
            'sortOrder' => [
                'name' => 'sortOrder',
                'type' => Type::string(),
                'description' => 'sort order'
            ],
        ];
    }

    public function resolve($root, array $args, SelectFields $fields): LengthAwarePaginator
    {
        $limit = 10;
        $page = 1;
        $orderBy = 'created_at';
        $sortOrder = 'asc';
        $query = Model::query();
        if (isset($args['id']) && !empty($args['id'])) {
            $query->where('id', $args['id']);
        }
        if (isset($args['code']) && !empty($args['code'])) {
            $query->where('code', $args['code']);
        }
        if (isset($args['name']) && !empty($args['name'])) {
            $query->where('name', 'like', '%'.$args['name'].'%');
        }
        if (isset($args['is_active']) && !empty($args['is_active'])) {
            $query->where('is_active', $args['is_active']);
        }
        if (isset($args['abbreviation']) && !empty($args['abbreviation'])) {
            $query->where('abbreviation', 'like', '%'.$args['abbreviation'].'%');
        }
        if (isset($args['slug']) && !empty($args['slug'])) {
            $query->where('slug', 'like', '%'.$args['slug'].'%');
        }
        if (isset($args['unit_type_id']) && !empty($args['unit_type_id'])) {
            $query->where('unit_type_id', $args['unit_type_id']);
        }
        if (isset($args['institution_id']) && !empty($args['institution_id'])) {
            $query->where('institution_id', $args['institution_id']);
        }
        if (isset($args['description']) && !empty($args['description'])) {
            $query->where('description', 'like', '%'.$args['description'].'%');
        }
        if (isset($args['parent_id']) && !empty($args['parent_id'])) {
            $query->where('parent_id', $args['parent_id']);
        }
        if (isset($args['_lft']) && !empty($args['_lft'])) {
            $query->where('_lft', $args['_lft']);
        }
        if (isset($args['_rgt']) && !empty($args['_rgt'])) {
            $query->where('_rgt', $args['_rgt']);
        }
        if (isset($args['created_by']) && !empty($args['created_by'])) {
            $query->where('created_by', $args['created_by']);
        }
        if (isset($args['modified_by']) && !empty($args['modified_by'])) {
            $query->where('modified_by', $args['modified_by']);
        }
        if (isset($args['created_at']) && !empty($args['created_at'])) {
            $query->where('created_at', $args['created_at']);
        }
        if (isset($args['updated_at']) && !empty($args['updated_at'])) {
            $query->where('updated_at', $args['updated_at']);
        }
        if (isset($args['limit']) && !empty($args['limit'])) {
            $limit = intval($args['limit']);
        }
        if (isset($args['page']) && !empty($args['page'])) {
            $page = intval($args['page']);
        }
        if (isset($args['orderBy']) && !empty($args['orderBy'])) {
            $orderBy = $args['orderBy'];
        }
        if (isset($args['sortOrder']) && !empty($args['sortOrder'])) {
            $sortOrder = $args['sortOrder'];
        }
        $select = $fields->getSelect();
        $with = $fields->getRelations();
        array_pop($with);
        return $query->select($select)->orderBy($orderBy, $sortOrder)->with($with)->paginate($limit, ['*'], 'page', $page);

    }
}
