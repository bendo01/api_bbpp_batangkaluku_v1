<?php
declare (strict_types = 1);
namespace Whiteboks\Overrides\Notifications;

use Illuminate\Notifications\RoutesNotifications;

trait Notifiable
{
    use HasDatabaseNotifications, RoutesNotifications;
}
