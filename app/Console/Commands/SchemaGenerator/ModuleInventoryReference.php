<?php

namespace Whiteboks\Console\Commands\SchemaGenerator;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Artisan;

class ModuleInventoryReference extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'schema-generator:module-inventory-reference';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Refresh Schema Module Inventory Reference';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        DB::statement('CREATE EXTENSION IF NOT EXISTS "adminpack"');
        DB::statement('CREATE EXTENSION IF NOT EXISTS "pgcrypto"');
        DB::statement('CREATE EXTENSION IF NOT EXISTS "uuid-ossp"');
        DB::statement("DELETE FROM public.migrations WHERE migration LIKE '%_module_inventory_reference_%'");
        DB::statement('DROP SCHEMA IF EXISTS module_inventory_reference CASCADE');
        DB::statement('CREATE SCHEMA IF NOT EXISTS module_inventory_reference AUTHORIZATION bendo01');
        Artisan::call('migrate', ['--path'=>'database/migrations/module/inventory/reference']);
    }
}
