<?php

namespace Whiteboks\Console\Commands\SchemaGenerator;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;

class RefreshAll extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'schema-generator:refresh';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Refresh All Schema';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Artisan::call('schema-generator:public');
        Artisan::call('schema-generator:authentification');
        Artisan::call('schema-generator:common-master');
        Artisan::call('schema-generator:common-reference-decree');
        Artisan::call('schema-generator:common-reference-document');
        Artisan::call('schema-generator:common-reference-education');
        Artisan::call('schema-generator:common-reference-period');
        Artisan::call('schema-generator:common-reference-residence');
        Artisan::call('schema-generator:institution-master');
        Artisan::call('schema-generator:institution-reference');
        Artisan::call('schema-generator:location');
        Artisan::call('schema-generator:module-human-resource-master');
        Artisan::call('schema-generator:module-human-resource-reference');
        Artisan::call('schema-generator:module-inventory-master');
        Artisan::call('schema-generator:module-inventory-reference');
        Artisan::call('schema-generator:module-inventory-transaction');
        Artisan::call('schema-generator:module-revenue-transaction');
        Artisan::call('schema-generator:module-training-master');
        Artisan::call('schema-generator:module-training-reference');
        Artisan::call('schema-generator:module-training-reference-learning-material');
        Artisan::call('schema-generator:module-training-transaction');
        Artisan::call('schema-generator:person-master');
        Artisan::call('schema-generator:person-reference-biodata');
        Artisan::call('schema-generator:person-reference-career');
    }
}
