<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::post('graphql/{schema}/query', ['as' => 'api.graphql.query', 'uses' => '\Rebing\GraphQL\GraphQLController@query']);
Route::post('person/register', ['as'=>'api.person.register', 'uses' => 'Person\RegisterController@register']);
Route::middleware('auth:api')->get('oauth/authorize', ['as' => 'api.oauth.authorize', 'uses' => '\Laravel\Passport\Http\Controllers\AuthorizationController@authorize']);
Route::middleware('auth:api')->post('oauth/authorize', ['as' => 'api.oauth.approve', 'uses' => '\Laravel\Passport\Http\Controllers\ApproveAuthorizationController@approve']);
Route::middleware('auth:api')->delete('oauth/authorize', ['as' => 'api.oauth.delete', 'uses' => '\Laravel\Passport\Http\Controllers\DenyAuthorizationController@deny']);
Route::middleware('throttle')->post('oauth/token', ['as' => 'api.oauth.token.store', 'uses' => '\Laravel\Passport\Http\Controllers\AccessTokenController@issueToken']);
Route::middleware('auth:api')->get('oauth/tokens', ['as' => 'api.oauth.token.get', 'uses' => '\Laravel\Passport\Http\Controllers\AuthorizedAccessTokenController@forUser']);
Route::middleware('auth:api')->delete('oauth/tokens/{token_id}', ['as' => 'api.oauth.token.destroy', 'uses' => '\Laravel\Passport\Http\Controllers\AuthorizedAccessTokenController@destroy']);
Route::middleware('auth:api')->post('oauth/token/refresh', ['as' => 'api.oauth.token.refresh', 'uses' => '\Laravel\Passport\Http\Controllers\TransientTokenController@refresh']);
Route::middleware('auth:api')->get('oauth/clients', ['as' => 'api.oauth.clients.index', 'uses' => '\Laravel\Passport\Http\Controllers\ClientController@forUser']);
Route::middleware('auth:api')->post('oauth/clients', ['as' => 'api.oauth.clients.store', 'uses' => '\Laravel\Passport\Http\Controllers\ClientController@store']);
Route::middleware('auth:api')->put('oauth/clients/{client_id}', ['as' => 'api.oauth.clients.update', 'uses' => '\Laravel\Passport\Http\Controllers\ClientController@update']);
Route::middleware('auth:api')->delete('oauth/clients/{client_id}', ['as' => 'api.oauth.clients.destroy', 'uses' => '\Laravel\Passport\Http\Controllers\ClientController@destroy']);
Route::middleware('auth:api')->get('oauth/scopes', ['as' => 'api.oauth.clients.scopes', 'uses' => '\Laravel\Passport\Http\Controllers\ScopeController@all']);
Route::middleware('auth:api')->get('oauth/personal-access-tokens', ['as' => 'api.oauth.personal_access_tokens.index', 'uses' => '\Laravel\Passport\Http\Controllers\PersonalAccessTokenController@forUser']);
Route::middleware('auth:api')->post('oauth/personal-access-tokens', ['as' => 'api.oauth.personal_access_tokens.store', 'uses' => '\Laravel\Passport\Http\Controllers\PersonalAccessTokenController@store']);
Route::middleware('auth:api')->delete('oauth/personal-access-tokens/{token_id}', ['as' => 'api.oauth.personal_access_tokens.destroy', 'uses' => '\Laravel\Passport\Http\Controllers\PersonalAccessTokenController@destroy']);

