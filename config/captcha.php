<?php
/*
 * Secret key and Site key get on https://www.google.com/recaptcha
 * */
return [
    'secret' => env('CAPTCHA_SECRET', null),
    'sitekey' => env('CAPTCHA_SITEKEY', null)
];
