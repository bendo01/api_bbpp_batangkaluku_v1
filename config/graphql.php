<?php
// Auth
use Whiteboks\GraphQL\Type\Auth\Ability as AuthAbilityType;
use Whiteboks\GraphQL\Query\Auth\Ability as AuthAbilityQuery;
use Whiteboks\GraphQL\Mutation\Auth\Ability\Store as AuthAbilityStore;
use Whiteboks\GraphQL\Mutation\Auth\Ability\Update as AuthAbilityUpdate;
use Whiteboks\GraphQL\Mutation\Auth\Ability\Delete as AuthAbilityDelete;
// ---------------------
use Whiteboks\GraphQL\Type\Auth\Role as AuthRoleType;
use Whiteboks\GraphQL\Query\Auth\Role as AuthRoleQuery;
use Whiteboks\GraphQL\Mutation\Auth\Role\Store as AuthRoleStore;
use Whiteboks\GraphQL\Mutation\Auth\Role\Update as AuthRoleUpdate;
use Whiteboks\GraphQL\Mutation\Auth\Role\Delete as AuthRoleDelete;
// ---------------------
use Whiteboks\GraphQL\Type\Auth\SocialAccount as AuthSocialAccountType;
use Whiteboks\GraphQL\Query\Auth\SocialAccount as AuthSocialAccountQuery;
use Whiteboks\GraphQL\Mutation\Auth\SocialAccount\Store as AuthSocialAccountStore;
use Whiteboks\GraphQL\Mutation\Auth\SocialAccount\Update as AuthSocialAccountUpdate;
use Whiteboks\GraphQL\Mutation\Auth\SocialAccount\Delete as AuthSocialAccountDelete;
// ---------------------
use Whiteboks\GraphQL\Type\Auth\User as AuthUserType;
use Whiteboks\GraphQL\Query\Auth\User as AuthUserQuery;
use Whiteboks\GraphQL\Mutation\Auth\User\Store as AuthUserStore;
use Whiteboks\GraphQL\Mutation\Auth\User\Update as AuthUserUpdate;
use Whiteboks\GraphQL\Mutation\Auth\User\Delete as AuthUserDelete;
use Whiteboks\GraphQL\Mutation\Auth\User\ToggleUserAdmin as AuthUserToggleUserAdmin;
use Whiteboks\GraphQL\Mutation\Auth\User\ToggleUserActive as AuthUserToggleUserActive;
// End Auth
// Common Master Address
use Whiteboks\GraphQL\Type\Common\Master\Residence as CommonMasterResidenceType;
use Whiteboks\GraphQL\Query\Common\Master\Residence as CommonMasterResidenceQuery;
use Whiteboks\GraphQL\Mutation\Common\Master\Residence\Store as CommonMasterResidenceStore;
use Whiteboks\GraphQL\Mutation\Common\Master\Residence\Update as CommonMasterResidenceUpdate;
use Whiteboks\GraphQL\Mutation\Common\Master\Residence\Delete as CommonMasterResidenceDelete;
// End Common Master Address
// Common Reference Decree
use Whiteboks\GraphQL\Type\Common\Reference\Decree\DecreeType as CommonReferenceDecreeDecreeTypeType;
use Whiteboks\GraphQL\Query\Common\Reference\Decree\Type as CommonReferenceDecreeDecreeTypeQuery;
use Whiteboks\GraphQL\Mutation\Common\Reference\Decree\Store as CommonReferenceDecreeTypeStore;
use Whiteboks\GraphQL\Mutation\Common\Reference\Decree\Update as CommonReferenceDecreeTypeUpdate;
use Whiteboks\GraphQL\Mutation\Common\Reference\Decree\Delete as CommonReferenceDecreeTypeDelete;
// Common Reference Document
use Whiteboks\GraphQL\Type\Common\Reference\Document\DocumentType as CommonReferenceDocumentDocumentTypeType;
use Whiteboks\GraphQL\Query\Common\Reference\Document\DocumentType as CommonReferenceDocumentDocumentTypeQuery;
use Whiteboks\GraphQL\Mutation\Common\Reference\Document\DocumentType\Store as CommonReferenceDocumentDocumentTypeStore;
use Whiteboks\GraphQL\Mutation\Common\Reference\Document\DocumentType\Update as CommonReferenceDocumentDocumentTypeUpdate;
use Whiteboks\GraphQL\Mutation\Common\Reference\Document\DocumentType\Delete as CommonReferenceDocumentDocumentTypeDelete;
// Common Reference Education
use Whiteboks\GraphQL\Type\Common\Reference\Education\Category as CommonReferenceEducationCategoryType;
use Whiteboks\GraphQL\Query\Common\Reference\Education\Category as CommonReferenceEducationCategoryQuery;
use Whiteboks\GraphQL\Mutation\Common\Reference\Education\Category\Store as CommonReferenceEducationCategoryStore;
use Whiteboks\GraphQL\Mutation\Common\Reference\Education\Category\Update as CommonReferenceEducationCategoryUpdate;
use Whiteboks\GraphQL\Mutation\Common\Reference\Education\Category\Delete as CommonReferenceEducationCategoryDelete;
// ---------------------
use Whiteboks\GraphQL\Type\Common\Reference\Education\Education as CommonReferenceEducationEducationType;
use Whiteboks\GraphQL\Query\Common\Reference\Education\Education as CommonReferenceEducationEducationQuery;
use Whiteboks\GraphQL\Mutation\Common\Reference\Education\Education\Store as CommonReferenceEducationEducationStore;
use Whiteboks\GraphQL\Mutation\Common\Reference\Education\Education\Update as CommonReferenceEducationEducationUpdate;
use Whiteboks\GraphQL\Mutation\Common\Reference\Education\Education\Delete as CommonReferenceEducationEducationDelete;
// ---------------------
use Whiteboks\GraphQL\Type\Common\Reference\Education\Group as CommonReferenceEducationGroupType;
use Whiteboks\GraphQL\Query\Common\Reference\Education\Group as CommonReferenceEducationGroupQuery;
use Whiteboks\GraphQL\Mutation\Common\Reference\Education\Group\Store as CommonReferenceEducationGroupStore;
use Whiteboks\GraphQL\Mutation\Common\Reference\Education\Group\Update as CommonReferenceEducationGroupUpdate;
use Whiteboks\GraphQL\Mutation\Common\Reference\Education\Group\Delete as CommonReferenceEducationGroupDelete;
// ---------------------
use Whiteboks\GraphQL\Type\Common\Reference\Education\ScienceGroup as CommonReferenceEducationScienceGroupType;
use Whiteboks\GraphQL\Query\Common\Reference\Education\ScienceGroup as CommonReferenceEducationScienceGroupQuery;
use Whiteboks\GraphQL\Mutation\Common\Reference\Education\ScienceGroup\Store as CommonReferenceEducationScienceGroupStore;
use Whiteboks\GraphQL\Mutation\Common\Reference\Education\ScienceGroup\Update as CommonReferenceEducationScienceGroupUpdate;
use Whiteboks\GraphQL\Mutation\Common\Reference\Education\ScienceGroup\Delete as CommonReferenceEducationScienceGroupDelete;
// ---------------------
use Whiteboks\GraphQL\Type\Common\Reference\Education\Stage as CommonReferenceEducationStageType;
use Whiteboks\GraphQL\Query\Common\Reference\Education\Stage as CommonReferenceEducationStageQuery;
use Whiteboks\GraphQL\Mutation\Common\Reference\Education\Stage\Store as CommonReferenceEducationStageStore;
use Whiteboks\GraphQL\Mutation\Common\Reference\Education\Stage\Update as CommonReferenceEducationStageUpdate;
use Whiteboks\GraphQL\Mutation\Common\Reference\Education\Stage\Delete as CommonReferenceEducationStageDelete;
// ---------------------
use Whiteboks\GraphQL\Type\Common\Reference\Education\Type as CommonReferenceEducationTypeType;
use Whiteboks\GraphQL\Query\Common\Reference\Education\Type as CommonReferenceEducationTypeQuery;
use Whiteboks\GraphQL\Mutation\Common\Reference\Education\Type\Store as CommonReferenceEducationTypeStore;
use Whiteboks\GraphQL\Mutation\Common\Reference\Education\Type\Update as CommonReferenceEducationTypeUpdate;
use Whiteboks\GraphQL\Mutation\Common\Reference\Education\Type\Delete as CommonReferenceEducationTypeDelete;
// Common Reference Period
use Whiteboks\GraphQL\Type\Common\Reference\Period\EducationYear as CommonReferencePeriodEducationYearType;
use Whiteboks\GraphQL\Query\Common\Reference\Period\EducationYear as CommonReferencePeriodEducationYearQuery;
use Whiteboks\GraphQL\Mutation\Common\Reference\Period\EducationYear\Store as CommonReferencePeriodEducationStore;
use Whiteboks\GraphQL\Mutation\Common\Reference\Period\EducationYear\Update as CommonReferencePeriodEducationUpdate;
use Whiteboks\GraphQL\Mutation\Common\Reference\Period\EducationYear\Delete as CommonReferencePeriodEducationDelete;
// ---------------------
use Whiteboks\GraphQL\Type\Common\Reference\Period\EducationYearCategory as CommonReferencePeriodEducationYearCategoryType;
use Whiteboks\GraphQL\Query\Common\Reference\Period\EducationYearCategory as CommonReferencePeriodEducationYearCategoryQuery;
use Whiteboks\GraphQL\Mutation\Common\Reference\Period\EducationYearCategory\Store as CommonReferencePeriodEducationYearCategoryStore;
use Whiteboks\GraphQL\Mutation\Common\Reference\Period\EducationYearCategory\Update as CommonReferencePeriodEducationYearCategoryUpdate;
use Whiteboks\GraphQL\Mutation\Common\Reference\Period\EducationYearCategory\Delete as CommonReferencePeriodEducationYearCategoryDelete;
// ---------------------
use Whiteboks\GraphQL\Type\Common\Reference\Period\WeekDay as CommonReferencePeriodWeekDayType;
use Whiteboks\GraphQL\Query\Common\Reference\Period\WeekDay as CommonReferencePeriodWeekDayQuery;
use Whiteboks\GraphQL\Mutation\Common\Reference\Period\WeekDay\Store as CommonReferencePeriodWeekDayStore;
use Whiteboks\GraphQL\Mutation\Common\Reference\Period\WeekDay\Update as CommonReferencePeriodWeekDayUpdate;
use Whiteboks\GraphQL\Mutation\Common\Reference\Period\WeekDay\Delete as CommonReferencePeriodWeekDayDelete;
// Common Reference Residence
use Whiteboks\GraphQL\Type\Common\Reference\Residence\Category as CommonReferenceResidenceCategoryType;
use Whiteboks\GraphQL\Query\Common\Reference\Residence\Category as CommonReferenceResidenceCategoryQuery;
use Whiteboks\GraphQL\Mutation\Common\Reference\Residence\Category\Store as CommonReferenceResidenceCategoryStore;
use Whiteboks\GraphQL\Mutation\Common\Reference\Residence\Category\Update as CommonReferenceResidenceCategoryUpdate;
use Whiteboks\GraphQL\Mutation\Common\Reference\Residence\Category\Delete as CommonReferenceResidenceCategoryDelete;
// ---------------------
use Whiteboks\GraphQL\Type\Common\Reference\Residence\ResidentType as CommonReferenceResidenceResidentTypeType;
use Whiteboks\GraphQL\Query\Common\Reference\Residence\ResidentType as CommonReferenceResidenceResidentTypeQuery;
use Whiteboks\GraphQL\Mutation\Common\Reference\Residence\ResidentType\Store as CommonReferenceResidenceResidentTypeStore;
use Whiteboks\GraphQL\Mutation\Common\Reference\Residence\ResidentType\Update as CommonReferenceResidenceResidentTypeUpdate;
use Whiteboks\GraphQL\Mutation\Common\Reference\Residence\ResidentType\Delete as CommonReferenceResidenceResidentTypeDelete;
// ---------------------
use Whiteboks\GraphQL\Type\Common\Reference\Residence\RoomType as CommonReferenceResidenceRoomTypeType;
use Whiteboks\GraphQL\Query\Common\Reference\Residence\RoomType as CommonReferenceResidenceRoomTypeQuery;
use Whiteboks\GraphQL\Mutation\Common\Reference\Residence\RoomType\Store as CommonReferenceResidenceRoomTypeStore;
use Whiteboks\GraphQL\Mutation\Common\Reference\Residence\RoomType\Update as CommonReferenceResidenceRoomTypeUpdate;
use Whiteboks\GraphQL\Mutation\Common\Reference\Residence\RoomType\Delete as CommonReferenceResidenceRoomTypeDelete;
// ---------------------
use Whiteboks\GraphQL\Type\Common\Reference\Residence\Type as CommonReferenceResidenceTypeType;
use Whiteboks\GraphQL\Query\Common\Reference\Residence\Type as CommonReferenceResidenceypeQuery;
use Whiteboks\GraphQL\Mutation\Common\Reference\Residence\Type\Store as CommonReferenceResidenceTypeStore;
use Whiteboks\GraphQL\Mutation\Common\Reference\Residence\Type\Update as CommonReferenceResidenceTypeUpdate;
use Whiteboks\GraphQL\Mutation\Common\Reference\Residence\Type\Delete as CommonReferenceResidenceTypeDelete;
//End Common reference
// Institution Master
use Whiteboks\GraphQL\Type\Institution\Master\Coordinator as InstitutionMasterCoordinatorType;
use Whiteboks\GraphQL\Query\Institution\Master\Coordinator as InstitutionMasterCoordinatorQuery;
use Whiteboks\GraphQL\Mutation\Institution\Master\Coordinator\Store as InstitutionMasterCoordinatorStore;
use Whiteboks\GraphQL\Mutation\Institution\Master\Coordinator\Update as InstitutionMasterCoordinatorUpdate;
use Whiteboks\GraphQL\Mutation\Institution\Master\Coordinator\Delete as InstitutionMasterCoordinatorDelete;
// ----------------------
use Whiteboks\GraphQL\Type\Institution\Master\Institution as InstitutionMasterInstitutionType;
use Whiteboks\GraphQL\Query\Institution\Master\Institution as InstitutionMasterInstitutionQuery;
use Whiteboks\GraphQL\Mutation\Institution\Master\Institution\Store as InstitutionMasterInstitutionStore;
use Whiteboks\GraphQL\Mutation\Institution\Master\Institution\Update as InstitutionMasterInstitutionUpdate;
use Whiteboks\GraphQL\Mutation\Institution\Master\Institution\Delete as InstitutionMasterInstitutionDelete;
// ----------------------
use Whiteboks\GraphQL\Type\Institution\Master\Unit as InstitutionMasterUnitType;
use Whiteboks\GraphQL\Query\Institution\Master\Unit as InstitutionMasterUnitQuery;
use Whiteboks\GraphQL\Mutation\Institution\Master\Unit\Store as InstitutionMasterUnitStore;
use Whiteboks\GraphQL\Mutation\Institution\Master\Unit\Update as InstitutionMasterUnitUpdate;
use Whiteboks\GraphQL\Mutation\Institution\Master\Unit\Delete as InstitutionMasterUnitDelete;
// ----------------------
use Whiteboks\GraphQL\Type\Institution\Reference\Category as InstitutionReferenceCategoryType;
use Whiteboks\GraphQL\Query\Institution\Reference\Category as InstitutionReferenceCategoryQuery;
use Whiteboks\GraphQL\Mutation\Institution\Reference\Category\Store as InstitutionReferenceCategoryStore;
use Whiteboks\GraphQL\Mutation\Institution\Reference\Category\Update as InstitutionReferenceCategoryUpdate;
use Whiteboks\GraphQL\Mutation\Institution\Reference\Category\Delete as InstitutionReferenceCategoryDelete;
// ----------------------
use Whiteboks\GraphQL\Type\Institution\Reference\CoordinatorType as InstitutionReferenceCoordinatorTypeType;
use Whiteboks\GraphQL\Query\Institution\Reference\CoordinatorType as InstitutionReferenceCoordinatorTypeQuery;
use Whiteboks\GraphQL\Mutation\Institution\Reference\CoordinatorType\Store as InstitutionReferenceCoordinatorTypeStore;
use Whiteboks\GraphQL\Mutation\Institution\Reference\CoordinatorType\Update as InstitutionReferenceCoordinatorTypeUpdate;
use Whiteboks\GraphQL\Mutation\Institution\Reference\CoordinatorType\Delete as InstitutionReferenceCoordinatorTypeDelete;
// ----------------------
use Whiteboks\GraphQL\Type\Institution\Reference\Type as InstitutionReferenceInstitutionType;
use Whiteboks\GraphQL\Query\Institution\Reference\Type as InstitutionReferenceInstitutionTypeQuery;
use Whiteboks\GraphQL\Mutation\Institution\Reference\Type\Store as InstitutionReferenceInstitutionTypeStore;
use Whiteboks\GraphQL\Mutation\Institution\Reference\Type\Update as InstitutionReferenceInstitutionTypeUpdate;
use Whiteboks\GraphQL\Mutation\Institution\Reference\Type\Delete as InstitutionReferenceInstitutionTypeDelete;
// ----------------------
use Whiteboks\GraphQL\Type\Institution\Reference\UnitType as InstitutionReferenceUnitTypeType;
use Whiteboks\GraphQL\Query\Institution\Reference\UnitType as InstitutionReferenceUnitTypeQuery;
use Whiteboks\GraphQL\Mutation\Institution\Reference\UnitType\Store as InstitutionReferenceUnitTypeStore;
use Whiteboks\GraphQL\Mutation\Institution\Reference\UnitType\Update as InstitutionReferenceUnitTypeUpdate;
use Whiteboks\GraphQL\Mutation\Institution\Reference\UnitType\Delete as InstitutionReferenceUnitTypeDelete;
// End Institution Master
// Location
use Whiteboks\GraphQL\Type\Location\Country as LocationCountriesType;
use Whiteboks\GraphQL\Query\Location\Country as LocationCountriesQuery;
use Whiteboks\GraphQL\Mutation\Location\Country\Store as LocationCountriesStore;
use Whiteboks\GraphQL\Mutation\Location\Country\Update as LocationCountriesUpdate;
use Whiteboks\GraphQL\Mutation\Location\Country\Delete as LocationCountriesDelete;
// ----------------------
use Whiteboks\GraphQL\Type\Location\Province as LocationProvincesType;
use Whiteboks\GraphQL\Query\Location\Province as LocationProvincesQuery;
use Whiteboks\GraphQL\Mutation\Location\Province\Store as LocationProvincesStore;
use Whiteboks\GraphQL\Mutation\Location\Province\Update as LocationProvincesUpdate;
use Whiteboks\GraphQL\Mutation\Location\Province\Delete as LocationProvincesDelete;
// ----------------------
use Whiteboks\GraphQL\Type\Location\Regency as LocationRegenciesType;
use Whiteboks\GraphQL\Query\Location\Regency as LocationRegenciesQuery;
use Whiteboks\GraphQL\Mutation\Location\Regency\Store as LocationRegenciesStore;
use Whiteboks\GraphQL\Mutation\Location\Regency\Update as LocationRegenciesUpdate;
use Whiteboks\GraphQL\Mutation\Location\Regency\Delete as LocationRegenciesDelete;
// ----------------------
use Whiteboks\GraphQL\Type\Location\RegencyType as LocationRegencyTypesType;
use Whiteboks\GraphQL\Query\Location\RegencyType as LocationRegencyTypesQuery;
use Whiteboks\GraphQL\Mutation\Location\RegencyType\Store as LocationRegencyTypesStore;
use Whiteboks\GraphQL\Mutation\Location\RegencyType\Update as LocationRegencyTypesUpdate;
use Whiteboks\GraphQL\Mutation\Location\RegencyType\Delete as LocationRegencyTypesDelete;
// ----------------------
use Whiteboks\GraphQL\Type\Location\SubDistrict as LocationSubDistrictsType;
use Whiteboks\GraphQL\Query\Location\SubDistrict as LocationSubDistrictsQuery;
use Whiteboks\GraphQL\Mutation\Location\SubDistrict\Store as LocationSubDistrictsStore;
use Whiteboks\GraphQL\Mutation\Location\SubDistrict\Update as LocationSubDistrictsUpdate;
use Whiteboks\GraphQL\Mutation\Location\SubDistrict\Delete as LocationSubDistrictsDelete;
// ----------------------
use Whiteboks\GraphQL\Type\Location\Village as LocationVillagesType;
use Whiteboks\GraphQL\Query\Location\Village as LocationVillagesQuery;
use Whiteboks\GraphQL\Mutation\Location\Village\Store as LocationVillagesStore;
use Whiteboks\GraphQL\Mutation\Location\Village\Update as LocationVillagesUpdate;
use Whiteboks\GraphQL\Mutation\Location\Village\Delete as LocationVillagesDelete;
// End Location
// Module Training Master
// ----------------------
use Whiteboks\GraphQL\Type\Module\Training\Master\Course as ModuleTrainingMasterCourseType;
use Whiteboks\GraphQL\Query\Module\Training\Master\Course as ModuleTrainingMasterCourseQuery;
use Whiteboks\GraphQL\Mutation\Module\Training\Master\Course\Store as ModuleTrainingMasterCourseStore;
use Whiteboks\GraphQL\Mutation\Module\Training\Master\Course\Update as ModuleTrainingMasterCourseUpdate;
use Whiteboks\GraphQL\Mutation\Module\Training\Master\Course\Delete as ModuleTrainingMasterCourseDelete;
// ----------------------
// End Module Training Master
// Module Training Reference
// ----------------------
use Whiteboks\GraphQL\Type\Module\Training\Reference\Facilitator as ModuleTrainingReferenceFacilitatorType;
use Whiteboks\GraphQL\Query\Module\Training\Reference\Facilitator as ModuleTrainingReferenceFacilitatorQuery;
use Whiteboks\GraphQL\Mutation\Module\Training\Reference\Facilitator\Store as ModuleTrainingReferenceFacilitatorStore;
use Whiteboks\GraphQL\Mutation\Module\Training\Reference\Facilitator\Update as ModuleTrainingReferenceFacilitatorUpdate;
use Whiteboks\GraphQL\Mutation\Module\Training\Reference\Facilitator\Delete as ModuleTrainingReferenceFacilitatorDelete;
// ----------------------
// ----------------------
use Whiteboks\GraphQL\Type\Module\Training\Reference\TrainingPackage as ModuleTrainingReferenceTrainingPackageType;
use Whiteboks\GraphQL\Query\Module\Training\Reference\TrainingPackage as ModuleTrainingReferenceTrainingPackageQuery;
use Whiteboks\GraphQL\Mutation\Module\Training\Reference\TrainingPackage\Store as ModuleTrainingReferenceTrainingPackageStore;
use Whiteboks\GraphQL\Mutation\Module\Training\Reference\TrainingPackage\Update as ModuleTrainingReferenceTrainingPackageUpdate;
use Whiteboks\GraphQL\Mutation\Module\Training\Reference\TrainingPackage\Delete as ModuleTrainingReferenceTrainingPackageDelete;
// ----------------------
// ----------------------
use Whiteboks\GraphQL\Type\Module\Training\Reference\LearningMaterial\Category as ModuleTrainingReferenceLearningMaterialCategoryType;
use Whiteboks\GraphQL\Query\Module\Training\Reference\LearningMaterial\Category as ModuleTrainingReferenceLearningMaterialCategoryQuery;
use Whiteboks\GraphQL\Mutation\Module\Training\Reference\LearningMaterial\Category\Store as ModuleTrainingReferenceLearningMaterialCategoryStore;
use Whiteboks\GraphQL\Mutation\Module\Training\Reference\LearningMaterial\Category\Update as ModuleTrainingReferenceLearningMaterialCategoryUpdate;
use Whiteboks\GraphQL\Mutation\Module\Training\Reference\LearningMaterial\Category\UpdateDescription as ModuleTrainingReferenceLearningMaterialCategoryUpdateDescription;
use Whiteboks\GraphQL\Mutation\Module\Training\Reference\LearningMaterial\Category\Delete as ModuleTrainingReferenceLearningMaterialCategoryDelete;
// ----------------------
// ----------------------
use Whiteboks\GraphQL\Type\Module\Training\Reference\LearningMaterial\Method as ModuleTrainingReferenceLearningMaterialMethodType;
use Whiteboks\GraphQL\Query\Module\Training\Reference\LearningMaterial\Method as ModuleTrainingReferenceLearningMaterialMethodQuery;
use Whiteboks\GraphQL\Mutation\Module\Training\Reference\LearningMaterial\Method\Store as ModuleTrainingReferenceLearningMaterialMethodStore;
use Whiteboks\GraphQL\Mutation\Module\Training\Reference\LearningMaterial\Method\Update as ModuleTrainingReferenceLearningMaterialMethodUpdate;
use Whiteboks\GraphQL\Mutation\Module\Training\Reference\LearningMaterial\Method\Delete as ModuleTrainingReferenceLearningMaterialMethodDelete;
// ----------------------
// End Module Training Reference
// Module Training Transaction
// ----------------------
use Whiteboks\GraphQL\Type\Module\Training\Transaction\TrainingClass as ModuleTrainingTransactionTrainingClassType;
use Whiteboks\GraphQL\Query\Module\Training\Transaction\TrainingClass as ModuleTrainingTransactionTrainingClassQuery;
use Whiteboks\GraphQL\Mutation\Module\Training\Transaction\TrainingClass\Store as ModuleTrainingTransactionTrainingClassStore;
use Whiteboks\GraphQL\Mutation\Module\Training\Transaction\TrainingClass\Update as ModuleTrainingTransactionTrainingClassUpdate;
use Whiteboks\GraphQL\Mutation\Module\Training\Transaction\TrainingClass\Delete as ModuleTrainingTransactionTrainingClassDelete;
use Whiteboks\GraphQL\Mutation\Module\Training\Transaction\TrainingClass\AddMember as ModuleTrainingTransactionTrainingClassAddMember;
use Whiteboks\GraphQL\Mutation\Module\Training\Transaction\TrainingClass\RemoveMember as ModuleTrainingTransactionTrainingClassRemoveMember;
use Whiteboks\GraphQL\Mutation\Module\Training\Transaction\TrainingClass\AddFacilitator as ModuleTrainingTransactionTrainingClassAddFacilitator;
use Whiteboks\GraphQL\Mutation\Module\Training\Transaction\TrainingClass\RemoveFacilitator as ModuleTrainingTransactionTrainingClassRemoveFacilitator;
// ----------------------
// End Module Training Transaction
// Person master
// ----------------------
use Whiteboks\GraphQL\Type\Person\Master\Biodata as PersonMasterBiodataType;
use Whiteboks\GraphQL\Query\Person\Master\Biodata as PersonMasterBiodataQuery;
use Whiteboks\GraphQL\Mutation\Person\Master\Biodata\Store as PersonMasterBiodataStore;
use Whiteboks\GraphQL\Mutation\Person\Master\Biodata\Update as PersonMasterBiodataUpdate;
use Whiteboks\GraphQL\Mutation\Person\Master\Biodata\Delete as PersonMasterBiodataDelete;
// ----------------------
use Whiteboks\GraphQL\Type\Person\Master\Individual as PersonMasterIndividualType;
use Whiteboks\GraphQL\Query\Person\Master\Individual as PersonMasterIndividualQuery;
use Whiteboks\GraphQL\Mutation\Person\Master\Individual\Store as PersonMasterIndividualStore;
use Whiteboks\GraphQL\Mutation\Person\Master\Individual\Update as PersonMasterIndividualUpdate;
use Whiteboks\GraphQL\Mutation\Person\Master\Individual\Delete as PersonMasterIndividualDelete;
use Whiteboks\GraphQL\Mutation\Person\Master\Individual\Register as PersonMasterIndividualRegister;
// ----------------------
use Whiteboks\GraphQL\Type\Person\Master\Relative as PersonMasterRelativeType;
use Whiteboks\GraphQL\Query\Person\Master\Relative as PersonMasterRelativeQuery;
use Whiteboks\GraphQL\Mutation\Person\Master\Relative\Store as PersonMasterRelativeStore;
use Whiteboks\GraphQL\Mutation\Person\Master\Relative\Update as PersonMasterRelativeUpdate;
use Whiteboks\GraphQL\Mutation\Person\Master\Relative\Delete as PersonMasterRelativeDelete;
//end Module Person master
//Person Reference Biodata
use Whiteboks\GraphQL\Type\Person\Reference\Biodata\Gender as PersonReferenceBiodataGenderType;
use Whiteboks\GraphQL\Query\Person\Reference\Biodata\Gender as PersonReferenceBiodataGenderQuery;
use Whiteboks\GraphQL\Mutation\Person\Reference\Biodata\Gender\Store as PersonReferenceBiodataGenderStore;
use Whiteboks\GraphQL\Mutation\Person\Reference\Biodata\Gender\Update as PersonReferenceBiodataGenderUpdate;
use Whiteboks\GraphQL\Mutation\Person\Reference\Biodata\Gender\Delete as PersonReferenceBiodataGenderDelete;
// ----------------------
use Whiteboks\GraphQL\Type\Person\Reference\Biodata\IdentificationType as PersonReferenceBiodataIdentificationTypeType;
use Whiteboks\GraphQL\Query\Person\Reference\Biodata\IdentificationType as PersonReferenceBiodataIdentificationTypeQuery;
use Whiteboks\GraphQL\Mutation\Person\Reference\Biodata\IdentificationType\Store as PersonReferenceBiodataIdentificationTypeStore;
use Whiteboks\GraphQL\Mutation\Person\Reference\Biodata\IdentificationType\Update as PersonReferenceBiodataIdentificationTypeUpdate;
use Whiteboks\GraphQL\Mutation\Person\Reference\Biodata\IdentificationType\Delete as PersonReferenceBiodataIdentificationTypeDelete;
// ----------------------
use Whiteboks\GraphQL\Type\Person\Reference\Biodata\MaritalStatus as PersonReferenceBiodataMaritalStatusType;
use Whiteboks\GraphQL\Query\Person\Reference\Biodata\MaritalStatus as PersonReferenceBiodataMaritalStatusQuery;
use Whiteboks\GraphQL\Mutation\Person\Reference\Biodata\MaritalStatus\Store as PersonReferenceBiodataMaritalStatusStore;
use Whiteboks\GraphQL\Mutation\Person\Reference\Biodata\MaritalStatus\Update as PersonReferenceBiodataMaritalStatusUpdate;
use Whiteboks\GraphQL\Mutation\Person\Reference\Biodata\MaritalStatus\Delete as PersonReferenceBiodataMaritalStatusDelete;
// ----------------------
use Whiteboks\GraphQL\Type\Person\Reference\Biodata\RelativeType as PersonReferenceBiodataRelativeTypeType;
use Whiteboks\GraphQL\Query\Person\Reference\Biodata\RelativeType as PersonReferenceBiodataRelativeTypeQuery;
use Whiteboks\GraphQL\Mutation\Person\Reference\Biodata\RelativeType\Store as PersonReferenceBiodataRelativeTypeStore;
use Whiteboks\GraphQL\Mutation\Person\Reference\Biodata\RelativeType\Update as PersonReferenceBiodataRelativeTypeUpdate;
use Whiteboks\GraphQL\Mutation\Person\Reference\Biodata\RelativeType\Delete as PersonReferenceBiodataRelativeTypeDelete;
// ----------------------
use Whiteboks\GraphQL\Type\Person\Reference\Biodata\Religion as PersonReferenceBiodataReligionType;
use Whiteboks\GraphQL\Query\Person\Reference\Biodata\Religion as PersonReferenceBiodataReligionQuery;
use Whiteboks\GraphQL\Mutation\Person\Reference\Biodata\Religion\Store as PersonReferenceBiodataReligionStore;
use Whiteboks\GraphQL\Mutation\Person\Reference\Biodata\Religion\Update as PersonReferenceBiodataReligionUpdate;
use Whiteboks\GraphQL\Mutation\Person\Reference\Biodata\Religion\Delete as PersonReferenceBiodataReligionDelete;
//Person Reference Career
// ----------------------
use Whiteboks\GraphQL\Type\Person\Reference\Career\Occupation as PersonReferenceCareerOccupationType;
use Whiteboks\GraphQL\Query\Person\Reference\Career\Occupation as PersonReferenceCareerOccupationQuery;
use Whiteboks\GraphQL\Mutation\Person\Reference\Career\Occupation\Store as PersonReferenceCareerOccupationStore;
use Whiteboks\GraphQL\Mutation\Person\Reference\Career\Occupation\Update as PersonReferenceCareerOccupationUpdate;
use Whiteboks\GraphQL\Mutation\Person\Reference\Career\Occupation\Delete as PersonReferenceCareerOccupationDelete;
// ----------------------
use Whiteboks\GraphQL\Type\Person\Reference\Career\Profession as PersonReferenceCareerProfessionType;
use Whiteboks\GraphQL\Query\Person\Reference\Career\Profession as PersonReferenceCareerProfessionQuery;
use Whiteboks\GraphQL\Mutation\Person\Reference\Career\Profession\Store as PersonReferenceCareerProfessionStore;
use Whiteboks\GraphQL\Mutation\Person\Reference\Career\Profession\Update as PersonReferenceCareerProfessionUpdate;
use Whiteboks\GraphQL\Mutation\Person\Reference\Career\Profession\Delete as PersonReferenceCareerProfessionDelete;

return [
    'prefix' => 'api/graphql',
    'routes' => '{graphql_schema?}/query',
    'controllers' => \Rebing\GraphQL\GraphQLController::class . '@query',
    'middleware' => ['api'],
    'default_schema' => 'module_training_transaction',
    'schemas' => [
        'auth' => [
            'query' => [
                'AuthAbility'       => AuthAbilityQuery::class,
                'AuthRole'          => AuthRoleQuery::class,
                'AuthSocialAccount' => AuthSocialAccountQuery::class,
                'AuthUser'          => AuthUserQuery::class,
            ],
            'mutation' => [
                'AuthAbilityStore'         => AuthAbilityStore::class,
                'AuthAbilityUpdate'        => AuthAbilityUpdate::class,
                'AuthAbilityDelete'        => AuthAbilityDelete::class,
                'AuthUserStore'            => AuthUserStore::class,
                'AuthUserUpdate'           => AuthUserUpdate::class,
                'AuthUserDelete'           => AuthUserDelete::class,
                'AuthUserToggleUserActive' => AuthUserToggleUserActive::class,
                'AuthUserToggleUserAdmin'  => AuthUserToggleUserAdmin::class,
            ],
            'middleware' => ['auth:api']
        ],
        'institution_master' => [
            'query' => [
                'InstitutionMasterCoordinator' => InstitutionMasterCoordinatorQuery::class,
                'InstitutionMasterInstitution' => InstitutionMasterInstitutionQuery::class,
                'InstitutionMasterUnit'        => InstitutionMasterUnitQuery::class,
            ],
            'mutation' => [
                'InstitutionMasterCoordinatorStore'  => InstitutionMasterCoordinatorStore::class,
                'InstitutionMasterCoordinatorUpdate' => InstitutionMasterCoordinatorUpdate::class,
                'InstitutionMasterCoordinatorDelete' => InstitutionMasterCoordinatorDelete::class,
                'InstitutionMasterInstitutionStore'  => InstitutionMasterInstitutionStore::class,
                'InstitutionMasterInstitutionUpdate' => InstitutionMasterInstitutionUpdate::class,
                'InstitutionMasterInstitutionDelete' => InstitutionMasterInstitutionDelete::class,
                'InstitutionMasterUnitStore'         => InstitutionMasterUnitStore::class,
                'InstitutionMasterUnitUpdate'        => InstitutionMasterUnitUpdate::class,
                'InstitutionMasterUnitDelete'        => InstitutionMasterUnitDelete::class,
            ],
            'middleware' => ['auth:api']
        ],
        'institution_reference' => [
            'query' => [
                'InstitutionReferenceCategory'        => InstitutionReferenceCategoryQuery::class,
                'InstitutionReferenceCoordinatorType' => InstitutionReferenceCoordinatorTypeQuery::class,
                'InstitutionReferenceInstitutionType' => InstitutionReferenceInstitutionTypeQuery::class,
                'InstitutionReferenceUnitType'        => InstitutionReferenceUnitTypeQuery::class,
            ],
            'mutation' => [
                'InstitutionReferenceCategoryStore'         => InstitutionReferenceCategoryStore::class,
                'InstitutionReferenceCategoryUpdate'        => InstitutionReferenceCategoryUpdate::class,
                'InstitutionReferenceCategoryDelete'        => InstitutionReferenceCategoryDelete::class,
                'InstitutionReferenceCoordinatorTypeStore'  => InstitutionReferenceCoordinatorTypeStore::class,
                'InstitutionReferenceCoordinatorTypeUpdate' => InstitutionReferenceCoordinatorTypeUpdate::class,
                'InstitutionReferenceCoordinatorTypeDelete' => InstitutionReferenceCoordinatorTypeDelete::class,
                'InstitutionReferenceInstitutionTypeStore'  => InstitutionReferenceInstitutionTypeStore::class,
                'InstitutionReferenceInstitutionTypeUpdate' => InstitutionReferenceInstitutionTypeUpdate::class,
                'InstitutionReferenceInstitutionTypeDelete' => InstitutionReferenceInstitutionTypeDelete::class,
                'InstitutionReferenceUnitTypeStore'         => InstitutionReferenceUnitTypeStore::class,
                'InstitutionReferenceUnitTypeUpdate'        => InstitutionReferenceUnitTypeUpdate::class,
                'InstitutionReferenceUnitTypeDelete'        => InstitutionReferenceUnitTypeDelete::class,
            ],
            'middleware' => ['auth:api']
        ],
        'location' => [
            'query' => [
                'LocationCountry'     => LocationCountriesQuery::class,
                'LocationProvince'    => LocationProvincesQuery::class,
                'LocationRegency'     => LocationRegenciesQuery::class,
                'LocationRegencyType' => LocationRegencyTypesQuery::class,
                'LocationSubDistrict' => LocationSubDistrictsQuery::class,
                'LocationVillage'     => LocationVillagesQuery::class,
            ],
            'mutation' => [
                'LocationCountryStore'      => LocationCountriesStore::class,
                'LocationCountryUpdate'     => LocationCountriesUpdate::class,
                'LocationCountryDelete'     => LocationCountriesDelete::class,
                'LocationProvinceStore'     => LocationProvincesStore::class,
                'LocationProvinceUpdate'    => LocationProvincesUpdate::class,
                'LocationProvinceDelete'    => LocationProvincesDelete::class,
                'LocationRegencyStore'      => LocationRegenciesStore::class,
                'LocationRegencyUpdate'     => LocationRegenciesUpdate::class,
                'LocationRegencyDelete'     => LocationRegenciesDelete::class,
                'LocationRegencyTypeStore'  => LocationRegencyTypesStore::class,
                'LocationRegencyTypeUpdate' => LocationRegencyTypesUpdate::class,
                'LocationRegencyTypeDelete' => LocationRegencyTypesDelete::class,
                'LocationSubDistrictStore'  => LocationSubDistrictsStore::class,
                'LocationSubDistrictUpdate' => LocationSubDistrictsUpdate::class,
                'LocationSubDistrictDelete' => LocationSubDistrictsDelete::class,
                'LocationVillageStore'      => LocationVillagesStore::class,
                'LocationVillageUpdate'     => LocationVillagesUpdate::class,
                'LocationVillageDelete'     => LocationVillagesDelete::class,
            ],
            'middleware' => ['auth:api']
        ],
        'common_master' => [
            'query' => [
                'CommonMasterResidence' => CommonMasterResidenceQuery::class,
            ],
            'mutation' => [
                'CommonMasterResidenceStore' => CommonMasterResidenceStore::class,
                'CommonMasterResidenceUpdate' => CommonMasterResidenceUpdate::class,
                'CommonMasterResidenceDelete' => CommonMasterResidenceDelete::class
            ],
            'middleware' => ['auth:api']
        ],
        'common_reference_decree' => [
            'query' => [
                'CommonReferenceDecreeDecreeType' => CommonReferenceDecreeDecreeTypeQuery::class,
            ],
            'mutation' => [
                'CommonReferenceDecreeTypeStore'  => CommonReferenceDecreeTypeStore::class,
                'CommonReferenceDecreeTypeUpdate' => CommonReferenceDecreeTypeUpdate::class,
                'CommonReferenceDecreeTypeDelete' => CommonReferenceDecreeTypeDelete::class
            ],
            'middleware' => ['auth:api']
        ],
        'common_reference_document' => [
            'query' => [
                'CommonReferenceDocumentDocumentType'     => CommonReferenceDocumentDocumentTypeQuery::class,
            ],
            'mutation' => [
                'CommonReferenceDocumentDocumentTypeStore'  => CommonReferenceDocumentDocumentTypeStore::class,
                'CommonReferenceDocumentDocumentTypeUpdate' => CommonReferenceDocumentDocumentTypeUpdate::class,
                'CommonReferenceDocumentDocumentTypeDelete' => CommonReferenceDocumentDocumentTypeDelete::class
            ],
            'middleware' => ['auth:api']
        ],
        'common_reference_education' => [
            'query' => [
                'CommonReferenceEducationCategory'     => CommonReferenceEducationCategoryQuery::class,
                'CommonReferenceEducationEducation'    => CommonReferenceEducationEducationQuery::class,
                'CommonReferenceEducationGroup'        => CommonReferenceEducationGroupQuery::class,
                'CommonReferenceEducationScienceGroup' => CommonReferenceEducationScienceGroupQuery::class,
                'CommonReferenceEducationStage'        => CommonReferenceEducationStageQuery::class,
                'CommonReferenceEducationType'         => CommonReferenceEducationTypeQuery::class,
            ],
            'mutation' => [
                'CommonReferenceEducationCategoryStore'      => CommonReferenceEducationCategoryStore::class,
                'CommonReferenceEducationCategoryUpdate'     => CommonReferenceEducationCategoryUpdate::class,
                'CommonReferenceEducationCategoryDelete'     => CommonReferenceEducationCategoryDelete::class,
                'CommonReferenceEducationEducationStore'     => CommonReferenceEducationEducationStore::class,
                'CommonReferenceEducationEducationUpdate'    => CommonReferenceEducationEducationUpdate::class,
                'CommonReferenceEducationEducationDelete'    => CommonReferenceEducationEducationDelete::class,
                'CommonReferenceEducationGroupStore'         => CommonReferenceEducationGroupStore::class,
                'CommonReferenceEducationGroupUpdate'        => CommonReferenceEducationGroupUpdate::class,
                'CommonReferenceEducationGroupDelete'        => CommonReferenceEducationGroupDelete::class,
                'CommonReferenceEducationScienceGroupStore'  => CommonReferenceEducationScienceGroupStore::class,
                'CommonReferenceEducationScienceGroupUpdate' => CommonReferenceEducationScienceGroupUpdate::class,
                'CommonReferenceEducationScienceGroupDelete' => CommonReferenceEducationScienceGroupDelete::class,
                'CommonReferenceEducationStageStore'         => CommonReferenceEducationStageStore::class,
                'CommonReferenceEducationStageUpdate'        => CommonReferenceEducationStageUpdate::class,
                'CommonReferenceEducationStageDelete'        => CommonReferenceEducationStageDelete::class,
                'CommonReferenceEducationTypeStore'          => CommonReferenceEducationTypeStore::class,
                'CommonReferenceEducationTypeUpdate'         => CommonReferenceEducationTypeUpdate::class,
                'CommonReferenceEducationTypeDelete'         => CommonReferenceEducationTypeDelete::class,

            ],
            'middleware' => ['auth:api']
        ],
        'common_reference_period' => [
            'query' => [
                'CommonReferencePeriodEducationYear'         => CommonReferencePeriodEducationYearQuery::class,
                'CommonReferencePeriodEducationYearCategory' => CommonReferencePeriodEducationYearCategoryQuery::class,
                'CommonReferencePeriodWeekDay'               => CommonReferencePeriodWeekDayQuery::class,
            ],
            'mutation' => [
                'CommonReferencePeriodEducationStore'              => CommonReferencePeriodEducationStore::class,
                'CommonReferencePeriodEducationUpdate'             => CommonReferencePeriodEducationUpdate::class,
                'CommonReferencePeriodEducationDelete'             => CommonReferencePeriodEducationDelete::class,
                'CommonReferencePeriodEducationYearCategoryStore'  => CommonReferencePeriodEducationYearCategoryStore::class,
                'CommonReferencePeriodEducationYearCategoryUpdate' => CommonReferencePeriodEducationYearCategoryUpdate::class,
                'CommonReferencePeriodEducationYearCategoryDelete' => CommonReferencePeriodEducationYearCategoryDelete::class,
                'CommonReferencePeriodWeekDayStore'                => CommonReferencePeriodWeekDayStore::class,
                'CommonReferencePeriodWeekDayUpdate'               => CommonReferencePeriodWeekDayUpdate::class,
                'CommonReferencePeriodWeekDayDelete'               => CommonReferencePeriodWeekDayDelete::class,
            ],
            'middleware' => ['auth:api']
        ],
        'common_reference_residence' => [
            'query' => [
                'CommonReferenceResidenceCategory'     => CommonReferenceResidenceCategoryQuery::class,
                'CommonReferenceResidenceResidentType' => CommonReferenceResidenceResidentTypeQuery::class,
                'CommonReferenceResidenceRoomType'     => CommonReferenceResidenceRoomTypeQuery::class,
                'CommonReferenceResidenceype'          => CommonReferenceResidenceypeQuery::class,
            ],
            'mutation' => [
                'CommonReferenceResidenceCategoryStore'      => CommonReferenceResidenceCategoryStore::class,
                'CommonReferenceResidenceCategoryUpdate'     => CommonReferenceResidenceCategoryUpdateb::class,
                'CommonReferenceResidenceCategoryDelete'     => CommonReferenceResidenceCategoryDelete::class,
                'CommonReferenceResidenceResidentTypeStore'  => CommonReferenceResidenceResidentTypeStore::class,
                'CommonReferenceResidenceResidentTypeUpdate' => CommonReferenceResidenceResidentTypeUpdate::class,
                'CommonReferenceResidenceResidentTypeDelete' => CommonReferenceResidenceResidentTypeDelete::class,
                'CommonReferenceResidenceRoomTypeStore'      => CommonReferenceResidenceRoomTypeStore::class,
                'CommonReferenceResidenceRoomTypeUpdate'     => CommonReferenceResidenceRoomTypeUpdate::class,
                'CommonReferenceResidenceRoomTypeDelete'     => CommonReferenceResidenceRoomTypeDelete::class,
                'CommonReferenceResidenceTypeStore'          => CommonReferenceResidenceTypeStore::class,
                'CommonReferenceResidenceTypeUpdate'         => CommonReferenceResidenceTypeUpdate::class,
                'CommonReferenceResidenceTypeDelete'         => CommonReferenceResidenceTypeDelete::class,
            ],
            'middleware' => ['auth:api']
        ],
        'module_human_resource_master' => [
            'query' => [
            ],
            'mutation' => [
            ],
            'middleware' => ['auth:api']
        ],
        'module_human_resource_reference' => [
            'query' => [
            ],
            'mutation' => [
            ],
            'middleware' => ['auth:api']
        ],
        'module_inventory_master' => [
            'query' => [
            ],
            'mutation' => [
            ],
            'middleware' => ['auth:api']
        ],
        'module_inventory_reference' => [
            'query' => [
            ],
            'mutation' => [
            ],
            'middleware' => ['auth:api']
        ],
        'module_inventory_transaction' => [
            'query' => [
            ],
            'mutation' => [
            ],
            'middleware' => ['auth:api']
        ],
        'module_training_master' => [
            'query' => [
                'ModuleTrainingMasterCourse' => ModuleTrainingMasterCourseQuery::class,
            ],
            'mutation' => [
                'ModuleTrainingMasterCourseStore'  => ModuleTrainingMasterCourseStore::class,
                'ModuleTrainingMasterCourseUpdate' => ModuleTrainingMasterCourseUpdate::class,
                'ModuleTrainingMasterCourseDelete' => ModuleTrainingMasterCourseDelete::class,
            ],
            'middleware' => ['auth:api']
        ],
        'module_training_reference' => [
            'query' => [
                'ModuleTrainingReferenceFacilitator'              => ModuleTrainingReferenceFacilitatorQuery::class,
                'ModuleTrainingReferenceTrainingPackage'          => ModuleTrainingReferenceTrainingPackageQuery::class,
                'ModuleTrainingReferenceLearningMaterialCategory' => ModuleTrainingReferenceLearningMaterialCategoryQuery::class,
                'ModuleTrainingReferenceLearningMaterialMethod'   => ModuleTrainingReferenceLearningMaterialMethodQuery::class,
            ],
            'mutation' => [
                'ModuleTrainingReferenceFacilitatorStore'                          => ModuleTrainingReferenceFacilitatorStore::class,
                'ModuleTrainingReferenceFacilitatorUpdate'                         => ModuleTrainingReferenceFacilitatorUpdate::class,
                'ModuleTrainingReferenceFacilitatorDelete'                         => ModuleTrainingReferenceFacilitatorDelete::class,
                'ModuleTrainingReferenceTrainingPackageStore'                      => ModuleTrainingReferenceTrainingPackageStore::class,
                'ModuleTrainingReferenceTrainingPackageUpdate'                     => ModuleTrainingReferenceTrainingPackageUpdate::class,
                'ModuleTrainingReferenceTrainingPackageDelete'                     => ModuleTrainingReferenceTrainingPackageDelete::class,
                'ModuleTrainingReferenceLearningMaterialCategoryStore'             => ModuleTrainingReferenceLearningMaterialCategoryStore::class,
                'ModuleTrainingReferenceLearningMaterialCategoryUpdate'            => ModuleTrainingReferenceLearningMaterialCategoryUpdate::class,
                'ModuleTrainingReferenceLearningMaterialCategoryUpdateDescription' => ModuleTrainingReferenceLearningMaterialCategoryUpdateDescription::class,
                'ModuleTrainingReferenceLearningMaterialCategoryDelete'            => ModuleTrainingReferenceLearningMaterialCategoryDelete::class,
                'ModuleTrainingReferenceLearningMaterialMethodStore'               => ModuleTrainingReferenceLearningMaterialMethodStore::class,
                'ModuleTrainingReferenceLearningMaterialMethodUpdate'              => ModuleTrainingReferenceLearningMaterialMethodUpdate::class,
                'ModuleTrainingReferenceLearningMaterialMethodDelete'              => ModuleTrainingReferenceLearningMaterialMethodDelete::class,
            ],
            'middleware' => ['auth:api']
        ],
        'module_training_transaction' => [
            'query' => [
                'ModuleTrainingTransactionTrainingClass' => ModuleTrainingTransactionTrainingClassQuery::class,
            ],
            'mutation' => [
                'ModuleTrainingTransactionTrainingClassStore'  => ModuleTrainingTransactionTrainingClassStore::class,
                'ModuleTrainingTransactionTrainingClassUpdate' => ModuleTrainingTransactionTrainingClassUpdate::class,
                'ModuleTrainingTransactionTrainingClassDelete' => ModuleTrainingTransactionTrainingClassDelete::class,
                'ModuleTrainingTransactionTrainingClassAddMember' => ModuleTrainingTransactionTrainingClassAddMember::class,
                'ModuleTrainingTransactionTrainingClassRemoveMember' => ModuleTrainingTransactionTrainingClassRemoveMember::class,
                'ModuleTrainingTransactionTrainingClassAddFacilitator' => ModuleTrainingTransactionTrainingClassAddFacilitator::class,
                'ModuleTrainingTransactionTrainingClassRemoveFacilitator' => ModuleTrainingTransactionTrainingClassRemoveFacilitator::class,
            ],
            'middleware' => ['auth:api']
        ],
        'person_master' => [
            'query' => [
                'PersonMasterIndividual' => PersonMasterIndividualQuery::class,
            ],
            'mutation' => [
                'PersonMasterIndividualStore'  => PersonMasterIndividualStore::class,
                'PersonMasterIndividualUpdate' => PersonMasterIndividualUpdate::class,
                'PersonMasterIndividualDelete' => PersonMasterIndividualDelete::class,
            ],
            'middleware' => ['auth:api']
        ],
        'person_reference_biodata' => [
            'query' => [
                'PersonReferenceBiodataGender'              => PersonReferenceBiodataGenderQuery::class,
                'PersonReferenceBiodataIdentificationType'  => PersonReferenceBiodataIdentificationTypeQuery::class,
                'PersonReferenceBiodataMaritalStatus'       => PersonReferenceBiodataMaritalStatusQuery::class,
                'PersonReferenceBiodataRelativeType'        => PersonReferenceBiodataRelativeTypeQuery::class,
                'PersonReferenceBiodataReligion'            => PersonReferenceBiodataReligionQuery::class,
            ],
            'mutation' => [
                'PersonReferenceBiodataGenderStore'              => PersonReferenceBiodataGenderStore::class,
                'PersonReferenceBiodataGenderUpdate'             => PersonReferenceBiodataGenderUpdate::class,
                'PersonReferenceBiodataGenderDelete'             => PersonReferenceBiodataGenderDelete::class,
                'PersonReferenceBiodataIdentificationTypeStore'  => PersonReferenceBiodataIdentificationTypeStore::class,
                'PersonReferenceBiodataIdentificationTypeUpdate' => PersonReferenceBiodataIdentificationTypeUpdate::class,
                'PersonReferenceBiodataIdentificationTypeDelete' => PersonReferenceBiodataIdentificationTypeDelete::class,
                'PersonReferenceBiodataMaritalStatusStore'       => PersonReferenceBiodataMaritalStatusStore::class,
                'PersonReferenceBiodataMaritalStatusUpdate'      => PersonReferenceBiodataMaritalStatusUpdate::class,
                'PersonReferenceBiodataMaritalStatusDelete'      => PersonReferenceBiodataMaritalStatusDelete::class,
                'PersonReferenceBiodataRelativeTypeStore'        => PersonReferenceBiodataRelativeTypeStore::class,
                'PersonReferenceBiodataRelativeTypeUpdate'       => PersonReferenceBiodataRelativeTypeUpdate::class,
                'PersonReferenceBiodataRelativeTypeDelete'       => PersonReferenceBiodataRelativeTypeDelete::class,
                'PersonReferenceBiodataReligionStore'            => PersonReferenceBiodataReligionStore::class,
                'PersonReferenceBiodataReligionUpdate'           => PersonReferenceBiodataReligionUpdate::class,
                'PersonReferenceBiodataReligionDelete'           => PersonReferenceBiodataReligionDelete::class,
            ],
            'middleware' => ['auth:api']
        ],
        'person_reference_career' => [
            'query' => [
                'PersonReferenceCareerOccupation' => PersonReferenceCareerOccupationQuery::class,
                'PersonReferenceCareerProfession' => PersonReferenceCareerProfessionQuery::class,
            ],
            'mutation' => [
                'PersonReferenceCareerOccupationStore'  => PersonReferenceCareerOccupationStore::class,
                'PersonReferenceCareerOccupationUpdate' => PersonReferenceCareerOccupationUpdate::class,
                'PersonReferenceCareerOccupationDelete' => PersonReferenceCareerOccupationDelete::class,
                'PersonReferenceCareerProfessionStore'  => PersonReferenceCareerProfessionStore::class,
                'PersonReferenceCareerProfessionUpdate' => PersonReferenceCareerProfessionUpdate::class,
                'PersonReferenceCareerProfessionDelete' => PersonReferenceCareerProfessionDelete::class,
            ],
            'middleware' => ['auth:api']
        ],
        'person_transaction' => [
            'query' => [
            ],
            'mutation' => [
            ],
            'middleware' => ['auth:api']
        ],
        'registration' => [
            'query' => [
                'CommonReferenceEducationEducation'               => CommonReferenceEducationEducationQuery::class,
                'LocationCountry'                                 => LocationCountriesQuery::class,
                'LocationProvince'                                => LocationProvincesQuery::class,
                'LocationRegency'                                 => LocationRegenciesQuery::class,
                'LocationRegencyType'                             => LocationRegencyTypesQuery::class,
                'LocationSubDistrict'                             => LocationSubDistrictsQuery::class,
                'LocationVillage'                                 => LocationVillagesQuery::class,
                'ModuleTrainingMasterCourse'                      => ModuleTrainingMasterCourseQuery::class,
                'ModuleTrainingReferenceFacilitator'              => ModuleTrainingReferenceFacilitatorQuery::class,
                'ModuleTrainingReferenceTrainingPackage'          => ModuleTrainingReferenceTrainingPackageQuery::class,
                'ModuleTrainingReferenceLearningMaterialCategory' => ModuleTrainingReferenceLearningMaterialCategoryQuery::class,
                'ModuleTrainingReferenceLearningMaterialMethod'   => ModuleTrainingReferenceLearningMaterialMethodQuery::class,
                'PersonReferenceBiodataGender'                    => PersonReferenceBiodataGenderQuery::class,
                'PersonReferenceBiodataIdentificationType'        => PersonReferenceBiodataIdentificationTypeQuery::class,
                'PersonReferenceBiodataMaritalStatus'             => PersonReferenceBiodataMaritalStatusQuery::class,
                'PersonReferenceBiodataRelativeType'              => PersonReferenceBiodataRelativeTypeQuery::class,
                'PersonReferenceBiodataReligion'                  => PersonReferenceBiodataReligionQuery::class,
                'PersonReferenceCareerOccupation'                 => PersonReferenceCareerOccupationQuery::class,
                'PersonReferenceCareerProfession'                 => PersonReferenceCareerProfessionQuery::class,
            ],
            'mutation' => [
                'PersonIndividualMasterRegister' => PersonMasterIndividualRegister::class,
	        ],
            'middleware' => []
        ],
    ],
    'types' => [
        'AuthAbility'                                       => AuthAbilityType::class,
        'AuthRole'                                          => AuthRoleType::class,
        'AuthSocialAccount'                                 => AuthSocialAccountType::class,
        'AuthUser'                                          => AuthUserType::class,
        'InstitutionMasterCoordinator'                      => InstitutionMasterCoordinatorType::class,
        'InstitutionMasterInstitution'                      => InstitutionMasterInstitutionType::class,
        'InstitutionMasterUnit'                             => InstitutionMasterUnitType::class,
        'InstitutionReferenceCategory'                      => InstitutionReferenceCategoryType::class,
        'InstitutionReferenceCoordinatorType'               => InstitutionReferenceCoordinatorTypeType::class,
        'InstitutionReferenceInstitutionType'               => InstitutionReferenceInstitutionType::class,
        'InstitutionReferenceUnitType'                      => InstitutionReferenceUnitTypeType::class,
        'LocationCountry'                                   => LocationCountriesType::class,
        'LocationProvince'                                  => LocationProvincesType::class,
        'LocationRegency'                                   => LocationRegenciesType::class,
        'LocationRegencyType'                               => LocationRegencyTypesType::class,
        'LocationSubDistrict'                               => LocationSubDistrictsType::class,
        'LocationVillage'                                   => LocationVillagesType::class,
        'ModuleTrainingMasterCourse'                        => ModuleTrainingMasterCourseType::class,
        'ModuleTrainingReferenceFacilitator'                => ModuleTrainingReferenceFacilitatorType::class,
        'ModuleTrainingReferenceTrainingPackage'            => ModuleTrainingReferenceTrainingPackageType::class,
        'ModuleTrainingReferenceLearningMaterialCategory'   => ModuleTrainingReferenceLearningMaterialCategoryType::class,
        'ModuleTrainingReferenceLearningMaterialMethod'     => ModuleTrainingReferenceLearningMaterialMethodType::class,
        'ModuleTrainingTransactionTrainingClass'            => ModuleTrainingTransactionTrainingClassType::class,
        'PersonMasterIndividual'                            => PersonMasterIndividualType::class,
        'PersonReferenceBiodataGender'                      => PersonReferenceBiodataGenderType::class,
        'PersonReferenceBiodataIdentificationType'          => PersonReferenceBiodataIdentificationTypeType::class,
        'PersonReferenceBiodataMaritalStatus'               => PersonReferenceBiodataMaritalStatusType::class,
        'PersonReferenceBiodataRelativeType'                => PersonReferenceBiodataRelativeTypeType::class,
        'PersonReferenceBiodataReligion'                    => PersonReferenceBiodataReligionType::class,
        'PersonReferenceCareerOccupation'                   => PersonReferenceCareerOccupationType::class,
        'PersonReferenceCareerProfession'                   => PersonReferenceCareerProfessionType::class,
        'CommonMasterResidence'                             => CommonMasterResidenceType::class,
        'CommonReferenceDecreeType'                         => CommonReferenceDecreeDecreeTypeType::class,
        'CommonReferenceDocumentDocumentType'               => CommonReferenceDocumentDocumentTypeType::class,
        'CommonReferenceEducationCategory'                  => CommonReferenceEducationCategoryType::class,
        'CommonReferenceEducationEducation'                 => CommonReferenceEducationEducationType::class,
        'CommonReferenceEducationGroup'                     => CommonReferenceEducationGroupType::class,
        'CommonReferenceEducationScienceGroup'              => CommonReferenceEducationScienceGroupType::class,
        'CommonReferenceEducationStage'                     => CommonReferenceEducationStageType::class,
        'CommonReferenceEducationType'                      => CommonReferenceEducationTypeType::class,
        'CommonReferencePeriodEducationYear'                => CommonReferencePeriodEducationYearType::class,
        'CommonReferencePeriodEducationYearCategory'        => CommonReferencePeriodEducationYearCategoryType::class,
        'CommonReferencePeriodWeekDay'                      => CommonReferencePeriodWeekDayType::class,
        'CommonReferenceResidenceCategory'                  => CommonReferenceResidenceCategoryType::class,
        'CommonReferenceResidenceResidentType'              => CommonReferenceResidenceResidentTypeType::class,
        'CommonReferenceResidenceRoomType'                  => CommonReferenceResidenceRoomTypeType::class,
        'CommonReferenceResidenceType'                      => CommonReferenceResidenceTypeType::class,
    ],
    'error_formatter' => ['\Rebing\GraphQL\GraphQL', 'formatError'],
    // You can set the key, which will be used to retrieve the dynamic variables
    'params_key'    => 'params',
];
